<?php

namespace Modules\Block\Admin;

use Modules\Admin\Ui\AdminTable;

class BlockTable extends AdminTable
{
    /**
     * Raw columns that will not be escaped.
     *
     * @var array
     */
    protected $rawColumns = [];

    /**
     * Make table response for the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function make()
    {
        return $this->newTable()
                ->editColumn('tags', function ($entity) {
                
                $tags = \DB::table('block_tags')->where('id',$entity->tags)->first();
                if($tags)
                    return $tags->name;
                return '';
            });
    }
}
