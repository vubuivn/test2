<?php

namespace Modules\Builder\Entities;

use Modules\Admin\Ui\AdminBuilderTable;
use Modules\Element\Entities\Element;
use Modules\Support\Eloquent\Model;
use Modules\Meta\Eloquent\HasMetaData;
use Modules\Support\Eloquent\Sluggable;
use Modules\Support\Eloquent\Translatable;

class Builder extends Model
{
    use Translatable, Sluggable, HasMetaData;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['slug', 'is_active', 'is_redirect', 'is_captcha', 'site_key', 'secret_key'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $translatedAttributes = ['name', 'body', 'embedded'];

    /**
     * The attribute that will be slugged.
     *
     * @var string
     */
    protected $slugAttribute = 'name';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addActiveGlobalScope();

        static::saved(function ($builder) {
            if ($builder->body == '')
                $builder->body = '{}';
        });
        static::creating(function($builder) {
            if ($builder->body == '')
                $builder->body = '{}';
        });
    }

    public function reply()
    {
        return $this->hasMany(Reply::class);
    }

    public function noti()
    {
        return $this->hasMany(Noti::class);
    }

    public function elements()
    {
        return $this->hasMany(Element::class)->orderBy('position');
    }

    public static function urlForBuilder($id)
    {
        return static::select('slug')->firstOrNew(['id' => $id])->url();
    }

    public function url()
    {
        if (is_null($this->slug)) {
            return '#';
        }

        return localized_url(locale(), $this->slug);
    }

//    public function getEmbeddedAttribute() {
//        return $this->embedded;
//    }

    /**
     * Get table data for the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function table()
    {
        return new AdminBuilderTable($this->newQuery()->withoutGlobalScope('active'));
    }
}
