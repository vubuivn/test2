<?php

namespace Modules\Builder\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;

class BuilderTabs extends Tabs
{
    public function make()
    {
        $this->group('page_information', trans('builder::builders.tabs.group.page_information'))
            ->active()
            ->add($this->general())
            ->add($this->elements())
            ->add($this->captcha())
            ->add($this->seo());
    }

    private function general()
    {
        return tap(new Tab('general', trans('builder::builders.tabs.general')), function (Tab $tab) {
            $tab->active();
            $tab->weight(5);
            $tab->fields(['title', 'body', 'is_active', 'slug']);
            $tab->view('builder::admin.builders.tabs.general');
        });
    }

    private function elements()
    {
        return tap(new Tab('elements', trans('builder::builders.tabs.elements')), function (Tab $tab) {
            $tab->weight(10);
            $tab->view('builder::admin.builders.tabs.elements');
        });
    }

    private function captcha()
    {
        return tap(new Tab('captcha', trans('builder::builders.tabs.captcha')), function (Tab $tab) {
            $tab->weight(10);
            $tab->view('builder::admin.builders.tabs.captcha');
        });
    }

    private function seo()
    {
        return tap(new Tab('seo', trans('builder::builders.tabs.seo')), function (Tab $tab) {
            $tab->weight(10);
            $tab->view('builder::admin.builders.tabs.seo');
        });
    }
}
