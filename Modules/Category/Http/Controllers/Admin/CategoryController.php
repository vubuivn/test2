<?php

namespace Modules\Category\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Category\Entities\Category;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Category\Http\Requests\SaveCategoryRequest;
use Modules\Category\Excel\CategoriesExport;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use Modules\Group\Entities\Group;

class CategoryController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'category::categories.category';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'category::admin.categories';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveCategoryRequest::class;

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::withoutGlobalScope('active')
                            ->find($id);
        $category->coverImage = count($category->CoverImage->toArray()) > 0 ? $category->CoverImage->toArray() : null;
        $category->featureImage = count($category->FeatureImage->toArray()) > 0 ? $category->FeatureImage->toArray() : null;

        $category->reference_groups = $category->groups->pluck('name', 'id');
        $category->featured_groups = $category->groups()->wherePivot('featured', FEATURED)->pluck('id');
        $category->pin_groups = explode(',',$category->pin_groups);
        unset($category->groups);
        return $category;
    }

    /**
     * Destroy resources by given ids.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::withoutGlobalScope('active')
                ->findOrFail($id)
                ->delete();

        return back()->withSuccess(
            trans(
                'admin::messages.resource_deleted',
                ['resource' => $this->getLabel()]
            )
        );
    }

    public function excel()
    {
        try {
            $categories = Category::get();
            $name             = "categories_" . strtotime(date('Y-m-d H:i:s'));
            Excel::store(new CategoriesExport($categories), 'categories/' . $name . '.xlsx');
            $filePath = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))->path('categories/' . $name . '.xlsx');
            $binaryContent = file_get_contents($filePath);
            unlink($filePath);
            $response = [
                'name' => $name,
                'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"
                    . base64_encode($binaryContent)
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }
    }


    public function index(Request $request)
    {
        if ($request->has('query')) {
            return $this->getModel()
                ->search($request->get('query'))
                ->query()
                ->limit($request->get('limit', 10))
                ->get();
        }

        if ($request->has('table')) {
            return $this->getModel()->table($request);
        }
        $grps = Group::where('is_active',1)->get();
        return view("{$this->viewPath}.index",compact('grps'));
    }



    public function store()
    {
        $this->disableSearchSyncing();

        $data = $this->getRequest('store')->all();

        if($this->getRequest('store')->has('featured_groups')){
            $f = implode(',',$this->getRequest('store')->get('featured_groups'));
            $data['featured_groups'] = $f;
        }
        if($this->getRequest('store')->has('pin_groups')){
            $f = implode(',',$this->getRequest('store')->get('pin_groups'));
            $data['pin_groups'] = $f;
        }

        $entity = $this->getModel()->create(
            // $this->getRequest('store')->all()
            $data
        );

        $this->searchable($entity);

        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo($entity);
        }

        return redirect()->route("{$this->getRoutePrefix()}.index")
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
    }


    public function update($id)
    {
        $entity = $this->getEntity($id);
        $data = $this->getRequest('update')->all();
        if($this->getRequest('update')->has('featured_groups')){
            $f = implode(',',$this->getRequest('update')->get('featured_groups'));
            $data['featured_groups'] = $f;
        }

        if($this->getRequest('store')->has('pin_groups')){
            $f = implode(',',$this->getRequest('store')->get('pin_groups'));
            $data['pin_groups'] = $f;
        }

        $this->disableSearchSyncing();

        $entity->update(
            // $this->getRequest('update')->all()
            $data
        );

        $this->searchable($entity);
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo($entity)
                ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
        }

        return redirect()->route("{$this->getRoutePrefix()}.index")
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
    }
}
