@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('store::stores.stores'))

    <li class="active">{{ trans('store::stores.stores') }}</li>
@endcomponent

@component('admin::components.page.index_table')
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="form-group">
                <h2>{{trans('store::stores.update_location_cities')}}</h2>
            </div> 
        </div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="loading"></div>
        <div class="col-md-12">
            <div class="form-group">
                <select class="form-control select_cities" data-none-selected-text="Nothing selected" data-live-search="true" >
                    <option>Chọn</option>
                    @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Lat</label>
                <input  class="form-control" type="text" name="lat">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Long</label>
                <input  class="form-control" type="text" name="long">
            </div>
        </div>
        <div class="col-md-12">

                <button class="btn btn-primary update_city">Update</button>
 
        </div>
    </div>
</div>

@endcomponent


@push('scripts')
    <style type="text/css">
        .loading{position: absolute;top: 0;left: 0;background: rgba(255, 255, 255, 0.5);z-index: 10;display: block;width: 100%;height: 100%;display: none}
    </style>
    <link rel="stylesheet" type="text/css" href="{{v(Theme::url('public/css/bootstrap-select.min.css'))}}" id="reset-css">
    <script src="{{v(Theme::url('public/js/bootstrap-select.min.js'))}}"></script>
    <script type="text/javascript">
        (function($){
            $(document).ready(function(){
                $('.select_cities').selectpicker();
            })
            

            $(document).on('click', '.update_city', function(){
                var check = false;
                if(check != true){
                    var id = $('.select_cities').children("option:selected").val();
                    var lat = $('input[name="lat"]').val();
                    var long = $('input[name="long"]').val();
                    check = true;
                    $.ajax({
                        type: 'POST',
                        url: "{{route('cities.ajax')}}",
                        data: { id: id,lat: lat, long: long },
                        beforeSend: function() {
                            // button.html('<img src="/assets/images/loading.gif">');
                            $('.loading').show();
                        },
                        success: function(data){
                            if(data.message == 'required'){
                                $('input[name="'+data.target+'"]').after('<p class="error">required</p>');
                            }
                            $('.error').hide();
                        },
                        complete: function(){
                            $('.loading').hide();
                            // button.html("{{trans('storefront::storefront.news.view_more')}}");
                            check = false;
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });


            $('.select_cities').on('change',function(){
                var check = false;
                if(check != true){
                    var id = $(this).val();
                    check = true;
                    $.ajax({
                        type: 'POST',
                        url: "{{route('cities.change')}}",
                        data: { id: id},
                        beforeSend: function() {
                            $('.loading').show();
                        },
                        success: function(data){
                          
                            $('input[name="lat"]').val(data.loc_lat);
                            $('input[name="long"]').val(data.loc_long);
                            $('.error').hide();
                        },
                        complete: function(){
                            $('.loading').hide();
                            check = false;
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            })
        })(jQuery);
    </script>
@endpush
