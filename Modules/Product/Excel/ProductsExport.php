<?php

namespace Modules\Product\Excel;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
// use Maatwebsite\Excel\Concerns\Exportable;

use Illuminate\Http\Request;
use Modules\Attribute\Entities\Attribute;
use Modules\Product\Entities\Product;

class ProductsExport implements FromArray, WithHeadings
{
    // use Exportable;
    const CHUNK_SIZE = 500;

    protected $productAttr = [];
    protected $productIds = [];
    private $dbMappingDifferentKeys = [
        'width' => 'wide',
        'height' => 'high',
        'length' => 'long',
        'price' => 'selling_price'
    ];

    public function __construct(array $productIds)
    {
        $this->productAttr = Attribute::withoutGlobalScope('active')->get();   
        $this->productIds = $productIds;
        $this->productConfig = config('imports.products');
    }

    public function array(): array
    {
        $attributes = $this->productAttr;

        $rows = [];
        Product::with(['group', 'attributes'])->without('translations')->whereIn('id', $this->productIds)->chunk(
            self::CHUNK_SIZE,
            function ($items) use ($attributes, &$rows) {
                foreach ($items as $item) {
                    $row = [];
                    foreach ($this->productConfig AS $key => $title) {
                        if($key != 'out_of_stock'){
                           $mappingKey = $this->dbMappingDifferentKeys[$key] ?? $key;
                            if ($mappingKey === 'group_code') {
                                $row[] = $item->group->code ?? '';

                            }elseif($mappingKey == 'long' || $mappingKey == 'wide' || $mappingKey == 'high'){
                                $row[] = str_replace(',','-',str_replace(' ','',$item->{$mappingKey}));
                            } else {
                                $row[] = (string) $item->{$mappingKey};
                            } 
                        }
                        
                    }

                    $item->load('attributes');
                    foreach ($attributes as $attr) {
                        $params = '';
                        $productAttr = $item->attributes()
                                                ->where(
                                                    'attribute_id',
                                                    $attr->id
                                                )
                                                ->first();
                        if ($productAttr) {
                            $productAttr->load('values', 'values.attributeValue');
                            
                            foreach ($productAttr->values as $value) {
                                if ($params != '') {
                                    $params .= ',';
                                }
                                $params .= $value->attributeValue->code;
                            }
                        }
                        $row[] = $params;
                    }

                    $rows[] = $row;
                }
            }
        );
        return $rows;
    }

    public function headings(): array
    {
        // $heading = [
        //     'Mã sản phẩm (SKU)',
        //     'Mã nhóm',
        //     'Dài (cm)',
        //     'Rộng (cm)',
        //     'Cao (cm)',
        //     'Thể tích',
        //     'Trạng thái',
        //     'Giá',
        //     'Số chỗ ngồi',
        // ];
        $heading = array_values($this->productConfig);
        foreach ($this->productAttr as $attr) {
            $heading[] = $attr->name;
        }

        return $heading;
    }
}