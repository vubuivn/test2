<?php

Route::get('designer/ajax','DesignerController@designerMore')->name('designer.ajax.more');
Route::get('designer/{slug}','DesignerController@designerDetail')->name('designer.detail');
Route::get('designer','DesignerController@designer')->name('designer');