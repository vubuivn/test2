<?php

namespace Modules\Cart\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Cart\Facades\Cart;
use Illuminate\Routing\Controller;
use Modules\Cart\Http\Requests\StoreCartItemRequest;

class CartItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('in_stock')->only('store');
    }

    /**
     * Add a product to cart
     *
     * @param \Modules\Cart\Http\Requests\StoreCartItemRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCartItemRequest $request)
    {

        Cart::store($request->product_id, $request->qty, $request->options ?? []);

        // return redirect()->route('cart.index')->withSuccess(trans('cart::messages.added'));
        return back()->withSuccess(trans('cart::messages.added'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param mixed $cartItemId
     * @return \Illuminate\Http\Response
     */
    public function update($cartItemId)
    {
        Cart::updateQuantity(decrypt($cartItemId), request('qty'));

        return back()->withSuccess(trans('cart::messages.quantity_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $cartItemId
     * @return \Illuminhtate\Http\Response
     */
    public function destroy($cartItemId)
    {
        Cart::remove(decrypt($cartItemId));

        return back()->withSuccess(trans('cart::messages.removed'));
    }

    public function addToCartAjax(Request $request){
        try{
            
            Cart::store($request->id, $request->qty, $request->options ?? []);
            $cart = Cart::instance();
            
            return view('public.partials.mini-cart',compact('cart'));
            
        }catch(\Exception $e){
            // $message = trans('cart::messages.something_wrong');
            $message = $e->getMessage();
        }
        return $message;
    }
    public function loadCartAjax(){
        $cart = Cart::instance();
        return view('public.partials.mini-cart',compact('cart'));
    }
}
