<tr class="cart-item">
	<td>
		<div class="image-holder">
			<img src="{{ productImageUrl($cartItem->product->sku, 1) }}">
		</div>
	</td>
	<td class="product-info">
		<h5>
		<a href="{{ route('products.show', ['slug' => $cartItem->product->group->slug]) . '?sku=' . $cartItem->product->sku }}">{{ $cartItem->product->name }}</a>
		</h5>
		<div class="desc">
			{!! $cartItem->product->group->description !!}
		</div>
		<div class="option">
			@foreach ($cartItem->product->attributes AS $attribute)
			<span>
				<span class="label">{{ $attribute->name }}:</span>
				<span>{{ $attribute->values[0]->attributeValue->value }}</span>
			</span>
			@endforeach
		</div>
	</td>
	<td>
		<label>{{ trans('storefront::cart.price') }}:</label>
		<span>{{ $cartItem->unitPrice()->convertToCurrentCurrency()->format() }}</span>
	</td>
	<td class="clearfix">
		<div class="quantity-wrapper pull-left clearfix">
			<div class="quantity">
				<button type="button" class="btn-number btn-minus" data-type="minus" ><i class="icon-minus-2"></i></button>
				<input type="text" name="qty" value="{{ $cartItem->qty }}" class="input-number input-quantity " min="1" max="{{ $cartItem->product->manage_stock ? $cartItem->product->qty : '' }}">
				<button type="button" class="btn-number btn-plus" data-type="plus"><i class="icon-plus-2"></i></button>
			</div>
			<!-- <button data-toggle="tooltip" data-placement="top" title="Update" class="btn-update"  data-quantity-field=".qty-0">
			<i class="icon icon-spin" aria-hidden="true"></i>
			</button> -->
			<form method="POST" action="{{ route('cart.items.update', ['cartItemId' => encrypt($cartItem->id)]) }}">
				@csrf
				{{ method_field('put') }}
				<input type="hidden" name="qty" value="1" />
			</form>
		</div>
	</td>
	<td>
		<label>{{ trans('storefront::cart.subtotal') }}:</label>
		<span>{{ $cartItem->total()->convertToCurrentCurrency()->format() }}</span>
	</td>
	<td>
		
		<form method="POST" action="{{ route('cart.items.destroy', encrypt($cartItem->id)) }}"
			onsubmit="return confirm('{{ trans('cart::messages.confirm') }}');">
				{{ csrf_field() }}
				{{ method_field('delete') }}
			<button type="submit" class="btn-close" data-toggle="tooltip" data-placement="top"
				title="{{ trans('storefront::cart.remove') }}">
			<i class="icon icon-close"></i>
			</button>
		</form>
	</td>
</tr>