/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 293);
/******/ })
/************************************************************************/
/******/ ({

/***/ 293:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(294);


/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__News__ = __webpack_require__(295);


new __WEBPACK_IMPORTED_MODULE_0__News__["a" /* default */]();

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _class = function () {
    function _class() {
        _classCallCheck(this, _class);

        $(document).on('click', '.banner-image', function (e) {
            var _this = this;

            var picker = new MediaPicker({ type: 'image' });

            picker.on('select', function (file) {
                $(_this).find('i').remove();
                $(_this).find('img').attr('src', file.path).removeClass('hide');
                $(_this).find('.banner-file-id').val(file.id);
            });
        });
        if (!$('body').hasClass('sidebar-collapse')) {
            $('body').addClass('sidebar-collapse');
        }
        $(document).on('change', '.block-name-field', function (e) {
            $(this).parent().parent().parent().parent().parent().parent().find('#block-name').text($(this).val());
        });
        $(document).on('click', '.delete-block', function (e) {
            $(this).parent().parent().parent().parent().parent().remove();
        });
        $(document).on('click', '.add_more_image', function (e) {
            var block = $(this).attr('data-block');
            var number = $(this).attr('data-id');
            var key = $(this).attr('data-key');
            $(this).attr('data-id', ++number);

            var template = document.getElementById(key + '-template');
            var templateHtml = template.innerHTML;
            templateHtml = templateHtml.replace(/<%- i %>/gi, block);
            templateHtml = templateHtml.replace(/&lt;%- i %&gt;/gi, block);
            templateHtml = templateHtml.replace(/&lt;%- y %&gt;/gi, number);
            $(this).parent().find('.banner-template').append(templateHtml);
        });
        $(document).on('click', '.delete-image', function (e) {
            $(this).parent().remove();
        });
        // var block = $('#block-number').val();
        // $(document).on('click', '.block-item', function (e) {
        //     var blockId = $(this).data('id');
        //     var template = document.getElementById(blockId + '-template');
        //     var templateHtml = template.innerHTML;
        //     templateHtml = templateHtml.replace(/<%- i %>/gi, block);
        //     templateHtml = templateHtml.replace(/&lt;%- i %&gt;/gi, block);
        //     $('#page-block').append(templateHtml);
        //     $('.content-accordion').find('.banner-body:eq(0)').find('.delete-image').remove();
        //     tinymce.init({
        //         selector: '.wysiwyg',
        //         theme: 'modern',
        //         mobile: { theme: 'mobile' },
        //         height: 300,
        //         menubar: false,
        //         branding: false,
        //         image_advtab: true,
        //         image_title: true,
        //         relative_urls: false,
        //         cache_suffix: '?v=' + FleetCart.version,
        //         plugins: 'lists, link, table, paste, autosave, autolink, wordcount, code',
        //         toolbar: 'styleselect bold italic underline | bullist numlist | alignleft aligncenter alignright | outdent indent | link table code'
        //     });
        //     if (toggleFormat === true) {
        //         $('#custom-collapse-' + block).addClass('in');
        //         $('#block-form-' + block + ' .collapsed').removeClass('collapsed');
        //     }
        //     block++;
        // });
        // var toggleFormat = false;
        // $('#expandAllFormats').text(FleetCart.langs['page::pages.expand_all']);
        // $('#expandAllFormats').on('click', function (e) {
        //     e.preventDefault();
        //     $("#page-block .panel-collapse").each(function (index, value) {
        //         if (toggleFormat) {
        //             if ($(this).hasClass('in')) {
        //                 $(this).collapse('toggle');
        //             }
        //         } else {
        //             if (!$(this).hasClass('in')) {
        //                 $(this).collapse('toggle');
        //             }
        //         }
        //     });
        //     toggleFormat = toggleFormat ? false : true;
        //     if (toggleFormat === true) {
        //         $('#expandAllFormats').text(FleetCart.langs['page::pages.collapse_all']);
        //     } else {
        //         $('#expandAllFormats').text(FleetCart.langs['page::pages.expand_all']);
        //     }
        // });
        $('#btn-save-form').click(function () {
            $('#news-form .has-error').removeClass('has-error');
            // var form = document.getElementsByName('block-form');
            tinymce.triggerSave();

            var myForm = document.getElementById('news-form');
            var formData = new FormData(myForm);

            var error = false;
            if ($.trim(formData.get('name')) === '') {
                $('#news-form #name').parent().parent().addClass('has-error');
                $('.accordion-tab li:eq(0)').addClass('has-error');
                error = true;
            }
            if ($.trim(formData.get('short_description')) === '') {
                $('#news-form #short_description').parent().parent().addClass('has-error');
                $('.accordion-tab li:eq(0)').addClass('has-error');
                error = true;
            }
            if ($.trim(formData.get('description')) === '') {
                $('#news-form #description').parent().parent().addClass('has-error');
                $('.accordion-tab li:eq(0)').addClass('has-error');
                error = true;
            }
            if (formData.has('slug')) {
                if ($.trim(formData.get('slug')) === '') {
                    $('#news-form #slug').parent().parent().addClass('has-error');
                    $('.accordion-tab li:eq(1)').addClass('has-error');
                    error = true;
                }
            }

            var formData = $('#news-form').serializeArray();
            // var blockItem = 0;
            // form.forEach(function (element, index) {
            //     var blockData = $('#' + element.id).serializeArray();
            //     blockData.forEach(function (item) {
            //         formData.push(item);
            //     });
            //     blockItem++;
            // });
            // if (blockItem === 0) {
            //     $('.accordion-tab li:eq(1)').addClass('has-error');
            //     error = true;
            // }
            if (error === true) {
                return false;
            }

            var url = $('#news-form').attr('action');
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                success: function success(res) {
                    window.location.assign(route('admin.news.index'));
                },
                error: function error(xhr) {
                    alert(xhr);
                }
            });
        });

        // this.sortable();
    }

    // _createClass(_class, [{
    //     key: 'sortable',
    //     value: function sortable() {
    //         Sortable.create(document.getElementById('page-block'), {
    //             handle: '.drag-icon',
    //             animation: 150
    //         });
    //     }
    // }]);

    return _class;
}();

/* harmony default export */ __webpack_exports__["a"] = (_class);

/***/ })

/******/ });