@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('category::categories.categories'))

    <li class="active">{{ trans('category::categories.categories') }}</li>
@endcomponent

@section('content')
    <div class="box box-default">
        <div class="box-body clearfix">
            <div class="col-lg-2 col-md-3">
                <div class="row">
                    <button
                        class="btn btn-default add-root-category">{{ trans('category::categories.tree.add_root_category') }}</button>
                    <button
                        class="btn btn-default add-sub-category disabled">{{ trans('category::categories.tree.add_sub_category') }}</button>
                    <button
                        class="btn btn-default export-excel">{{ trans('category::categories.tree.export_excel') }}</button>
                    <div class="m-b-10">
                        <a href="#" class="collapse-all">{{ trans('category::categories.tree.collapse_all') }}</a> |
                        <a href="#" class="expand-all">{{ trans('category::categories.tree.expand_all') }}</a>
                    </div>

                    <div class="category-tree"></div>
                </div>
            </div>

            <div class="col-lg-10 col-md-9">
                <div class="tab-wrapper category-details-tab">
                    <ul class="nav nav-tabs">
                        <li class="general-information-tab active">
                            <a data-toggle="tab" href="#general-information">
                                {{ trans('category::categories.tabs.general') }}
                            </a>
                        </li>
                        <li class="seo-tab hide">
                            <a data-toggle="tab" href="#seo">
                                {{ trans('category::categories.tabs.seo') }}
                            </a>
                        </li>
                    </ul>

                    <form method="POST" action="{{ route('admin.categories.store') }}" class="form-horizontal"
                          id="category-form" novalidate>
                        {{ csrf_field() }}

                        <div class="tab-content">
                            <div id="general-information" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-8">
                                        {{ Form::text('name', trans('category::attributes.name'), $errors, null, ['required' => true]) }}
                                        {{-- Form::select('featured_groups', trans('category::attributes.featured_groups'), $errors, [], null, ['class' => 'selectize prevent-creation', 'multiple' => true]) --}}

                                        <div class="form-group">
                                            <label for="featured_groups[]-selectized" class="col-md-3 control-label text-left">Featured Groups</label>
                                            <div class="col-md-9">
                                                <select name="featured_groups[]" class="selectize prevent-creation selectized" multiple="multiple" id="featured_groups[]">
                                                    @foreach($grps as $g)
                                                        <option value="{{$g->id}}">{{$g->name}}({{$g->code}})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Pin group -->
                                        <div class="form-group">
                                            <label for="pin_groups[]-selectized" class="col-md-3 control-label text-left">Pin Groups</label>
                                            <div class="col-md-9">
                                                <select name="pin_groups[]" class="selectize prevent-creation selectized" multiple="multiple" id="pin_groups[]">
                                                    @foreach($grps as $g)
                                                        <option value="{{$g->id}}">{{$g->name}}({{$g->code}})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        {{ Form::number('position', trans('category::attributes.position'), $errors,null) }}

                                        {{ Form::checkbox('is_searchable', trans('category::attributes.is_searchable'), trans('category::categories.form.show_this_category_in_search_box'), $errors) }}
                                        {{ Form::checkbox('is_active', trans('category::attributes.is_active'), trans('category::categories.form.enable_the_category'), $errors) }}
                                        {{ Form::textarea('description', trans('category::attributes.description'), $errors, null) }}
                                        <div class="form-group ">
                                            <label for="description" class="col-md-3 control-label text-left">
                                                {{trans('category::attributes.cover_image')}}
                                            </label>
                                            <div class="col-md-9">
                                                <button type="button" class="image-picker btn btn-default" data-input-name="files[cover_image]">
                                                    <i class="fa fa-folder-open m-r-5"></i>{{ trans('media::media.browse') }}
                                                </button>
                                                <div class="clearfix"></div>
                                                <div class="single-image image-holder-wrapper clearfix" id="cover_image">
                                                    <div class="image-holder placeholder">
                                                        <i class="fa fa-picture-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="description" class="col-md-3 control-label text-left">
                                                {{trans('category::attributes.feature_image')}}
                                            </label>
                                            <div class="col-md-9">
                                                <button type="button" class="image-picker btn btn-default" data-input-name="files[feature_image]">
                                                    <i class="fa fa-folder-open m-r-5"></i>{{ trans('media::media.browse') }}
                                                </button>
                                                <div class="clearfix"></div>
                                                <div class="single-image image-holder-wrapper clearfix" id="feature_image">
                                                    <div class="image-holder placeholder">
                                                        <i class="fa fa-picture-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="seo" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="hide" id="slug-field">
                                            {{ Form::text('slug', trans('category::attributes.slug'), $errors) }}
                                        </div>
                                        <div>
                                            {{ Form::text('meta_title', trans('category::attributes.meta_title'), $errors, null) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="submit" class="btn btn-primary" data-loading>
                                    {{ trans('admin::admin.buttons.save') }}
                                </button>

                                <button type="button" class="btn btn-link text-red btn-delete p-l-0 hide" data-confirm>
                                    {{ trans('admin::admin.buttons.delete') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="overlay loader hide">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
@endsection
@push('scripts')
   <!--  <link rel="stylesheet" type="text/css" href="{{v(Theme::url('public/css/bootstrap-select.min.css'))}}" id="reset-css">
        <script src="{{v(Theme::url('public/js/bootstrap-select.min.js'))}}"></script>
    <script type="text/javascript">
        (function($){
            $(document).ready(function(){
                $('.conutries').selectpicker();
            });
        })(jQuery);
    </script> -->
    <script>
        $(".export-excel").on('click', function (e) {
            $.ajax({
                type: 'GET',
                url: route('admin.categories.export.excel'),
                success: function (response, textStatus, request) {
                    var a = document.createElement("a");
                    a.href = response.file;
                    a.download = response.name;
                    document.body.appendChild(a);
                    a.click();
                    a.remove();
                },
                error(xhr) {
                },
            });
        })
    </script>
@endpush
