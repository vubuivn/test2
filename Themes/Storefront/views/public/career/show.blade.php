@extends('public.layout')

@section('title', 'Career')

@section('content')
    <div class="career-wrapper clearfix">
        @include('public.partials.bannertop',['banner'=> '/assets/images/tuyen-dung.jpg','title'=>'Tuyển dụng'])

        <main id="main">
            <div class="container">
                <section class="career sec-tb">
                    <div class="max670">
                       <h6 class="w6 text-career-center">{{trans('storefront::storefront.join_the_team')}}</h5>
                       <h5 class="w6 text-career-center title-aconcept">aconcept</h5>
                       <p class="content-career">{{trans('storefront::storefront.become_member')}}</p>
                    </div>
                </section>
                <div>
                    <div>
                        <form class="max800" method="GET" action="{{route('career')}}">
                            <div class="row">
                                <div class="col-5 col-sm-5">
                                    <div class="item">
                                        <span class="title-career-info">{{trans('storefront::storefront.position')}}</span>
                                        <select name="position" id="" class="select select-career">
                                            <option value="">{{trans('storefront::storefront.all_position')}}</option>
                                            @foreach($positions as $position)
                                                <option value="{{$position->position}}" {{request()->get('position') == $position->position ? 'selected' : ''}}>{{$position->position}}</option>
                                            @endforeach
                        
                                        <select>
                                    </div>
                                </div>
                                <div class="col-5 col-sm-5">
                                    <div class="item">
                                        <span class="title-career-info">{{trans('storefront::storefront.location')}}</span>
                                        <?php 
                                            $arrCities = [];
                                        ?>
                                        <select name="location" id="" class="select select-career">
                                            <option value="">{{trans('storefront::storefront.all_location')}}</option>
                                            @foreach($cities as $city)
                                            <?php 
                                            $arrtitle =[];
                                           
                                            $arr = explode('[:en]',$city->title);
                                            $arrtitle['vi'] = substr($arr[0],5);
                                            $arrtitle['en'] = substr($arr[1],-3);
                                            $arrCities[$city->id] = $arrtitle;
                                            ?>
                                                <option value="{{$city->id}}" {{request()->get('location') == $city->id ? 'selected' : ''}}>{{ (locale() == 'en') ? $arrtitle['en'] : $arrtitle['vi']}}</option>
                                            @endforeach
                                        <select>
                                    </div>
                                </div>
                                <div class="col-2 col-sm-2">
                                    <button class="btn search-career"><i class="icon-search"></i></button>
                                </div>
                            </div>
                        </form>
                        <div class="wrap-list-career">
                            <div class="row grid-space-10 titlediv ">
                                <div class="col-4 col-sm-4">{{trans('storefront::storefront.position')}}</div>
                                <div class="col-4 col-sm-4">{{trans('storefront::storefront.career.recruitment_classification')}}</div>
                                <div class="col-4 col-sm-4">{{trans('storefront::storefront.career.work_location')}}</div>
                            </div>
                            
                            
                            <ul class="list-career-info" id="career-info">
                                @if(count($careers)>0)
                                @foreach($careers as $career)
                                    <li class = "row grid-space-10 my-career-info">
                                        <a href="{{route('career.detail',$career->slug)}}">
                                            <span class="col-4 col-sm-4">{{$career->position}}</span>
                                            <span class="col-4 col-sm-4">{{$career->time_job}}</span>
                                            <span class="col-4 col-sm-4">{{ locale() == 'en' ? $arrCities[$career->city_id]['en'] : $arrCities[$career->city_id]['vi']}}</span>
                                        </a>
                                    </li>
                                @endforeach
                                @endif
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@push('styles')
    <link rel='stylesheet' href="/assets/css/custom.css" type='text/css'/>
@endpush
