require('codemirror/mode/xml/xml');
require('codemirror/mode/javascript/javascript');
require('codemirror/mode/htmlmixed/htmlmixed');
require('codemirror/addon/edit/matchbrackets');
import CodeMirror from 'codemirror';
export default class {
    constructor() {
        CodeMirror.fromTextArea(document.getElementById('html'), {
            lineNumbers: true,
            matchBrackets: true,
            mode: 'text/x-php',
            indentUnit: 4,
            indentWithTabs: true,
        }).refresh();
    }
}
