<?php

namespace Modules\Group\Entities;

use Modules\Support\Eloquent\TranslationModel;

class GroupBlockTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content'
    ];
}
