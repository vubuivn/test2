<?php

namespace Modules\Filter\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Modules\Category\Http\ViewComposers\CategoryComposer;
use Modules\CategoryNews\Http\ViewComposers\CategoryNewsComposer;

class FilterServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register ()
    {
        View::composer(
            'filter::admin.category',
            CategoryComposer::class
        );
        View::composer(
            'filter::admin.category_news',
            CategoryNewsComposer::class
        );
    }
}
