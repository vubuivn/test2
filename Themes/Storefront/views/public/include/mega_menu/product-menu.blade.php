<!-- Modal -->

<div id="product-menu">
    <div class ="close-menu-x">
        <div class ="inner-x">
            <label>Back</label>
        </div>
    </div>
<!-- <span class="close-menu-x"><i class="icon-close"></i></span> -->
    <div class="container">
        <div class="row ">
            <div class="col-md-7">
                <h1 class="h2 h2-hidden">{{ $menu->name() }}</h1>
                <ul class="menu">
                    @foreach ($menu->subMenus() AS $subMenu)
                    <li>
                        <span class="item"
                            data-categoryid ="{{$subMenu->category()}}"
                            data-link="{{ $subMenu->url() }}"
                        >
                        @if($level='level1')
                            <span class="item" >{{ $subMenu->name() }}</span>
                        @else
                            <a href="{{$subMenu->url() }}">{{ $subMenu->name() }}</a>
                        @endif

                        </span>

                        <?php 
                            $arr = array(
                                'subMenus' => $subMenu->items(),
                            );
                            if($level == 'level1'){
                                $arr['level'] = 'level2';
                            }
                            $arr['sublink'] = $subMenu->url();
                        ?>

                        @if ($subMenu->hasItems())
                            @include('public.include.mega_menu.product-dropdown', $arr)
                        @else
                            <ul>
                                <li><span class="item"><a href="{{$subMenu->url()}}">{{ trans('group::group-categories.show_all_products') }} <i class="icon-arrow-14"></i></a></span></li>
                            </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-5">
            
                <div class="synproduct">
                    <div class="img tRes_90"><img src="https://aconceptest.mangoads.com.vn/storage/media/vKg4QudZVOPKs8UvOu0ErB33e2otR5HdJva8MVYn.png" alt=""></div>
                    <h2 class="title"></h2>
                    <a href="#3" class="viewall readmore">
                        {{ trans('group::group-categories.show_all_products') }}
                        <i class="icon-arrow-14"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
