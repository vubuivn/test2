@extends('public.layout')

@section('title', 'Career')

@section('content')
    <div class="career-wrapper clearfix">
        @include('public.partials.bannertop',['banner'=>$designer->baseImage->path,'title'=>$designer->name,'subtitle'=>trans('storefront::storefront.designer.designer')])
        <main id="main">
            <div class="container">
                <div class="t4-detail">
                    <div class = "detail-text">
                        {!!$designer->body!!}
                    </div>  
                </div>
            <div class="row no-gutter detail-contai">
                @php($i = 1)
                @foreach($designer->additional_images as $image)
                    @if($i%3 == 0)
                        <div class="col-lg-12">
                    @else
                        <div class="col-lg-6">
                    @endif
                        <a href="{{$image->path}}" class="img  tRes_58">
                            <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg" data-lazy-src="{{$image->path}}"  alt="" />      
                        </a>       
                    </div>
                    @php($i++)
                @endforeach
            </div>
            @if(count($related)>0)
            <div class="list-items-design ">
                <h3>{{trans('storefront::storefront.designer.related')}}</h3>
                <div class="row list-designer-detail">
                    @foreach($related as $d)
                    <div class="col-md-3 designer">
                        <a href="{{route('designer.detail',$d->slug)}}">
                            <div class="img tRes_67">
                                <img class="lazy-hidden " data-lazy-type="image" src="assets/images/image.svg" data-lazy-src="{{$d->featureImage->path}}"  alt=""/>
                            </div>
                        </a>
                        <div class="items-design">
                            <h3 class="title-design"><a href="{{route('designer.detail',$d->slug)}}">{{$d->name}}</a></h3>
                            <div class="desc-info">
                                {{$d->short_description}}
                            </div>
                            <div class="more">
                                <a href="{{route('designer.detail',$d->slug)}}" >{{trans('storefront::storefront.news.view_more')}} <i class="icon-arrow-14"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
        </main>
    </div>
@endsection

@push('styles')
    <link rel='stylesheet' href="/assets/css/custom.css" type='text/css'/>
@endpush