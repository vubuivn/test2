<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuilderTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('builder_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('builder_id')->unsigned();
            $table->string('locale');
            $table->string('name');
            $table->text('body');
            $table->text('embedded');

            $table->unique(['builder_id', 'locale']);
            $table->foreign('builder_id')->references('id')->on('builders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('builder_translations');
    }
}
