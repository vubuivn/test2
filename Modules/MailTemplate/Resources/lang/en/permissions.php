<?php

return [
    'index' => 'Index Mail Template',
    'create' => 'Create Mail Template',
    'edit' => 'Edit Mail Template',
    'destroy' => 'Delete Mail Template',
];
