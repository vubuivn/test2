<?php

/** @var Illuminate\Routing\Router $router */
Route::get('{builder}/api-terms', [
    'as' => 'admin.terms.api-index',
    'uses' => 'TermController@api_index'
]);
Route::get('{builder}/api-terms-html', [
    'as' => 'admin.terms.api-index-html',
    'uses' => 'TermController@api_index_html'
]);
Route::get('{id}/term-api-show', [
    'as' => 'admin.terms.api-show',
    'uses' => 'TermController@api_show'
]);
