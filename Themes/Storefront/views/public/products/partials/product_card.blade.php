@php
    $price = priceGroup($group->id);
    $attribute = get_product_attribute($group, request()->get('attribute'));
    $main_color_code = $attribute[env('DEFAULT_COLOR_ID')]->other ?? null;
    $main_attribute_code = $attribute[env('DEFAULT_MATERIAL_ID')]->other ?? null;
@endphp
<a href="{{ route('products.show', $group->slug) }}" class="product-card">
    <div class="product-card-inner">
        <div class="product-image clearfix">
            <ul class="product-ribbon list-inline">
                {{--@if ($product->isOutOfStock())--}}
                {{--<li><span class="ribbon bg-red">{{ trans('storefront::product_card.out_of_stock') }}</span></li>--}}
                {{--@endif--}}

                {{--@if ($product->isNew())--}}
                {{--<li><span class="ribbon bg-green">{{ trans('storefront::product_card.new') }}</span></li>--}}
                {{--@endif--}}
            </ul>

            {{--@if (! $product->base_image->exists)--}}
            {{--<div class="image-placeholder">--}}
            {{--<i class="fa fa-picture-o" aria-hidden="true"></i>--}}
            {{--</div>--}}
            {{--@else--}}
            <div class="image-holder">
                id : {{$group->id}}<br>
                sku : {{$group->products[0]->sku}}<br>
                @foreach($attribute as $key => $value)
                    {{$value->name}} code : {{$value->other}} <br>
                    {{$value->name}} name : {{$value->value}} <br>
                @endforeach
                {{--color other code : {{$color_other_code}}<br>--}}
                {{--color code : {{$main_color_code}}<br>--}}
                {{--chat lieu code : {{$main_attribute_code}}<br>--}}
                {{--color name : {{$main_color_value}}<br>--}}
                {{--chat lieu name : {{$main_attribute_value}}<br>--}}

                {{--cl code : {{$material_other_code}}<br>--}}
                {{--                ten hinh : {{ $group->products[0]->sku.'_'.$main_color_code.'_'.$main_attribute_code }}--}}
                {{--<br>--}}
                {{--{{ $main_color_value.'_'.$main_attribute_value }}--}}
                {{--<br>--}}
                images : {{ $group->products[0]->sku.'_'.$main_color_code.'_'.$main_attribute_code}}_feature.jpg<br>
                {{--                <img src="{{ $product->base_image->path }}">--}}
            </div>
            {{--@endif--}}

            <div class="quick-view-wrapper" data-toggle="tooltip" data-placement="top"
                 title="{{ trans('storefront::product_card.quick_view') }}">
                <button type="button" class="btn btn-quick-view" data-slug="{{ $group->slug }}">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                </button>
            </div>
        </div>

        <div class="product-content clearfix">
            <span class="product-price"> {{ product_price($group->products[0]) }}</span>
            <span class="product-name">{{ $group->name }}</span>
        </div>

        <div class="add-to-actions-wrapper">
            <form method="POST" action="{{ route('wishlist.store') }}">
                {{ csrf_field() }}

                <input type="hidden" name="product_id" value="{{ $group->id }}">

                <button type="submit" class="btn btn-wishlist" data-toggle="tooltip"
                        data-placement="{{ is_rtl() ? 'left' : 'right' }}"
                        title="{{ trans('storefront::product_card.add_to_wishlist') }}">
                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                </button>
            </form>

            {{--@if ($product->options_count > 0)--}}
            {{--<button class="btn btn-default btn-add-to-cart"--}}
            {{--onClick="location = '{{ route('products.show', ['slug' => $product->slug]) }}'">--}}
            {{--{{ trans('storefront::product_card.view_details') }}--}}
            {{--</button>--}}
            {{--@else--}}
            {{--<form method="POST" action="{{ route('cart.items.store') }}">--}}
            {{--{{ csrf_field() }}--}}

            {{--<input type="hidden" name="product_id" value="{{ $product->id }}">--}}
            {{--<input type="hidden" name="qty" value="1">--}}

            {{--<button class="btn btn-default btn-add-to-cart" {{ $product->isOutOfStock() ? 'disabled' : '' }}>--}}
            {{--{{ trans('storefront::product_card.add_to_cart') }}--}}
            {{--</button>--}}
            {{--</form>--}}
            {{--@endif--}}

            <form method="POST" action="{{ route('compare.store') }}">
                {{ csrf_field() }}

                <input type="hidden" name="product_id" value="{{ $group->id }}">

                <button type="submit" class="btn btn-compare" data-toggle="tooltip"
                        data-placement="{{ is_rtl() ? 'right' : 'left' }}"
                        title="{{ trans('storefront::product_card.add_to_compare') }}">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                </button>
            </form>
        </div>
    </div>
</a>
