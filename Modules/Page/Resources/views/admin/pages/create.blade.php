@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('page::pages.page')]))

    <li><a href="{{ route('admin.pages.index') }}">{{ trans('page::pages.pages') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('page::pages.page')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.pages.store') }}" class="form-horizontal" id="page-form" novalidate>
    {{--<form class="form-horizontal" id="page-create-form" novalidate>--}}
        {{ csrf_field() }}

        {!! $tabs->render(compact('page')) !!}
    </form>
@endsection

@include('page::admin.pages.partials.shortcuts')




@push('scripts')
<script>

        function _repeatContent(number,key,count){
        	count++;
              return `<div class="row group-outner" data-group="`+count+`" style="margin-bottom:20px">
                        <div class="group-content">
                            <div class="col-md-11">
                                <input type="text" class="form-control" name="blocks[`+number+`][`+key+`][`+count+`][group][title]">
                                <textarea class="form-control wysiwyg" name="blocks[`+number+`][`+key+`][`+count+`][group][content]"></textarea>

                            </div>
                             <div class="col-md-1">
                                <a class="btn btn-default remove_repeat" data-key="[`+key+`]" data-number="[`+number+`]"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                    </div>
                `;
        }

        function _repeatChildContent(number,key,group,child){
        	return `
        			<div class="col-md-12">
	        			<label class="col-md-12">Childs</label>
	        			<div class="col-md-6">
	        				<input class="form-control" type="text" name="blocks[`+number+`][`+key+`][`+group+`][group][children][`+child+`][content]">
	        			</div>
	        			<div class="col-md-6"><input class="form-control" type="text" name="blocks[`+number+`][`+key+`][`+group+`][group][children][`+child+`][content2]">
	        			</div>
       				</div>
        		`;
        }

		$(document).on('click','.add_repeat',function(e){
            e.preventDefault();
            var key = $(this).data('key'),
            number = $(this).data('number'),	
			groupcount = $(this).next().children().length;
			var content = _repeatContent(number,key,groupcount);
            $(this).next().append(content);
        });

        $(document).on('click','.remove_repeat',function(e){
            e.preventDefault();
            // var key = $(this).data('key'),
            var number = $(this).data('number') +1;
            // groupcount = $(this).next().children().length;

            var $currentItem = $(this).closest('.group-outner');
            $currentItem.remove();
             // $(this).parent().find('.list-repeat').remove($currentItem);
        });

        $(document).on('click','.add_repeat_child',function(e){
        	e.preventDefault();
        	var aData = $(this).closest('.list-repeat').prev();
        	var number = aData.data('number');
        	var key = aData.data('key');
        	var group = $(this).closest('.group-outner').data('group');
        	var child = $(this).closest('.group-content').find('.list-child-group').children().length;
        	var content = _repeatChildContent(number,key,group,child);
        	$(this).closest('.group-content').find('.list-child-group').append(content);

        });


    </script>
@endpush