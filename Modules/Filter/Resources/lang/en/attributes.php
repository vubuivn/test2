<?php

return [
    'filter_categories' => 'Category',
    'is_filter' => 'Filterable',
    'enable_filter_categories' => 'Enable filter categories',
];
