<?php

/** @var Illuminate\Routing\Router $router */
Route::get('terms', [
    'as' => 'admin.terms.index',
    'uses' => 'TermController@index',
    'middleware' => 'can:admin.terms.index',
]);

Route::get('{builder}/terms', [
    'as' => 'admin.terms.builder',
    'uses' => 'TermController@index',
    'middleware' => 'can:admin.terms.index',
]);

Route::get('terms/create', [
    'as' => 'admin.terms.create',
    'uses' => 'TermController@create',
    'middleware' => 'can:admin.terms.create',
]);

Route::post('terms', [
    'as' => 'admin.terms.store',
    'uses' => 'TermController@store',
    'middleware' => 'can:admin.terms.create',
]);
Route::get('terms/{id}/show', [
    'as' => 'admin.terms.show',
    'uses' => 'TermController@show',
    'middleware' => 'can:admin.terms.edit',
]);
Route::get('terms/{id}/edit', [
    'as' => 'admin.terms.edit',
    'uses' => 'TermController@edit',
    'middleware' => 'can:admin.terms.edit',
]);

Route::put('terms/{id}/edit', [
    'as' => 'admin.terms.update',
    'uses' => 'TermController@update',
    'middleware' => 'can:admin.terms.edit',
]);

Route::delete('terms/{ids?}', [
    'as' => 'admin.terms.destroy',
    'uses' => 'TermController@destroy',
    'middleware' => 'can:admin.terms.destroy',
]);

Route::get('terms/excelAll', [
    'as' => 'admin.terms.exportAll.excel',
    'uses' => 'TermController@exportAllTermsExcel',
]);

Route::get('terms/download/{file}', [
    'as' => 'admin.terms.downloadTermsFile.excel',
    'uses' => 'TermController@downloadExcelFile'
]);