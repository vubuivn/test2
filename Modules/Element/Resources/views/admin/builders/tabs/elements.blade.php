<div id="elements-group">
    {{--  Elements will be added here dynamically using JS  --}}
</div>

<div class="box-footer no-border p-t-0">
    <div class="form-group pull-left">
        <div class="col-md-10">
            <button type="button" class="btn btn-default m-r-10" id="add-new-element">
                {{ trans('element::elements.form.add_new_element') }}
            </button>
        </div>
    </div>

    @hasAccess('admin.elements.index')
        @if ($globalElements->isNotEmpty())
            <div class="add-global-element clearfix pull-right">
                <div class="form-group pull-left">
                    <select class="form-control custom-select-black" id="global-element">
                        <element value="">{{ trans('element::elements.select_global_element') }}</element>

                        @foreach ($globalElements as $globalElement)
                            <element value="{{ $globalElement->id }}">{{ $globalElement->name }}</element>
                        @endforeach
                    </select>
                </div>

                <button type="button" class="btn btn-default" id="add-global-element" data-loading>
                    {{ trans('element::elements.form.add_global_element') }}
                </button>
            </div>
        @endif
    @endHasAccess
</div>

@push('globals')
    <script>
        FleetCart.data['builder.elements'] = {!! old_json('elements', $builder->elements) !!};
        FleetCart.errors['builder.elements'] = @json($errors->get('elements.*'), JSON_FORCE_OBJECT);
    </script>
@endpush

@include('element::admin.elements.templates.builder_element')
