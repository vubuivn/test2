<section class="block-single-2 sec-b group-ef lazy-hidden">
    <div class="container">
        <div class="row sec-b">
            <div class="col-md-6 col-img">
                <div class="img efch-1 ef-img-r">
                    <img class="lazy-hidden" data-lazy-type="image" src="/assets/images/image.svg"
						data-lazy-src="{{ $image1 ? $image1->file : '' }}"
						alt="{{ $image1 ? $image1->caption_1 : 'missing image here' }}" />
                </div>
            </div>

            <div class="col-md-6 col-text">
                <p class="img efch-2 ef-img-t">
                    <img class="lazy-hidden" data-lazy-type="image" src="/assets/images/image.svg"
						data-lazy-src="{{ $image2 ? $image2->file : '' }}"
						alt="{{ $image2 ? $image2->caption_1 : 'missing image here' }}" />
                </p>
                <div class="divtext ">
                    <h2 class="title efch-2 ef-tx-t ">
						{{ $title }}
                    </h2>
                    <div class="desc efch-3 ef-tx-t">
						{{ $description }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>