@extends('public.layout')

@section('title', $post->meta->meta_title != '' ? $post->meta->meta_title : $post->name)

@push('meta')
    @include('public.partials.meta',['type'=>'post'])
@endpush

@section('content')

@include('public.partials.bannertop', ['banner'=> '/assets/images/tin-tuc.jpg', 'title'=>'Tin tức'])
    <main id="main">
        <div class="container">
            <section class="page-news-detail entry-content sec-tb">
                <div class="max670">
                    <h1 class="h3 text-center">{{$post->name}}</h1>
                    <div class="meta">
                        <div class="cate"><span>{{$category->name}}</span></div>
                        <span class="date">{{date("d/m/Y", strtotime($post->created_at))}}</span>
                    </div>
                    {!!$post->description!!} 

                </div>
            </section>

            <!-- Related -->
            @if(count($related)>0)
            <section class="sec-news-list sec-news-list-related sec-tb">
                <h3>{{trans('storefront::storefront.news.related')}}</h3>
                <div class=" owl-carousel  s-auto" data-res="3,3,2,1" paramowl="margin=10" >
                    @foreach($related as $p)
                        <div class="item" >
                        <a href="{{route('news.detail',$p->slug)}}">   
                            <div class="img tRes_85">
                                <img src="{{$p->baseImage->path}}" alt="{{$p->name}}">
                            </div>
                        </a>
                            <div class="divtext">
                                <div class="meta">
                                    <div class="cate"><span>{{$category->name}}</span></div>
                                    <span class="date">{{date("d/m/Y", strtotime($p->created_at))}}</span>
                                </div>                  
                                <h3 class="title"><a href="{{route('news.detail',$p->slug)}}"> {{$p->name}}</h3>
                                <div class="desc">
                                    {{$p->short_description}}
                                </div>
                                <div class="more">
                                    <a href="#" >{{trans('storefront::storefront.news.view_more')}} <i class="icon-arrow-14"></i></a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
            @endif
        </div>
    </main>
@endsection
