<?php

namespace Modules\Element\Entities;

use Modules\Support\Eloquent\TranslationModel;

class ElementTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
}
