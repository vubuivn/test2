@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => trans('product::products.product')]))
    @slot('subtitle', $product->name)

    <li><a href="{{ route('admin.products.index') }}">{{ trans('product::products.products') }}</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => trans('product::products.product')]) }}</li>
@endcomponent

@section('content')
	@if(isset($_REQUEST['group']))<a href="@if(isset($_REQUEST['group'])){{route('admin.groups.edit',$_REQUEST['group'])}}@endif" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> {{trans('product::products.back')}}</a>@endif
	<div style="margin-bottom: 20px"></div>
    <form method="POST" action="{{ route('admin.products.update', $product) }}" class="form-horizontal" id="product-edit-form" novalidate>
        {{ csrf_field() }}
        {{ method_field('put') }}
        {!! $tabs->render(compact('product')) !!}
    </form>
@endsection

@include('product::admin.products.partials.shortcuts')
