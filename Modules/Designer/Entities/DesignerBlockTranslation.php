<?php

namespace Modules\Designer\Entities;

use Modules\Support\Eloquent\TranslationModel;

class DesignerBlockTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content'
    ];
}
