<?php

namespace Modules\Block\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Block\Entities\Block;
use Modules\Block\Http\Requests\SaveBlockRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlockController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Block::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'block::blocks.block';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'block::admin.blocks';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveBlockRequest::class;

    public function store ()
    {
        $this->disableSearchSyncing();
        $entity = $this->getModel()
            ->create(
                $this->getRequest('store')
                    ->all()
            );

        $this->searchable($entity);

        if (method_exists(
            $this,
            'redirectTo'
        )) {
            return $this->redirectTo($entity);
        }
        Artisan::call(
            'block:generate',
            ['id' => $entity->id]
        );
        return redirect()
            ->route("{$this->getRoutePrefix()}.index")
            ->withSuccess(
                trans(
                    'admin::messages.resource_saved',
                    ['resource' => $this->getLabel()]
                )
            );
    }

    public function update ($id)
    {
        $entity = $this->getEntity($id);

        $this->disableSearchSyncing();

        $entity->update(
            $this->getRequest('update')
                ->all()
        );

        $this->searchable($entity);
        if (method_exists(
            $this,
            'redirectTo'
        )) {
            return $this->redirectTo($entity)
                ->withSuccess(
                    trans(
                        'admin::messages.resource_saved',
                        ['resource' => $this->getLabel()]
                    )
                );
        }
        Artisan::call(
            'block:generate',
            ['id' => $entity->id]
        );
        return redirect()
            ->route("{$this->getRoutePrefix()}.index")
            ->withSuccess(
                trans(
                    'admin::messages.resource_saved',
                    ['resource' => $this->getLabel()]
                )
            );
    }

    public function block_tags(Request $request){
        $data = [];
        if ($request->ajax()) {
            $totalget = DB::table('block_tags')->get();
            $total = collect($totalget)->count();

            $query = DB::table('block_tags');

            # Search title
            if ('' !== $search = $request->search['value']) {
                $query = $query->whereRaw("UPPER(name) LIKE UPPER('%{$search}%')");
                $filtered = $query->count();
            }

            # Pagination
            $tags = $query->orderBy('id','desc')->skip($request->start)->take($request->length)->get();
            # Output
            $rows = [];
            foreach ($tags as $tag) {
                $r = str_random();
                $rows[] = [
                    '<div class="checkbox"><input type="checkbox" class="select-row" value="'. $tag->id .'" id="'.$r.'"><label for="'.$r.'"></label></div>',
                    $tag->id,
                    $tag->name,
                ];
            }
            return response()->json([
                'data'            => $rows,
                "recordsTotal"    => $total,
                'recordsFiltered' => isset($filtered) ? $filtered : $total
            ]);
        }
        return view("{$this->viewPath}.block_tags", $data);
    }

    public function create_block_tags(){
        $data = [];
        return view("{$this->viewPath}.create_tag", $data);

    }

    public function tag_store(Request $request){
        $data = $request->all();
        $check = DB::table('block_tags')->where('name',trim($data['name']))->first();
        if($check){
            return redirect()->route('admin.blocks.block_tags')->with('error', trans('block::blocks.tag_already_exsist'));
        }
        try{
            DB::table('block_tags')->insert(['name' => trim($data['name']) ]);
            return redirect()->route('admin.blocks.block_tags')->withSuccess(trans('block::blocks.tag_add_success'));
        }catch(\Exception $e){
            return redirect()->route('admin.blocks.block_tags')->with('error', trans('block::blocks.tag_add_error'));
        }
        
    }
}
