<?php

Route::get('blocks', [
    'as' => 'admin.blocks.index',
    'uses' => 'BlockController@index',
    'middleware' => 'can:admin.blocks.index',
]);

Route::get('blocks/create', [
    'as' => 'admin.blocks.create',
    'uses' => 'BlockController@create',
    'middleware' => 'can:admin.blocks.create',
]);

Route::post('blocks', [
    'as' => 'admin.blocks.store',
    'uses' => 'BlockController@store',
    'middleware' => 'can:admin.blocks.create',
]);

Route::get('block_tags', [
    'as' => 'admin.blocks.block_tags',
    'uses' => 'BlockController@block_tags',
    'middleware' => 'can:admin.blocks.index',
]);


Route::post('block_tags', [
    'as' => 'admin.blocks.block_tags',
    'uses' => 'BlockController@block_tags',
    'middleware' => 'can:admin.blocks.index',
]);

Route::get('block_tags/create', [
    'as' => 'admin.blocks.create_block_tags',
    'uses' => 'BlockController@create_block_tags',
    'middleware' => 'can:admin.blocks.create',
]);

Route::post('block_tags/tag_store', [
    'as' => 'admin.blocks.tag_store',
    'uses' => 'BlockController@tag_store',
    'middleware' => 'can:admin.blocks.create',
]);

Route::get('blocks/{id}/edit', [
    'as' => 'admin.blocks.edit',
    'uses' => 'BlockController@edit',
    'middleware' => 'can:admin.blocks.edit',
]);

Route::put('blocks/{id}', [
    'as' => 'admin.blocks.update',
    'uses' => 'BlockController@update',
    'middleware' => 'can:admin.blocks.edit',
]);

Route::delete('blocks/{ids?}', [
    'as' => 'admin.blocks.destroy',
    'uses' => 'BlockController@destroy',
    'middleware' => 'can:admin.blocks.destroy',
]);



Route::get('blocks/test', [
    'as' => 'admin.blocks.test',
    'uses' => 'BlockController@test',
]);


;
// append

