<?php

namespace Modules\Builder\Entities;

use Modules\Support\Eloquent\TranslationModel;

class BuilderTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'body', 'embedded'];
}
