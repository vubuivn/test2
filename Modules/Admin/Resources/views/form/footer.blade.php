<div class="form-group">
    <div class="{{ ($buttonOffset ?? true) ? 'col-md-offset-2' : '' }} col-md-10">
        @if($submit== true)
            <button type="submit" class="btn btn-primary" data-loading>
                {{ trans('admin::admin.buttons.save') }}
            </button>
        @else
            <button id="btn-save-form" type="button" class="btn btn-primary">
                {{ trans('admin::admin.buttons.save') }}
            </button>
        @endif
    </div>
</div>
