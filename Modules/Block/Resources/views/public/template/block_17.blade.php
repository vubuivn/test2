<div class="container">
<section class="sec-b about-2 ">
<div class="max670">
	<h2>{{$title}}</h2>
	{!!$description!!}
</div>
<div class="row mb-20">
	@php($i = 1)
	@foreach($images as $key=>$value)
		@if($i == 2)
			<div class="col-md-6">
		@endif
		@if($i == 1)
			<div class="col-md-6">
				<div class="img tRes_90"><img class="lazy-hidden" data-lazy-type="image"   data-lazy-src="{{$value->file}}"   alt="">
				</div>
			</div>
		@else
			<div class="img tRes_42">
				<img class="lazy-hidden" data-lazy-type="image"   data-lazy-src="{{$value->file}}"   alt="">
			</div>
		@endif
		@php($i++)
	@endforeach
	@if($i>1)
	</div>
	@endif
</div>
</section>
</div>