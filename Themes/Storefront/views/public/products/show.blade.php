@extends('public.layout')

@section('title', (isset($group->meta->meta_title) && $group->meta->meta_title != '') ? $group->meta->meta_title : $group->name )
<?php 
    $meta_desc = trans('group::group-categories.height').': '.$defaultProduct['height'].' '.trans('group::group-categories.length').': '.$defaultProduct['length'].' '. trans('group::group-categories.width').': '.$defaultProduct['width'].', ';
    $i=1;
    foreach($attributes AS $id => $attribute){
        if($i > 1){
            $meta_desc .= ', ';
        }
        if(isset($defaultProduct['attribute_details'][$id])){
            $meta_desc .= $attribute['name'] . ": ".$defaultProduct['attribute_details'][$id]->value;
        }
        $i++;
    }
    if($group->content_1 != ''){
        $meta_desc .= $group->content_1;
    }
    if($group->content_1 != ''){
       $meta_desc .= ' '.$group->content_2;
    }
    $meta_desc .= ' '.$defaultProduct['sku'];
?>
@push('meta')
<meta name="title" content="{{ isset($group->meta->meta_title) ? $group->meta->meta_title : strip_tags($group->description) }}">
<meta name="keywords" content="{{ (isset($group->meta->meta_keywords)) ? implode(',', $group->meta->meta_keywords) : null }}">
<meta name="description" content="{{ isset($group->meta->meta_description) ? $group->meta->meta_description : strip_tags($meta_desc) }}">
<meta property="og:title" content="{{ isset($group->meta->meta_title) ? $group->meta->meta_title : strip_tags($group->description) }}">
<meta property="og:description" content="{{ isset($group->meta->meta_description) ? $group->meta->meta_description : strip_tags($meta_desc) }}">
<meta property="og:image" content="{{ productImageUrl($defaultProduct['sku'], 1) }}">

@endpush

@php
    $breadcrumbCategory = null;
    if (is_array($group->categories)){
        foreach ($group->categories as $category){
            if ($category->parent_id == null){
                $breadcrumbCategory = $category;
            }
        }
        if ($breadcrumbCategory == null){
            $breadcrumbCategory = $group->categories[0];
        }
    }
@endphp
<!-- 
@section('breadcrumb')
    <li>
        <a href="{{-- route('products.index') --}}">
            {{ trans('storefront::products.shop') }}
        </a>
    </li>
    @if($breadcrumbCategory != null)
    <li>
        <a href="{{ route('products.category', $breadcrumbCategory->slug) }}">
            {{$breadcrumbCategory->name}}
        </a>
    </li>
    @endif
    <li class="active">{{ $group->name }}</li>
@endsection -->

@section('content')

<section id="bannertop2" class="detailbanner sec-b">
    <div class="container">
        <div class="row grid-space-120">
            <div class="col-lg-7 images">

                <div class="owl-carousel s-dots s-auto_ s-nav">
                    @for ($i = 1; $i <= $defaultProduct['image_count']; $i++)
                        <div class="item">
                            <div class="tRes_60 img">
                                <img class="owl-lazy" src="/assets/images/blank.png"
                                    data-src="{{ productImageUrl($defaultProduct['sku'], $i) }}" alt="">
                            </div>
                        </div> <!-- end item -->
                    @endfor
                </div>

            </div>
            <div class="col-lg-5">
                <div class="info">
                    <h1 class="h2">{{ $group->name }}</h1>
                    <div>{!! $group->description !!}</div>
                    <p></p>

                    <div class="rowlabel label-140">
                        @if($defaultProduct['height'] != NULL && $defaultProduct['height'] != '')
                        <div class="item">
                            <span class="title">{{ trans('group::group-categories.height') }}</span>
                            <span class="text">
                                <span class="u-product-height">{{ $defaultProduct['height'] }}</span> cm
                            </span>
                        </div>
                        @endif
                        @if($defaultProduct['width'] != NULL && $defaultProduct['width'] != '')
                        <div class="item">
                            <span class="title">{{ trans('group::group-categories.width') }}</span>
                            <span class="text">
                                <span class="u-product-width">{{ $defaultProduct['width'] }}</span> cm
                            </span>
                        </div>
                        @endif
                        @if($defaultProduct['length'] != NULL && $defaultProduct['length'] != '')
                        <div class="item">
                            <span class="title">{{ trans('group::group-categories.length') }}</span>
                            <span class="text">
                                <span class="u-product-length">{{ $defaultProduct['length'] }}</span> cm
                            </span>
                        </div>
                        @endif
                    </div>
                    <div class="variation row grid-space-10">
                        @foreach ($attributes AS $id => $attribute)
                        @if (isset($defaultProduct['attribute_details'][$id]))
                        <div class="col-4 col-lg-3">
                            <div class="item">
                                <div class="key">
                                    <strong>{{ $attribute['name'] }}</strong>
                                    <i class="icon-arrow-3"></i>
                                </div>

                                <select data-id="{{ $id }}" class="selectpicker attributes" title="">
                                    @foreach ($attribute['values'] AS $item)
                                    <option value="{{ $item['value'] }}"
                                        {{ isset($defaultProduct['attributes'][$id])
                                            && $item['value'] === $defaultProduct['attributes'][$id][0]
                                            ? 'selected' : '' }}>{{ $item['text'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                    <form action="{{ route('cart.items.store') }}" method="POST" name="addToCart">
                        <div class="qualitys clearfix">
                            <div class="title">{{ trans('group::group-categories.quantity') }}</div>
                            <div class="quality">
                                <span class="minus">
                                    <a href="#"><i class="icon-minus-2"></i></a>
                                </span>
                                <input class="numb" type="mumber" value="1" name="qty">
                                <span class="plus">
                                    <a href="#"><i class="icon-plus-2"></i></a>
                                </span>
                            </div>
                            <!-- <select name="" id="" class="select">
                                <option value="Miễn phí giao hàng nội thành"> Miễn phí giao hàng nội thành</option>
                                <option value="Miễn phí giao hàng nội thành"> Miễn phí giao hàng nội thành 2</option>
                            </select> -->
                        </div>
                        <div class="line-border"></div>
                        <div class="price-info row">

                            @if(($defaultProduct['special_price'] != $defaultProduct['normal_price']) && $defaultProduct['special_price'] != 0)
                            <div class="price col-xs-6">
                                {{ formatMoney((int)$defaultProduct['special_price']) }}
                            </div>
                            <div class ="price-cost col-xs-6">
                                {{ formatMoney((int)$defaultProduct['normal_price']) }}
                            </div>
                            @else
                                <div class="price col-xs-6">
                                    {{ formatMoney($defaultProduct['price']) }}
                                </div>
                            @endif
                        </div>
                        @if($defaultProduct['out_of_stock'] == 1)
                            <div class="btn btn-default out_of_stock_button">{{trans('storefront::storefront.out_of_stock')}}</div>
                        @endif
                        <div class="clearfix"></div>
                        <div class="btns">
                            <!-- <span class="btn btn2" onclick="document.forms['addToCart'].submit()"> -->
                            <a href="#" class="btn btn2 detail_add_to_cart_2 {{ $defaultProduct['out_of_stock'] == 1 ? 'out_of_stock' : '' }}">
                                <i class="icon-cart"></i> 
                                {{ trans('group::group-categories.buy_now') }}
                            </a> &nbsp;
                            <!-- <button type="submit" class="btn btn2">
                                <i class="icon-cart"></i> 
                                {{ trans('group::group-categories.buy_now') }}
                            </button> &nbsp; -->
                            <span class="btn {{ $defaultProduct['wishlist'] ? 'btn3' : '' }}" id="detail_wishlist" data-id="{{ $defaultProduct['id'] }}" data-type="detail">
                                {!! $defaultProduct['wishlist'] ? '<i class="icon-heart-add"><span class="path1"></span></i>' : '<i class="icon-heart"></i>' !!}
                                {{ trans('group::group-categories.wishlist') }}
                            </span>
                        </div>

                        @csrf
                        <input type="hidden" name="product_id" id="product_id" value="{{ $defaultProduct['id'] }}" />
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>

<main id="main" class="page-product-detail">
    <div class="container">

        <div class="winfo entry-content">

            <div class="feature">
                <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/blank.png"
                    data-lazy-src="{{ groupImageUrl($group->code, 1) }}" alt="" />
            </div>
   
            <div class="info">
                <h3>{{ trans('group::group-categories.product_detail') }}</h3>

                <div class="toggleHeight">
                    <div class="tgh-content">
                        <div class="tgh-full">
                            <div class="tgh-first">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6>{{ trans('group::group-categories.width_and_height') }}</h6>
                                        <div>{{ trans('group::group-categories.height') }}:
                                            <span class="u-product-height">{{ $defaultProduct['height'] }}</span> cm
                                        </div>
                                        <div>{{ trans('group::group-categories.length') }}: 
                                            <span class="u-product-length">{{ $defaultProduct['length'] }}</span> cm
                                        </div>
                                        <div>{{ trans('group::group-categories.width') }}: 
                                            <span class="u-product-width">{{ $defaultProduct['width'] }}</span> cm
                                        </div>
                                        <!-- <div>{{ trans('group::group-categories.volume') }}: 
                                            <span class="u-product-volume">{{ $defaultProduct['volume'] }}</span> cm<sup>3</sup>
                                        </div> -->
                                    </div>
                                    <div class="col-md-6" style="padding-top: 20px;">
                                        <h6>{{ trans('group::group-categories.material') }}</h6>
                                        @foreach ($attributes AS $id => $attribute)
                                        @if (isset($defaultProduct['attribute_details'][$id]))
                                        <div>{{ $attribute['name'] }}:
                                            <span class="u-attribute-{{ $id }}">
                                                {{ $defaultProduct['attribute_details'][$id]->value }}
                                            </span>
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                            <div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6>{{ trans('group::group-categories.special_features') }}</h6>
                                        {!! $group->content_1 !!}
                                    </div>
                                    <div class="col-md-6" style="padding-top: 20px;">
                                        <h6>{{ trans('group::group-categories.product_sku') }}</h6>
                                        <p class="u-product-sku">{{ $defaultProduct['sku'] }}</p>
                                        <h6>{{ trans('group::group-categories.download') }}</h6>
                                        {!! $group->content_2 !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="toggle">
                        <span class="m"><i class="icon-arrow-12"></i></span>
                        <span class="l"><i class="icon-arrow-13"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <section class=" sec-tb  Collage effect-parent">
            @for($i = 1; $i <= $group->image_count; $i++)
                <div class="Image_Wrapper">
                  <div class="inner">
                    <img src="{{ groupImageUrl($group->code, $i) }}" alt="" />
                  </div>  
                </div>
            @endfor   
        </section>

        <!-- San pham lien quan -->
        @include('public.products.detail.related-groups')

        <!-- San pham da xem -->
        @include('public.products.detail.visited-groups')
    </div>
</main>

@include('public.products.detail.script')

@endsection

@push('scripts')
<script type='text/javascript' src='/assets/js/cart.js'></script>
@endpush