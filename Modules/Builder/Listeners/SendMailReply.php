<?php

namespace Modules\Builder\Listeners;

use Modules\Builder\Events\EventReplyMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailReply implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param EventReplyMail $event
     * @return void
     */
    public function handle(EventReplyMail $event)
    {
        if ($event->noti){
            $emails = explode(",", $event->reply->reply_to);
            $isEmails = count($emails);
            $email = array_shift($emails);
            if ($isEmails > 1){
                \Mail::to($email)->cc($emails)->send(
                    new \Modules\Builder\Mail\ReplyMail($event)
                );
            }else{
                \Mail::to($email)->send(
                    new \Modules\Builder\Mail\ReplyMail($event)
                );
            }
        }else{
            $reply_to = str_replace(['[',']'],'',$event->reply->reply_to);
            \Mail::to($event->data->{$reply_to})->send(
                new \Modules\Builder\Mail\ReplyMail($event)
            );
        }

    }
}
