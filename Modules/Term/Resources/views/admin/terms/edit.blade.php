@extends('admin::layout')

@component('admin::components.term.header')
    @slot('title', trans('admin::resource.edit', ['resource' => trans('term::terms.term')]))
    @slot('subtitle', $term->title)

    <li><a href="{{ route('admin.terms.index') }}">{{ trans('term::terms.terms') }}</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => trans('term::terms.term')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.terms.update', $term) }}" class="form-horizontal" id="term-edit-form" novalidate>
        {{ csrf_field() }}
        {{ method_field('put') }}

        {!! $tabs->render(compact('term')) !!}
    </form>
@endsection

@include('term::admin.terms.partials.shortcuts')
