<?php

use Illuminate\Support\HtmlString;

function block_data ($id)
{
    if(Cache::tags(['blocks'])->has('blocks.page'.$id.':' . locale())){
        return Cache::tags(['blocks'])->get('blocks.page'.$id.':' . locale());
    }else{
        return Cache::tags(['blocks'])
    ->rememberForever('blocks.page'.$id.':' . locale(),function () use($id){
        return \Modules\Block\Entities\Block::withoutGlobalScope('active')
                ->where('id',$id)
                ->first();
    });
    }
}

function block_values ($id)
{
    $block = block_data($id);

    if(Cache::tags(['blocks'])->has('block_values'.$id.':' . locale())){
        return Cache::tags(['blocks'])->get('block_values'.$id.':' . locale());
    }else{
        return Cache::tags(['blocks'])
            ->rememberForever('block_values'.$id.':' . locale(),function () use($block){
                return $block->values;
            });
    }
}

function block_html ($block, $number = '<%- i %>', $data = null)
{
    switch ($block->type) {
        case 'editor':
            $view = 'block::admin.blocks.html.textarea';
            break;
        case 'single-image':
        case 'multiple-images':
            $view = 'block::admin.blocks.html.image';
            break;
        case 'multiple-images-custom':
            $view = 'block::admin.blocks.html.image_custom';
            break;
        default:
            $view = 'block::admin.blocks.html.' . $block->type;
            break;
    }
    $formcontacts = [];
    if($block->type == 'contact'){
        $formcontacts = \Modules\Builder\Entities\Builder::where('is_active',1)->get();
    }

    try {
        $html = view(
            $view,
            compact(
                [
                    'block',
                    'data',
                ]
            )
        )
            ->with(['number' => $number,'formcontacts' => $formcontacts])
            ->render();
    } catch (ErrorException $error) {
        $html = null;
    }

    return new HtmlString($html);
}

function _checkBlocks ($blocks)
{
    $result = [];
    foreach ($blocks as $key => $items) {
        $tmp = [];
        foreach ($items as $k => $item) {
            switch ($k) {
                case 'main_block_id':
                    $tmp['id'] = $item;
                    break;
                case 'block_id':
                    $tmp['block_id'] = $item;
                    break;
                case 'main_block_title':
                    $tmp['title'] = $item;
                    break;
                default:
                    $tmp['content'][$k] = $item;
                    break;
            }
        }
        $tmp['content'] = json_encode($tmp['content']);
        $result[$key]   = $tmp;
    }
    return $result;
}
