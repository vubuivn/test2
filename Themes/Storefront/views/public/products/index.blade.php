@extends('public.layout')

@section('title')
    @if (request()->has('query'))
        {{ trans('storefront::products.search_results_for') }}: "{{ request('query') }}"
    @else
        @if(isset($category))
            @if(isset($category->meta->meta_title))
            {{$category->meta->meta_title}}
            @else{{$category->name}}
            @endif
        @endif
    @endif
@endsection

@push('meta')
    @include('public.partials.meta',['type'=>'category'])
@endpush

@section('content')
    <section id="bannertop" class="tRes_48 subbanner" style="background-image: url({{ $category->coverImage->path }});">
        <div class="divtext"><h1>{{ $category->name }}</h1></div>
    </section>
    @if(isset($category) && $category->slug == 'sale')
        @include('public.products.listing.filter',['category_type' => 'sale'])
    @else
        @include('public.products.listing.filter')
    @endif
    <main id="main">
        <div class="container">
            <div class="worder clearfix">
                <div class="iorder">
                    <img src="/assets/images/order.svg" alt="" />
                    <select onchange="location = this.value">
                        <option value="{{ request()->fullUrlWithQuery(['sort' => 'latest']) }}" {{ $request['sort'] === 'latest' ? 'selected' : '' }}>
                            {{ trans('storefront::products.sort_options.latest') }}
                        </option>

                        <option value="{{ request()->fullUrlWithQuery(['sort' => 'priceLowToHigh']) }}" {{ $request['sort'] === 'priceLowToHigh' ? 'selected' : '' }}>
                            {{ trans('storefront::products.sort_options.price_low_to_high') }}
                        </option>

                        <option value="{{ request()->fullUrlWithQuery(['sort' => 'priceHighToLow']) }}" {{ $request['sort'] === 'priceHighToLow' ? 'selected' : '' }}>
                            {{ trans('storefront::products.sort_options.price_high_to_low') }}
                        </option>
                    </select>
                </div>
            </div>        
            @if($category->slug != 'sale' && \Route::current()->getName() != 'products.categorysale')
            @include('public.products.listing.feature-groups')
            @endif
            @include('public.products.listing.group-list')
        </div>
        
    </main>
@endsection

@push('scripts')
@if(request()->has('fromPrice') || request()->has('attribute'))
<script type="text/javascript">
    (function($){
        $(window).load(function() {
            $('html, body').animate({ scrollTop: $('.lists-4').offset().top - 150 }, 'slow');
        })
        
    })(jQuery) 
</script>
@endif
@endpush