<?php

return [
    'name' => 'Name',
    'slug' => 'URL',
    'body' => 'Body',
    'is_active' => 'Status',
    'block' => 'Block',
    'position' => 'Position',
    'city' => 'City',
    'request' => 'Request',
    'welfare' => 'Welfare',
    'time_job' => 'Time for job',
    'banner_image' => 'Banner image',
    'note_content' => 'Note',
];
