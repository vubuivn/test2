<?php

namespace Modules\Store\Entities;

use Modules\Admin\Ui\AdminTable;
use Modules\Support\Eloquent\Model;
use Modules\Meta\Eloquent\HasMetaData;
use Modules\Support\Eloquent\Sluggable;
use Modules\Support\Eloquent\Translatable;
use Modules\Support\Search\Searchable;
use Modules\Media\Entities\File;
use Modules\Media\Eloquent\HasMedia;

class Store extends Model
{
    use Translatable, Sluggable, HasMetaData, HasMedia;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['slug', 'is_active','template','loc_lat','loc_long','district_id','email'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $translatedAttributes = ['name','address','phone','open'];

    /**
     * The attribute that will be slugged.
     *
     * @var string
     */
    protected $slugAttribute = 'name';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addActiveGlobalScope();
    }
    public function saveRelations(array $store)
    {
        // $this->saveBlocks(array_get($store, 'blocks', []));

    }


    public function scopeWithBaseImage($query)
    {
        $query->with(['files' => function ($q) {
            $q->wherePivot('zone', 'base_image');
        }]);
    }

    public function getBaseImageAttribute()
    {
        return $this->files->where('pivot.zone', 'base_image')->first() ?: new File;
    }


    public function saveBlocks($blocks = [])
    {
        $ids = $this->getDeleteCandidates($blocks);

        if ($ids->isNotEmpty()) {
            $this->blocks()->whereIn('id', $ids)->delete();
        }

        foreach (array_reset_index($blocks) as $index => $value) {
            $this->blocks()->updateOrCreate(
                ['id' => $value['id'] ?? null],
                $value + ['position' => $index]
            );
        }
    }
    private function getDeleteCandidates($values = [])
    {
        return $this->blocks()
            ->pluck('id')
            ->diff(array_pluck($values, 'id'));
    }
    public static function urlForStore($id)
    {
        return static::select('slug')->firstOrNew(['id' => $id])->url();
    }

    public function url()
    {
        // if (is_null($this->slug)) {
        //     return '#';
        // }

        // return localized_url(locale(), $this->slug);
        return route('store.detail',$this->slug);
    }

    /**
     * Get table data for the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function table()
    {
        return new AdminTable($this->newQuery()->withoutGlobalScope('active'));
    }

}
