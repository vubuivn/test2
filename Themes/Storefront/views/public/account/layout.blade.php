@extends('public.layout')

{{--@section('breadcrumb') --}}
    {{--@if (request()->routeIs('account.dashboard.index')) --}}
        <!-- <li class="active">{{-- trans('storefront::account.links.my_account') --}}</li> -->
    {{--@else --}}
        <!-- <li><a href="{{ route('account.dashboard.index') }}">{{-- trans('storefront::account.links.my_account') --}}</a></li> -->
    {{--@endif --}}

    {{--@yield('account_breadcrumb') --}}
{{--@endsection --}}

@section('content')
    @php($classpage = request()->segment(count(request()->segments())))
    @include('public.partials.bannertop',['title'=>trans('storefront::account.information'),'banner'=>'/assets/images/heading.jpg'])
    <main id="main" class="page-account">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3">

                    <div class="uname">
                        <span class="img">
                            <img width="15" height="20" src="/assets/images/svg/user.svg" alt="">
                        </span>
                        <div class="text">
                            <span class="t1">{{ trans('storefront::account.links.my_profile') }}</span>
                            <span class="t2">{{$my->first_name}} {{$my->last_name}}</span>
                        </div>
                    </div>

                    <ul class="menu-account">
                        <li class="{{ request()->routeIs('account.profile.edit') ? 'active' : '' }}"><a href="{{ route('account.profile.edit') }}"><img src="/assets/images/svg/ac-1.svg"   alt="" /> {{ trans('storefront::account.links.my_profile') }}</a></li>
                        <li class="{{ request()->routeIs('account.orders.index') ? 'active' : '' }}"><a href="{{ route('account.orders.index') }}"><img src="/assets/images/svg/ac-2.svg"   alt="" /> {{ trans('storefront::account.links.my_orders') }}</a></li>
                        <li><a href="#">
                            <img src="/assets/images/svg/ac-3.svg"   alt="" />
                            Sản phẩm đã xem</a></li>
                        <li class="{{ request()->routeIs('account.wishlist.index') ? 'active' : '' }}"><a href="{{ route('account.wishlist.index') }}"><img src="/assets/images/svg/ac-4.svg"   alt="" /> {{ trans('storefront::account.links.my_wishlist') }}</a></li>
                        <li><a href="{{ route('logout') }}">
                            <!-- <img src="/assets/images/svg/ac-5.svg"   alt="" /> -->
                             {{ trans('storefront::account.links.logout') }}</a></li>
                    </ul>
                </div>
                <div class="col-md-8 col-lg-9">
                    @yield('content_right')
                </div>
            </div>

        </div>
    </main>
    <!-- <div class="row">
        <div class="my-account clearfix {{$classpage}}">
            <div class="col-left">
                <div class="sidebar-menu">
                    <div class="icon-profile text-center">
                        <div class="image-profile">{{ Theme::image('public/images/no-profile-image.jpg') }}</div>
                        <div class="profile-info-top">{{$my->first_name}} {{$my->last_name}}</div>
                    </div>
                    <ul class="list-inline">

                        <li class="{{ request()->routeIs('account.profile.edit') ? 'active' : '' }}"">
                            <a href="{{ route('account.profile.edit') }}">
                                <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                {{ trans('storefront::account.links.my_profile') }}
                            </a>
                        </li> -->

                       <!--  <li class="{{ request()->routeIs('account.dashboard.index') ? 'active' : '' }}">
                            <a href="{{ route('account.dashboard.index') }}">
                                <i class="fa fa-dashboard" aria-hidden="true"></i>
                                {{-- trans('storefront::account.links.dashboard') --}}
                            </a>
                        </li> -->

                       <!--  <li class="{{ request()->routeIs('account.orders.index') ? 'active' : '' }}">
                            <a href="{{ route('account.orders.index') }}">
                                <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                                {{ trans('storefront::account.links.my_orders') }}
                            </a>
                        </li> -->

                      <!--   <li class="{{ request()->routeIs('account.wishlist.index') ? 'active' : '' }}">
                            <a href="{{ route('account.wishlist.index') }}">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                                {{-- trans('storefront::account.links.my_wishlist') --}}
                            </a>
                        </li> -->

                        <!-- <li class="{{ request()->routeIs('account.reviews.index') ? 'active' : '' }}">
                            <a href="{{ route('account.reviews.index') }}">
                                <i class="fa fa-comment" aria-hidden="true"></i>
                                {{-- trans('storefront::account.links.my_reviews') --}}
                            </a>
                        </li> -->
<!-- 
                        <li>
                            <a href="{{ route('logout') }}">
                                <i class="fa fa-sign-out" aria-hidden="true"></i>
                                {{ trans('storefront::account.links.logout') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-right">
                <div class="clearfix"></div>

                <div class="content-right clearfix">
                    {{-- @yield('content_right') --}}
                </div>
            </div>
        </div>
    </div> -->
@endsection
