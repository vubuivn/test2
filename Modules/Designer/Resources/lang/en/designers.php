<?php

return [
    'designer' => 'Designer',
    'designers' => 'Designers',
    'table' => [
        'name' => 'Name',
    ],
    'tabs' => [
        'group' => [
            'designer_information' => 'Designer Infortmation',
        ],
        'general' => 'General',
        'seo' => 'SEO',
        'block' => 'Block',
    ],
    'form' => [
        'enable_the_designer' => 'Enable the designer',
        'add_more_image' => 'Add more image',
        'banner_image' => 'Banner image',
        'feature' => 'Feature',
        'additional_images' => 'Additional Images'
    ],
    'expand_all' => 'Expand All',
    'collapse_all' => 'Collapse All',
    'template' => 'Template',
    'normal' => 'Normal',
    'has_sidebar' => 'Has sidebar',
    'questions' => 'Question',
];
