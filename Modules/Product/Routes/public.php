<?php


// Route::get('products', 'ProductController@index')->name('products.index');

Route::post('ajax/category/image', 'ProductController@categoryAjaxImage')->name('products.category.ajaximage');

Route::get('categories/sale/{slug1}/{slug2?}/{slug3?}', 'ProductController@categorysale')->name('products.categorysale');

Route::get('categories/{slug1}/{slug2?}/{slug3?}', 'ProductController@category')->name('products.category');


Route::get('/products/{any}', 'ProductController@show')->name('products.show');

