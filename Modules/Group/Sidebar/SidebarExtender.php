<?php

namespace Modules\Group\Sidebar;

use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{
    /**
     * @param \Maatwebsite\Sidebar\Menu $menu
     * @return \Maatwebsite\Sidebar\Menu
     */
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.content'), function (Group $group) {
            $group->item(trans('product::sidebar.products'), function (Item $item) {
                $item->item(trans('group::groups.groups'), function (Item $item) {
                    $item->weight(10);
                    $item->route('admin.groups.index');
                    $item->authorize(
                        $this->auth->hasAccess('admin.groups.index')
                    );
                });

            });
        });
    }
}
