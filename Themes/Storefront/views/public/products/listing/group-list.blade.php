<section class="lists-4 sec-tb ">
    @if (count($groupList) > 0)
    <div class="row" id="group-item-loop">
        @if($category->slug != 'sale' && \Route::current()->getName() != 'products.categorysale')
        <!-- PIN GROUPS -->
        @if(isset($category))
            @if($category->pin_groups != null)
                <?php 
                    $pinarry = explode(',',$category->pin_groups);
                    // var_dump($pinarry);
                    $pins = buildFromToPriceQueryBuilder()->whereIn('groups.id',$pinarry)->get();
                    // dd($pins);
                ?>
                @if(count($pins)>0)
                    @foreach($pins as $index => $group)
                        @include('public.products.listing.group-list-item')
                    @endforeach
                @endif
            @endif
        @endif
        <!-- END PIN GROUPS -->
        @endif
        @foreach ($groupList AS $index => $group)
            @include('public.products.listing.group-list-item')
        @endforeach
    </div>
    @else
    <p class="text-center">
        <b>{{ trans('group::group-categories.product_not_found') }}</b>
    </p>
    @endif
    @if(isset($category) && $category->slug=='sale')
        @if (count($groupList) === 24)
        <div class="showmore text-center">
            <a href="#" class="btn btn1" id="load-more-groups">
                {{ trans('group::group-categories.view_more') }}
            </a>
        </div>
        @endif
    @else
        @if (count($groupList) === 6)
        <div class="showmore text-center">
            <a href="#" class="btn btn1" id="load-more-groups">
                {{ trans('group::group-categories.view_more') }}
            </a>
        </div>
        @endif
    @endif
</section>

<script>
    (function($){
        var query = "{!! request()->fullUrlWithQuery(['ajax' => 1]) !!}";
        var page = 1;
        $('#load-more-groups').on('click', function(e) {
            e.preventDefault();
            var ajaxQuery = query + '&page=' + (page++);
            $.ajax(ajaxQuery).done(function(res) {
                if (res) {
                    $('#group-item-loop').append(res);
                } else {
                    $('#load-more-groups').hide();
                }
            });
        });
    })(jQuery);
</script>