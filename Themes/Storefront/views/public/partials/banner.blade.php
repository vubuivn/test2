@if(isset($banner))
    <div class="banner">
        <img src="@if(isset($banner['image'])){{$banner['image']}}@else {{ Theme::image('public/assets/images/banner.jpg') }} @endif">
        <h1>@if(isset($banner['title'])){{$banner['title']}}@endif</h1>
    </div>
@endif