<section class="block-single-1 sec-4 sec-b group-ef lazy-hidden">
    <div class="container">
        <div class="row grid-space-0 end center">
            <div class="col-lg-8 col-md-6">
                <div class="img  efch-1 ef-img-l">
                    <img class="lazy-hidden" data-lazy-type="image" src="/assets/images/image.svg"
						data-lazy-src="{{ $image ? $image->file : '' }}" alt="" />
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="divtext">
                    <div class="inner">
                        <div class="inner2">
                            <div class="title2 efch-2 ef-tx-t ">
								{{ $title1 }}
                            </div>
                            <h2 class="title efch-3 ef-tx-t">
								{{ $title2 }}
                            </h2>
                            <div class="desc efch-4 ef-tx-t">
								{{ $description }}
                            </div>
                            <div class="sec-more efch-5 ef-tx-t">
                                <a class="readmore" href="{{ $button_url }}">
									{{ $button_text }} <i class="icon-arrow-14"></i>
								</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>