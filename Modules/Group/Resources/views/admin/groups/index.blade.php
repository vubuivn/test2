@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('group::groups.groups'))

    <li class="active">{{ trans('group::groups.groups') }}</li>
@endcomponent

@component('admin::components.page.index_table')

    @slot('search', ['category'])
    @slot('buttons', ['create'])
    @slot('resource', 'groups')
    @slot('name', trans('group::groups.group'))
    @slot('buttons_js', ['export_excel','import_excel','list_product'])

    @component('admin::components.table')
        @slot('thead')
            <tr>
                @include('admin::partials.table.select_all')
                <th>{{ trans('group::attributes.name') }}</th>
                <th>{{ trans('group::attributes.code') }}</th>
                <th>{{ trans('admin::admin.table.status') }}</th>
                <th data-sort>{{ trans('admin::admin.table.created') }}</th>
            </tr>
        @endslot
    @endcomponent
@endcomponent

<div style="display: none">
    <input type="file" name="file" id="file"
           accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
</div>
@push('styles')
    <link href="{{ asset('loading.css') }}" rel="stylesheet">
@endpush
@push('scripts')
    <script src="{{ asset('loading.js') }}"></script>
    <script>
        $("#btn_export_excel_id").on('click', () => {
            $('body').loading('start');
            $.ajax({
                type: 'GET',
                url: route('admin.groups.export.excel'),
                success: function (response, textStatus, request) {
                    var a = document.createElement("a");
                    a.href = response.file;
                    a.download = response.name;
                    document.body.appendChild(a);
                    a.click();
                    a.remove();
                    $('body').loading('stop');
                },
                error(xhr) {
                    $('body').loading('stop');
                    alert('{{trans('admin::messages.system_error')}}');
                },
            });
        });
        $("#btn_import_excel_id").on('click', () => {
            $('#file').click();
        });

        $('#btn_list_product_id').on('click',() =>{
            window.location = route('admin.products.index');
        });

        new DataTable('#groups-table .table', {
            columns: [
                {data: 'checkbox', orderable: false, searchable: false, width: '3%'},
                {data: 'name', name: 'translations.name', orderable: false, defaultContent: ''},
                {data: 'code', name: 'code', orderable: false, defaultContent: ''},
                {data: 'status', name: 'is_active', searchable: false},
                {data: 'created', name: 'created_at'},
                {data: 'link', name: 'link',searchable: false,},
            ],
        }, function () {
            let exportButton = `
            <button type="button" class="btn btn-default btn-delete">
                    {{trans('category::categories.tree.export_excel')}}
                </button>
            `;
            exportButton = $(exportButton).appendTo(
                this.element.closest('.dataTables_wrapper').find('.dataTables_length')
            );
            exportButton.on('click', () => {
                let checked = $('#groups-table .table').find('.select-row:checked');

                if (checked.length === 0) {
                    return;
                }
                let confirmationModal = $('#confirmation-export-modal');
                let exported = [];

                confirmationModal.modal('show').find('form').on('submit', (e) => {
                    e.preventDefault();

                    confirmationModal.modal('hide');

                    let table = $('#groups-table .table').DataTable();

                    table.processing(true);

                    let ids = this.constructor.getRowIds(checked);

                    // Don't make ajax request if an id was previously deleted.
                    if (exported.length !== 0 && _.difference(exported, ids).length === 0) {
                        return;
                    }
                    // console.log(ids.join());
                    window.DataTable.setSelectedIds($('#groups-table .table'), []);

                    window.DataTable.reload($('#groups-table .table'));
                    $.ajax({
                        type: 'GET',
                        url: route('admin.groups.export.excel'),
                        data: {
                            ids: ids.join()
                        },
                        success: function (response) {
                            var a = document.createElement("a");
                            a.href = response.file;
                            a.download = response.name;
                            document.body.appendChild(a);
                            a.click();
                            a.remove();

                            exported = _.flatten(exported.concat(ids));

                            window.DataTable.setSelectedIds($('#groups-table .table'), []);

                            window.DataTable.reload($('#groups-table .table'));
                        },
                        error: (xhr) => {
                            error(`${xhr.statusText}: ${xhr.responseJSON.message}`);

                            exported = _.flatten(exported.concat(ids));

                            window.DataTable.setSelectedIds($('#groups-table .table'), []);

                            window.DataTable.reload($('#groups-table .table'));
                        },
                    });
                });
            })
        });

        function uploadFile(){
            var file_data = $('#file').prop('files')[0];
            var type = file_data.type;

            if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                var formData = new FormData();

                formData.append('import_file', file_data);
                $.ajaxSetup({
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Csrf-Token', $('input[name="_token"]').attr('content'));
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: route('admin.groups.import.excel'),
                    data: formData,
                    processData: false,
                    async: false,
                    cache: false,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    success: function (res) {
                        // $('body').loading('stop');

                        if (!res.status) {
                            alert(res.message);
                        } else {
                            if (res.errorFileName) {
                                var a = document.createElement("a");
                                a.href = res.errorFile;
                                a.download = res.errorFileName;
                                document.body.appendChild(a);
                                a.click();
                                a.remove();
                            }
                            
                            var messages = [
                                res.groupCreatedCount + ' nhom duoc tao moi',
                                res.groupUpdatedCount + ' nhom duoc cap nhat',
                                res.errorGroupGount + ' nhom bi loi',
                            ];
                            alert(messages.join("\n"));
                        }

                        location.reload();
                    },
                    error(xhr) {
                        alert('{{trans('admin::messages.system_error')}}');
                    },
                });
            } else {
                location.reload();
                $('body').loading('stop');
            }
        }
        $('#file').change(function () {
            $('body').loading('start');
            _.delay(uploadFile,1000);
        });
    </script>
@endpush
