@extends('public.layout')

@section('content')
@php($page_title = trans('storefront::cart.cart') )
@include('public.checkout.partials.banner_cart')
<div class="container">
    <div class="row">
        <div class="col-sm-10 margin-auto">
            @php($active = 'cart')
            @include('public.checkout.partials.cart_wizard')
        </div>
    </div>

    <div class="cart-list-wrapper row">
        <div class="col-md-10 cart-list-inner">
        @if ($cart->isEmpty())
            <p>
            <h2 class="text-center">{{ trans('storefront::cart.your_cart_is_empty') }}</h2>
        @else
            <div class="cart-list">
                @if (session('success'))
                <div class="alert alert-success clearfix">
                    <!-- <button type="button" class="close" data-dismiss="alert" aria-label="close">×</button> -->
                    <div class="alert-icon">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </div>
                    <p></p>
                    <p class="alert-text" style="color: green;">{{ session('success') }}</span>
                </div>
                @endif
            
                <div class="table-responsive">
                    <table class="table">
                        <thead class="hidden-xs">
                            <tr class="cart-title">
                                <th colspan="2">{{ trans('storefront::cart.product') }}</th>
                                <th>{{ trans('storefront::cart.price') }}</th>
                                <th>{{ trans('storefront::cart.quantity') }}</th>
                                <th>{{ trans('storefront::cart.total') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cart->items() as $cartItem)
                                @include('public.cart.partials.cart_item')
                            @endforeach
                        </tbody> 
                    </table>
                </div>
            </div> 
            <div class="cart-list-bottom row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <!-- <form method="POST" action="{{ route('cart.coupon.store') }}" id="coupon-apply-form" class="clearfix">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="coupon" style="text-transform: uppercase;">{{ trans('storefront::cart.coupon') }}</label>
                            <input type="text" name="coupon" class="form-control input" id="coupon" value="{{ old('coupon') }}"
                                placeholder="{{ trans('storefront::cart.coupon') }}">
                            <button type="submit" class="btn ico-r" id="coupon-apply-submit"
                                data-loading title="{{ trans('storefront::cart.apply_coupon') }}">
                                <i class="icon icon-tick"></i>
                            </button>
                        </div>
                    </form> -->
                </div>
                <div class="col-lg-3 offset-lg-4 col-md-4 offset-md-0 sub-total">
                    <!-- <div class="item-amount">
                        <label for="sub-total">{{ trans('storefront::cart.subtotal') }}</label>
                        <span class="price">{{ $cart->subTotal()->convertToCurrentCurrency()->format() }}</span>
                    </div>
                    <div class="item-amount coupon">
                        <label for="coupon-price">GIẢM GIÁ</label> <span class="price">- 500.000 VND </span>
                    </div> -->
                    <div class="final-total total">
                        <label for="total">{{ trans('storefront::cart.total') }}</label>
                        <span id="total-amount" class="price">{{ $cart->total()->convertToCurrentCurrency()->format() }}</span>
                    </div>

                </div>

            </div>

            @if ($cart->hasNoAvailableShippingMethod())
                <span class="error-message text-center">{{ trans('storefront::cart.no_shipping_method_is_available') }}</span>
            @endif
            
        @endif
            <div class="cart-list-actions">
                <a href="{{-- route('products.index') --}}" class=" btn btn-back">{{ trans('storefront::checkout.back_to_store') }}</a>
                @if (!$cart->isEmpty())
                <a href="{{ route('checkout.create') }}" class=" btn btn-primary ico-r btn-checkout {{ $cart->hasNoAvailableShippingMethod() ? 'disabled' : '' }}" @auth{{''}}@else{{'data-target=#myLogin'}}@endauth>
                    {{ trans('storefront::cart.checkout') }}
                    <i class="icon icon-arrow-14"></i>
                </a>
                @endif
            </div>
        </div>
    </div>
</div>

<script>
    (function($){
        // handle update product quantity
        var isSubmitted = false;
        $('.quantity .btn-number').on('click', function(e) {
            if (isSubmitted) {
                return false;
            }
            var quantity = parseInt($(this).parent().find('input[name=qty]').val());
            if ($(this).hasClass('btn-plus')) {
                quantity += 1;
            } else if (quantity > 1) {
                quantity -= 1;
            } else {
                return false;
            }
            
            $(this).prev().val(quantity);
            var form = $(this).parent().parent().find('form');
            form.find('input[name=qty]').val(quantity);
            isSubmitted = true;
            form.submit();
        });

        @auth

        @else
        $('.btn-checkout').on('click',function(e){
            e.preventDefault();
            $('#myLogin').modal({
                show: 'true'
            }); 
        });
        @endauth
    })(jQuery);
</script>
@endsection