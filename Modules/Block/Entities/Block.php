<?php

namespace Modules\Block\Entities;

use Modules\Block\Admin\BlockTable;
use Modules\Support\Eloquent\Model;
use Modules\Support\Eloquent\Translatable;

class Block extends Model
{
    use Translatable;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations', 'values'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_active',
        'html',
        'tags',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'name',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addActiveGlobalScope();
        static::saved(function (self $block) {
            $block->saveRelations(request()->all());
        });
    }
    public function saveRelations(array $attributes)
    {
        $this->saveValues(array_get($attributes, 'values', []));
    }
    public function saveValues($values = [])
    {
        $ids = $this->getDeleteCandidates($values);

        if ($ids->isNotEmpty()) {
            $this->values()->whereIn('id', $ids)->delete();
        }

        foreach (array_reset_index($values) as $index => $value) {
            $this->values()->updateOrCreate(
                ['id' => $value['id']],
                $value + ['position' => $index]
            );
        }
    }
    private function getDeleteCandidates($values = [])
    {
        return $this->values()
            ->pluck('id')
            ->diff(array_pluck($values, 'id'));
    }

    public function values(){
        return $this->hasMany(BlockValue::class)->orderBy('position');
    }
    /**
     * Get table data for the resource
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function table($request)
    {
        $query = $this->newQuery()
            ->withoutGlobalScope('active')
            ->addSelect(
                [
                    'id',
                    'is_active',
                    'tags',
                    'created_at'
                ]
            )
            ->when(
                $request->has('except'),
                function ($query) use ($request) {
                    $query->whereNotIn(
                        'id',
                        explode(
                            ',',
                            $request->except
                        )
                    );
                }
            );

        return new BlockTable($query);
    }
}
