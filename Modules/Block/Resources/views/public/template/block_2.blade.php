<section class="block-single-1 sec-1 sec-tb group-ef lazy-hidden">
    <div class="container">
        <div class="row grid-space-0 end center">
            <div class="col-lg-7 col-md-6">
                <div class="img efch-1 ef-img-l">
                    <img class="lazy-hidden" data-lazy-type="image" src="/assets/images/image.svg"
						data-lazy-src="{{ $right_image ? $right_image->file : '' }}" alt="" />
                </div>
            </div>
            <div class="col-lg-5 col-md-6">
                <div class="divtext ">
                    <div class="inner">
                        <div class="inner2">
                            <div class="title2 efch-2 ef-tx-t ">
								{{ $subtitle }}
                            </div>
                            <h2 class="title efch-3 ef-tx-t">
								{{ $main_title }}
                            </h2>
                            <div class="desc efch-4 ef-tx-t">
								{{ $description }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>