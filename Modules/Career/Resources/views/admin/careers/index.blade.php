@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('career::careers.careers'))

    <li class="active">{{ trans('career::careers.careers') }}</li>
@endcomponent

@component('admin::components.page.index_table')
    @slot('resource', 'careers')
    @slot('buttons', ['create'])
    @slot('name', trans('career::careers.career'))

    @slot('thead')
        <tr>
            @include('admin::partials.table.select_all')

            <th>{{ trans('career::careers.table.name') }}</th>
            <th>{{ trans('career::attributes.position') }}</th>
            <th>{{ trans('career::attributes.city') }}</th>
            <th>{{ trans('admin::admin.table.status') }}</th>
            <th data-sort>{{ trans('admin::admin.table.created') }}</th>
        </tr>
    @endslot
@endcomponent

@push('scripts')
    <script>
        new DataTable('#careers-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                { data: 'name', name: 'translations.name', orderable: false, defaultContent: '' },
                { data: 'position', name: 'translations.position', orderable: false, defaultContent: '' },
                { data: 'city', name: 'city', orderable: false, defaultContent: '' },
                { data: 'status', name: 'is_active', searchable: false },
                { data: 'created', name: 'created_at' },
            ],
        });
    </script>
@endpush
