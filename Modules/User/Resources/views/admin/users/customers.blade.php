@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('user::users.customer'))

    <li class="active">{{ trans('user::users.customer') }}</li>
@endcomponent

@component('admin::components.page.index_table')
  
    @slot('resource', 'customers')
    @slot('name', trans('user::users.customer'))

    @slot('thead')
        <tr>
            @include('admin::partials.table.select_all')

            <th>{{ trans('user::users.table.name') }}</th>
            <th>{{ trans('user::users.table.email') }}</th>
            <th>{{ trans('user::users.table.last_login') }}</th>
            <th data-sort>{{ trans('admin::admin.table.created') }}</th>
        </tr>
    @endslot
@endcomponent

@push('scripts')
    <script>
        new DataTable('#customers-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                { data: 'first_name', name: 'first_name' },
                { data: 'email', name: 'email' },
                { data: 'last_login', name: 'last_login', searchable: false },
                { data: 'created', name: 'created_at' },
            ]
        });
    </script>
@endpush
