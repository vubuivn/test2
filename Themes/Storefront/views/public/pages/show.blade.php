@extends('public.layout')

@section('title', $page->meta->meta_title != '' ? $page->meta->meta_title : $page->name )

@push('meta')
    @include('public.partials.meta',['type'=>'page'])
@endpush

@section('content')
    <div class="page-wrapper clearfix {{$page->template}}">
        @include('public.partials.bannertop')
        <main id="main">
            @if($page->template == 'has_sidebar')
                <div class="container">
                    <div class="row">
                        @includeIf('public.pages.template.hassidebar')
                    </div>
                </div>
            @else
                @includeIf('public.pages.template.normal')
            @endif
        </main>
    </div>
@endsection
