<?php

namespace Modules\Builder;

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
// use Box\Spout\Common\Entity\Row;

// use Illuminate\Http\Request;
// use Modules\Attribute\Entities\Attribute;
use Modules\Builder\Entities\Builder;
use DB;
// use Log;

class DataExportAll
{
    private $productAttr;
    private $productFields;
    private $productConfig = [];
    private $dbMappingDifferentKeys = [
        'width' => 'wide',
        'height' => 'high',
        'length' => 'long',
        'price' => 'selling_price'
    ];

    public function __construct(string $filePath, $errorProducts = [],$id)
    {
        // $this->productAttr = Attribute::withoutGlobalScope('active')->get();


        // $this->productConfig = config('imports.products');


        // $this->productFields = $this->generateProductFields($this->productConfig, $this->productAttr);

        // $this->productFields = $this->getField($id);

        // if (!empty($errorProducts)) {
        //     $this->productFields['errorMessages'] = 'Error message';
        // }

        $writer = WriterEntityFactory::createXLSXWriter();

        $writer->openToFile($filePath); // write data to a file or to a PHP stream
        // $writer->openToBrowser('export.xlsx');

        // create heading row
        $headings = $this->headings($id);
        $headingsRow = WriterEntityFactory::createRowFromArray($headings);
        $writer->addRow($headingsRow);

        // create body rows
        if (empty($errorProducts)) {
            $this->body($writer,$id);
        } else {
            $this->bodyError($writer, $errorProducts);
        }

        // finish and close the file
        $writer->close();
    }

    private function body($writer,$id)
    {
        // $attributes = $this->productAttr;
        // lay data
        $items = $this->buildGroupQueryResult($id);
        // dua vao excel
        // dd($items);
        foreach ($items as $item) {
            $row = [];
            foreach($item as $key => $value){
                if($key != 'id' && $key != 'locale' && $key != 'name' && $key != 'term_id' )
                $row[] = $value;
            }
            $bodyRow = WriterEntityFactory::createRowFromArray($row);
            $writer->addRow($bodyRow);
        }
    }

    private function bodyError($writer, $errorProducts)
    {
        foreach ($errorProducts as $item) {
            $formattedRow = [];
            foreach ($this->productFields AS $key => $val) {
                $productVal = $item[$key] ?? '';
                if (is_array($productVal)) {
                    $productVal = implode(';', $productVal);
                }
                $formattedRow[] = $productVal;
            }

            $bodyRow = WriterEntityFactory::createRowFromArray($formattedRow);
            $writer->addRow($bodyRow);
        }
    }

    private function headings($id): array
    {
        // return array_values($this->productFields);
        // $heading = [
        //     'Mã sản phẩm (SKU)',
        //     'Mã nhóm',
        //     'Dài (cm)',
        //     'Rộng (cm)',
        //     'Cao (cm)',
        //     'Thể tích',
        //     'Feature',
        //     'Giá',
        //     'Số chỗ ngồi',
        // ];

        // return \Schema::getColumnListing('term'.$id.'_translations');
        $builder = Builder::findOrFail($id);
        $row = [];
        foreach($builder->elements as $element){
            $row[] = $element->name;
        }
        return $row;
    }

    // private function generateProductFields($productFields, $productAttr)
    // {
    //     foreach ($productAttr as $attr) {
    //         $productFields[(string) $attr->id] = $attr->name;
    //     }

    //     return $productFields;
    // }

    private function buildGroupQueryResult($id)
    {
        $query = 'SELECT * FROM term'.$id.'_translations';

        return DB::select(DB::raw($query));
    }

    private function parseProductAttribute($attributeStr)
    {
        $productAttr = [];

        if ($attributeStr) {
            $attributes = explode(';', $attributeStr);
            foreach ($attributes AS $attribute) {
                $arr = explode(':', $attribute);
                $productAttr[$arr[0]] = $arr[1];
            }
        }

        return $productAttr;
    }

    private function getField($id){

    }
}