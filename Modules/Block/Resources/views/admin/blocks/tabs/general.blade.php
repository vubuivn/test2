{{ Form::text('name', trans('block::attributes.name'), $errors, $block, ['labelCol' => 2, 'required' => true]) }}

<div class="form-group">
             <label for="html" class="col-md-2 control-label text-left">
            {{trans('block::attributes.tag')}}
        </label>
    <div class="col-md-8">
        <select name="tags" class="form-control">
            <option value="0">{{trans('block.attributes.select_tag')}}</option>
            @if(isset($tags))
            @foreach($tags as $tag)
                <option value="{{$tag->id}}" @if($block->tags == $tag->id){{'selected'}} @endif>{{$tag->name}}</option>
            @endforeach
            @endif
        </select>
    </div>
</div>


<div class="row">
    <div class="col-md-8">
        {{ Form::checkbox('is_active', trans('block::attributes.is_active'), trans('block::blocks.form.enable_the_block'), $errors, $block) }}
    </div>
</div>
{{--{{ Form::textarea('html', trans('block::attributes.html'), $errors, $block, ['labelCol' => 2, 'required' => true]) }}--}}
<div class="form-group {{ ($errors->has('html')) ? 'has-error' : '' }}">
    <label for="html" class="col-md-2 control-label text-left">
        {{trans('block::attributes.html')}}
        <span class="m-l-5 text-red">*</span>
    </label>
    <div class="col-md-10">
        <textarea name="html" id="html">{{old('html', $block->html)}}</textarea>
        @if($errors->has('html'))
            <span class="help-block">{{ $errors->first('html') }}</span>
        @endif
    </div>
</div>
@push('styles')
    <link rel="stylesheet" href="{{ asset('codemirror/lib/codemirror.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('modules/block/admin/js/block.js') }}"></script>
@endpush
