<?php

namespace Modules\MailTemplate\Entities;

use Modules\Support\Eloquent\TranslationModel;

class MailTemplateTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'subject',
        'content'
    ];
}
