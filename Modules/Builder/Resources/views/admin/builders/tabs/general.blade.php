{{ Form::text('name', trans('builder::attributes.name'), $errors, $builder, ['labelCol' => 2, 'required' => true]) }}

<textarea name="body" id="body" style="display: none">{!! (!empty(optional($builder)->body) ? optional($builder)->body : '{}') !!}</textarea>
<textarea name="embedded" id="embedded" style="display: none">{{ (!empty(optional($builder)->embedded) ? optional($builder)->embedded : '{}') }}</textarea>

    <div id="formeo-editor"></div>
    <div class="render-form" style="display: none"></div>

<div class="row">
    <div class="col-md-8">
        {{ Form::checkbox('is_active', trans('builder::attributes.is_active'), trans('builder::builders.form.enable_the_page'), $errors, $builder) }}
    </div>
</div>
