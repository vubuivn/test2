<?php

return [
    'resource_saved' => ':resource has been saved.',
    'resource_deleted' => ':resource has been deleted.',
    'permission_denied' => 'Permission Denied (required permission: ":permission").',
    'system_error' => 'System error, Please try again!.',
];
