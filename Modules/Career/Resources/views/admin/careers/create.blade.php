@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('career::careers.career')]))

    <li><a href="{{ route('admin.careers.index') }}">{{ trans('career::careers.careers') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('career::careers.career')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.careers.store') }}" class="form-horizontal" id="career-form" novalidate>
    {{--<form class="form-horizontal" id="career-create-form" novalidate>--}}
        {{ csrf_field() }}

        {!! $tabs->render(compact('career')) !!}
    </form>
@endsection

@include('career::admin.careers.partials.shortcuts')