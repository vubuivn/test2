<?php

Route::get('products/{slug}/quick-view', 'ProductQuickViewController@show')->name('products.quick_view.show');


Route::get('search', 'ProductSearchController@search')->name('products.search.show');
