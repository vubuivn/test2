<div class="table-responsive">
    <table class="table table-striped table-hover dataTable no-footer" id="{{ $id ?? '' }}">
        <thead>
        <tr>
            @foreach($builder->elements as $element)
                <th>{{ $element->name }}</th>
            @endforeach
            <th>{{ trans('admin::admin.table.created') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($terms as $term)
            <tr role="row">
                @foreach($builder->elements as $element)
                    <td>
                            @if ($element->type == 'checkbox' && is_array(json_decode($term->{$element->element_name})))
                                @foreach(json_decode($term->{$element->element_name}) as $value)
                                    <p>{{ $value }}</p>
                                @endforeach
                            @elseif ($element->type == 'file')
                                @if (!is_null($term->{$element->element_name}))
                                    <a href="{!! \Illuminate\Support\Facades\Storage::disk(config('filesystems.default'))->url($term->{$element->element_name}); !!}" target="_blank">{{ basename(\Illuminate\Support\Facades\Storage::disk(config('filesystems.default'))->url($term->{$element->element_name})) }}</a>
                                @endif
                            @else
                                {{ trim($term->{$element->element_name}, '"') }}
                            @endif
                    </td>
                @endforeach
                <td style="cursor: pointer;">
                    {{ $term->created_at->diffForHumans() }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div>{{ $terms->links() }}</div>
</div>
