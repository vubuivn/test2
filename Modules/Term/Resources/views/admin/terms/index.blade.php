@extends('admin::layout')

@component('admin::components.term.header')
    @slot('title', optional($builder)->name)

    <li class="active">{{ optional($builder)->name }}</li>
@endcomponent

@section('content')

    <div class="box box-primary">

        <div class="box-body index-table">
            <a class="btn btn-default exportAll" data-id="{{$builder->id}}">Export</a>
            <div class="table-responsive">
                <table class="table table-striped table-hover dataTable no-footer" id="{{ $id ?? '' }}">
                    <thead>
                    <tr>
                        @foreach($builder->elements as $element)
                            <th>{{ $element->name }}</th>
                        @endforeach
                        <th>{{ trans('admin::admin.table.created') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($terms as $term)
                    <tr role="row">
                    @foreach($builder->elements as $element)
                        <td style="cursor: pointer;"><a href="{{ route('admin.terms.show',['id' => $term->id]) }}">
                        @if ($element->type == 'checkbox' && is_array(json_decode($term->{$element->element_name})))
                        @foreach(json_decode($term->{$element->element_name}) as $value)
                        <p>{{ $value }}</p>
                        @endforeach
                        @elseif ($element->type == 'file')
                            @if (!is_null($term->{$element->element_name}))
                                <a href="{!! \Illuminate\Support\Facades\Storage::disk(config('filesystems.default'))->url($term->{$element->element_name}); !!}" target="_blank">{{ basename(\Illuminate\Support\Facades\Storage::disk(config('filesystems.default'))->url($term->{$element->element_name})) }}</a>
                            @endif
                        @else
                        {{ trim($term->{$element->element_name}, '"') }}
                        @endif
                        </a>
                        </td>
                    @endforeach
                    <td style="cursor: pointer;">
                        {{ $term->created_at->diffForHumans() }}
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
<div>{{ $terms->links() }}</div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('loading.js') }}"></script>
    <script type="text/javascript">
        $('.exportAll').on('click',function (){
            var id = $(this).data('id');
            $('body').loading('start');
            $.ajax({
                type: 'GET',
                data: {id:id},
                url: route('admin.terms.exportAll.excel'),
                success: function (response, textStatus, request) {
                    $('body').loading('stop');
                    if (response.fileUrl) {
                        window.location.href = response.fileUrl;
                    }
                    
                    // var a = document.createElement("a");
                    // a.href = response.file;
                    // a.download = response.name;
                    // document.body.appendChild(a);
                    // a.click();
                    // a.remove();
                },
                error(xhr) {
                    $('body').loading('stop');
                    alert('System error, Please try again!.');
                },
            });
        })

    </script>
@endpush
