@foreach($page->blocks as $block)
    @php($content = show_block_page($block))
        @includeIf('block::public.template.block_'.$block->block_id, $content)
@endforeach

