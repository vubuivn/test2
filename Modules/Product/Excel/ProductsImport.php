<?php

namespace Modules\Product\Excel;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

use Modules\Attribute\Entities\Attribute;
use DB;
use Artisan;
// use Log;

class ProductsImport
{
    const MAX_INT = 99999999999;
    const CHUNK_SIZE = 1000;

    // protected $productAttr = [];
    private $dbGroupCodes = [];
    private $dbAttributeValueCodes = [];
    private $dbAttributeFields = [];
    private $dbProductSkus = [];
    private $productsCfg = [];
    private $uniqueSkus = [];

    private $productIdsToUpdate = [];
    private $newProductsToInsert = [];
    private $productSkusToInsert = [];
    private $rawProductsToImport = [];
    private $productsToUpdate = [];
    private $productAttributesToInsert = [];
    private $productAttributeValuesToInsert = [];

    private $productCreatedCount = 0;
    private $productUpdatedCount = 0;
    private $errorProducts = [];

    public function __construct()
    {
        // ability to apply Redis Cache to hold all of this data instead of database query
        $this->generateDbGroupCodes();
        $this->generateDbAttributeValueCodes();
        $this->generateDbAttributeFields();
        $this->generateDbProductSkus();
        $this->productsCfg = config('imports.products');
    }

    public function __toString()
    {
        return 'Fuck';
    }

    public function import($filePath)
    {
        $result = $this->preprocessImport($filePath);

        if (!$result['status']) {
            return [
                'status' => false,
                'message' => 'Thiếu cột: ' . implode(', ', $result['missingColumns'])
            ];
        }

        $this->doImport();
        // Log::info($this->errorProducts);
        return [
            'status' => true,
            'productCreatedCount' => $this->productCreatedCount,
            'productUpdatedCount' => $this->productUpdatedCount,
            'errorProducts' => $this->errorProducts,
            'errorProductCount' => count($this->errorProducts)
        ];
    }

    private function doImport()
    {
        DB::raw('LOCK TABLES products WRITE');
        DB::beginTransaction();

        try {
            // remove all appropriate product_attributes & product_attribute_values before updating
            foreach (array_chunk($this->productIdsToUpdate, self::CHUNK_SIZE) AS $productIds) {
                DB::table('product_attributes')->whereIn('product_id', $productIds)->delete();
                DB::table('product_attribute_values')->whereIn('product_id', $productIds)->delete();
            }

            // update exists products
            
            foreach (array_chunk($this->productsToUpdate, self::CHUNK_SIZE) AS $products) {
                updateBatch($table = 'products', $products, $index = 'id');
            }

            // import new products
            foreach (array_chunk($this->newProductsToInsert, self::CHUNK_SIZE) AS $products) {
                DB::table('products')->insert($products);
            }
            $productIdsBySkus = $this->getProductIdsBySkus($this->productSkusToInsert);
            foreach ($this->rawProductsToImport AS $row) {
                $productId = $productIdsBySkus[strtolower($row['sku'])];
                $this->prepareProductDataToImport($row, $productId);
            }

            // import product_attributes
            foreach (array_chunk($this->productAttributesToInsert, self::CHUNK_SIZE) AS $data) {
                DB::table('product_attributes')->insert($data);
            }

            // import product_attribute_values
            foreach (array_chunk($this->productAttributeValuesToInsert, self::CHUNK_SIZE) AS $data) {
                DB::table('product_attribute_values')->insert($data);
            }

            DB::table('products')->whereNotNull('special_price')->update(['selling_price'=> DB::raw('products.special_price')]);

            Artisan::call('cache:clear');

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }
    }

    private function preprocessImport($filePath)
    {
        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open($filePath);
        $reader->setShouldPreserveEmptyRows(true);
        
        foreach ($reader->getSheetIterator() as $sheet) {
            $headingKeys = [];
            // only read data from the first sheet
            if ($sheet->getIndex() === 0) { // index is 0-based
                foreach ($sheet->getRowIterator() as $rowNumber => $row) {
                    $cells = $row->getCells();
                    if ($rowNumber === 1) {
                        $heading = array_map(function($cell) {
                            return strtolower($cell->getValue());
                        }, $cells);

                        // do validation for header
                        $result = $this->validateHeading($heading);

                        if (!empty($result['missingColumns'])) {
                            return [
                                'status' => false,
                                'missingColumns' => $result['missingColumns']
                            ];
                        }
                        $headingKeys = $result['headingKeys'];
                        continue;
                    }

                    $formattedRow = $this->generateFormattedRow($headingKeys, $cells);
                    $result = $this->validateRow($formattedRow);
                    if (!$result['status']) {
                        $formattedRow['errorMessages'] = $result['messages'];
                        $this->errorProducts[] = $formattedRow;
                        continue;
                    }

                    $productId = $this->_getSku($formattedRow['sku']);
                    if ($productId) {
                        // update product
                        $this->generateProductsToUpdate($formattedRow, $productId);
                    } else {
                        // insert product
                        $this->generateProductsToInsert($formattedRow);
                    }
                }
                break; // no need to read more sheets
            }
        }

        return [
            'status' => true
        ];
    }

    private function generateFormattedRow($headingKeys, $cells)
    {

        $formattedRow = [];
        foreach ($headingKeys AS $index => $headingKey) {
            $cellValue = isset($cells[$index]) ? trim($cells[$index]->getValue()) : null;

            if($headingKey === 'width' && $cellValue){
                $cellValue = str_replace('-', ', ', $cellValue);
            }
            if($headingKey === 'height' && $cellValue){
                $cellValue = str_replace('-', ', ', $cellValue);
            }
            if($headingKey === 'length' && $cellValue){
                $cellValue = str_replace('-', ', ', $cellValue);
            }
            if ($headingKey === 'sku' && $cellValue) {
                $cellValue = preg_replace('/[^A-Za-z0-9_\-]/m', '', $cellValue);
            }

            if (in_array($headingKey, $this->dbAttributeFields) && $cellValue) {
                $cellValue = preg_replace('/[^A-Za-z0-9_,\-]/m', '', $cellValue);
            }
   
            $formattedRow[$headingKey] = $cellValue;
        }

        return $formattedRow;
    }

    private function validateHeading($heading)
    {
        $headingKeys = [];
        $missingColumns = [];

        $validHeadingFields = custom_array_merge($this->productsCfg, $this->dbAttributeFields);

        foreach ($validHeadingFields AS $key => $name) {
            $index = array_search(strtolower($name), $heading);

            if ($index === false) {
                $missingColumns[] = $name;
            } else {
                unset($heading[$index]);
                $headingKeys[$index] = $key;
            }
        }

        return [
            'missingColumns' => $missingColumns,
            'headingKeys' => $headingKeys
        ];
    }

    private function validateRow(&$row)
    {
        $requiredFields = ['sku', 'group_code', 'is_active'];

        $messages = [];
        foreach ($requiredFields AS $field) {
            if (!strlen($row[$field])) {
                $fieldName = $this->productsCfg[$field];
                $messages[] = 'Trường: ' . $fieldName . ' không thể rỗng';
            }
        }

        if (empty($messages)) {
            // check duplicate sku
            if (isset($this->uniqueSkus[strtolower($row['sku'])])) {
                $messages[] = 'Trùng lấp ' . $this->productsCfg['sku'] . ': ' . $row['sku'];
            } else {
                $this->uniqueSkus[strtolower($row['sku'])] = 1;
            }

            // check group code
            $groupId = $this->_getGroup($row['group_code']);
            if (empty($groupId)) {
                $messages[] = $this->productsCfg['group_code'] . ': ' . $row['group_code'] . ' không tồn tại';
            } else {
                $row['group_id'] = $groupId;
            }

            // check valid is_active
            $isActive = (int) $row['is_active'];
            if (!in_array($isActive, [0, 1, -1])) {
                $messages[] = $this->productsCfg['is_active'] . ' phải là 0, 1 hoặc 1';
            }
        }

        // check number field
        $decimalFields = ['price', 'seats', 'image_count']; // leave 'length', 'width', 'height', 'volume' fields
        foreach ($decimalFields AS $field) {
            $rowVal = $row[$field] ?? null;

            if (!empty($rowVal) && !is_numeric($rowVal)) {
                $messages[] = $this->productsCfg[$field] . ' phải là số';
            } else {
                $rowVal = abs($rowVal);
                if ($field !== 'price') {
                    $rowVal = (int) $rowVal;
                }

                if ($field === 'image_count' && $rowVal > 126) {
                    $messages[] = $this->productsCfg[$field] . ' vượt quá giá trị cho phép';
                } 

                if ($rowVal > self::MAX_INT) {
                    $messages[] = $this->productsCfg[$field] . ' vượt quá giá trị cho phép';
                }

                $row[$field] = $rowVal;
            }
        }

        // validate attribute & generate product_attributes field
        $result = $this->validateAttributes($row);
        if ($result['status']) {
            $row['product_attributes'] = $result['product_attributes'];
        } else {
            $messages = array_merge($messages, $result['messages']);
        }

        if (!empty($messages)) {
            return [
                'status' => false,
                'messages' => $messages
            ];
        }

        return [
            'status' => true
        ];
    }

    private function validateAttributes($row)
    {
        $status = true;
        $messages = [];
        $productAttributes = [];

        foreach ($this->dbAttributeFields AS $attrId => $name) {
            if (!empty($row[$attrId])) {

                $attributeCodes = explode(',', $row[$attrId]);
                $attributeCodes = array_unique($attributeCodes);
                foreach ($attributeCodes AS $attrCode) {
                    $attrValueId = $this->dbAttributeValueCodes[$attrId . '_' . $attrCode] ?? null;

                    if (!$attrValueId) {
                        $messages[] = $name . ': ' . $attrCode . ' không hợp lệ';
                        $status = false;
                    }
                }

                $productAttributes[] = $attrId . ':' . implode(',', $attributeCodes);
            }
        }

        if (!$status) {
            return [
                'status' => false,
                'messages' => $messages
            ];
        }

        return [
            'status' => true,
            'product_attributes' => implode(';', $productAttributes)
        ];
    }

    private function generateDbGroupCodes()
    {
        $groups = DB::select(DB::raw('SELECT code, id FROM groups WHERE deleted_at IS NULL'));
        foreach ($groups AS $group) {
            $this->dbGroupCodes[strtolower($group->code)] = $group->id;
        }
    }

    private function generateDbProductSkus()
    {
        $products = DB::select(DB::raw('SELECT sku, id FROM products WHERE deleted_at IS NULL'));
        foreach ($products AS $product) {
            $this->dbProductSkus[strtolower($product->sku)] = $product->id;
        }
    }
    
    private function generateDbAttributeValueCodes()
    {
        $attributeValues = DB::select(DB::raw('SELECT id, code, attribute_id FROM attribute_values'));
        foreach ($attributeValues AS $attr) {
            $this->dbAttributeValueCodes[$attr->attribute_id . '_' . $attr->code] = $attr->id;
        }
    }

    private function generateDbAttributeFields()
    {
        $attributes = Attribute::withoutGlobalScope('active')->get();
        foreach ($attributes AS $attr) {
            $this->dbAttributeFields[$attr->id . ''] = $attr->name;
        }
    }

    private function _getGroup($code)
    {
        return $this->dbGroupCodes[strtolower($code)] ?? null;
    }

    private function _getSku($sku)
    {
        return $this->dbProductSkus[strtolower($sku)] ?? null;
    }

    private function generateProductsToInsert(array $row)
    {
        $this->productCreatedCount++;
        $this->rawProductsToImport[] = $row;
        $this->productSkusToInsert[] = $row['sku'];
        $this->newProductsToInsert[$row['sku']] = [
            'sku' => $row['sku'],
            'wide' => $row['width'],
            'long' => $row['length'],
            'high' => $row['height'],
            'volume' => $row['volume'] ?? null,
            // 'seats' => abs((int) $row['seats']),
            'price' => $row['price'],
            'selling_price' => $row['price'],
            'group_id' => $row['group_id'],
            'product_attributes' => $row['product_attributes'],
            'is_active' => $row['is_active'],
            'image_count' => $row['image_count'],
            'out_of_stock' => $row['out_of_stock'] != '' ? $row['out_of_stock'] : 0,
        ];
    }
    
    private function generateProductsToUpdate(array $row, int $productId)
    {
        $this->productUpdatedCount++;
        $this->productsToUpdate[] = [
            'id' => $productId,
            'sku' => $row['sku'],
            'wide' => $row['width'],
            'long' => $row['length'],
            'high' => $row['height'],
            'volume' => $row['volume'] ?? null,
            // 'seats' => abs((int) $row['seats']),
            'price' => $row['price'],
            'selling_price' => $row['price'],
            'group_id' => $row['group_id'],
            'product_attributes' => $row['product_attributes'],
            'is_active' => $row['is_active'],
            'image_count' => $row['image_count'],
            'out_of_stock' => $row['out_of_stock'] != '' ? $row['out_of_stock'] : 0,
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        $this->prepareProductDataToImport($row, $productId);
        $this->productIdsToUpdate[] = $productId;
    }

    private function prepareProductDataToImport(array $row,int $productId)
    {
        foreach ($this->dbAttributeFields AS $attrId => $name) {
            if (isset($row[$attrId]) && strlen($row[$attrId])) {
                $this->productAttributesToInsert[] = [
                    'product_id' => $productId,
                    'attribute_id' => $attrId
                ];
                $attributeCodes = explode(',', $row[$attrId]);
                $attributeCodes = array_unique($attributeCodes);
                foreach ($attributeCodes AS $attrCode) {
                    $attrValueKey = $attrId . '_' . $attrCode;
                    $attrValueId = $this->dbAttributeValueCodes[$attrValueKey];
                    $this->productAttributeValuesToInsert[] = [
                        'product_id' => $productId,
                        'attribute_id' => $attrId,
                        'attribute_value_id' => $attrValueId
                    ];
                }
            }
        }
    }

    private function getProductIdsBySkus(array $skus)
    {
        $productIds = [];
        foreach (array_chunk($skus, 10 * self::CHUNK_SIZE) AS $skuChunk) {
            $items = DB::table('products')->select('id', 'sku')->whereIn('sku', $skuChunk)->get();
        
            foreach ($items AS $item) {
                $productIds[strtolower($item->sku)] = $item->id;
            }
        }

        return $productIds;
    }

}