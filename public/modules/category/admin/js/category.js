// !function(e){var t={};function r(a){if(t[a])return t[a].exports;var n=t[a]={i:a,l:!1,exports:{}};return e[a].call(n.exports,n,n.exports,r),n.l=!0,n.exports}r.m=e,r.c=t,r.d=function(e,t,a){r.o(e,t)||Object.defineProperty(e,t,{configurable:!1,enumerable:!0,get:a})},r.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return r.d(t,"a",t),t},r.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},r.p="/",r(r.s=254)}({254:function(e,t,r){e.exports=r(255)},255:function(e,t,r){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var a=function(){function e(e,t){for(var r=0;r<t.length;r++){var a=t[r];a.enumerable=a.enumerable||!1,a.configurable=!0,"value"in a&&(a.writable=!0),Object.defineProperty(e,a.key,a)}}return function(t,r,a){return r&&e(t.prototype,r),a&&e(t,a),t}}();var n=function(){function e(t,r){var a=this;!function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,e),this.selector=r,$.jstree.defaults.dnd.touch=!0,$.jstree.defaults.dnd.copy=!1,this.fetchCategoryTree(),r.on("select_node.jstree",function(e,r){return t.fetchCategory(r.selected[0])}),r.on("loaded.jstree",function(){return r.jstree("open_all")}),$(document).on("dnd_stop.vakata",function(e,t){setTimeout(function(){a.updateCategoryTree(t)},100)})}return a(e,[{key:"fetchCategoryTree",value:function(){this.selector.jstree({core:{data:{url:route("admin.categories.tree")},check_callback:!0},plugins:["dnd"]})}},{key:"updateCategoryTree",value:function(e){var t=this;this.loading(e,!0),$.ajax({type:"PUT",url:route("admin.categories.tree.update"),data:{category_tree:this.getCategoryTree()},success:function(e){function t(t){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(r){success(r),t.loading(e,!1)}),error:function(e){function t(t){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(r){error(r.statusText+": "+r.responseJSON.message),t.loading(e,!1)})})}},{key:"getCategoryTree",value:function(){return this.selector.jstree(!0).get_json("#",{flat:!0}).reduce(function(e,t){return e.concat({id:t.id,parent_id:"#"===t.parent?null:t.parent,position:t.data.position})},[])}},{key:"loading",value:function(e,t){var r=!0,a=!1,n=void 0;try{for(var o,i=e.data.obj[Symbol.iterator]();!(r=(o=i.next()).done);r=!0){var l=o.value;t?$(l).addClass("jstree-loading"):$(l).removeClass("jstree-loading")}}catch(e){a=!0,n=e}finally{try{!r&&i.return&&i.return()}finally{if(a)throw n}}}}]),e}(),o=function(){function e(e,t){for(var r=0;r<t.length;r++){var a=t[r];a.enumerable=a.enumerable||!1,a.configurable=!0,"value"in a&&(a.writable=!0),Object.defineProperty(e,a.key,a)}}return function(t,r,a){return r&&e(t.prototype,r),a&&e(t,a),t}}();new(function(){function e(){!function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,e);var t=$(".category-tree");new n(this,t),this.collapseAll(t),this.expandAll(t),this.addRootCategory(),this.addSubCategory(),$("#category-form").on("submit",this.submit)}return o(e,[{key:"collapseAll",value:function(e){$(".collapse-all").on("click",function(t){t.preventDefault(),e.jstree("close_all")})}},{key:"expandAll",value:function(e){$(".expand-all").on("click",function(t){t.preventDefault(),e.jstree("open_all")})}},{key:"addRootCategory",value:function(){var e=this;$(".add-root-category").on("click",function(){e.loading(!0),$(".add-sub-category").addClass("disabled"),$(".category-tree").jstree("deselect_all"),e.clear(),setTimeout(e.loading,150,!1)})}},{key:"addSubCategory",value:function(){var e=this;$(".add-sub-category").on("click",function(){var t=$(".category-tree").jstree("get_selected")[0];void 0!==t&&(e.clear(),e.loading(!0),window.form.appendHiddenInput("#category-form","parent_id",t),setTimeout(e.loading,150,!1))})}},{key:"fetchCategory",value:function(e){var t=this;this.loading(!0),$(".add-sub-category").removeClass("disabled"),$.ajax({type:"GET",url:route("admin.categories.show",e),success:function(e){t.update(e),t.loading(!1)},error:function(e){function t(t){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(e){error(e.statusText+": "+e.responseJSON.message),t.loading(!1)})})}},{key:"update",value:function(e){window.form.removeErrors(),$(".btn-delete").removeClass("hide"),$(".form-group .help-block").remove(),$("#confirmation-form").attr("action",route("admin.categories.destroy",e.id)),$("#name").val(e.name),$("#slug").val(e.slug),$("#description").val(e.description),$("#featured_groups").val(e.featured_groups),$("#meta_title").val(e.meta_title),null!==e.coverImage?$("#cover_image").html('<div class="image-holder">\n            <img src="'+e.coverImage.path+'">\n            <button type="button" class="btn remove-image"></button>\n            <input type="hidden" name="files[cover_image]" value="'+e.coverImage.id+'">\n            </div>'):$("#cover_image").html('<div class="image-holder placeholder">\n                <i class="fa fa-picture-o"></i>\n            </div>'),null!==e.featureImage?$("#feature_image").html('<div class="image-holder">\n            <img src="'+e.featureImage.path+'">\n            <button type="button" class="btn remove-image"></button>\n            <input type="hidden" name="files[feature_image]" value="'+e.featureImage.id+'">\n            </div>'):$("#feature_image").html('<div class="image-holder placeholder">\n                <i class="fa fa-picture-o"></i>\n            </div>'),$("#slug-field").removeClass("hide"),$(".category-details-tab .seo-tab").removeClass("hide"),$("#is_searchable").prop("checked",e.is_searchable),$("#is_active").prop("checked",e.is_active),$('#category-form input[name="parent_id"]').remove()}},{key:"clear",value:function(){$("#name").val(""),$("#slug").val(""),$("#slug-field").addClass("hide"),$(".category-details-tab .seo-tab").addClass("hide"),$("#is_searchable").prop("checked",!1),$("#is_active").prop("checked",!1),$(".btn-delete").addClass("hide"),$(".form-group .help-block").remove(),$('#category-form input[name="parent_id"]').remove(),$(".general-information-tab a").click()}},{key:"loading",value:function(e){!0===e?$(".overlay.loader").removeClass("hide"):$(".overlay.loader").addClass("hide")}},{key:"submit",value:function(e){var t=$(".category-tree").jstree("get_selected")[0];$("#slug-field").hasClass("hide")||(window.form.appendHiddenInput("#category-form","_method","put"),$("#category-form").attr("action",route("admin.categories.update",t))),e.currentTarget.submit()}}]),e}())}});

// import CategoryTree from './CategoryTree';
class CategoryTree{
    constructor(categoryForm, selector) {
        this.selector = selector;

        $.jstree.defaults.dnd.touch = true;
        $.jstree.defaults.dnd.copy = false;

        this.fetchCategoryTree();

        // On selecting a category.
        selector.on('select_node.jstree', (e, node) => categoryForm.fetchCategory(node.selected[0]));

        // Expand categories when jstree is loaded.
        selector.on('loaded.jstree', () => selector.jstree('open_all'));

        // On updating category tree.
        $(document).on('dnd_stop.vakata', (e, node) => {
            setTimeout(() => {
                this.updateCategoryTree(node);
            }, 100);
        });
    }

    fetchCategoryTree() {
        this.selector.jstree({
            core: {
                data: { url: route('admin.categories.tree') },
                check_callback: true,
            },
            plugins: ['dnd'],
        });
    }

    updateCategoryTree(node) {
        this.loading(node, true);

        $.ajax({
            type: 'PUT',
            url: route('admin.categories.tree.update'),
            data: { category_tree: this.getCategoryTree() },
            success: (message) => {
                success(message);

                this.loading(node, false);
            },
            error: (xhr) => {
                error(`${xhr.statusText}: ${xhr.responseJSON.message}`);

                this.loading(node, false);
            },
        });
    }

    getCategoryTree() {
        let categories = this.selector.jstree(true).get_json('#', { flat: true });

        return categories.reduce((formatted, category) => {
            return formatted.concat({
                id: category.id,
                parent_id: (category.parent === '#') ? null : category.parent,
                position: category.data.position,
            });
        }, []);
    }

    loading(node, state) {
        for (let nodeElement of node.data.obj) {
            if (state) {
                $(nodeElement).addClass('jstree-loading');
            } else {
                $(nodeElement).removeClass('jstree-loading');
            }
        }
    }
}
// import CategoryTree from './CategoryTree';

class Category{
    constructor() {
        let tree = $('.category-tree');
        new CategoryTree(this, tree);

        this.collapseAll(tree);
        this.expandAll(tree);
        this.addRootCategory();
        this.addSubCategory();

        $('#category-form').on('submit', this.submit);
    }

    collapseAll(tree) {
        $('.collapse-all').on('click', (e) => {
            e.preventDefault();

            tree.jstree('close_all');
        });
    }

    expandAll(tree) {
        $('.expand-all').on('click', (e) => {
            e.preventDefault();

            tree.jstree('open_all');
        });
    }

    addRootCategory() {
        $('.add-root-category').on('click', () => {
            this.loading(true);

            $('.add-sub-category').addClass('disabled');

            $('.category-tree').jstree('deselect_all');

            this.clear();

            // Intentionally delay 150ms so that user feel new form is loaded.
            setTimeout(this.loading, 150, false);
        });
    }

    addSubCategory() {
        $('.add-sub-category').on('click', () => {
            let selectedId = $('.category-tree').jstree('get_selected')[0];

            if (selectedId === undefined) {
                return;
            }

            this.clear();
            this.loading(true);

            window.form.appendHiddenInput('#category-form', 'parent_id', selectedId);

            // Intentionally delay 150ms so that user feel new form is loaded.
            setTimeout(this.loading, 150, false);
        });
    }

    fetchCategory(id) {
        this.loading(true);

        $('.add-sub-category').removeClass('disabled');

        $.ajax({
            type: 'GET',
            url: route('admin.categories.show', id),
            success: (category) => {
                this.update(category);

                this.loading(false);
            },
            error: (xhr) => {
                error(`${xhr.statusText}: ${xhr.responseJSON.message}`);

                this.loading(false);
            },
        });
    }

    update(category) {
        window.form.removeErrors();
        $('.btn-delete').removeClass('hide');
        $('.form-group .help-block').remove();

        $('#confirmation-form').attr('action', route('admin.categories.destroy', category.id));

        $('#name').val(category.name);

        $('#slug').val(category.slug);
        $('#description').val(category.description);
        $('#position').val(category.position);
        var featuredSelect = $('select[name="featured_groups[]"]');
        var pinSelect = $('select[name="pin_groups[]"]');
        // var arrSelect = category.featured_groups.split(',');
        // console.log(category.featured_groups);
        // featuredSelect.selectize('val', category.featured_groups);
        // featuredSelect.selectize()[0].selectize.clear();
  //       var selectize = featuredSelect[0].selectize;
  //       var selectize = $select[0].selectize;
		// selectize.setValue(selectize.search("My Default Value").items[0].id);

        // featuredSelect.empty();

        $.each(featuredSelect, function (i, el) {
            el.selectize.destroy({
                plugins: ['remove_button'],
                delimiter: ',',
                persist: false,
                create: function(input) {
                    return {
                        value: input,
                        text: input
                    }
                }
            });
           });
        $.each(pinSelect, function (i, el) {
            el.selectize.destroy({
			    plugins: ['remove_button'],
			    delimiter: ',',
			    persist: false,
			    create: function(input) {
			        return {
			            value: input,
			            text: input
			        }
			    }
			});
           });
        for (const key of Object.keys(category.reference_groups)) {
            featuredSelect.append(new Option(category.reference_groups[key], key));
        }

        $(pinSelect).val(category.pin_groups);
        $(pinSelect).selectize({
                plugins: ['remove_button'],
                delimiter: ',',
                persist: false,
                create: function(input) {
                    return {
                        value: input,
                        text: input
                    }
                }
            });

        $(featuredSelect).val(category.featured_groups);
        $(featuredSelect).selectize({
			    plugins: ['remove_button'],
			    delimiter: ',',
			    persist: false,
			    create: function(input) {
			        return {
			            value: input,
			            text: input
			        }
			    }
			});


        $('#meta_title').val(category.meta_title);
        if (category.coverImage !== null) {
            $('#cover_image').html('<div class="image-holder">\n' +
                '            <img src="' + category.coverImage.path + '">\n' +
                '            <button type="button" class="btn remove-image"></button>\n' +
                '            <input type="hidden" name="files[cover_image]" value="' + category.coverImage.id + '">\n' +
                '            </div>'
            )
            ;
        } else {
            $('#cover_image').html('<div class="image-holder placeholder">\n' +
                '                <i class="fa fa-picture-o"></i>\n' +
                '            </div>');
        }
        if (category.featureImage !== null) {
            $('#feature_image').html('<div class="image-holder">\n' +
                '            <img src="' + category.featureImage.path + '">\n' +
                '            <button type="button" class="btn remove-image"></button>\n' +
                '            <input type="hidden" name="files[feature_image]" value="' + category.featureImage.id + '">\n' +
                '            </div>'
            )
            ;
        } else {
            $('#feature_image').html('<div class="image-holder placeholder">\n' +
                '                <i class="fa fa-picture-o"></i>\n' +
                '            </div>');
        }

        $('#slug-field').removeClass('hide');
        $('.category-details-tab .seo-tab').removeClass('hide');

        $('#is_searchable').prop('checked', category.is_searchable);
        $('#is_active').prop('checked', category.is_active);

        $('#category-form input[name="parent_id"]').remove();
    }

    clear() {
        $('#name').val('');

        $('#slug').val('');
        $('#slug-field').addClass('hide');
        $('.category-details-tab .seo-tab').addClass('hide');

        $('#is_searchable').prop('checked', false);
        $('#is_active').prop('checked', false);

        $('.btn-delete').addClass('hide');
        $('.form-group .help-block').remove();

        $('#category-form input[name="parent_id"]').remove();

        $('.general-information-tab a').click();
    }

    loading(state) {
        if (state === true) {
            $('.overlay.loader').removeClass('hide');
        } else {
            $('.overlay.loader').addClass('hide');
        }
    }

    submit(e) {
        let selectedId = $('.category-tree').jstree('get_selected')[0];

        if (!$('#slug-field').hasClass('hide')) {
            window.form.appendHiddenInput('#category-form', '_method', 'put');

            $('#category-form').attr('action', route('admin.categories.update', selectedId));
        }

        e.currentTarget.submit();
    }
}

new Category();