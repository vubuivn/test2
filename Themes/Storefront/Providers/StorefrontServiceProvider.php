<?php

namespace Themes\Storefront\Providers;

use Illuminate\Support\Facades\View;
use Modules\Support\Traits\AddsAsset;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\LoadsConfig;
use Modules\Admin\Ui\Facades\TabManager;
use Themes\Storefront\Admin\StorefrontTabs;
use Themes\Storefront\Http\ViewComposers\LayoutComposer;
use Themes\Storefront\Http\ViewComposers\HomePageComposer;
use Themes\Storefront\Http\ViewComposers\ProductsComposer;
use Themes\Storefront\Http\ViewComposers\ProductsFilterComposer;
use Themes\Storefront\Http\ViewComposers\ProductShowComposer;

class StorefrontServiceProvider extends ServiceProvider
{
    use LoadsConfig, AddsAsset;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        TabManager::register('storefront', StorefrontTabs::class);

        View::composer('public.layout', LayoutComposer::class);
        View::composer('public.home.index', HomePageComposer::class);
        View::composer('public.products.listing.filter', ProductsFilterComposer::class);
        View::composer('public.products.index', ProductsComposer::class);
        View::composer('public.products.listing.group-list-ajax', ProductsComposer::class);
        View::composer('public.products.show', ProductShowComposer::class);

        $this->addAdminAssets('admin.storefront.settings.edit', [
            'admin.storefront.css', 'admin.media.css', 'admin.storefront.js', 'admin.media.js',
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadConfigs(['assets.php', 'permissions.php']);
    }
}
