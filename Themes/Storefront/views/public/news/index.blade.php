@extends('public.layout')

@section('title', $category->meta->meta_title != '' ? $category->meta->meta_title : $category->name)

@push('meta')
    @include('public.partials.meta',['type'=>'category'])
@endpush

@section('content')

    @include('public.partials.bannertop',['banner'=> '/assets/images/tin-tuc.jpg','title'=>trans('storefront::storefront.page.tin_tuc')])
    <main id="main">
        <div class="container">
            <section class="sec-news-list sec-tb">
                <div class="row">
                    @if(count($posts)>0)
                    @foreach($posts as $post)
                    <div class="col-md-6">
                        <div class="item" >   
                            <a class="img tRes_70" href="{{route('news.detail',$post->slug)}}">
                                <img src="{{$post->baseImage->path}}" alt="">
                            </a>
                            <div class="divtext">
                                <div class="meta">
                                    <div class="cate"><span>{{$category->name}}</span></div>
                                    <span class="date">{{date("d/m/Y", strtotime($post->created_at))}}</span>
                                </div>                  
                                <h3 class="title"><a href="{{route('news.detail',$post->slug)}}"> {{$post->name}}</a></h3>
                                <div class="desc">
                                    {{$post->short_description}}
                                </div>
                                <div class="more">
                                    <a href="{{route('news.detail',$post->slug)}}"> {{trans('storefront::storefront.news.view_more')}} <i class="icon-arrow-14"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif 
                </div>
                <div class="text-center viewmore">
                    <a href="#" class="btn btn_more">{{trans('storefront::storefront.news.view_more')}}
                    </a> 
                </div>
            </section>
        </div>
    </main>
@endsection


@push('scripts')
<style type="text/css">
    .btn_more {text-align: center}
    .btn_more img {height: 30px;
    line-height: 40px;
    display: inline;
    margin-top: 5px;}
</style>
<script type="text/javascript" src="/assets/js/isotope.pkgd.min.js"></script>
<script type="text/javascript">
    (function($){

        var $grid = $('.sec-news-list .row').isotope({
            itemSelector: '.col-md-6',
            percentPosition: true,
            masonry: {
              columnWidth: '.col-md-6'
            }
        });
        // layout Isotope after each image loads
          $grid.imagesLoaded().progress( function() {
            $grid.isotope('layout');
          });  

        $('a.btn_more').on('click',function(e){
            e.preventDefault();
            var check=false;
            var countp = $('.sec-news-list>div.row>div.col-md-6').length;
            var button = $(this);
            if(check != true){
                check = true;
                 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '{{route('news.ajax')}}',
                    data: { page: countp },
                    beforeSend: function() {
                        button.html('<img src="/assets/images/loading.gif">');
                    },
                    success: function(data){
                        if(data == 'limit'){
                            $('.viewmore').html("{{trans('storefront::storefront.news.no_more_post')}}");   
                        }else{
                            $grid.append(data).isotope('appended',data);
                            $grid.isotope('reLayout');
                            $grid.isotope('reLayout',funvtion(){
                                $grid.isotope('layout');
                            });
                            $grid.imagesLoaded().progress( function() {
                                $grid.isotope('layout');
                            });  
                            $('.sec-news-list .row').isotope('insert',data);
                            $('.sec-news-list .row').isotope('reloadItems');
                        }                      
                    },
                    complete: function(){
                        button.html("{{trans('storefront::storefront.news.view_more')}}");
                        check = false;
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    })(jQuery)
</script>
@endpush
