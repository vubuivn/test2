<?php

namespace Modules\Product\Entities;

use Modules\Support\Eloquent\Model;

class ProductQueue extends Model
{
    protected $table = 'products_queue';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sku', 'value', 'status', 'type'];

    static function pushUpdateProductActionToQueue($data)
    {
        $data = self::filterData($data);

        self::create([
            'sku' => $data['sku'],
            'value' => \json_encode($data),
            'type' => 'update'
        ]);
    }

    static function pushCreateProductActionToQueue($data)
    {
        $data = self::filterData($data);

        self::create([
            'sku' => $data['sku'],
            'value' => \json_encode($data),
            'type' => 'create'
        ]);
    }

    static function filterData($data)
    {
        return $data;

        $availableKeys = ['sku', 'wide', 'long', 'high', 'attribute', ''];

        $items = [];
        foreach ($data AS $key => $value) {
            if (in_array($key, $availableKeys)) {
                $items[$key] = $value;
            }
        }
        return $items;
    }
}
