<?php

namespace Modules\Career\Entities;

use Modules\Support\Eloquent\TranslationModel;

class CareerTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','body','request','welfare','position','time_job','note_content'];
}
