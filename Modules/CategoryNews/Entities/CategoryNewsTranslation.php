<?php

namespace Modules\CategoryNews\Entities;

use Modules\Support\Eloquent\TranslationModel;

class CategoryNewsTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];
}
