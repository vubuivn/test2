<?php

namespace Modules\Block\Http\ViewComposers;

class BlockComposer
{
    public function compose ($view)
    {
        $blockItem = [
            'input'           => trans('block::blocks.select.input'),
            'textarea'        => trans('block::blocks.select.textarea'),
            'editor'          => trans('block::blocks.select.editor'),
            'single-image'    => trans('block::blocks.select.single-image'),
            'multiple-images' => trans('block::blocks.select.multiple-images'),
            'button'          => trans('block::blocks.select.button'),
            'group'           => trans('block::blocks.select.group'),
            'repeat'          => trans('block::blocks.select.repeat'),
            'contact'         => trans('block::blocks.select.contact'),
            'multiple-images-custom' => trans('block::blocks.select.multiple-images-custom'),
        ];
        $view->with(
            'blockItem',
            $blockItem
        );
    }
}
