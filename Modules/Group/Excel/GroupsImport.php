<?php

namespace Modules\Group\Excel;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

// use Illuminate\Support\Arr;
// use Illuminate\Http\Request;
// use Modules\Product\Entities\Product;
// use Modules\Attribute\Entities\AttributeValue;
// use Modules\Attribute\Entities\ProductAttributeValue;
// use Modules\Group\Entities\Group;
use DB;
// use Log;

class GroupsImport
{
    const LOCALE_EN = 'en';
    const LOCALE_VI = 'vi_VN';
    const CHUNK_SIZE = 1000;

    private $categoryList = [];
    private $dbGroupCodes = [];
    private $uniqueCodes = [];
    private $uniqueGroupSlugs = [];
    private $groupsCfg = [];

    private $groupCreatedCount = 0;
    private $groupUpdatedCount = 0;
    private $errorGroups = [];

    private $groupCategoriesToInsert = []; // old and new rows insert to group_categories table
    private $groupTranslationToInsert = []; // old and new rows to insert to group_translations table
    private $groupIdsToUpdate = []; // used to delete all group_categories & group_translations before updating
    private $newGroupsToInsert = []; // new groups to insert to groups table
    private $groupsToUpdate = [];
    private $groupCodesToInsert = []; // hold groupCodes that will be used to insert new group later
    private $rawGroupsToImport = []; // save raw new group to process insert new groups later

    public function __construct()
    {
        $this->generateGroupCodes();
        $this->generateCategoryList();
        $this->groupsCfg = config('imports.groups');
    }

    public function import($filePath)
    {
        $result = $this->preprocessImport($filePath);

        if (!$result['status']) {
            return [
                'status' => false,
                'message' => 'Thiếu cột: ' . implode(', ', $result['missingColumns'])
            ];
        }

        $this->doImport();
        // Log::info($this->errorGroups);
        $returnData = [
            'status' => true,
            'groupCreatedCount' => $this->groupCreatedCount,
            'groupUpdatedCount' => $this->groupUpdatedCount,
            'errorGroups' => $this->errorGroups,
            'errorGroupGount' => count($this->errorGroups)
        ];

        return $returnData;
    }

    private function doImport()
    {
        DB::beginTransaction();

        try {

            // Save featured 
            $featureds = DB::table('group_categories')->select('group_id','category_id')->where('featured',1)->get();

            // remove all group_categories & group_translations before updating
            foreach (array_chunk($this->groupIdsToUpdate, self::CHUNK_SIZE) AS $groupIds) {
                DB::table('group_categories')->whereIn('group_id', $groupIds)->delete();
                DB::table('group_translations')->whereIn('group_id', $groupIds)->delete();
            }

            // update exists groups
            foreach (array_chunk($this->groupsToUpdate, self::CHUNK_SIZE) AS $groups) {
                updateBatch($table = 'groups', $groups, $index = 'id');
            }

            // import new groups
            foreach (array_chunk($this->newGroupsToInsert, self::CHUNK_SIZE) AS $groups) {
                DB::table('groups')->insert($groups);
            }
            $groupIdsByGroupCode = $this->getGroupIdsByGroupCodes($this->groupCodesToInsert);
            foreach ($this->rawGroupsToImport AS $row) {
                $groupId = $groupIdsByGroupCode[strtolower($row['code'])];
                $this->prepareGroupDataToImport($row, $groupId);
            }

            // import group_translations
            foreach (array_chunk($this->groupTranslationToInsert, self::CHUNK_SIZE) AS $data) {
                DB::table('group_translations')->insert($data);
            }

            // import group_categories
            foreach (array_chunk($this->groupCategoriesToInsert, self::CHUNK_SIZE) AS $data) {
                DB::table('group_categories')->insert($data);
            }

            //update featured agian
            foreach($featureds as $f){
                DB::table('group_categories')->where('group_id',$f->group_id)->where('category_id',$f->category_id)->update(['featured'=>1]);
            }
            

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }
        
    }

    private function preprocessImport($filePath)
    {
        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open($filePath);
        $reader->setShouldPreserveEmptyRows(true);
        
        foreach ($reader->getSheetIterator() as $sheet) {
            $headingKeys = [];
            // only read data from 3rd sheet
            if ($sheet->getIndex() === 0) { // index is 0-based
                foreach ($sheet->getRowIterator() as $rowNumber => $row) {
                    $cells = $row->getCells();
                    if ($rowNumber === 1) {
                        $heading = array_map(function($cell) {
                            return strtolower($cell->getValue());
                        }, $cells);

                        // do validation for header
                        $result = $this->validateHeading($heading);

                        if (!empty($result['missingColumns'])) {
                            return [
                                'status' => false,
                                'missingColumns' => $result['missingColumns']
                            ];
                        }
                        $headingKeys = $result['headingKeys'];
                        continue;
                    }

                    $formattedRow = $this->generateFormattedRow($headingKeys, $cells);

                    $result = $this->validateRow($formattedRow);
                    if (!$result['status']) {
                        $formattedRow['errorMessages'] = $result['messages'];
                        $this->errorGroups[] = $formattedRow;
                        continue;
                    }

                    $groupId = $this->_checkGroup($formattedRow['code']);
                    if ($groupId) {
                        // update group
                        $this->generateGroupsToUpdate($formattedRow, $groupId);
                    } else {
                        $slug = str_slug($formattedRow['en_name']);
                        $formattedRow['slug'] = $this->generateUniqueSlug($slug);
                        
                        // insert group
                        $this->generateGroupsToInsert($formattedRow);
                    }
                }
                break; // no need to read more sheets
            }
        }

        return [
            'status' => true
        ];
    }

    private function validateRow(&$row)
    {
        $requiredFields = ['code', 'vi_name', 'en_name', 'is_active'];

        $messages = [];
        foreach ($requiredFields AS $field) {
            if (empty($row[$field])) {
                $fieldName = $this->groupsCfg[$field];
                $messages[] = 'Trường: ' . $fieldName . ' không thể rỗng';
            }
        }

        if (empty($messages)) {
            if (isset($this->uniqueCodes[strtolower($row['code'])])) {
                $messages[] = 'Trùng lấp ' . $this->groupsCfg['code'] . ': ' . $row['code'];
            } else {
                $this->uniqueCodes[strtolower($row['code'])] = 1;
            }

            if (!in_array($row['is_active'], [0, 1])) {
                $messages[] = $this->groupsCfg['is_active'] . ' phải là 0 hoặc 1';
            }

            $imageCount = (int) $row['image_count'];
            if (!is_numeric($imageCount)) {
                $messages[] = $this->groupsCfg['image_count'] . ' phải là số';
            } else {
                $row['image_count'] = abs($imageCount);
            }
        }

        if ($row['categories']) {
            $categories = explode(',', $row['categories']);
            $categories = array_unique($categories);
            foreach ($categories AS $catId) {
                if (!isset($this->categoryList[(string) $catId])) {
                    $messages[] = $this->groupsCfg['categories'] . ': ' . $catId . ' không tồn tại';
                }
            }
        }

        // if ($row['collections']) {
        //     $collections = explode(',', $row['collections']);
        //     $collections = array_unique($collections);
        //     foreach ($collections AS $groupCode) {
        //         if (!isset($this->dbGroupCodes[strtolower($groupCode)])) {
        //             $messages[] = $this->groupsCfg['collections'] . ': ' . $groupCode . ' không tồn tại';
        //         }
        //     }
        // }

        if (!empty($messages)) {
            return [
                'status' => false,
                'messages' => $messages
            ];
        }

        return [
            'status' => true
        ];

    }

    private function validateHeading($heading)
    {
        $headingKeys = [];
        $missingColumns = [];
        foreach ($this->groupsCfg AS $key => $name) {
            $index = array_search(strtolower($name), $heading);

            if ($index === false) {
                $missingColumns[] = $name;
            } else {
                unset($heading[$index]);
                $headingKeys[$index] = $key;
            }
        }

        return [
            'missingColumns' => $missingColumns,
            'headingKeys' => $headingKeys
        ];
    }

    private function generateGroupsToInsert($row)
    {
        $this->groupCreatedCount++;

        $this->rawGroupsToImport[] = $row;

        $this->groupCodesToInsert[] = $row['code'];

        $this->newGroupsToInsert[$row['code']] = [
            'code' => $row['code'],
            'slug' => $row['slug'],
            'is_active' => $row['is_active'],
            'collections' => $row['collections'],
            'image_count' => $row['image_count'],
            'is_new' => $row['is_new'] != '' ? $row['is_new'] : 0,
        ];
        if(($row['new_end'] != '') && ((int)$row['new_end'] > 0)){
            $this->newGroupsToInsert[$row['code']]['new_start'] = \Carbon\Carbon::now();
            $this->newGroupsToInsert[$row['code']]['new_end'] = \Carbon\Carbon::now()->addDays($row['new_end']);
        }
    }
    
    private function generateGroupsToUpdate($row, $groupId)
    {
        $this->groupUpdatedCount++;

        $this->prepareGroupDataToUpdate($row, $groupId);

        $this->prepareGroupDataToImport($row, $groupId);

        $this->groupIdsToUpdate[] = $groupId;
    }

    private function generateFormattedRow($headingKeys, $cells)
    {
        $formattedRow = [];
        foreach ($headingKeys AS $index => $headingKey) {
            $cellValue = !empty($cells[$index]) ? trim($cells[$index]->getValue()) : null;
            if ($headingKey) {
                if ($headingKey === 'code') {
                    $cellValue = preg_replace('/[^A-Za-z0-9_\-]/m', '', $cellValue);
                }

                if ($headingKey === 'collections') {
                    $cellValue = preg_replace('/[^A-Za-z0-9_,\-]/m', '', $cellValue);
                }

                if ($headingKey === 'categories') {
                    $cellValue = preg_replace('/[^0-9,]/m', '', $cellValue);
                }
                $formattedRow[$headingKey] = $cellValue;
            }
        }

        return $formattedRow;
    }

    private function getGroupIdsByGroupCodes($groupCodes)
    {
        $groupIds = [];
        $items = DB::table('groups')->select('id', 'code')->whereIn('code', $groupCodes)->get();
        
        foreach ($items AS $item) {
            $groupIds[strtolower($item->code)] = $item->id;
        }

        return $groupIds;
    }

    private function prepareGroupDataToUpdate($row, $groupId)
    {
        $this->groupsToUpdate[] = [
            'id' => $groupId,
            'is_active' => $row['is_active'],
            'collections' => $row['collections'],
            'image_count' => $row['image_count'],
            'updated_at' => date('Y-m-d H:i:s'),
            'is_new' => $row['is_new'] != '' ? $row['is_new'] : 0,
            'new_start' => $row['new_end'] != '' ? \Carbon\Carbon::now() : NULL,
            'new_end' => $row['new_end'] != '' ? \Carbon\Carbon::now()->addDays($row['new_end']) : NULL,
        ];
    }

    private function prepareGroupDataToImport($row, $groupId)
    {
        if ($row['categories']) {
            $categories = explode(',', $row['categories']);
            $categories = array_unique($categories);
            foreach ($categories AS $catId) {
                $this->groupCategoriesToInsert[] = [
                    'category_id' => $catId,
                    'group_id' => $groupId
                ];
            }
        }

        $this->groupTranslationToInsert[] = [
            'group_id' => $groupId,
            'locale' => SELF::LOCALE_EN,
            'name' => $row['en_name'],
            'description' => $row['en_description'],
            'content_1' => $row['en_content_1'],
            'content_2' => $row['en_content_2'] ?? null
        ];

        $this->groupTranslationToInsert[] = [
            'group_id' => $groupId,
            'locale' => SELF::LOCALE_VI,
            'name' => $row['vi_name'],
            'description' => $row['vi_description'],
            'content_1' => $row['vi_content_1'],
            'content_2' => $row['vi_content_2'] ?? null
        ];
    }

    private function _checkGroup($code)
    {
        return $this->dbGroupCodes[strtolower($code)] ?? null;
    }

    private function generateGroupCodes()
    {
        $groups = DB::select(DB::raw('SELECT code, id, slug FROM groups WHERE deleted_at IS NULL'));
        // $groups = Group::select('code', 'id')->whereNull('deleted_at')->get();
        foreach ($groups AS $group) {
            $this->dbGroupCodes[strtolower($group->code)] = $group->id;
            $this->uniqueGroupSlugs[$group->slug] = 1;
        }
    }

    private function generateUniqueSlug($slug)
    {
        $newSlug = $slug;
        $i = 1;
        while (isset($this->uniqueGroupSlugs[$newSlug])) {
            $newSlug = $slug . $i;
            $i++;
        }

        $this->uniqueGroupSlugs[$newSlug] = 1;

        return $newSlug;
    }

    private function generateCategoryList()
    {
        $categories = DB::select(DB::raw('SELECT id FROM categories'));

        foreach ($categories AS $cat) {
            $this->categoryList[(string) $cat->id] = 1;
        }
    }

}