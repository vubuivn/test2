<?php

namespace Modules\Group\Providers;

use Modules\Admin\Ui\Facades\TabManager;
use Modules\Group\Admin\GroupTabs;
use Modules\Support\Traits\AddsAsset;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\LoadsConfig;

class GroupServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot ()
    {
        $this->addAdminAssets(
            'admin.groups.(create|edit)',
            [
                'admin.media.css',
                'admin.media.js',
                'admin.option.css',
                'admin.option.js',
                'admin.storefront.css',
                'admin.storefront.js',
            ]
        );
        TabManager::register(
            'groups',
            GroupTabs::class
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register ()
    {
        $this->loadConfigs(
            [
                'assets.php',
                'permissions.php'
            ]
        );
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
