<?php

namespace Modules\Product\Excel;

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
// use Box\Spout\Common\Entity\Row;

// use Illuminate\Http\Request;
use Modules\Attribute\Entities\Attribute;
use Modules\Product\Entities\Product;
use DB;
// use Log;

class ProductsExportAll
{
    private $productAttr;
    private $productFields;
    private $productConfig = [];
    private $dbMappingDifferentKeys = [
        'width' => 'wide',
        'height' => 'high',
        'length' => 'long',
        'price' => 'selling_price'
    ];

    public function __construct(string $filePath, $errorProducts = [])
    {
        $this->productAttr = Attribute::withoutGlobalScope('active')->get();
        $this->productConfig = config('imports.products');
        $this->productFields = $this->generateProductFields($this->productConfig, $this->productAttr);
        if (!empty($errorProducts)) {
            $this->productFields['errorMessages'] = 'Error message';
        }

        $writer = WriterEntityFactory::createXLSXWriter();

        $writer->openToFile($filePath); // write data to a file or to a PHP stream
        // $writer->openToBrowser('export.xlsx');

        // create heading row
        $headings = $this->headings();
        $headingsRow = WriterEntityFactory::createRowFromArray($headings);
        $writer->addRow($headingsRow);

        // create body rows
        if (empty($errorProducts)) {
            $this->body($writer);
        } else {
            $this->bodyError($writer, $errorProducts);
        }

        // finish and close the file
        $writer->close();
    }

    private function body($writer)
    {
        $attributes = $this->productAttr;

        $items = $this->buildGroupQueryResult();

        foreach ($items as $item) {
            $row = [];
            // dd($item);
            foreach ($this->productConfig AS $key => $name) {
                $mappingKey = $this->dbMappingDifferentKeys[$key] ?? $key;
                
                if($mappingKey == 'long' || $mappingKey == 'wide' || $mappingKey == 'high')
                {
                    // dd($item->{$mappingKey});
                    $row[] = str_replace(',','-',str_replace(' ','',$item->{$mappingKey}));
                }else{
                    $row[] = $item->{$mappingKey};
                }
            }

            $productAttributes = $this->parseProductAttribute($item->product_attributes);

            foreach ($attributes AS $attr) {
                $row[] = $productAttributes[(string) $attr->id] ?? '';
            }

            $bodyRow = WriterEntityFactory::createRowFromArray($row);
            $writer->addRow($bodyRow);
        }
    }

    private function bodyError($writer, $errorProducts)
    {
        foreach ($errorProducts as $item) {
            $formattedRow = [];
            foreach ($this->productFields AS $key => $val) {
                $productVal = $item[$key] ?? '';
                if (is_array($productVal)) {
                    $productVal = implode(';', $productVal);
                }
                $formattedRow[] = $productVal;
            }

            $bodyRow = WriterEntityFactory::createRowFromArray($formattedRow);
            $writer->addRow($bodyRow);
        }
    }

    private function headings(): array
    {
        return array_values($this->productFields);
        // $heading = [
        //     'Mã sản phẩm (SKU)',
        //     'Mã nhóm',
        //     'Dài (cm)',
        //     'Rộng (cm)',
        //     'Cao (cm)',
        //     'Thể tích',
        //     'Feature',
        //     'Giá',
        //     'Số chỗ ngồi',
        // ];
    }

    private function generateProductFields($productFields, $productAttr)
    {
        foreach ($productAttr as $attr) {
            $productFields[(string) $attr->id] = $attr->name;
        }

        return $productFields;
    }

    private function buildGroupQueryResult()
    {
        $query = 'SELECT p.id, p.long, p.high, p.wide, p.volume, p.seats, p.is_active, p.selling_price, p.sku, product_attributes, p.image_count, p.out_of_stock, 
                        groups.code AS group_code
                    FROM products AS p
                    INNER JOIN groups ON groups.id=p.group_id';

        return DB::select(DB::raw($query));
    }

    private function parseProductAttribute($attributeStr)
    {
        $productAttr = [];

        if ($attributeStr) {
            $attributes = explode(';', $attributeStr);
            foreach ($attributes AS $attribute) {
                $arr = explode(':', $attribute);
                $productAttr[$arr[0]] = $arr[1];
            }
        }

        return $productAttr;
    }
}