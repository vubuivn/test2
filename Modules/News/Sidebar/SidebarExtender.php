<?php

namespace Modules\News\Sidebar;

use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{

    /**
     * @param \Maatwebsite\Sidebar\Menu $menu
     * @return \Maatwebsite\Sidebar\Menu
     */
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.content'), function (Group $group) {
            $group->item(trans('news::news.news'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(0);
                $item->authorize(
                     /* append */
                );

                $item->item(trans('news::news.news'), function (Item $item) {
                    $item->weight(5);
                    $item->route('admin.news.index');
                    $item->authorize(
                        $this->auth->hasAccess('admin.news.index')
                    );
                });
            });
        });

        return $menu;
    }
}
