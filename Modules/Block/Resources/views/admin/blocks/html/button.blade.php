<div class="accordion-box-content form-group ">
    <div class="tab-content clearfix">
        <div class="banner-image-wrapper">
            <div class="single-banner">
                <div class="banner-header">
                    <h5>{{$block->title}}</h5>
                </div>
                <div class="banner-template">
                    <div class="banner-body">
                        <div class="col-lg-6 col-md-12 col-sm-6 clearfix">
                            <div class="form-group">
                                <label>{{ trans('slider::attributes.call_to_action_text') }}</label>
                                <input type="text"
                                       name="blocks[{{$number}}][{{$block->key}}][call_to_action_text]"
                                       value="{{$data->{$block->key}->call_to_action_text ?? '' }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-6 clearfix">
                            <div class="form-group">
                                <label>{{ trans('slider::attributes.call_to_action_url') }}</label>
                                <input type="text"
                                       name="blocks[{{$number}}][{{$block->key}}][call_to_action_url]"
                                       value="{{$data->{$block->key}->call_to_action_url ?? '' }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
