<script>
    (function($) {
        window.ATTRIBUTE_ORDERS = {!! json_encode($attributeOrders) !!};
        window.PRODUCTS = {!! json_encode($products) !!};
        window.DEFAULT_PRODUCT = {!! json_encode($defaultProduct) !!};
        window.PRODUCT_ATTRIBUTE_RELATIONSHIPS = {!! json_encode($productAttributeRelationships) !!};
        window.ATTRIBUTES = {!! json_encode($attributes) !!};

        window.ASSOC_ATTRIBUTES = {!! json_encode($assocAttributes) !!};
        window.GROUP_ATTRIBUTES = {!! json_encode($groupAttributes) !!};
        window.GROUP_ATTRIBUTES_KEYS = Object.keys(DEFAULT_PRODUCT.attribute_details);

        (function(){
            // clean up unused attributes after window fnishes loading
            $.each(ATTRIBUTE_ORDERS, function(index, attrId) {
                if (index === 0) {
                    // first dropdown
                    var attrCodes = Object.keys(PRODUCT_ATTRIBUTE_RELATIONSHIPS).map(function(item) {
                        return item.split('_')[0];
                    });
                    attrCodes = $.uniqueSort(attrCodes);
                    cleanUpTheDropdown(attrId, attrCodes);
                } else {
                    var attrStr = getAttributeStringUntilCurrentDropdown(ATTRIBUTE_ORDERS.slice(0, index));
                    var attrCodes = getAttributeCodeOfNextDropdown(attrStr, PRODUCT_ATTRIBUTE_RELATIONSHIPS);
                    cleanUpTheDropdown(attrId, attrCodes);
                }
            });

            function cleanUpTheDropdown(attrId, attrCodes) {
                $('select[data-id=' + attrId + '] option').each(function() {
                    if (attrCodes.indexOf($(this).val()) === -1 ) {
                        $(this).remove();
                    }
                });
            }
        })();

        function formatNumber(num) {
	        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
	      }

        $(document).on('change','select.attributes', function() {
            var attrId = $(this).data('id');
            var nextVals = [];
            var selected = [];
            var productValue = null;
            $('select.attributes').each(function() {
                if ((currentAttrId = $(this).data('id')) <= attrId) {
                    selected.push($(this).val());
                    return;
                }
                $(this).empty();

                productValue = getNextAttributeDropdownOptions(currentAttrId, selected);
                nextVals = Object.keys(productValue);
                for (var i = 0; i < nextVals.length; i++) {
                    code = nextVals[i];
                    $(this).append(new Option(ASSOC_ATTRIBUTES[currentAttrId]['values'][code], code));  
                }

                $(this).prop('selectedIndex',0);
                selected.push($(this).val());
                // product id
                productValue = productValue[$(this).val()];
                $(this).selectpicker('refresh');
            });

            // Update product here
            if (!productValue) {
                productValue = getNextAttributeDropdownOptions($(this).data('id'), selected);
            }
            var product = PRODUCTS[productValue];

            //Change image prduct
            $('.images').html('');
            $('.images').append('<div class="owl-carousel s-dots s-auto_ s-nav"></div>');

            for(var i = 1;i <= product.image_count;i++){
                var pathImage = '{{url("/")}}/storage/files/products/'+product.sku+'-NB00'+i+'.png';
                $('.images .owl-carousel').append(imageBuid(pathImage));
            }

            // price
            if(product.normal_price != product.price){
                $('.price-info').html('<div class="price col-xs-6">'+formatNumber(parseInt(product.price))+ ' {{currency() == "VND" ? "₫" : currency() }}</div><div class ="price-cost col-xs-6">'+formatNumber(parseInt(product.normal_price))+ ' {{currency() == "VND" ? "₫" : currency() }}</div>');
            }else{
                $('.price-info').html('<div class="price col-xs-6">'+formatNumber(parseInt(product.price))+ ' {{currency() == "VND" ? "₫" : currency() }}</div>');
            }

            //out of stock
            if(product.out_of_stock == 1){
                if(!$('.out_of_stock_button').length){
                    $('.price-info').after('<div class="btn btn-default out_of_stock_button">{{trans('storefront::storefront.out_of_stock')}}</div>');
                }
            	$('.detail_add_to_cart_2 ').addClass('out_of_stock');
            }else{
            	if($('.out_of_stock_button').length>0){
            		$('.out_of_stock_button').remove();
            	}
            	$('.detail_add_to_cart_2 ').removeClass('out_of_stock');
            }
            
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                items:1,
                navText: ["<i class='icon-arrow-1'></i>","<i class='icon-arrow-2'></i>"],
            });
            
            if (product) {
                updateProduct(product);
            }
        });

        function imageBuid($path){
            return `
            <div class="item">
                <div class="tRes_60 img">
                    <img class="owl-lazy" src="`+$path+`" alt="">
                </div>
            </div> 
            `;
        }

        function getNextAttributeDropdownOptions(currentAttrId, selected)
        {
            attrIndex = GROUP_ATTRIBUTES_KEYS.indexOf(currentAttrId.toString());
            var groupAttributes = GROUP_ATTRIBUTES;

            var selectedSize = selected.length;
            for(var i = 0; i < selectedSize; i++) {
                groupAttributes = groupAttributes[selected[i]];
            }

            for(var i = selectedSize; i < attrIndex; i++) {
                var first = Object.keys(groupAttributes)[0];
                groupAttributes = groupAttributes[first];
            }
            return groupAttributes;
        }


        function loadTheDropdown() {
            
        }

        function getAttributeStringUntilCurrentDropdown(arr) {
            var attrCodes = [];
            $.each(arr, function(index, attrId) {
                var attrCode = $('select[data-id=' + attrId + ']').val();
                attrCodes.push(attrCode);
            });

            return attrCodes.join('_');
        }

        function getAttributeCodeOfNextDropdown(attrStr, productAttributeRelationships) {
            productAttributeRelationships = Object.keys(productAttributeRelationships);
            attrStr += '_'; 
            var items = productAttributeRelationships.filter(function(item) {
                if (item.indexOf(attrStr) === 0) {
                    return true;
                }

                return false;
            });

            return items.map(function(item) {
                var str = item.replace(attrStr, '');
                
                return str.split('_')[0];
            });
        }

        function disableNextDropdowns(index, attributeOrders) {
            var nextAttributes = attributeOrders.slice(index);
            $.each(nextAttributes, function(index, attrId) {
                $('select[data-id=' + attrId + ']').val('').prop('disabled', true).selectpicker('refresh');
            });
        }

        function loadNextDropdown(attrId, attrCodes) {
            if (attrId) {
                $('select[data-id=' + attrId + ']').prop('disabled', false);

                var options = [];
                $.each(attrCodes, function(index, code) {
                    $.each(ATTRIBUTES[attrId].values, function(index, obj) {

                        if (attrCodes.indexOf(obj.value) > -1) {
                            options.push('<option value="' + obj.value + '">' + obj.text + '</option>');
                        }
                    });
                });
                if (options.length === 1) {
                    options.unshift('<option hidden></option>');
                }
                
                $('select[data-id=' + attrId + ']').html(options.join('')).selectpicker('refresh');
            }
        }

        function updateProduct(product) {
            $('.u-product-height').text(product.height);
            $('.u-product-width').text(product.width);
            $('.u-product-length').text(product.length);
            $('.u-product-volume').text(product.volume);
            $('.u-product-price').text(product.price);
            $('.u-product-sku').text(product.sku);

            $('select.attributes').each(function() {
                $('.u-attribute-' + $(this).data('id')).text($(this).find('option:selected').text());
            });

            $('#product_id').val(product.id);
            $('#detail_wishlist').attr('data-id', product.id);
            if (product.wishlist) {
                $('#detail_wishlist').addClass('btn3');
            } else {
                $('#detail_wishlist').removeClass('btn3');
            }

            // change url without reloading page
            (function(){
                var title = $('title').text().trim();
                var urls = window.location.href.split('=');
                urls[1] = product.sku;
                history.pushState({}, title, urls.join('='));
            })();
        }

        $('.minus a').on('click', function(e) {
            e.preventDefault();

            var numb = parseInt($('.numb').val());
            var newVal = numb > 1 ? --numb : 1;
            $('.numb').val(newVal);
        });
        $('.plus a').on('click', function(e) {
            e.preventDefault();
            
            var numb = parseInt($('.numb').val());
            $('.numb').val(++numb);
        });
    })(jQuery);
</script>
