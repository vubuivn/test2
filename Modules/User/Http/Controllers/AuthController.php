<?php

namespace Modules\User\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Modules\Page\Entities\Page;
use Modules\User\Entities\User;
use Laravel\Socialite\Facades\Socialite;
use Modules\User\Http\Requests\LoginRequest;


class AuthController extends BaseAuthController
{
    /**
     * Where to redirect users after login..
     *
     * @return string
     */
    protected function redirectTo()
    {
        return route('account.profile.edit');
    }

    /**
     * The login URL.
     *
     * @return string
     */
    protected function loginUrl()
    {
        return route('login');
    }

    /**
     * Show login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        $banner = [];
        $banner['title_banner'] = 'Đăng nhập'; 
        return view('public.auth.login',compact('banner'));
    }

    /**
     * Redirect the user to the given provider authentication page.
     *
     * @param string $provider
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        if (! in_array($provider, app('enabled_social_login_providers'))) {
            abort(404);
        }

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from the given provider.
     *
     * @param string $provider
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        if (! in_array($provider, app('enabled_social_login_providers'))) {
            abort(404);
        }

        try {
            $user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return redirect()->route('login');
        }

        if (User::registered($user->getEmail())) {
            auth()->login(
                User::findByEmail($user->getEmail())
            );

            return redirect($this->redirectTo());
        }

        [$firstName, $lastName] = $this->extractName($user->getName());

        $registeredUser = $this->auth->registerAndActivate([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $user->getEmail(),
            'password' => str_random(),
        ]);

        $this->assignCustomerRole($registeredUser);

        auth()->login($registeredUser);

        return redirect($this->redirectTo());
    }

    private function extractName($name)
    {
        return explode(' ', $name, 2);
    }

    /**
     * Show registrations form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        $privacyPageURL = Page::urlForPage(setting('storefront_privacy_page'));
        $cities = \DB::table('cities')->get();
        return view('public.auth.register', compact('privacyPageURL','cities'));
    }

    /**
     * Show reset password form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getReset()
    {
        return view('public.auth.reset.begin');
    }

    /**
     * Reset complete form route.
     *
     * @param \Modules\User\Entities\User $user
     * @param string $code
     * @return string
     */
    protected function resetCompleteRoute($user, $code)
    {
        return route('reset.complete', [$user->email, $code]);
    }

    /**
     * Password reset complete view.
     *
     * @return string
     */
    protected function resetCompleteView()
    {
        return view('public.auth.reset.complete');
    }

    public function ajaxLogin(LoginRequest $request)
    {
        try {
            $loggedIn = $this->auth->login([
                'email' => $request->email,
                'password' => $request->password,
            ], (bool) $request->get('remember_me', false));

            if (! $loggedIn) {
                return response()->json([
                    'status' => false,
                    'message' => trans('user::messages.users.invalid_credentials')
                ]);
            }

            return response()->json(['status' => true]);

        } catch (NotActivatedException $e) {
            
            return response()->json([
                'status' => false,
                'message' => trans('user::messages.users.account_not_activated')
            ]);
        } catch (ThrottlingException $e) {
            
            return response()->json([
                'status' => false,
                'message' => trans('user::messages.users.account_is_blocked', ['delay' => intl_number($e->getDelay())])
            ]);
        }
    }

    public function postRegisterAjax(Request $request)
    {  
        try{
            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed|min:6',
                'phone' => ['required', 'digits_between:10,11'],
                'agree' => 'accepted',
                'address' => 'required',
            ];

            $messages = [
                'name.required' => trans('storefront::account.messages.name_required'),
                'phone.required' => trans('storefront::account.messages.phone_required'),
                'email.unique' => trans('storefront::account.messages.email_unique'),
                'email.email' => trans('storefront::account.messages.email_email'),
                'email.exists' => trans('storefront::account.messages.email_exists'),
                'agree.accepted' => trans('storefront::account.messages.privacy_policy_accepted'),
                'password.required' => trans('storefront::account.messages.password_required'),
                'password.confirmed' => trans('storefront::account.messages.password_confirmed'),
                'address.required' => trans('storefront::account.messages.address_required'),
                'phone.digits_between' => trans('storefront::account.messages.phone_between_10_11')
            ];
            $validator = \Validator::make($request->all(),$rules,$messages);
            $return = [];
            if($validator->fails()){
                $htmlerror = '';
                $errors = $validator->errors();
                $return['phone'] = '';
                $return['email'] = '';
                $return['name'] = '';
                $return['password'] = '';
                if($errors->has('phone')){
                    $return['phone'] = 'phone';
                }
                if($errors->has('email')){
                    $return['email'] = 'email';
                }
                if($errors->has('name')){
                    $return['name'] = 'name';
                }
                if($errors->has('password')){
                    $return['password'] = 'password';
                }
                if($errors->has('address')){
                    $return['address'] = 'address';
                }
                foreach ($errors->all() as $message) {
                    $htmlerror .= '<p class="error">'.$message.'</p>';
                }
                $return['status'] = 'error';
                $return['text']=$htmlerror;
                return $return;
            }
            $dataregis = $request->only([
                'email',
                'password',
                'birthday',
                'gender',
                'phone',
                'address'
            ]);
            $dataregis['first_name'] = $request->name;
            $dataregis['last_name'] = $request->name;
            if($request->has('day') && $request->has('month') && $request->has('year')){
                $day = $request->day;
                $month = $request->month;
                $year = $request->year;
                $dataregis['birthday'] = $year.'-'.$month.'-'.$day;
            }
            $dataregis['gender'] = (int)$dataregis['gender'];
            // var_dump($dataregis);die;
            $user = $this->auth->registerAndActivate($dataregis);
        
            // $user = User::where('id',$userid)->first();
            $this->assignCustomerRole($user);
            $return['status'] = 'success';
            $return['text']='<p class="success">'.trans('user::messages.users.account_created').'</p>';
            
            return $return;
        }catch(\Exception $e){
            return $e->getMessage();
        }
    }
}
