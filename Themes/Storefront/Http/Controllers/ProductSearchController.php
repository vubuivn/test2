<?php

namespace Themes\Storefront\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Product\Entities\Product;
use Modules\Category\Entities\Category;

class ProductSearchController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $request = \Request();
        if($request->has('name') && $request->name !=""){
            $keyword = $request->name;
            $groupList = buildFromToPriceQueryBuilder()
                ->leftjoin('group_translations','groups.id','=','group_translations.group_id')
                // ->where('group_translations.name','like','%'.$request->name.'%')
                ->where(function($q) use ($keyword) {
                    $q->where('group_translations.name','like','%'.$keyword.'%')
                    ->orWhere('group_translations.name','like','%'.$keyword)
                    // ->orWhere('group_translations.short_description','like',$keyword.'%')
                    // ->orWhere('group_translations.short_description','like','%'.$keyword)
                    ->orWhere('group_translations.short_description','like','%'.$keyword.'%')
                    // ->orWhere('group_translations.content_1','like','%'.$keyword.'%')
                    ->orWhere('group_translations.name','like',$keyword.'%')
                    ->orWhere('groups.code','like','%'.$keyword.'%')
                    ->orWhereHas('products',function($q) use($keyword){
                        $q->where('products.sku','like','%'.$keyword);
                    })
                    ->orWhereHas('products',function($q) use($keyword){
                        $q->where('products.sku','like','%'.$keyword.'%');
                    });
                })->where('locale',locale())->get();


            // if(count($groupList)<=0){

                $cats = Category::leftJoin('category_translations','categories.id','=','category_translations.category_id')->where(function($query) use($keyword){
                    $query->where('category_translations.name','like','%'.$keyword.'%')
                        ->orWhere('category_translations.name','like',$keyword.'%')
                        ->orWhere('category_translations.name','like','%'.$keyword);
                    })
                ->where('category_translations.locale',locale())->get(['categories.id']);
                if($cats){
                    $cats = $cats->toArray();
                    $groupList2 = buildFromToPriceQueryBuilder()->leftJoin('group_categories','groups.id','=','group_categories.group_id')->whereIn('group_categories.category_id',$cats)->get();
                }
                // buildFromToPriceQueryBuilder()->leftJoin('group_categories',)
                // dd($groupList);
            // }
            return view('public.products.search',compact('groupList','groupList2'));
        }else{
            return abort(404);
        }
    }
}
