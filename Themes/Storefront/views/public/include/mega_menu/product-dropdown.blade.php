<ul>
@foreach ($subMenus as $subMenu)
    <li class="active">
        <span class="item"
                data-categoryid ="{{$subMenu->category()}}"
                data-link="{{ $subMenu->url() }}"
              >
            <a href="{{ $subMenu->url() }}">{{ $subMenu->name() }}</a>
        </span>
        @if ($subMenu->hasItems())
            @include('public.include.mega_menu.product-dropdown', ['subMenus' => $subMenu->items()])
        @endif
    </li>
@endforeach
@if($level == 'level2')
    <li><span class="item"><a href="{{$sublink}}">{{ trans('group::group-categories.show_all_products') }} <i class="icon-arrow-14"></i></a></span></li>
@endif

</ul>