<?php

use Faker\Generator as Faker;
use Modules\Element\Entities\Element;
use Modules\Element\Entities\ElementValue;

$factory->define(ElementValue::class, function (Faker $faker) {
    return [
        'element_id' => function () {
            return factory(Element::class)->create()->id;
        },
        'name' => $faker->word(),
        'price' => $faker->randomNumber(),
        'price_type' => $faker->randomElement(['fixed', 'percent']),
    ];
});

$factory->define(Element::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'type' => $faker->randomElement(['field', 'dropdown']),
        'is_required' => $faker->boolean(),
    ];
});
