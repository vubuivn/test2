@extends('public.layout')

@section('title', 'Career')

@section('content')
    <div class="career-wrapper clearfix">
    @include('public.partials.bannertop',['banner'=> '/assets/images/tuyen-dung.jpg','title'=>'Tuyển dụng'])
        <main id="main">
            <div class="container">
        <section class="career sec-b">
            <div class="max670">
                <h2 class="mb-20">{{$career->name}}</h2>
                <div class="row divcareer">
                    <div class="col-md-5">
                        <div class="title-career">
                            <p>{{trans('storefront::storefront.career.position')}} :&nbsp</p>
                            <p class="w6"> {{$career->name}}</p>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="title-career">
                            <p>{{trans('storefront::storefront.career.recruitment_classification')}} :&nbsp</p>
                            <p class="w6">{{$career->time_job}}</p>
                        </div>
                        <div class="title-career">
                            <p>{{trans('storefront::storefront.career.work_location')}} :&nbsp</p>
                            <?php 
                                $arr = explode('[:en]',$city->title);
                                $vi = substr($arr[0],5);
                                $en= substr($arr[1],-3);
                            ?>
                            <p class="w6">{{ locale() == 'en' ? $en : $vi}}</p>
                        </div>
                    </div>
                </div>
                <div class="container-career">
                    <div class="job-description">
                        <h5 class="w6">Mô tả công việc</h5>
                        {!!$career->body!!}

                    </div>
                    <div class="job-description">
                        <h5 class="w6">Yêu cầu</h5>
                        {!!$career->request!!}

                    </div>
                    <div class="job-description">
                        <h5 class="w6">Phúc lợi</h5>
                        {!!$career->welfare!!}
   
                    </div>
                    <div class="title-career">
                        <p class="w6 cv">Hạn nộp hồ sơ :&nbsp</p>
                        <p class="w6 cv">20/10/2019</p>
                    </div>
                    <div class="title-career">
                        {!!$career->note_content!!}
                    </div>
                </div>
            </div>
        </section>
    </div>
        </main>
    </div>
@endsection

@push('styles')
    <link rel='stylesheet' href="/assets/css/custom.css" type='text/css'/>
@endpush