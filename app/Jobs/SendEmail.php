<?php

namespace FleetCart\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\MailTemplate\Entities\MailTemplate;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $details;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::send(
                [],
                [],
                function ($message) {
                    $data = [
                        'username' => 'asd'
                    ];
                    $emailTemplate = MailTemplate::first();

                    $message->to('info@mangoads.vn')
                            ->subject('Test email template')
                            ->setBody($emailTemplate->parse($data), 'text/html');
                }
            );
        }catch (\Exception $e){
            Log::error('send mail : '.$e->getMessage());
            Log::error('send mail : '.json_encode(app('config')->get('mail')));
            dd($e->getMessage());
        }
    }
}
