<?php

namespace Modules\MailTemplate\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;
use Modules\Tax\Entities\TaxClass;
use Modules\Category\Entities\Category;

class MailTemplateTabs extends Tabs
{
    public function make()
    {
        $this->group('basic_information', trans('mailtemplate::mail_templates.tabs.mail_template.basic_information'))
            ->active()
            ->add($this->general());
    }

    private function general()
    {
        return tap(new Tab('general', trans('mailtemplate::mail_templates.tabs.general')), function (Tab $tab) {
            $tab->active();
            $tab->weight(5);
            $tab->fields(['name', 'description', 'is_active']);
            $tab->view('mailtemplate::admin.mail_templates.tabs.general');
        });
    }

}
