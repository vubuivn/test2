@extends('admin::layout')

@component('admin::components.builder.header')
    @slot('title', trans('builder::builders.embedded', ['resource' => trans('builder::builders.builder')]))

    <li><a href="{{ route('admin.builders.index') }}">{{ trans('builder::builders.builders') }}</a></li>
    <li class="active">{{ trans('builder::builders.embedded', ['resource' => trans('builder::builders.builder')]) }}</li>
@endcomponent

@section('content')
    <div class="box box-primary">
        <pre>
            <code>

                {{ $embedded }}

            </code>
        </pre>
        Shortcut @@mgform({{$builder->id}})
    </div>
@endsection
@push('styles')
    <style>
        pre, code {
            font-family: monospace, monospace;
        }
        pre {
            overflow: auto;
        }
        pre > code {
            display: block;
            padding: 1rem;
            word-wrap: normal;
        }
    </style>
@endpush