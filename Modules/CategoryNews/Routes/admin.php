<?php

Route::get('category-news/tree', [
    'as' => 'admin.category_news.tree',
    'uses' => 'CategoryNewsTreeController@index',
    'middleware' => 'can:admin.category_news.index',
]);

Route::put('category-news/tree', [
    'as' => 'admin.category_news.tree.update',
    'uses' => 'CategoryNewsTreeController@update',
    'middleware' => 'can:admin.category_news.edit',
]);

Route::get('category-news', [
    'as' => 'admin.category_news.index',
    'uses' => 'CategoryNewsController@index',
    'middleware' => 'can:admin.category_news.index',
]);
Route::get('category-news/excel', [
    'as' => 'admin.category_news.export.excel',
    'uses' => 'CategoryNewsController@excel',
    'middleware' => 'can:admin.category_news.index',
]);

Route::post('category-news', [
    'as' => 'admin.category_news.store',
    'uses' => 'CategoryNewsController@store',
    'middleware' => 'can:admin.category_news.create',
]);

Route::get('category-news/{id}', [
    'as' => 'admin.category_news.show',
    'uses' => 'CategoryNewsController@show',
    'middleware' => 'can:admin.category_news.edit',
]);

Route::put('category-news/{id}', [
    'as' => 'admin.category_news.update',
    'uses' => 'CategoryNewsController@update',
    'middleware' => 'can:admin.category_news.edit',
]);

Route::delete('category-news/{id}', [
    'as' => 'admin.category_news.destroy',
    'uses' => 'CategoryNewsController@destroy',
    'middleware' => 'can:admin.category_news.destroy',
]);
