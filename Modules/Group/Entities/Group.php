<?php

namespace Modules\Group\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Category\Entities\Category;
use Modules\Filter\Eloquent\HasFilter;
use Modules\Group\Admin\GroupTable;
use Modules\Media\Eloquent\HasMedia;
use Modules\Product\Entities\Product;
use Modules\Support\Eloquent\Model;
use Modules\Support\Eloquent\Sluggable;
use Modules\Support\Eloquent\Translatable;
use Modules\Support\Search\Searchable;
use Illuminate\Support\Facades\Cache;

class Group extends Model
{
    use Translatable, Searchable, Sluggable, HasMedia, SoftDeletes;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'code',
        'is_active',
        'collections',
        'is_new',
        'new_start',
        'new_end',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'description',
        'short_description',
        'content_1',
        'content_2',
    ];

    /**
     * The attribute that will be slugged.
     *
     * @var string
     */
    protected $slugAttribute = 'name';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot ()
    {
        parent::boot();

        static::saved(
            function ($group) {
                $group->saveRelations(request()->all());
            }
        );

    }

    /**
     * Save associated relations for the group.
     *
     * @param array $attributes
     *
     * @return void
     */
    public function saveRelations ($attributes = [])
    {
        $this->categories()
            ->sync(
                array_get(
                    $attributes,
                    'categories',
                    []
                )
            );
    }

    /**
     * Get table data for the resource
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function table ($request)
    {
        $query = $this->newQuery()
            ->withoutGlobalScope('active')
            ->addSelect(
                [
                    'id',
                    'is_active',
                    'created_at',
                    'code',
                    'slug'
                ]
            )
            ->when(
                $request->has('except'),
                function ($query) use ($request) {
                    $query->whereNotIn(
                        'id',
                        explode(
                            ',',
                            $request->except
                        )
                    );
                }
            )
            ->when(
                $request->get('category') != null,
                function ($query) use ($request) {
                    $query->whereHas(
                        'categories',
                        function ($q) use ($request) {
                            return $q->where(
                                'category_id',
                                $request->get('category')
                            );
                        }
                    );
                }
            );

        return new GroupTable($query);
    }

    public function categories ()
    {
        return $this->belongsToMany(
            Category::class,
            'group_categories'
        );
    }

    public function products ()
    {
        return $this->hasMany(
            Product::class,
            'group_id',
            'id'
        );
    }

    public static function findBySlug ($slug)
    {
        if(Cache::tags(['groups'])->has('group_'.$slug)){
            return Cache::tags(['groups'])->get('group_'.$slug);
        }else{
            return Cache::tags(['groups'])->rememberForever('group_'.$slug,function () use($slug){
                    return static::where('slug',$slug)->firstOrFail();
            });
        }
        // return static::where(
        //     'slug',
        //     $slug
        // )->firstOrFail();
    }

    public static function list ()
    {
        return static::orderByRaw('id DESC')
            ->get();
    }

    /**
     * Get the indexable data array for the group.
     *
     * @return array
     */
    public function toSearchableArray ()
    {
        // MySQL Full-Text search handles indexing automatically.
        if (config('scout.driver') === 'mysql') {
            return [];
        }

        $translations = $this->translations()
            ->withoutGlobalScope('locale')
            ->get(
                [
                    'name',
                    'description',
                    'short_description'
                ]
            );

        return [
            'id' => $this->id,
            'translations' => $translations
        ];
    }

    public function searchTable ()
    {
        return 'group_translations';
    }

    public function searchKey ()
    {
        return 'group_id';
    }

    public function searchColumns ()
    {
        return ['name'];
    }


    public function saveBlocks($blocks = [])
    {
        $ids = $this->getDeleteCandidates($blocks);

        if ($ids->isNotEmpty()) {
            $this->blocks()->whereIn('id', $ids)->delete();
        }

        foreach (array_reset_index($blocks) as $index => $value) {
            $this->blocks()->updateOrCreate(
                ['id' => $value['id'] ?? null],
                $value + ['position' => $index]
            );
        }
    }
    private function getDeleteCandidates($values = [])
    {
        return $this->blocks()
            ->pluck('id')
            ->diff(array_pluck($values, 'id'));
    }
    public function blocks(){
        return $this->hasMany(GroupBlock::class)->orderBy('position');
    }
}
