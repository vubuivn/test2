{{ Form::text('code', trans('group::attributes.code'), $errors, $group , ['labelCol' => 2, 'required' => true]) }}

@push('scripts')
    <script>
        @if($errors->has('code') )
        $("#basic_information .accordion-tab li:eq(1)").addClass('has-error');
        @endif
    </script>
@endpush
