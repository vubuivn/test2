<?php

return [
    'admin.careers' => [
        'index' => 'career::permissions.index',
        'create' => 'career::permissions.create',
        'edit' => 'career::permissions.edit',
        'destroy' => 'career::permissions.destroy',
    ],
];
