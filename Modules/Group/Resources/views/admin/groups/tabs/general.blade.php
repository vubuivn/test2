<a class="btn btn-default" href="{{route('products.show',$group->slug)}}">View {{$group->name}}</a>
{{ Form::text('name', trans('group::attributes.name'), $errors, $group, ['labelCol' => 2, 'required' => true]) }}

{{ Form::text('code', trans('group::attributes.code'), $errors, $group , ['labelCol' => 2, 'required' => true]) }}

{{ Form::text('collections', trans('group::attributes.collection'), $errors, $group , ['labelCol' => 2, 'required' => false]) }}

{{ Form::wysiwyg('description', trans('group::attributes.description'), $errors, $group, ['labelCol' => 2, 'required' => true]) }}

<div class="row">
    <div class="col-md-8">
        {{ Form::select('categories', trans('group::attributes.categories'), $errors, $categories, $group, ['class' => 'selectize prevent-creation', 'multiple' => true]) }}
        {{ Form::checkbox('is_active', trans('group::attributes.is_active'), trans('group::groups.form.enable_the_group'), $errors, $group) }}
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        {{ Form::checkbox('is_new', trans('group::attributes.is_new'), trans('group::groups.form.is_new'), $errors, $group) }}
    </div>
</div>
{{ Form::text('new_start', trans('group::attributes.new_start'), $errors, $group, ['labelCol' => 2, 'class' => 'datetime-picker']) }}
{{ Form::text('new_end', trans('group::attributes.new_end'), $errors, $group, ['labelCol' => 2, 'class' => 'datetime-picker']) }}

<input type="hidden" id="block-number" value="{{ isset($group->blocks) ? count($group->blocks) : 0 }}">
<div class="form-group ">
    <div class="col-md-12">
        <button class="btn btn-default btn-sm pull-right" id="expandAllFormats"></button>
    </div>
</div>
<div class="content-accordion panel-group options-group-wrapper" id="page-block" role="tablist"
     aria-multiselectable="true">
    @if($group->blocks)
        @foreach ($group->blocks as $key => $item)
            <form></form>
            @php($block = \Modules\Block\Entities\Block::where('id',$item->block_id)->withoutGlobalScope('active')->first())
            @php($data = null)
            @if($item->content != null)
                @php($data = json_decode($item->content))
            @endif
            <form name="block-form" id="block-form-{{$key}}">
                <input type="hidden" name="blocks[{{$key}}][main_block_id]" value="{{$item->id}}">
                <input type="hidden" name="blocks[{{$key}}][block_id]" value="{{$block->id}}">
                <div class="panel panel-default" style="margin-bottom: 15px">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="collapsed" data-parent="#block-{{$key}}-edit"
                               href="#custom-collapse-{{$key}}-edit">
                                <span class="drag-icon pull-left">
                                    <i class="fa"></i>
                                    <i class="fa"></i>
                                </span>

                                <span id="block-name" class="pull-left">
                                    {{$item->title}}
                                </span>
                            </a>
                        </h4>
                    </div>
                    <div id="custom-collapse-{{$key}}-edit" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="new-option clearfix">
                                <div class="col-lg-6 col-md-12 p-l-0">
                                    <div class="form-group">
                                        <label for="block-{{$key}}-edit-name">
                                            {{ trans('block::blocks.form.name') }}
                                        </label>
                                        <input type="text" name="blocks[{{$key}}][main_block_title]"
                                               class="form-control block-name-field"
                                               id="block-{{$key}}-edit-name"
                                               value="{{$item->title}}">
                                    </div>
                                </div>

                                <button type="button" class="btn btn-default delete-block pull-right"
                                        data-toggle="tooltip" title=""
                                        data-original-title="{{trans('block::blocks.form.delete_block')}}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>

                            <div class="clearfix"></div>

                            <div class="option-values clearfix" id="block-{{$key}}-edit-values">
                                @foreach ($block->values as $value)
                                    {{block_html($value, $key, $data)}}
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @endforeach
    @endif
</div>
@push('scripts')
    <script src="{{ asset('modules/group/admin/js/group.js') }}"></script>
    <script>
        $('.content-accordion').find('.banner-body:eq(0)').find('.delete-image').remove();
    </script>
    
@endpush
@push('globals')
    <script>
        FleetCart.langs['page::pages.expand_all'] = '{{trans('page::pages.expand_all')}}';
        FleetCart.langs['page::pages.collapse_all'] = '{{trans('page::pages.collapse_all')}}';
    </script>
@endpush
