<?php

namespace Modules\Wishlist\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Product\Entities\Product;
use Illuminate\Support\Facades\Cache;

class WishlistController extends Controller
{
    public function toggleWishlist()
    {
        $user = auth()->user();
        if (empty($user)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        $type = request('type');
        $text = '';
        

        $productId = request('product_id');
        Product::findOrFail($productId);
        
        
        $wishlistStr = '<span class="like active">
                            <i class="icon-heart-add">
                                <span class="path1"></span>
                                <span class="path2"></span>
                                <span class="path3"></span>
                            </i>
                        '.$text.'</span>';
        if($type == 'detail'){
            $text = trans('group::group-categories.wishlist');
            $wishlistStr = '<i class="icon-heart-add">
                                <span class="path1"></span>
           
                            </i>
                        '.$text;
        }                 
        
        if ($this->alreadyInWishlist()) {
            auth()->user()->wishlist()->detach($productId);
            $wishlistStr = '<span class="like"><i class="icon-heart cl1"></i>'.$text.'</span>';
            if($type == 'detail'){
                $wishlistStr = '<i class="icon-heart cl1"></i>'.$text;

            }
        } else {
            auth()->user()->wishlist()->attach($productId);
        }

        Cache::forget('wishlistProductIds_' . $user->id);

        return response()->json(['html' => $wishlistStr], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        // dd(1);
        if ($this->alreadyInWishlist()) {
            return back();
        }

        auth()->user()->wishlist()->attach(request('product_id'));

        return back()->withSuccess(trans('wishlist::messages.added'));
    }

    private function alreadyInWishlist()
    {
        $wishlist = auth()->user()->wishlist()->pluck('product_id');

        return $wishlist->contains(request('product_id'));
    }

    public function removeWishlist(){
        $request = \Request();
        if($request->has('id')){
            try{
                $productId = $request->id;
                auth()->user()->wishlist()->detach($productId);
                return 'Done';
            }catch(\Exception $e){
                return $e->getMessage();
            }
        }
        
    }
}
