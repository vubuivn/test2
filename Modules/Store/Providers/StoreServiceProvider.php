<?php

namespace Modules\Store\Providers;

use Modules\Store\Admin\StoreTabs;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\AddsAsset;
use Modules\Support\Traits\LoadsConfig;
use Modules\Admin\Ui\Facades\TabManager;

class StoreServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot ()
    {
        $this->addAdminAssets(
            'admin.stores.(create|edit)',
            [
                'admin.media.css',
                'admin.media.js',
                'admin.option.css',
                'admin.option.js',
                'admin.store.js',
                'admin.storefront.css',
                'admin.storefront.js',
            ]
        );
        TabManager::register(
            'stores',
            StoreTabs::class
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register ()
    {
        $this->loadConfigs(
            [
                'assets.php',
                'permissions.php'
            ]
        );
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
