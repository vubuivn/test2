@foreach($posts as $post)
<div class="col-md-6">
    <div class="item" >   
        <a class="img" href="{{route('news.detail',$post->slug)}}">
            <img src="{{$post->baseImage->path}}" alt="">
        </a>
        <div class="divtext">
            <div class="meta">
                <div class="cate"><span>{{$category->name}}</span></div>
                <span class="date">{{date("d/m/Y", strtotime($post->created_at))}}</span>
            </div>                  
            <h3 class="title">{{$post->name}}</h3>
            <div class="desc">
                {{$post->short_description}}
            </div>
            <div class="more">
                <a href="{{route('news.detail',$post->slug)}}"> {{trans('storefront::storefront.news.view_more')}} <i class="icon-arrow-14"></i></a>
            </div>
        </div>
    </div>
</div>
@endforeach
               