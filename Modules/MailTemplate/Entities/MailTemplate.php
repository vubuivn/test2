<?php

namespace Modules\MailTemplate\Entities;

use Modules\MailTemplate\Admin\MailTemplateTable;
use Modules\Support\Eloquent\Model;
use Modules\Support\Eloquent\Translatable;

class MailTemplate extends Model
{
    use Translatable;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_active',
        'code',
        'email_cc',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'subject',
        'content'
    ];

    /**
     * Get table data for the resource
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function table($request)
    {
        $query = $this->newQuery()
                      ->withoutGlobalScope('active')
                      ->addSelect(
                          [
                              'id',
                              'is_active',
                              'created_at'
                          ]
                      )
                      ->when(
                          $request->has('except'),
                          function ($query) use ($request) {
                              $query->whereNotIn(
                                  'id',
                                  explode(
                                      ',',
                                      $request->except
                                  )
                              );
                          }
                      );

        return new MailTemplateTable($query);
    }

    public function parse($data)
    {
        $parsed = preg_replace_callback(
            '/{{(.*?)}}/',
            function ($matches) use ($data) {
                list($shortCode, $index) = $matches;

                if (isset($data[$index])) {
                    return $data[$index];
                }

            },
            $this->content
        );

        return $parsed;
    }
}
