<?php

namespace Modules\Page\Http\Controllers\Admin;

use Modules\Page\Entities\Page;
use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Page\Http\Requests\SavePageRequest;

class PageController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Page::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'page::pages.page';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'page::admin.pages';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SavePageRequest::class;

    public function store ()
    {
        $request           = $this->getRequest('store')
            ->all();
        if($this->getRequest('store')->has('blocks')){
            $request['blocks'] = _checkBlocks($request['blocks']);
        }
        
        $this->disableSearchSyncing();

        $entity = $this->getModel()
            ->create(
                $request
            );
        $entity->saveRelations($request);
    }



    public function update ($id)
    {

        $request           = $this->getRequest('update')
            ->all();
        if($this->getRequest('update')->has('blocks')){
            $request['blocks'] = _checkBlocks($request['blocks']);
        }

        $entity            = $this->getEntity($id);

        $this->disableSearchSyncing();
        $entity->update(
            $request
        );

        // Image banner
        // if(!isset($data['files']['base_image'])){
        //     \DB::table('entity_files')->where('entity_id',$id)->where('entity_type','Modules\Page\Entities\Page')->where('zone','base_image')->delete();
        // }


        $entity->saveRelations($request);
    }
}
