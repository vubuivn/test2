<?php

namespace Modules\CategoryNews\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\CategoryNews\Entities\CategoryNews;
use Modules\CategoryNews\Http\Requests\SaveCategoryNewsRequest;

class CategoryNewsController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = CategoryNews::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'categorynews::category_news.category_news';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'categorynews::admin.category_news';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveCategoryNewsRequest::class;
    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = CategoryNews::withoutGlobalScope('active')
                            ->find($id);
        $category->coverImage = count($category->CoverImage->toArray()) > 0 ? $category->CoverImage->toArray() : null;
        $category->featureImage = count($category->FeatureImage->toArray()) > 0 ? $category->FeatureImage->toArray() : null;
        $category->meta_title = $category->meta->meta_title;
        $category->meta_description = $category->meta->meta_description;
        return $category;
    }
    /**
     * Destroy resources by given ids.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CategoryNews::withoutGlobalScope('active')
            ->findOrFail($id)
            ->delete();

        return back()->withSuccess(
            trans(
                'admin::messages.resource_deleted',
                ['resource' => $this->getLabel()]
            )
        );
    }
}
