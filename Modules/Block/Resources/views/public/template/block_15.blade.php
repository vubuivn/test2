<div class="container">
<section class="sec-catalogue-2 sec-tb">
		<h3>{{$title}}</h3>
	    <div class=" owl-carousel  s-auto" data-res="4,3,3,2" paramowl="margin=30" >
	        @foreach($images as $key => $item)
			<a href="{{$item->call_to_action_url}}" class="item" >       
				<div class="img tRes_130">
		        	<img  class="owl-lazy"  data-src="{{$item->file}}"   alt="">
		        </div>
		        <h3 class="title">{{$item->caption_1}}</h3>
			</a>
			@endforeach
	    </div>
	</section>

  </div>