@if(isset($product))
    <?php 
        $group = $product->group;
    ?>
    @if($group)
    <a class="btn btn-default" href="{{route('products.show',$group->slug)}}">View {{$group->name}}</a>

    <a class="btn btn-default" href="{{route('admin.groups.edit',$product->group_id)}}">View {{$group->name}} - {{$group->code}}</a>
    Image Count: {{$product->image_count}}
    <div style="margin-bottom: 10px"></div>
    @endif
@endif

<div class="form-group {{ $errors->has('group_id') ? 'has-error' : '' }}">
    <label for="long" class="col-md-2 control-label text-left">
        {{trans('product::attributes.group')}} <span class="m-l-5 text-red">*</span>
    </label>
    <div class="col-md-10">
        <select name="group_id" id="group_id" class="form-control">
            @if($product->group_id != null)
                <option value="{{$product->group_id}}" selected="selected">{{$product->group->name}} - {{$product->group->code}}</option>
            @endif
            @if(isset($_REQUEST['group']))
                <option value="{{$_REQUEST['group']}}" selected="selected">@if(isset($_REQUEST['name'])){{$_REQUEST['name']}} - {{$_REQUEST['group']}} @endif</option>
            @endif
        </select>
        @if($errors->has('group_id'))
            <span class="help-block">{{ $errors->first('group_id')}}</span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('group_id') ? 'has-error' : '' }}">
    <label for="long" class="col-md-2 control-label text-left">
        {{trans('product::attributes.is_active')}}
    </label>
    <div class="col-md-10">
        @php
            if(!isset($product->is_active)){
                $product->is_active = '-1';
            }
        @endphp
        <input type="radio" value="-1" {{ ($product->is_active == '-1')? 'checked' : '' }} name="is_active"
               id="is_active_0"> &nbsp;
        <label for="is_active_0"> {{trans('product::products.form.hide_from_the_web')}} </label>
        <br>
        <input type="radio" value="0" {{ ($product->is_active == 0)? 'checked' : '' }} name="is_active"
               id="is_active_1"> &nbsp;
        <label for="is_active_1"> {{trans('product::products.form.show_products_by_side')}} </label>
        <br>
        <input type="radio" value="1" {{ ($product->is_active == 1)? 'checked' : '' }} name="is_active"
               id="is_active_2"> &nbsp;
        <label for="is_active_2"> {{trans('product::products.form.show_main_products')}} </label>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        {{ Form::checkbox('out_of_stock', '', trans('product::attributes.out_of_stock'), $errors, $product) }}
    </div>
</div>

{{ Form::text('long', trans('product::attributes.long'), $errors, $product, ['labelCol' => 2]) }}
{{ Form::text('wide', trans('product::attributes.wide'), $errors, $product, ['labelCol' => 2]) }}
{{ Form::text('high', trans('product::attributes.high'), $errors, $product, ['labelCol' => 2]) }}
{{ Form::text('volume', trans('product::attributes.volume'), $errors, $product, ['labelCol' => 2]) }}
{{ Form::text('seats', trans('product::attributes.seats'), $errors, $product, ['labelCol' => 2]) }}

@push('styles')
    <link href="{{ asset('select2.css') }}" rel="stylesheet">
@endpush
@push('scripts')
    <script src="{{ asset('select2.js') }}"></script>
    <script>
        $('#group_id').select2({
            theme: "bootstrap",
            placeholder: "Choose tags...",
            minimumInputLength: 1,
            ajax: {
                url: '{{ route('admin.groups.list') }}',
                dataType: 'json',
                data: function (params) {
                    return {
                        query: params.term,
                        limit: 1000,
                        name: params.name
                    };
                },
                processResults: function (result) {
                    return {
                        results: $.map(result.data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        }),
                    };
                },
                cache: true
            },
        });
    </script>
@endpush
