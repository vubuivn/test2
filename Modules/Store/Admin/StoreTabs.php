<?php

namespace Modules\Store\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;
use Modules\Block\Entities\Block;
use Illuminate\Support\Facades\DB;

class StoreTabs extends Tabs
{
    public function make()
    {
        $this->group(
            'store_information',
            trans('store::stores.tabs.group.store_information')
        )
             ->active()
             ->add($this->general());
        $this->group(
            'advanced_information',
            trans('product::products.tabs.group.advanced_information')
        )
            ->add($this->seo());

        $this->submit = false;
        
        // $this->block(Block::get(),DB::table('block_tags')->get());
    }

    private function general()
    {
        return tap(
            new Tab(
                'general',
                trans('store::stores.tabs.general')
            ),
            function (Tab $tab) {
                $tab->active();
                $tab->weight(5);
                $tab->fields(
                    [
                        'title',
                        'body',
                        'is_active',
                        'slug',
                        'email'
                    ]
                );
                $tab->view('store::admin.stores.tabs.general',['districts'=>\DB::table('districts')->get()]);
            }
        );
    }

    private function seo()
    {
        return tap(
            new Tab(
                'seo',
                trans('store::stores.tabs.seo')
            ),
            function (Tab $tab) {
                $tab->weight(10);
                $tab->view('store::admin.stores.tabs.seo');
            }
        );
    }

    public function to_slug($str) {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }
}
