<?php

return [
    'name'                => 'Name',
    'slug'                => 'URL',
    'description'         => 'Description',
    'short_description'   => 'Short Description',
    'is_active'           => 'Status',
    'categories'           => 'Category',
];
