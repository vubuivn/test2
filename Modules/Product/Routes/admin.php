<?php

Route::get('products', [
    'as' => 'admin.products.index',
    'uses' => 'ProductController@index',
    'middleware' => 'can:admin.products.index',
]);

Route::get('products/create', [
    'as' => 'admin.products.create',
    'uses' => 'ProductController@create',
    'middleware' => 'can:admin.products.create',
]);

Route::post('products', [
    'as' => 'admin.products.store',
    'uses' => 'ProductController@store',
    'middleware' => 'can:admin.products.create',
]);

Route::get('products/{id}/edit', [
    'as' => 'admin.products.edit',
    'uses' => 'ProductController@edit',
    'middleware' => 'can:admin.products.edit',
]);

Route::put('products/{id}', [
    'as' => 'admin.products.update',
    'uses' => 'ProductController@update',
    'middleware' => 'can:admin.products.edit',
]);

Route::delete('products/{ids}', [
    'as' => 'admin.products.destroy',
    'uses' => 'ProductController@destroy',
    'middleware' => 'can:admin.products.destroy',
]);
Route::get('products/excel', [
    'as' => 'admin.products.export.excel',
    'uses' => 'ProductController@exportExcel',
]);
Route::get('products/excelAll', [
    'as' => 'admin.products.exportAll.excel',
    'uses' => 'ProductController@exportAllProductsExcel',
]);

Route::post('products/excel', [
    'as' => 'admin.products.import.excel',
    'uses' => 'ProductController@importExcel',
]);

Route::get('products/download/{file}', [
    'as' => 'admin.products.downloadProductsFile.excel',
    'uses' => 'ProductController@downloadExcelFile'
]);

Route::get('products/download', [
    'as' => 'admin.products.downloadErrorProductsFile.excel',
    'uses' => 'ProductController@downloadErrorProductsFile',
]);

// Route::get('products/sample-product-attribute', [
//     'as' => 'admin.products.generateSampleProductAttribute',
//     'uses' => 'ProductController@generateSampleProductAttribute'
// ]);

// Route::get('products/sync-product-attribute', [
//     'as' => 'admin.products.syncProductAttribute',
//     'uses' => 'ProductController@syncProductAttribute',
// ]);
