<?php

namespace Modules\Store\Http\Controllers\Admin;

use Modules\Store\Entities\Store;
use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Store\Http\Requests\SaveStoreRequest;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Store::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'store::stores.store';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'store::admin.stores';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveStoreRequest::class;

    public function store ()
    {
        $request           = $this->getRequest('store')
            ->all();
        $this->disableSearchSyncing();

        $entity = $this->getModel()
            ->create(
                $request
            );
        $entity->saveRelations($request);
    }



    public function update ($id)
    {

        $request           = $this->getRequest('update')
            ->all();
        $entity            = $this->getEntity($id);

        $this->disableSearchSyncing();
        $entity->update(
            $request
        );

        // Image banner
        // if(!isset($data['files']['base_image'])){
        //     \DB::table('entity_files')->where('entity_id',$id)->where('entity_type','Modules\Store\Entities\Store')->where('zone','base_image')->delete();
        // }


        $entity->saveRelations($request);
    }

    public function cities(){
        $cities = \DB::table('cities')->get();
        return view("{$this->viewPath}.cities",compact('cities'));
    }

    public function citiesAjax(Request $request){
        $id = $request->id;
        $r = [];
        $r['message'] = 'required';
        if(!$request->has('lat')){
            $r['target'] = 'lat';
            return $r;
        }
        if(!$request->has('long')){
            $r['target'] = 'lat';
            return $r;
        }
        $lat = $request->lat;
        $long = $request->long;
        try{
            \DB::table('cities')->where('id',$id)->update([
                'loc_lat' => $lat,
                'loc_long' => $long
            ]);
            return 'update complete';
        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    public function citiesAjaxChange(Request $request){
        $id = $request->id;
        $city = \DB::table('cities')->select('loc_lat','loc_long')->where('id',$id)->first();
        $arr = [];

        if($city){
            $arr['loc_lat'] = $city->loc_lat;
            $arr['loc_long'] = $city->loc_long;
            return $arr;
        }

        return;
    }
}
