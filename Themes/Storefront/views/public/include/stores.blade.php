<section class="sec-google-maps sec-tb">
  <div class="container">

        <div class="filter-map ">
          <div class="title">TÌM CỬA HÀNG</div>

            <div class="row">
              <div class="col-sm-6">
                <div class="mb-10 ">Tỉnh / Thành phố</div>
                        <select name="city" class="select2-single" id="slb_city">
                            
                        </select>       


              </div>
              <div class="col-sm-6">
                <div class="mb-10">Quận / Huyện</div>
                    <select name="district" class="select2-single" id="slb_district" tabindex="-98">
                      <option value="1">Quận 1</option>
                    </select>

              </div>

            </div>

        </div>

        <div id="map" style="height: 550px;" >    </div>

        <div class="wrap-list-store">
          <div class="top row grid-space-10">
            <div class="col-3 col-sm-3">{{trans('storefront::storefront.store.store')}}</div>
            <div class="col-3 col-sm-3">{{trans('storefront::storefront.store.address')}}</div>
            <div class="col-3 col-sm-2">{{trans('storefront::storefront.store.email')}}</div>
            <div class="col-3 col-sm-2">{{trans('storefront::storefront.store.phone')}}</div>
            <div class="col-3 col-sm-2">{{trans('storefront::storefront.store.open')}}</div>
            
          </div>
          <ul class="map-list-store"></ul>
        </div>


  </div>
</section>
<?php 
    $arrcities = [];
    $arrayjs = [];
    $ids = getDistrictIds();
    $return = getCities($ids);
    foreach($return['cities'] as $city){
        $arrayjs['id'] = $city->id;
        $title = get_local_title($city->title);
        $arrayjs['name'] = $title;
        $arrayjs['lat'] = $city->loc_lat;
        $arrayjs['long'] = $city->loc_long;
        $arrayjs['district'] = [];
        foreach($return['districts'] as $district){
            if($district->city_id == $city->id){
                $arrd = [];
                $arrd['id'] = $district->id;
                $title = get_local_title($district->title);
                $arrd['name'] = $title;
                $arrd['store'] = [];
                $stores = getStores($district->id);
                foreach($stores as $store){
                    if($store->district_id == $district->id){
                        $astore = [];
                        $astore['lat'] = $store->loc_lat;
                        $astore['long'] = $store->loc_long;
                        $astore['name'] = $store->name;
                        $astore['address'] = $store->address;
                        $astore['phone'] = $store->phone;
                        $astore['open'] = $store->open;
                        $astore['email'] = $store->email != null ? $store->email : '';
                        $arrd['store'][] = $astore;
                    } 
                }
                $arrayjs['district'][] = $arrd;
            } 
        }
        $arrcities[] = $arrayjs;
    }

    // var_dump($arrayjs);die;
?>
@push('scripts')
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script> -->
<script src="{{ v(Theme::url('public/js/markerclusterer.js')) }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLozFL2sjFSOp4AnFeIjoIXliYLJZZYe8" type="text/javascript"></script>
<script type='text/javascript' src="{{ v(Theme::url('public/js/select2.full.js')) }}"></script>
<script type="text/javascript">
    (function($){

        var menu = {};
        var page = {};
        var index = {};
        var map;
        var infoWindow;
        var curentPos;
        $(document).ready(function() {
            page.district();
        });


        index.map = function() {
            map = new google.maps.Map(document.getElementById('map'), {});
            infoWindow = new google.maps.InfoWindow({
                maxWidth: 350
            });
            var latlngbounds = new google.maps.LatLngBounds();
            var LatLngList = [];
            $('.map-list-store li').each(function() {
                var latlng = $(this).find("a.link").data("latlng");
                latlng = latlng.split("-");
                var lat = Number(latlng[0]);
                var lng = Number(latlng[1]);
                
                var marker = new google.maps.Marker({
                    position: {
                        lat: lat,
                        lng: lng
                    },
                    map: map,
                    icon: '/assets/images/pin.png'


                });



                LatLngList.push(new google.maps.LatLng(lat, lng));
                $(this).find("a.link").click(function(e) {
                    e.preventDefault();
                    var latlng = $(this).data("latlng");
                    latlng = latlng.split("-");
                    var pos = {
                        lat: Number(latlng[0]),
                        lng: Number(latlng[1])
                    };
                    infoWindow.setPosition(pos);
                    infoWindow.setContent($(this).text());
                    infoWindow.open(map,marker);
                    map.setCenter(pos);
                });
            });
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    curentPos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var marker = new google.maps.Marker({
                        position: curentPos,
                        map: map,
                        //icon: 'assets/images/pin.png'
                    });
                    LatLngList.push(new google.maps.LatLng(curentPos.lat, curentPos.lng));
                });
            }
            LatLngList.forEach(function(latLng) {
                latlngbounds.extend(latLng);
            });
            map.setCenter(latlngbounds.getCenter());
            map.setZoom(13);
        };
        index.direction = function() {
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer({
                suppressMarkers: true,
                polylineOptions: {
                    strokeColor: "#0C713D"
                }
            });
            directionsDisplay.setMap(map);
            $('.map-list-store li').each(function() {
                var latlng = $(this).find("a.link").data("latlng");
                latlng = latlng.split("-");
                var lat = Number(latlng[0]);
                var lng = Number(latlng[1]);
                $(this).children("button").click(function() {
                    directionsService.route({
                        origin: curentPos,
                        destination: {
                            lat: lat,
                            lng: lng
                        },
                        travelMode: 'DRIVING'
                    }, function(response, status) {
                        if (status === 'OK') {
                            directionsDisplay.setDirections(response);
                        } else {
                            window.alert('Directions request failed due to ' + status);
                        }
                    });
                })
            });
        };



        var data_district = <?php echo json_encode($arrcities) ?>;
        console.log(data_district);
        var data_district2 = [
            {
                "id": "1",
                "name": "Tp Hồ Chí Minh",
                "lat": "10.7687085",
                "long": "106.4141728",
                "district": [
                {
                    "id": "1",
                    "name": "Quận 1",
                    "store": [
                    {
                        "lat": "10.776987610178267",
                        "long": "106.69783473014832",
                        "name": "q 1 - 1",
                        "address": "101 TÔN DẬT TIÊN, Q.7, TPHCM",
                        "phone": "0123456789",
                        "open": "10:00 - 22:00"
                    },
                    {
                        "lat": "10.775090475213744",
                        "long": "106.70176148414612",
                        "name": "q 1 - 1",
                        "address": "101 TÔN DẬT TIÊN, Q.7, TPHCM",
                        "phone": "0123456789",
                        "open": "10:00 - 22:00"
                    },            
                    {
                        "lat": "10.779158761076673",
                        "long": "106.70217990875244",
                        "name": "q 1 -2 ",
                        "address": "101 TÔN DẬT TIÊN, Q.7, TPHCM",
                        "phone": "0123456789",
                        "open": "10:00 - 22:00"
                    }            
                    ]
                },
                {
                    "id": "11",
                    "name": "Quận 2",
                    "store": [{
                        "lat": "10.779158761076673",
                        "long": "106.70217990875244",
                        "name": "Ô 9-10A, LẦU 22222",
                        "address": "101 TÔN DẬT TIÊN, Q.7, TPHCM",
                        "phone": "0123456789",
                        "open": "10:00 - 22:00"
                    }]
                },        
                ]
            }, {
                "id": "2",
                "name": "Hà Nội",
                "lat": "20.9088516",
                "long": "105.908203",
                "district": [{
                    "id": "1",
                    "name": "Đống Đa",
                    "store": [{
                        "lat": "21.0232779",
                        "long": "105.809787",
                        "name": "Ô 9-10A, LẦU 2, CRESCENT MALL",
                        "address": "101 TÔN DẬT TIÊN, Q.7, TPHCM ",
                        "phone": "0123456789",
                        "open": "10:00 - 22:00"
                    }]
                    
                }]
        }];

console.log(data_district2);



        //fillCity();


        page.district = function() {
            fillCity();
            loadDistrict();
            loadDistrictFirst();
            loadStore();
            index.map();
            index.direction();
            $('#slb_city').on('change', function(e) {
                loadDistrict();
                loadStore();
                index.map();
                index.direction();
            });
            $("#slb_district").on('change', function() {
                loadStore();
                index.map();
                index.direction();
            });
        };

        function fillCity() {
                for (var i = 0; i < data_district.length; i++) {
                    $("#slb_city").append('<option value="' + data_district[i].id + '">' + data_district[i].name + '</option>');
                }            
        }        

        function loadDistrictFirst() {
            for (var i = 0; i < data_district.length; i++) {
                $('#slb_district').html('');
                for (var j = 0; j < data_district[0].district.length; j++) {
                    $("#slb_district").append('<option value="' + data_district[0].district[j].id + '">' + data_district[0].district[j].name + '</option>');
                }
            }
        }


        function loadDistrict() {
            $('#slb_city').on('change', function () {
                console.log($(this).val());
                for (var i = 0; i < data_district.length; i++) {
                    if (data_district[i].id == $(this).val()) {
                        //$('#slb_district').html('<option value="000">-Select State-</option>');
                        $('#slb_district').html('');
                        for (var j = 0; j < data_district[i].district.length; j++) {
                            $("#slb_district").append('<option value="' + data_district[i].district[j].id + '">' + data_district[i].district[j].name + '</option>');
                        }
                    }
                }
            });
        }



        function loadStore() {
            var windowWidth = $(window).width();
            var c = Number($("#slb_city").val());
            var d = Number($("#slb_district").val());
            var store = [];
            for (var i = 0; i < data_district.length; i++) {
                if (c == data_district[i].id) {
                    for (var j = 0; j < data_district[i].district.length; j++) {
                        if (d == data_district[i].district[j].id) {
                            store = data_district[i].district[j].store;
                            break;
                        }
                    }
                }
            }
            var textname = '';
            var textaddress = '';
            var textphone = '';
            var textopen = '';
            var textemail = '';
            if(windowWidth <=767){
                textname = "{{trans('storefront::storefront.store.store')}}: ";
                textaddress = "{{trans('storefront::storefront.store.address')}}: ";
                textphone = "{{trans('storefront::storefront.store.phone')}}: ";
                textopen = "{{trans('storefront::storefront.store.open')}}: ";
                textemail = "{{trans('storefront::storefront.store.eamil')}}: ";
            }

            var html = "";

            for (var i = 0; i < store.length; i++) {
        //        html += '<li><a target="_blank" href="http://maps.google.com/?q=' + store[i].name + '"  data-img="' + store[i].image + '" data-site="' + store[i].website + '" data-lat="' + store[i].lat + '" data-lng="' + store[i].long + '">';
                html += '<li class="row grid-space-10">';
                html += '<span class="col-3 col-sm-3 name"><a class="link" href="http://maps.google.com/?q=' + store[i].name + '" data-latlng="' + store[i].lat + '-' + store[i].long + '">' + store[i].name + '</a></span> ';
                html += '<span class="col-3 col-sm-3 address">'+ textaddress + store[i].address + '</span> ';
                html += '<span class="col-3 col-sm-2 email">'+ textemail + store[i].email + '</span> ';
                html += '<span class="col-3 col-sm-2 phone">'+textphone + store[i].phone + '</span>';
                html += '<span class="col-3 col-sm-2 time"> '+textopen + store[i].open + '</span>';
                html += '</li>';
            }
         
            $(".map-list-store").html(html);


        }


            $(".select2-single").select2({
                theme: "bootstrap",
                placeholder: "Select a State",
                maximumSelectionSize: 6,
                containerCssClass: ':all:'
            });

            


        })(jQuery);



</script>
@endpush

@push('styles')
<style type="text/css">
ul.map-list-store{padding-left: 0}
    @media screen and (max-width: 767px){
        .wrap-list-store .top.row.grid-space-10{
            display: none
        }
        .wrap-list-store .row{min-width: 100%}
        
        ul.map-list-store li{
            max-width: 100%;
            min-width: 100%;
            width: 100%
        }
        ul.map-list-store li > span{
            width: 100%;display: block;max-width: 100%;
                -ms-flex: auto;flex: auto;-webkit-box-flex: auto;
        }
    }
</style>
@endpush