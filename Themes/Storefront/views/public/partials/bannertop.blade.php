<?php 
	
	if(!isset($title)){
		$title = '';
	}
	if(!isset($banner)){
		$banner = '';
	}
	if(isset($page->name)){
	$title = $page->name;
	 	$banner = $page->baseImage->path;
	  }
	  //elseif(isset($category->name)){
	//  	$title = $category->name;
	//  	$banner = $category->coverImage->path;
	//  }elseif(isset($post->name)){
	//  	$title = $post->name;
	//  	$banner = $post->baseImage->path;
	//  }elseif(isset($product->name)){
	//  	$title = $product->name;
	//  	$banner = $product->baseImage->path;
	//  }
?>

<section id="bannertop" class="lazy-hidden tRes_48 subbanner" data-lazy-type="bg" data-lazy-src="{{$banner}}" >
  <div class="divtext">@if(isset($subtitle))<h5>{{$subtitle}}</h5>@endif<h1>{{$title}}</h1></div>
</section>