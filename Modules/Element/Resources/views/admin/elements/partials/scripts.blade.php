{{--@push('globals')--}}
    {{--<script>--}}
        {{--FleetCart.data['element.values'] = {!! old_json('values', $element->values) !!};--}}
        {{--FleetCart.errors['element.values'] = @json($errors->get('values.*'), JSON_FORCE_OBJECT);--}}
    {{--</script>--}}
{{--@endpush--}}

@push('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('admin::admin.shortcuts.back_to_index', ['name' => trans('element::elements.element')]) }}</dd>
    </dl>
@endpush

@push('scripts')
    <script>
        keypressAction([
            { key: 'b', route: "{{ route('admin.elements.index') }}" },
        ]);
    </script>
@endpush
