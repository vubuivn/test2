<?php

namespace Modules\CategoryNews\Providers;

use Modules\Support\Traits\AddsAsset;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\LoadsConfig;

class CategoryNewsServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->addAdminAssets(
            'admin.category_news.index',
            [
                'admin.media.css',
                'admin.media.js',
                'admin.category.css',
                'admin.jstree.js',
//                'admin.category.js',
//                'admin.categorynews.js'
            ]
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadConfigs(
            [
                'assets.php',
                'permissions.php'
            ]
        );
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
