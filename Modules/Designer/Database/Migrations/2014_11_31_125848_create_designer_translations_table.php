<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('designer_id')->unsigned();
            $table->string('locale');
            $table->string('name');
            $table->text('body');

            $table->unique(['designer_id', 'locale']);
            $table->foreign('designer_id')->references('id')->on('designers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_translations');
    }
}
