<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Define which assets will be available through the asset manager
    |--------------------------------------------------------------------------
    | These assets are registered on the asset manager
    */
    'all_assets'      => [
        'admin.designer.js' => ['module' => 'designer:admin/js/designer.js'],
    ],

    /*
    |--------------------------------------------------------------------------
    | Define which default assets will always be included in your designers
    | through the asset pipeline
    |--------------------------------------------------------------------------
    */
    'required_assets' => [],
];
