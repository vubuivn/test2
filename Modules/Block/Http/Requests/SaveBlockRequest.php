<?php

namespace Modules\Block\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Block\Entities\BlockValue;
use Modules\Core\Http\Requests\Request;

class SaveBlockRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'block::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        return [
            'name'           => 'required',
            'html'           => 'required',
            'values.*.key'   => 'required',
            'values.*.title' => 'required',
            'values.*.type'  => 'required',
        ];
    }
}
