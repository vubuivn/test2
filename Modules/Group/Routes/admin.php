<?php

Route::get('groups', [
    'as' => 'admin.groups.index',
    'uses' => 'GroupController@index',
    'middleware' => 'can:admin.groups.index',
]);

Route::get('groups/create', [
    'as' => 'admin.groups.create',
    'uses' => 'GroupController@create',
    'middleware' => 'can:admin.groups.create',
]);

Route::post('groups', [
    'as' => 'admin.groups.store',
    'uses' => 'GroupController@store',
    'middleware' => 'can:admin.groups.create',
]);

Route::get('groups/{id}/edit', [
    'as' => 'admin.groups.edit',
    'uses' => 'GroupController@edit',
    'middleware' => 'can:admin.groups.edit',
]);

Route::put('groups/{id}', [
    'as' => 'admin.groups.update',
    'uses' => 'GroupController@update',
    'middleware' => 'can:admin.groups.edit',
]);

Route::delete('groups/{ids?}', [
    'as' => 'admin.groups.destroy',
    'uses' => 'GroupController@destroy',
    'middleware' => 'can:admin.groups.destroy',
]);
Route::get('groups/excel', [
    'as' => 'admin.groups.export.excel',
    'uses' => 'GroupController@exportExcel',
]);
Route::post('groups/excel', [
    'as' => 'admin.groups.import.excel',
    'uses' => 'GroupController@importExcel',
]);
Route::post('groups/read-excel', [
    'as' => 'admin.groups.read.excel',
    'uses' => 'GroupController@readExcel',
]);
Route::get('groups/list', [
    'as' => 'admin.groups.list',
    'uses' => 'GroupController@list',
]);
