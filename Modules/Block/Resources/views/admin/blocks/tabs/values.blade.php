<div id="attribute-values-wrapper">
    <div class="table-responsive">
        <table class="options table table-bordered">
            <thead>
            <tr>
                <th></th>
                <th>{{ trans('block::blocks.form.key') }} <span style="color: red">*</span></th>
                <th>{{ trans('block::blocks.form.title') }} <span style="color: red">*</span></th>
                <th>{{ trans('block::blocks.form.type') }} <span style="color: red">*</span></th>
                <th></th>
            </tr>
            </thead>

            <tbody id="attribute-values">
            </tbody>
        </table>
    </div>

    <button type="button" class="btn btn-default" id="add-new-value">
        {{ trans('block::blocks.form.add_new_block') }}
    </button>
</div>

@include('block::admin.blocks.tabs.templates.block_value')

@push('globals')
    <script>
        FleetCart.data['attribute.values'] = {!! old_json('values', $block->values) !!};
        FleetCart.errors['option.values'] = @json($errors->get('values.*'), JSON_FORCE_OBJECT);
    </script>
@endpush
@push('scripts')
    <script>
        @if($errors->has('values.*'))
            for (var k in FleetCart.errors['option.values']) {
            var convertK = k.split(".");
            var name = convertK[0] + '[' + convertK[1] + '][' + convertK[2] + ']';
            var div = $("input[name='" + name + "']").parent();
            div.addClass('has-error');
            var message = FleetCart.errors['option.values'][k][0].replace(k, "values");
            div.append('<span class="help-block">' + message + '</span>');
        }
        $("#attribute_set_information .accordion-tab li:eq(1)").addClass('has-error');

        @endif
        for (var k in FleetCart.data['attribute.values']) {
            var data = FleetCart.data['attribute.values'][k];
            $("select[name='values["+k+"][type]']").val(data.type).change();
        }
    </script>
@endpush
