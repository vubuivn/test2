<div class="container">
<section class="about-1 sec-b">
<div class="max670">
<div class="mb-10 w6">{{$sub_title}}</div>
<h2>{{$title}}</h2>
{!!$description!!}
</div>

<div class="row grid-space-0 mb-20">
@foreach($images as $key=>$value)
<div class="col-sm-6"><div class="tRes_90">
<img class="lazy-hidden" data-lazy-type="image"   data-lazy-src="{{$value->file}}"   alt=""></div></div>
@endforeach
</div>
</section>
</div>