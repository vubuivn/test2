<html>
<body>
@php

    @endphp
<table>
    <thead>
    <tr>
        <th>Mã sản phẩm (SKU)</th>
        <th>Mã nhóm</th>
        <th>Dài (cm)</th>
        <th>Rộng (cm)</th>
        <th>Cao (cm)</th>
        <th>Thể tích</th>
        <th>Feature</th>
        <th>Giá</th>
        @foreach($attribute as $attr)
            <th>{{$attr->name}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($product as $item)
        <tr>
            <td>{{ $item->sku }}</td>
            <td>{{ $item->group->code ?? null }}</td>
            <td>{{ $item->long }}</td>
            <td>{{ $item->wide }}</td>
            <td>{{ $item->high }}</td>
            <td>{{ $item->volume }}</td>
            <td>{{ $item->is_active ?: 0 }}</td>
            <td>{{ $item->selling_price }}</td>
            @foreach($attribute as $attr)
                @php
                    $params = '';
                    $product_attr = $item->attributes()->where('attribute_id',$attr->id)->first();
                    if($product_attr) {
                        foreach ($product_attr->values as $value){
                            if($params != ''){
                                $params .= ',';
                            }
                            $params .= $value->attributeValue->code;
                        }
                    }
                @endphp
                <td>{{$params}}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
