<?php
function get_product_attribute ($group, $attribute = [])
{
    $productsAttribute = $group->products[0]->attributes()
        ->whereIn(
            'attribute_id',
            [
                env('DEFAULT_MATERIAL_ID'),
                env('DEFAULT_COLOR_ID')
            ]
        )
        ->get();
    $results           = [];
    foreach ($productsAttribute as $item) {
        $value                        = $item->values[0]->attributeValue ?? null;
        $results[$item->attribute_id] = (object)[
            'name'  => $value->name ?? null,
            'key'   => $value->key ?? null,
            'code'  => $value->code ?? null,
            'other' => $value->other ?? null,
            'value' => $value->value ?? null,
        ];
    }
    return $results;
}

function format_product_attributes($attributes, $idOrCode = 'code')
{
    $formattedAttributes = [];
    foreach ($attributes AS $attr) {
        $attribute = [
            'name' => $attr->name,
            'values' => []
        ];

        foreach ($attr->values AS $val) {
            $attribute['values'][] = [
                'value' => $val->$idOrCode,
                'text' => $val->value
            ];
        }

        $formattedAttributes[(string) $attr->id] = $attribute;
    }

    return $formattedAttributes;
}

function format_assoc_product_attributes($attributes, $idOrCode = 'code')
{
    $formattedAttributes = [];
    foreach ($attributes AS $attr) {
        $attribute = [
            'name' => $attr->name,
            'values' => []
        ];
        
        foreach ($attr->values AS $val) {
            $attribute['values'][$val->$idOrCode] = $val->value;
        }

        $formattedAttributes[(string) $attr->id] = $attribute;
    }

    return $formattedAttributes;
}
