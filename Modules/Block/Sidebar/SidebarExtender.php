<?php

namespace Modules\Block\Sidebar;

use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{

    /**
     * @param \Maatwebsite\Sidebar\Menu $menu
     * @return \Maatwebsite\Sidebar\Menu
     */
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.content'), function (Group $group) {
            $group->item(trans('page::sidebar.pages'), function (Item $item) {
                $item->icon('fa fa-file');
                $item->weight(25);
                $item->authorize(
                );
                $item->item(trans('block::blocks.block'), function (Item $item) {
                    $item->weight(10);
                    $item->route('admin.blocks.index');
                    $item->authorize(
                        $this->auth->hasAccess('admin.blocks.index')
                    );
                });
                 $item->item(trans('block::blocks.tags'), function (Item $item) {
                    $item->weight(15);
                    $item->route('admin.blocks.block_tags');
                    $item->authorize(
                        $this->auth->hasAccess('admin.blocks.index')
                    );
                });
            });
        });
    }
}
