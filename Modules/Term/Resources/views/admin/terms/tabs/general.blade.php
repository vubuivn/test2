{{ Form::text('name', trans('term::attributes.name'), $errors, $term, ['labelCol' => 2, 'required' => true]) }}
{{ Form::wysiwyg('body', trans('term::attributes.body'), $errors, $term, ['labelCol' => 2, 'required' => true]) }}

<div class="row">
    <div class="col-md-8">
        {{ Form::checkbox('is_active', trans('term::attributes.is_active'), trans('term::terms.form.enable_the_term'), $errors, $term) }}
    </div>
</div>
