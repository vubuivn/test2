<div class="shipping-address clearfix {{ old('ship_to_a_different_address') ? '' : 'hide' }}">
    <h6>{{ trans("storefront::checkout.tabs.shipping_address") }}</h6>
    <div class="row">
        <div class="col-lg-6">
            <div class="rowblock {{ $errors->has('shipping.first_name') ? 'has-error': '' }}">
                <label for="firstname">
                    {{ trans('storefront::checkout.tabs.attributes.shipping.last_name') }}
                    <span>*</span>
                </label>
                <input type="text" class="form-control input"  name="shipping[last_name]"
                    placeholder="{{ trans('storefront::checkout.tabs.attributes.shipping.last_name') }}"
                    value="{{ isset($my) ? $my->last_name : old('shipping.last_name') }}">
                {!! $errors->first('shipping.last_name', '<span class="error-message">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-6">
            <div class="rowblock">
                <label for="lastname">
                    {{ trans('storefront::checkout.tabs.attributes.shipping.first_name') }}
                    <span>*</span>
                </label>
                <input type="text" class="form-control input" name="shipping[first_name]"
                    placeholder="{{ trans('storefront::checkout.tabs.attributes.shipping.first_name') }}"
                    value="{{ isset($my) ? $my->first_name : old('shipping.first_name') }}">
                {!! $errors->first('shipping.first_name','<span class="error-message">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-6">
            <div class="rowblock">
                <label for="shipping-state">
                    {{ trans('storefront::checkout.tabs.attributes.shipping.state') }}
                    <span>*</span>
                </label>
               <select name="shipping[state]" class="custom-select-black " id="shipping-state">
                       <option value="">{{ trans("storefront::checkout.please_select") }}</option>
                       @foreach ($states AS $id => $state)
                        <option value="{{ $state }}" data-id="{{ $id }}" {{ (isset($my) && $my->city_id == $id) ? 'selected' : $state == old('shipping.state') ? 'selected' : '' }}
                        >{{ $state }}</option>
                       @endforeach
               </select>
               {!! $errors->first('shipping.state','<span class="error-message">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-6">
            <div class="rowblock">
                <label for="shipping-city">
                    {{ trans('storefront::checkout.tabs.attributes.shipping.city') }}
                    <span>*</span>
                </label>
               <select name="shipping[city]" class="custom-select-black " id="shipping-city">
                       <option value="">{{ trans("storefront::checkout.please_select") }}</option>
               </select>
               {!! $errors->first('shipping.city','<span class="error-message">:message</span>') !!}
            </div>
        </div>
           
        <div class="col-lg-12">
            <div class="rowblock">
                <label for="address">
                       {{ trans('storefront::checkout.tabs.attributes.shipping.address_1') }}
                       <span>*</span>
                </label>
                <input type="text" class="form-control input" name="shipping[address_1]"
                    placeholder="{{ trans('storefront::checkout.tabs.attributes.shipping.address_1') }}"
                    value="{{ isset($my) ? $my->address : old('shipping.address_1') }}">
                {!! $errors->first('shipping.address_1','<span class="error-message">:message</span>') !!}
            </div>
        </div>
        <div class="col-lg-12">
            <div class="rowblock">
                <label for="address">
                       {{ trans('storefront::checkout.tabs.attributes.shipping.phone') }}
                       <span>*</span>
                </label>
                <input type="text" class="form-control input" name="shipping[phone]"
                    placeholder="{{ trans('storefront::checkout.tabs.attributes.shipping.phone') }}"
                    value="{{ isset($my) ? $my->phone : old('shipping.phone') }}">
                {!! $errors->first('shipping.phone','<span class="error-message">:message</span>') !!}
            </div>
        </div>
    </div>

 
</div>
