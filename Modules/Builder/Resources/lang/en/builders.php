<?php

return [
    'builder' => 'Form builder',
    'builders' => 'Form builders',
    'form-data' => 'Form data',
    'embedded' => 'Embedded form',
    'reply' => 'Response mail',
    'noti' => 'Notification mail',
    'table' => [
        'name' => 'Name',
    ],
    'tabs' => [
        'group' => [
            'page_information' => 'Form Builder Infortmation',
        ],
        'general' => 'General',
        'reply' => 'Reply',
        'elements' => 'Redirect after submit',
        'captcha' => 'Captcha',
        'seo' => 'SEO',
    ],
    'form' => [
        'enable_the_page' => 'Enable the form builder',
    ],
];
