<section class="block-single-3 sec-b group-ef lazy-hidden">
    <div class="container">
        <div class="item">
            <div><i class="icon-mail"></i></div>
            <p class="desc">{{ $description }}</p>
            
            <link rel="stylesheet" type="text/css" href="https://aconceptest.mangoads.com.vn/modules/common/css/formeo.min.css" />
            <form method="POST" class="add-email-info" action=https://aconceptest.mangoads.com.vn/builder/7/embedded-form enctype="multipart/form-data" onsubmit="return get_action()">
            <input type="hidden" name="_token" value="6e6LkLcKoNLQWtPMZxYOpgTPx9Rxc9GYdfo1W5yQ">
            <div class="formeo-render formeo formeo-rendered-0" id="f-09637a3b-bda9-411f-a74c-65757ccf5a5d">
                <div class="formeo-stage" id="f-cfb549d8-3466-499c-a3a5-a2737ed89089">
                    <div class="formeo-row-wrap" id="a3baf7f5-830f-4e1d-8b8e-331e922ae35c">
                        <div class="formeo-row" id="f-32bc4de3-13a6-46be-9f64-5059d9975a82">
                            <div class="formeo-column" id="f-9de8db9c-ee40-447e-8389-8791118b9efa" style="width: 100%;">
                                <div class="f-field-group">
                                    <input name="mg_22_field" class="custom-email" type="email" required="true" id="mg_22_field" placeholder="Email của bạn...">
                                </div>
                                <div class="f-field-group">
                                     <button id="94fcc84e-ac5a-481e-a439-09e3a7b35c9a">Đăng ký</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>

            <div class="img">
                @if ($image)
                <img class="lazy-hidden" data-lazy-type="image" src="/assets/images/image.svg"
                    data-lazy-src="{{ $image->file }}"
                    alt="{{ $image->caption_1 }}" />
                @endif
            </div>
        </div>
    </div>
</section>
