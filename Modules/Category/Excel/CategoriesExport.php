<?php

namespace Modules\Category\Excel;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CategoriesExport implements FromView
{
    protected $categories;

    public function __construct($categories)
    {
        $this->categories = $categories;
    }

    public function view(): View
    {
        return view('category::admin.categories.excel.list', [
            'category' => $this->categories
        ]);
    }
}