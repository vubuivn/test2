<?php

namespace Modules\Term\Entities;

use Modules\Support\Eloquent\TranslationModel;

class TermTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
    public function __construct()
    {
        if (config('mango.translation_table')){
        parent::__construct();
        $this->table = config('mango.translation_table');
        }
    }
}
