<?php

namespace Modules\News\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\News\Entities\News;
use Modules\CategoryNews\Entities\CategoryNews;
use Modules\News\Http\Requests\SaveNewsRequest;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public $paged = 24;

    public function index(Request $request){
        $category = CategoryNews::where('slug','tin-tuc')->firstOrFail();
        $posts =[];
        if($category){
            $posts = News::where('category_news_id',$category->id)->orderBy('id','desc')->paginate($this->paged);
        }
        if ($request->ajax()) {
            $total = News::count();
            $page = round($request->page/$this->paged);
            $skip = $this->paged*$page;
            if($request->page < $this->paged){
                return 'limit';  
            }
            if($skip < $total){
                $posts = News::where('category_news_id',$category->id)->orderBy('id','desc')->skip((int)$skip)->take($this->paged)->get();
                return view('public.news.ajax.item',compact('posts','category'));
            }
            return 'limit';
            
        }
        return view('public.news.index',compact('category','posts'));
    }

    public function detail($slug){
    	$post = News::where('slug',$slug)->firstOrFail();
    	$category = CategoryNews::where('id',$post->category_news_id)->first();
    	$related = [];
    	if($category){
    		$related = News::where('category_news_id',$category->id)->where('id','<>',$post->id)->limit(3)->get();
    	}
 		
    	return view('public.news.detail',compact('post','category','related'));
    }

}
