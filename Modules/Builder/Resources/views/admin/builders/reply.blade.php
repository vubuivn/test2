@extends('admin::layout')

@component('admin::components.builder.header')
    @slot('title', optional($builder)->name.' - '.trans('builder::builders.reply', ['resource' => trans('builder::builders.builder')]))

    <li><a href="{{ route('admin.builders.index') }}">{{ trans('builder::builders.builders') }}</a></li>
    <li class="active">{{ trans('builder::builders.reply', ['resource' => trans('builder::builders.builder')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.builders.reply.store',['id' => optional($builder)->id]) }}" class="form-horizontal" id="builder-create-form" novalidate>
        {{ csrf_field() }}
    <div class="box box-primary" style="padding: 10px">
        {{ Form::text('reply_subject', trans('builder::attributes.subject'), $errors, $reply, ['labelCol' => 2, 'required' => true]) }}
        {{ Form::text('reply_to', trans('builder::attributes.reply-to'), $errors, $reply, ['labelCol' => 2, 'required' => true]) }}
        {{ Form::wysiwyg('reply_body', trans('builder::attributes.message-body'), $errors, $reply, ['labelCol' => 2, 'required' => true]) }}

        <div class="form-group ">
            <label for="name" class="col-md-2 control-label text-left">Fields</label>
            <div class="col-md-10">
                <table class="table">
                    @foreach($builder->elements as $element)
                        <tr><td><span>{{ $element->name }}</span>: <span>[{{ $element->element_name }}]</span></td></tr>
                    @endforeach
                </table>

            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group ">
                    <label for="is_active" class="col-md-3 control-label text-left">Status</label>
                    <div class="col-md-9">
                        <div class="checkbox">
                            <input type="checkbox" name="is_active" class="" id="is_active" value="1" @if (optional($reply)->is_active)
                                checked
                            @endif><label for="is_active">Enable the form reply</label></div></div></div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn btn-primary" data-loading="">
                    Save
                </button>
            </div>
        </div>
    </div>
    </form>
@endsection
@push('styles')
    <style>

    </style>
@endpush