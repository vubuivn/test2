<div class="row {{$i%2 == 0 ? 'column-reverse' : '' }}">
    @if($i%2 == 0) 
        <div class="col-md-4">
            <div class="meeting-design">
                <h3 class="title"><a href="{{route('designer.detail',$designer->slug)}}">{{$designer->name}}</a></h3>
                <div class="desc-info">
                {!!$designer->short_description!!}
                </div>
                <div class="more">
                    <a href="{{route('designer.detail',$designer->slug)}}" >{{trans('storefront::storefront.news.view_more')}} <i class="icon-arrow-14"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-8 img-info">
            <a href="{{route('designer.detail',$designer->slug)}}" class="img  tRes_80">
                <img class="lazy-hidden " data-lazy-type="image" src="assets/images/image.svg" data-lazy-src="{{$designer->featureImage->path}}"  alt="" />      
            </a>  
        </div>
    @else
        <div class="col-md-8 img-info">
            <a href="{{route('designer.detail',$designer->slug)}}" class="img  tRes_80">
                <img class="lazy-hidden " data-lazy-type="image" src="assets/images/image.svg" data-lazy-src="{{$designer->featureImage->path}}"  alt="" />      
            </a>  
        </div>
        <div class="col-md-4">
            <div class="meeting-design">
                <h3 class="title"><a href="{{route('designer.detail',$designer->slug)}}">{{$designer->name}}</a></h3>
                <div class="desc-info">
                {!!$designer->short_description!!}
                </div>
                <div class="more">
                    <a href="{{route('designer.detail',$designer->slug)}}" >{{trans('storefront::storefront.news.view_more')}} <i class="icon-arrow-14"></i></a>
                </div>
            </div>
        </div>
    @endif
</div>