<?php

namespace Modules\Career\Admin;

use Modules\Admin\Ui\AdminTable;

class CareerTable extends AdminTable
{
    /**
     * Raw columns that will not be escaped.
     *
     * @var array
     */
    protected $rawColumns = ['price'];

    /**
     * Make table response for the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function make()
    {
        $cities = \DB::table('cities')->get()->toArray();
        $arrCities = []
        foreach($cities as $city){
            $arrCities[$city->id] = $city;
        }
        return $this->newTable()
            ->editColumn('city', function ($entity) {

                return $arrCities[$entity->city_id]->title;
            })
            ->editColumn('status', function ($entity) {
                return product_status($entity->is_active);
            })

    }
}
