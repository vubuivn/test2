<?php

function show_block_career ($block)
{
    $content = json_decode($block->content);
    $values  = block_values($block->block_id);
    $result  = [];
    foreach ($values as $value) {
        switch ($value->type) {
            case 'single-image':
                if(isset($content->{$value->key})) {
                    $result[$value->key] = $content->{$value->key}[0] ?? null;
                    if($result[$value->key]->file && $result[$value->key]->file != null){
                        $result[$value->key]->file = \Modules\Media\Entities\File::find($result[$value->key]->file)->path ?? null;
                    }
                }else{
                    $result[$value->key] = null;
                }
                break;
            case 'multiple-images':
                $data = null;
                if(isset($content->{$value->key})) {
                    $data = $content->{$value->key} ?? null;
                    if($data){
                        $new_data = [];
                        foreach ($data as $key => $item){
                            if($item->file != null) {
                                $item->file = \Modules\Media\Entities\File::find($item->file)->path;
                            }
                            $new_data[] = $item;
                        }
                        $data = $new_data;
                    }
                }
                $result[$value->key] = $data;
                break;
            case 'group':
                if(isset($content->{$value->key})) {
                    $result[$value->key] = group_block($content->{$value->key});
                }else{
                    $result[$value->key] = null;
                }
                break;
            default:
                $result[$value->key] = $content->{$value->key} ?? null;
                break;
        }
    }
    return $result;
}
