<?php

namespace Modules\Element\Entities;

use Modules\Builder\Entities\Builder;
use Modules\Support\Eloquent\Model;
use Modules\Element\Admin\ElementTable;
use Modules\Support\Eloquent\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Element extends Model
{
    use Translatable;

    /**
     * Available element types.
     *
     * @var array
     */
    const types = [];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['builder_id', 'element_stages', 'type', 'is_required', 'is_global', 'position'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_required' => 'boolean',
        'is_global' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $translatedAttributes = ['name'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

//        static::saved(function ($element) {
//            $element->saveValues(request('values', []));
//        });
        static::creating(function($element) {
            $element->element_name = 'mg_'.(self::max('id')+1).'_field';
        });
    }

    /**
     * Get the values for the element.
     *
     * @return mixed
     */
    public function builder()
    {
        return $this->belongsTo(Builder::class);
    }

    /**
     * Get the values for the element.
     *
     * @return mixed
     */
//    public function values()
//    {
//        return $this->hasMany(ElementValue::class)->orderBy('position');
//    }

    /**
     * Scope a query to only include global elements.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGlobals($query)
    {
        return $query->where('is_global', true);
    }

    /**
     * Get table data for the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function table()
    {
        return new ElementTable($this->newQuery()->globals());
    }

    /**
     * Save values for the element.
     *
     * @param array $values
     * @return void
     */
    public function saveValues($values = [])
    {
        $ids = $this->getDeleteCandidates($values);

        if ($ids->isNotEmpty()) {
            $this->values()->whereIn('id', $ids)->delete();
        }

        foreach (array_reset_index($values) as $index => $attributes) {
            $attributes = $attributes + ['position' => $index];

            $this->values()->updateOrCreate([
                'id' => array_get($attributes, 'id'),
            ], $attributes);
        }
    }

    private function getDeleteCandidates($values)
    {
        return $this->values()
            ->pluck('id')
            ->diff(array_pluck($values, 'id'));
    }
}
