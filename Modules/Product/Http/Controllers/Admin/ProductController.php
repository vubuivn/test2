<?php

namespace Modules\Product\Http\Controllers\Admin;

use Carbon\Carbon;
// use FleetCart\Exports\ProductsExport;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

use Modules\Attribute\Entities\AttributeValue;
// use Modules\Attribute\Entities\Attribute;
// use Modules\Category\Entities\Category;

use Modules\Attribute\Listeners\SaveProductAttributes;
use Modules\Product\Entities\Product;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Product\Http\Requests\SaveProductRequest;
use Excel;
use Modules\Product\Excel\ProductsExport;
use Modules\Product\Excel\ProductsExportAll;
use Modules\Product\Excel\ProductsImport;
use Modules\Product\Excel\AttributesImport;
use Modules\Product\Excel\PromotionImport;
use DB;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class ProductController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'product::products.product';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'product::admin.products';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveProductRequest::class;

    public function store()
    {
        $this->disableSearchSyncing();

        Product::saved(SaveProductAttributes::class);
        $requestData = $this->getRequest('store')->all();
        $requestData['product_attributes'] = $this->generateProductAttributeStr($requestData);

        $entity = $this->getModel()->create($requestData);

        $this->searchable($entity);

        if (
        method_exists(
            $this,
            'redirectTo'
        )
        ) {
            return $this->redirectTo($entity);
        }

        return redirect()
            ->route("{$this->getRoutePrefix()}.index")
            ->withSuccess(
                trans(
                    'admin::messages.resource_saved',
                    ['resource' => $this->getLabel()]
                )
            );
    }

    private function generateProductAttributeStr($requestData)
    {
        $productAttributes = [];
        foreach ($requestData['attributes'] AS $attr) {
            $attrCodes = [];
            $attrValues = AttributeValue::whereIn('id', $attr['values'])->select('code')->get();
            foreach ($attrValues AS $item) {
                $attrCodes[] = $item->code;
            }
            
            $productAttributes[] = $attr['attribute_id'] . ':' . implode(',', $attrCodes);
        }

        return implode(';', $productAttributes);
    }

    public function update($id)
    {
        $entity = $this->getEntity($id);

        $requestData = $this->getRequest('update')->all();
        $requestData['product_attributes'] = $this->generateProductAttributeStr($requestData);

        $this->disableSearchSyncing();
        Product::saving(SaveProductAttributes::class);
        $entity->update($requestData);

        $this->searchable($entity);

        if (
        method_exists(
            $this,
            'redirectTo'
        )
        ) {
            return $this->redirectTo($entity)
                        ->withSuccess(
                            trans(
                                'admin::messages.resource_saved',
                                ['resource' => $this->getLabel()]
                            )
                        );
        }

        return redirect()
            ->route("{$this->getRoutePrefix()}.index")
            ->withSuccess(
                trans(
                    'admin::messages.resource_saved',
                    ['resource' => $this->getLabel()]
                )
            );
    }

    public function syncProductAttribute()
    {
        ini_set('memory_limit', -1);
        set_time_limit(-1);

        echo "START \n";

        DB::table('product_attributes_view')->orderBy('product_id')->chunk(5000, function($rows){
            foreach ($rows AS $row) {
                // DB::table('products')->where('id', $row->product_id)
                //     ->update(['product_attributes' => $row->product_attributes]);

                DB::update("UPDATE products SET product_attributes='{$row->product_attributes}' WHERE id={$row->product_id}");
            }
        });

        echo 'Done';
    }

    /**
     * need to create sample data for 3 table:
     * product_attributes, product_attribute_values & attribute_category
     */
    public function generateSampleProductAttribute()
    {
        ini_set('memory_limit', -1);
        echo 'start <br>';
        $productIds = DB::table('products')->select('id')->get();
        $categoryIds = DB::table('categories')->select('id')->get();
        
        // calculate attribute_value ids by attribute_id
        $attributes = DB::table('attributes')->select('id')->get();
        foreach ($attributes AS $attr) {
            $attrValIds = DB::table('attribute_values')->where('attribute_id', $attr->id)
                            ->select('id')->get()->pluck('id')->toArray();
            $attrValueIds[$attr->id] = $attrValIds;   
        }
        
        $productAttributesToInsert = [];
        $attributeCategoryToInsert = [];
        $productAttributeValuesToInsert = [];

        // each product has all attributes with a attribute_value_id
        foreach ($attrValueIds AS $attrId => $attrValIds) {
            foreach ($productIds AS $productId) {
                $productAttributesToInsert[] = [
                    'product_id' => $productId->id,
                    'attribute_id' => $attrId
                ];

                $randomKey = array_rand($attrValIds);
                $attrValueId = $attrValIds[$randomKey];
                $productAttributeValuesToInsert[] = [
                    'product_id' => $productId->id,
                    'attribute_id' => $attrId,
                    'attribute_value_id' => $attrValueId
                ];
            }
        }

        foreach ($categoryIds AS $catId) {
            foreach ($attrValueIds AS $attrId => $attrValIds) {
                $attributeCategoryToInsert[] = [
                    'category_id' => $catId->id,
                    'attribute_id' => $attrId
                ];
            }
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('product_attribute_values')->truncate();
        DB::table('product_attributes')->truncate();
        DB::table('attribute_categories')->truncate();

        DB::table('attribute_categories')->insert($attributeCategoryToInsert);
        
        // dd($productAttributesToInsert[0]);
        foreach (array_chunk($productAttributesToInsert, 1000) AS $arr) {
            DB::table('product_attributes')->insert($arr);
        }
        
        foreach (array_chunk($productAttributeValuesToInsert, 1000) AS $arr) {
            DB::table('product_attribute_values')->insert($arr);
        }
        
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        echo 'done';
    }

    public function exportAllProductsExcel()
    {
        try {
            ini_set('memory_limit', -1);

            $name = "product_" . time();
            $filePath = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))->path('products/' . $name . '.xlsx');
            // export all products to the excel file
            new ProductsExportAll($filePath);
            $fileUrl = route('admin.products.downloadProductsFile.excel', [
                'file' => $name . '.xlsx'
            ]);
            $response = [
                'fileUrl' => $fileUrl,
            ];

            return response()->json($response);
        } catch (\Exception $e) {

            echo $e->getMessage();
            die;
        }
    }

    public function downloadExcelFile($fileName)
    {
        try {
            $filePath = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))->path('products/' . $fileName);
            $binaryContent = file_get_contents($filePath);
            unlink($filePath);
    
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $fileName);
            header('Cache-Control: cache, must-revalidate');
            echo $binaryContent;
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }
    }

    public function exportExcel()
    {
        ini_set('memory_limit', -1);

        try {
            $name     = "product_" . time();
            $request = (Request::capture())->all();
            $ids = array_get($request, 'ids', null);
            $ids = explode(',', $ids);
            
            Excel::store(new ProductsExport($ids), 'products/' . $name . '.xlsx');
            
            $filePath = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))->path('products/' . $name . '.xlsx');
            $binaryContent = file_get_contents($filePath);
            unlink($filePath);
            $response = [
                'name' => $name,
                'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"
                    . base64_encode($binaryContent)
            ];

            return response()->json($response);
        } catch (\Exception $e) {

            echo $e->getMessage();
            die;
        }
    }

    public function downloadErrorProductsFile()
    {
        $userId = \Auth::user()->id;
        $errorProducts = session('errorProducts_' . $userId);

        if (empty($errorProducts)) {
            echo 'File not found'; die;
        }

        $name = "products_error_" . time();
        $filePath = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))->path($name . '.xlsx');
        // export products to the excel file
        new ProductsExportAll($filePath, $errorProducts);
        
        $binaryContent = file_get_contents($filePath);
        unlink($filePath);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $name . '.xlsx"');
        header('Cache-Control: cache, must-revalidate');
        echo $binaryContent;
        die;
    }

    public function importExcel(Request $request)
    {
        ini_set('memory_limit', -1);

        if ($request->hasFile('import_file')) {
            $file = $request->file('import_file');
            Storage::putFile('products', $file);
            $path = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))
                    ->path('products/' . $file->hashName());

            $importType = array_get($request, 'import_type', 0);
            // $importType = 2;
            
            if ($importType == 0) {
                return $this->_importProduct($path);
            } elseif ($importType == 1) {
                return $this->_importPromotion($path);
            } else {
                return $this->_importAttribute($path);
            }
        }
    }

    private function _importProduct($path)
    {
        try {
            $importObj = new ProductsImport();
            $result = $importObj->import($path);

            if (!empty($result['errorProducts'])) {
                $userId = \Auth::user()->id;
                session(['errorProducts_' . $userId => $result['errorProducts']]);
                $result['errorFilePath'] = route('admin.products.downloadErrorProductsFile.excel');
                $result['errorFileName'] = basename($result['errorFilePath']);
            }
            unset($result['errorProducts']);
            // unlink($path);
    
            return response()->json($result);
        } catch (\Exception $e) {

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    private function _importPromotion($path)
    {
        try {
            $importObj = new PromotionImport();
            $result = $importObj->import($path);

            if (!empty($result['errorProducts'])) {
                $userId = \Auth::user()->id;
                session(['errorProducts_' . $userId => $result['errorProducts']]);
                $result['errorFilePath'] = route('admin.products.downloadErrorProductsFile.excel');
                $result['errorFileName'] = basename($result['errorFilePath']);
            }
            unset($result['errorProducts']);
            unlink($path);
    
            return response()->json($result);
        } catch (\Exception $e) {

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    private function _importAttribute($path)
    {
        try {
            $importObj = new AttributesImport();
            $result = $importObj->import($path);
            unlink($path);

            if (!empty($result['errorAttributes'])) {
                dd($result['errorAttributes']);
            }
    
            return response()->json($result);
        } catch (\Exception $e) {

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }


    // USE forceDelete to delete solfDelete
    public function destroy($ids)
    {
        $this->getModel()
            ->withoutGlobalScope('active')
            ->whereIn('id', explode(',', $ids))
            ->forceDelete();
    }
}
