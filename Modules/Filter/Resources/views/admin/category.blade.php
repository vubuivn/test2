@php
    if($entity->filter->entity_filter_value === null){
        $entity_filter_value = [];
    }else{
        $entity_filter_value = json_decode($entity->filter->entity_filter_value);
    }
@endphp
@include('filter::admin._form')
