<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
// use Modules\Attribute\Entities\AttributeValue;
// use Modules\Group\Entities\Group;
// use Modules\Product\Entities\Product;
// use Modules\Product\Events\ProductViewed;
// use Modules\Product\Filters\ProductFilter;
// use Modules\Product\Events\ShowingProductList;
use Modules\Product\Http\Middleware\SetProductSortOption;
use Illuminate\Support\Facades\Cache;
use Modules\Category\Entities\Category;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct ()
    {
        $this->middleware(SetProductSortOption::class)
            ->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Modules\Product\Entities\Group        $model
     * @param \Modules\Product\Filters\ProductFilter $productFilter
     *
     * @return \Illuminate\Http\Response
     */
    public function category ($slug1, $slug2 = null, $slug3 = null)
    {
        $slug = $slug3 ?? $slug2 ?? $slug1;
        $isAjaxRequest = Request::capture()->all()['ajax'] ?? null;

        if(Cache::tags(['categories'])->has('category_detail_'.$slug)){
            $category = Cache::tags(['categories'])->get('category_detail_'.$slug);
        }else{
            $category = Cache::tags(['categories'])
              ->rememberForever('category_detail_'.$slug,function () use($slug){
                  return Category::where('slug',$slug)->firstOrFail();
            });
        }
        // $category = Category::where('slug',$slug)->firstOrFail();


        if ($isAjaxRequest) {
            // if($isAjaxRequest == 1){
            //     $isAjaxRequest++;   
            // }
            return view(
                'public.products.listing.group-list-ajax', [
                    'slug' => $slug,
                    'category' => $category
                ]);
        }
        return view(
            'public.products.index', [
                'slug' => $slug,
                'category' => $category
            ]
        );
    }

    public function index (Request $request)
    {
        $isAjaxRequest = $request->all()['ajax'] ?? null;

        if ($isAjaxRequest) {
            return view('public.products.listing.group-list-ajax');
        }

        return view('public.products.index');
    }

    /**
     * Show the specified resource.
     *
     * @param string $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show ($slug)
    {
        return view(
            'public.products.show',
            [
                'slug' => $slug
            ]
        );
    }

    /**
     * Get reviews for the given product.
     *
     * @param \Modules\Product\Entities\Product $product
     *
     * @return \Illuminate\Support\Collection
     */
    private function getReviews ($product)
    {
        if (!setting('reviews_enabled')) {
            return collect();
        }

        return $product->reviews()
            ->paginate(
                15,
                ['*'],
                'reviews'
            );
    }

    public function categoryAjaxImage(Request $request){
        if($request->has('ids')){
            $files = \DB::table('files')->leftJoin('entity_files','files.id','=','entity_files.file_id')->whereIn('entity_files.entity_id',$request->ids)->where('entity_files.entity_type','Modules\Category\Entities\Category')->where('entity_files.zone','feature_image')->get();

            $images = [];
            if($files){
                foreach($files as $file){
                    $images[$file->entity_id] = $file->path;
                } 
            }

            return $images;
        }
        return '';
    }

    public function categorysale($slug1, $slug2 = null, $slug3 = null)
    {
        $slug = $slug3 ?? $slug2 ?? $slug1;
        $isAjaxRequest = Request::capture()->all()['ajax'] ?? null;

        if(Cache::tags(['categories'])->has('category_detail_'.$slug)){
            $category = Cache::tags(['categories'])->get('category_detail_'.$slug);
        }else{
            $category = Cache::tags(['categories'])
              ->rememberForever('category_detail_'.$slug,function () use($slug){
                  return Category::where('slug',$slug)->firstOrFail();
            });
        }

        // $category = Category::where('slug',$slug)->firstOrFail();


        if ($isAjaxRequest) {
            // if($isAjaxRequest == 1){
            //     $isAjaxRequest++;   
            // }
            return view(
                'public.products.listing.group-list-ajax', [
                    'slug' => $slug,
                    'category' => $category
                ]);
        }
        return view(
            'public.products.index', [
                'slug' => $slug,
                'category' => $category
            ]
        );
    }
}
