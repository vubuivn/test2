<?php

namespace Modules\Page\Providers;

use Modules\Page\Admin\PageTabs;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\AddsAsset;
use Modules\Support\Traits\LoadsConfig;
use Modules\Admin\Ui\Facades\TabManager;

class PageServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot ()
    {
        $this->addAdminAssets(
            'admin.pages.(create|edit)',
            [
                'admin.media.css',
                'admin.media.js',
                'admin.option.css',
                'admin.option.js',
                'admin.page.js',
                'admin.storefront.css',
                'admin.storefront.js',
            ]
        );
        TabManager::register(
            'pages',
            PageTabs::class
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register ()
    {
        $this->loadConfigs(
            [
                'assets.php',
                'permissions.php'
            ]
        );
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
