@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => trans('user::users.customer')]))
    @slot('subtitle', $user->full_name)

    <li><a href="{{ route('admin.customers.index') }}">{{ trans('user::users.csutomers') }}</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => trans('user::users.user')]) }}</li>
@endcomponent

@section('content')
<div class="box box-primary">
	<div class="box-body">
	    <form method="POST" action="{{ route('admin.customers.update', $user) }}" class="form-horizontal" id="user-edit-form" novalidate>
	        {{ csrf_field() }}
	        {{ method_field('put') }}

	        <div class="row">
	        	<div class="col-md-8" style="margin-bottom: 20px"><h2>USER {{$user->first_name}}</h2></div>
	        	<p></p>
			    <div class="col-md-8">
			        {{ Form::text('first_name', trans('user::attributes.users.name'), $errors, $user, ['required' => true]) }}

			        {{ Form::email('email', trans('user::attributes.users.email'), $errors, $user, ['required' => true]) }}

			        {{ Form::text('phone', trans('user::attributes.users.phone'), $errors, $user) }}

			        {{ Form::email('birthday', trans('user::attributes.users.birthday'), $errors, $user, [ 'class' => 'datetime-picker']) }}
			        <div class="form-group ">
				        <label class="col-md-3 control-label text-left">{{trans('user::attributes.users.gender')}}</label>
				        <div class="col-md-9">
					        <input type="radio" name="gender" value="0" @if($user->gender == 0 || $user->gender == null) checked @endif> &nbsp; <label>Male</label>
					        <input type="radio" name="gender" value="1" @if($user->gender == 1) checked @endif> &nbsp; <label>Female</label>
						</div>
					</div>


				        {{ Form::password('password', trans('user::attributes.users.new_password'), $errors) }}
				        {{ Form::password('password_confirmation', trans('user::attributes.users.new_password_confirmation'), $errors) }}
				        <div class="form-group ">
							<div class="col-md-offset-3 col-md-10">
						        <h4>{{ trans('user::users.or_reset_password') }}</h4>

						        <a href="{{ route('admin.users.reset_password', $user) }}" class="btn btn-primary btn-reset-password" data-loading>
						            {{ trans('user::users.send_reset_password_email') }}
						        </a>
							</div>
						</div>
				        	
				        {{ Form::select('roles', trans('user::attributes.users.roles'), $errors, $roles, $user, ['multiple' => true, 'required' => true, 'class' => 'selectize prevent-creation']) }}

				        @if (request()->routeIs('admin.customers.create'))
				            {{ Form::password('password', trans('user::attributes.users.password'), $errors, null, ['required' => true]) }}
				            {{ Form::password('password_confirmation', trans('user::attributes.users.password_confirmation'), $errors, null, ['required' => true]) }}
				        @endif

				        @if (request()->routeIs('admin.customers.edit'))
				            {{ Form::checkbox('activated', trans('user::attributes.users.activated'), trans('user::users.form.activated'), $errors, $user, ['disabled' => $user->id === $currentUser->id, 'checked' => old('activated', $user->isActivated())]) }}
				        @endif
					
					<div class="form-group ">
						<div class="col-md-offset-3 col-md-10">
						<button type="submit" class="btn btn-primary" data-loading="">
			                Save
			            </button>
			            </div>
		            </div>

			    </div>


			</div>
	    </form>
	</div>
</div>
@endsection

@include('user::admin.users.partials.shortcuts')
