<?php

namespace Modules\Term\Http\Controllers\Admin;

use Modules\Term\Entities\Term;
use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Term\Http\Requests\SaveTermRequest;
use Illuminate\Http\Request;
use Modules\Builder\Entities\Builder;

use Illuminate\Support\Facades\Storage;
use Modules\Builder\DataExportAll;

class TermController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Term::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'term::terms.term';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'term::admin.terms';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveTermRequest::class;

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function index(Builder $builder, Request $request)
    {
        $this->tradeModel($builder);
        $terms = Term::where('builder_id', $builder->id)->paginate(20);
        $html = new \Htmldom($builder->embedded);
        return view("{$this->viewPath}.index", ['builder' => $builder, 'terms' => $terms, 'html' => $html->find('label')]);
    }

    public function show($id)
    {
        $entity = $this->getEntity($id);
        $builder = $entity->builder;
        $this->tradeModel($builder);
        $entity = $this->getEntity($id);
        return view("{$this->viewPath}.show")->with([$this->getResourceName() => $entity, 'builder' => $builder]);
    }

    private function tradeModel($term, $push=null){
        config(['term' => $term]);
        config(['mango.translation_table' => "term{$term->id}_translations"]);
        if ($term->elements) {
            $term_element = $term->elements
                //->where('type', '<>', 'file')
                //->where('type', '<>', 'image')
                ->pluck('element_name')->toArray();
            if ($push)
                array_push($term_element, $push);

            config(['mango.translatedAttributes' => $term_element]);
        }
    }

    public function destroy($ids)
    {
        $builder = $this->getModel()->where('id', $ids)->first()->builder;
        $this->getModel()
            ->withoutGlobalScope('active')
            ->whereIn('id', explode(',', $ids))
            ->delete();
        return redirect()->route("admin.terms.builder", ['builder' => $builder]);
    }

    public function exportAllTermsExcel()
    {
        try {
            ini_set('memory_limit', -1);
            $id = request()->id;
            $name = "term_" . time();
            $filePath = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))->path('terms/' . $name . '.xlsx');
            // export all products to the excel file
            new DataExportAll($filePath,[],$id);
            $fileUrl = route('admin.terms.downloadTermsFile.excel', [
                'file' => $name . '.xlsx'
            ]);
            $response = [
                'fileUrl' => $fileUrl,
            ];

            return response()->json($response);
        } catch (\Exception $e) {

            echo $e->getMessage();
            die;
        }
    }

    public function downloadExcelFile($fileName)
    {
        try {
            $filePath = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))->path('terms/' . $fileName);
            $binaryContent = file_get_contents($filePath);
            unlink($filePath);
    
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $fileName);
            header('Cache-Control: cache, must-revalidate');
            echo $binaryContent;
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }
    }
}
