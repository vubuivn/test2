@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => trans('group::groups.group')]))
    @slot('subtitle', $group->name)

    <li><a href="{{ route('admin.groups.index') }}">{{ trans('group::groups.groups') }}</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => trans('group::groups.group')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.groups.update', $group) }}" class="form-horizontal" id="group-form"
          novalidate>
        {{ csrf_field() }}
        {{ method_field('put') }}
        {!! $tabs->render(compact('group')) !!}
    </form>
@endsection

@include('group::admin.groups.partials.shortcuts')