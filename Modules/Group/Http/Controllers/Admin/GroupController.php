<?php

namespace Modules\Group\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
// use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
// use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Group\Entities\Group;
use Modules\Group\Http\Requests\SaveGroupRequest;
use Modules\Group\Excel\GroupsExportAll;
use Modules\Group\Excel\GroupsImport;

class GroupController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Group::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'group::groups.group';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'group::admin.groups';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveGroupRequest::class;
    public function store()
    {
        $request           = $this->getRequest('store')
            ->all();
        $request['blocks'] = _checkBlocks($request['blocks'] ?? []);
        $this->disableSearchSyncing();
        $entity = $this->getModel()->create(
            $request
        );

        $this->searchable($entity);
        $entity->saveBlocks(array_get($request, 'blocks', []));
    }
    public function update($id)
    {
        $request           = $this->getRequest('update')
            ->all();
        $request['blocks'] = _checkBlocks($request['blocks'] ?? []);
        $entity = $this->getEntity($id);


        $this->disableSearchSyncing();

        $entity->update(
            $request
        );
        $this->searchable($entity);

        $entity->saveBlocks(array_get($request, 'blocks', []));
    }

    public function exportExcel()
    {
        try {
            $request = (Request::capture())->all();
            $ids = array_get($request, 'ids', null);
            $groupIds = $ids ? explode(',', $ids) : [];
            $name = "group_" . time();
            $filePath = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))->path($name . '.xlsx');
            // export products to the excel file
            new GroupsExportAll($filePath, $groupIds);
            $binaryContent = file_get_contents($filePath);
            unlink($filePath);
            
            return response()->json([
                'status' => true,
                'name' => $name,
                'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"
                    . base64_encode($binaryContent)
            ]);
        } catch (\Exception $e) {

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function importExcel(Request $request)
    {
        if ($request->hasFile('import_file')) {
            $file = $request->file('import_file');
            Storage::putFile(
                'group',
                $file
            );
            $path = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))
                                 ->path('group/' . $file->hashName());
            
            $importObj = new GroupsImport();
            $result = $importObj->import($path);
            unlink($path);
            
            if (!empty($result['errorGroups'])) {
                $name = "group_error_" . time();
                $filePath = Storage::disk(env('FILESYSTEM_DRIVER', 'local'))->path($name . '.xlsx');
                // export products to the excel file
                new GroupsExportAll($filePath, [], $result['errorGroups']);
                
                $binaryContent = file_get_contents($filePath);
                unlink($filePath);
    
                $result['errorFileName'] = $name;
                $result['errorFile'] = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"
                                        . base64_encode($binaryContent);
            }
            unset($result['errorGroups']);

            return response()->json($result);
        }
    }
    public function list(Request $request)
    {
        $query = $this->getModel()
                       ->search($request->get('query'))
                       ->query();
        $result = [
            'total_count' => $query->count(),
            'data' => $query->limit($request->get('limit', 10))
                            ->get()
        ];

        return json_encode($result);
    }

    // USE forceDelete to delete solfDelete
    public function destroy($ids)
    {
        $this->getModel()
            ->withoutGlobalScope('active')
            ->whereIn('id', explode(',', $ids))
            ->forceDelete();
    }
}
