<?php

return [
    'page' => 'Page',
    'pages' => 'Pages',
    'table' => [
        'name' => 'Name',
    ],
    'tabs' => [
        'group' => [
            'page_information' => 'Page Infortmation',
        ],
        'general' => 'General',
        'seo' => 'SEO',
        'block' => 'Block',
    ],
    'form' => [
        'enable_the_page' => 'Enable the page',
        'add_more_image' => 'Add more image',
        'banner_image' => 'Banner image',
    ],
    'expand_all' => 'Expand All',
    'collapse_all' => 'Collapse All',
    'template' => 'Template',
    'normal' => 'Normal',
    'has_sidebar' => 'Has sidebar',
    'questions' => 'Question',
];
