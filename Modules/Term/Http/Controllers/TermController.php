<?php

namespace Modules\Term\Http\Controllers;

use Modules\Term\Entities\Term;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Builder\Entities\Builder;

class TermController extends Controller
{
    protected $viewPath = 'term::admin.terms';
    private $limit = 15;

    public function api_show($id)
    {
        $entity = Term::find($id);
        $builder = $entity->builder;
        $this->tradeModel($builder);
        $entity = Term::find($id);
        return response()
            ->json($entity);
    }
    public function api_index(Builder $builder, Request $request){
        $this->tradeModel($builder);
        $terms = Term::where('builder_id', $builder->id)->jsonPaginate($this->limit);
        return $terms;
    }
    public function api_index_html(Builder $builder, Request $request){
        $this->tradeModel($builder);
        $terms = Term::where('builder_id', $builder->id)->paginate($this->limit);
        $html = new \Htmldom($builder->embedded);
        return view("{$this->viewPath}.presult", ['builder' => $builder, 'terms' => $terms, 'html' => $html->find('label')]);
    }
    private function tradeModel($term, $push=null){
        config(['term' => $term]);
        config(['mango.translation_table' => "term{$term->id}_translations"]);
        if ($term->elements) {
            $term_element = $term->elements
                //->where('type', '<>', 'file')
                //->where('type', '<>', 'image')
                ->pluck('element_name')->toArray();
            if ($push)
                array_push($term_element, $push);

            config(['mango.translatedAttributes' => $term_element]);
        }
    }
}
