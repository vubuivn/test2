<?php

namespace Modules\Designer\Providers;

use Modules\Designer\Admin\DesignerTabs;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\AddsAsset;
use Modules\Support\Traits\LoadsConfig;
use Modules\Admin\Ui\Facades\TabManager;

class DesignerServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot ()
    {
        $this->addAdminAssets(
            'admin.designers.(create|edit)',
            [
                'admin.media.css',
                'admin.media.js',
                'admin.option.css',
                'admin.option.js',
                'admin.designer.js',
                'admin.storefront.css',
                'admin.storefront.js',
            ]
        );
        TabManager::register(
            'designers',
            DesignerTabs::class
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register ()
    {
        $this->loadConfigs(
            [
                'assets.php',
                'permissions.php'
            ]
        );
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
