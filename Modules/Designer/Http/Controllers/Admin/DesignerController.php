<?php

namespace Modules\Designer\Http\Controllers\Admin;

use Modules\Designer\Entities\Designer;
use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Designer\Http\Requests\SaveDesignerRequest;

class DesignerController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Designer::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'designer::designers.designer';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'designer::admin.designers';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveDesignerRequest::class;

}
