<?php

namespace Modules\Career\Http\Controllers\Admin;

use Modules\Career\Entities\Career;
use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Career\Http\Requests\SaveCareerRequest;

class CareerController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Career::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'career::careers.career';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'career::admin.careers';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveCareerRequest::class;

}
