<div id="address" class="tab-pane active" role="tabpanel">
    <div class="form-address">
        <div class="personal-information @if($errors->any()){{'hide'}}@endif">
            <div class="col-lg-12">
                <h6>{{ trans('storefront::checkout.tabs.shipping_address') }}</h6>
            </div>
            <div class="basic_info">
                <div class="row">
                    <div class="col-md-4">
                        {{ trans('storefront::checkout.tabs.attributes.customer_name') }}
                    </div>
                    <div class="col-md-8">
                        {{$my->first_name}} {{$my->last_name}}
                    </div>
                    <div class="col-md-4">
                        {{ trans('storefront::checkout.tabs.attributes.customer_address') }}
                    </div>
                    <div class="col-md-8">{{$my->address}}</div>
                    <div class="col-md-4">
                        {{ trans('storefront::checkout.tabs.attributes.customer_phone') }}
                    </div>
                    <div class="col-md-8">{{$my->phone}}</div>
                    <div class="col-md-4">{{ trans('storefront::checkout.tabs.payment.payment_method') }}</div>
                    <div class="col-md-8"> Ship COD</div>
                </div>
            </div>

        </div>

    </div>
    <div class="personal-billing @if($errors->any()){{'show'}}@endif">
        <div class="basic_info ">
            @include('public.checkout.partials.billing_address')
        </div>
    </div>
</div>
