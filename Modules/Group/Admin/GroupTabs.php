<?php

namespace Modules\Group\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;
use Modules\Block\Entities\Block;
use Modules\Tax\Entities\TaxClass;
use Modules\Category\Entities\Category;
use Illuminate\Support\Facades\DB;

class GroupTabs extends Tabs
{
    public function make()
    {
        $this->group('basic_information', trans('group::groups.tabs.group.basic_information'))
            ->active()
            ->add($this->products())
           ->add($this->general())
        //    ->add($this->inventory())
           ->add($this->additionals());
        $this->group('advanced_information', trans('group::groups.tabs.group.advanced_information'))
//            ->add($this->products())
            ->add($this->seo());
//            ->add($this->filter());
        $this->submit = false;
        $this->block(Block::get(),DB::table('block_tags')->get());
    }

    private function general()
    {
        return tap(new Tab('general', trans('group::groups.tabs.general')), function (Tab $tab) {
            $tab->active();
            $tab->weight(5);
            $tab->fields(['name', 'code', 'description', 'is_active']);
            $tab->view('group::admin.groups.tabs.general', [
                'categories' => Category::treeList()
            ]);
        });
    }

    // private function inventory()
    // {
    //     return tap(new Tab('inventory', trans('group::groups.tabs.inventory')), function (Tab $tab) {
    //         $tab->weight(15);
    //         $tab->fields(['manage_stock', 'qty', 'in_stock']);
    //         $tab->view('group::admin.groups.tabs.inventory');
    //     });
    // }

    private function seo()
    {
        return tap(new Tab('seo', trans('group::groups.tabs.seo')), function (Tab $tab) {
            $tab->weight(25);
            $tab->fields('slug');
            $tab->view('group::admin.groups.tabs.seo');
        });
    }
    private function additionals()
    {
        return tap(new Tab('additionals', trans('group::groups.tabs.additionals')), function (Tab $tab) {
            $tab->weight(35);
            // $tab->fields(['new_from', 'new_to']);
            $tab->view('group::admin.groups.tabs.additionals');
        });
    }

    private function products(){
        return tap(new Tab('group_products', trans('group::groups.tabs.products')), function (Tab $tab) {
            $tab->weight(45);
            $tab->view('group::admin.groups.tabs.products');
        });
    }
    // private function filter(){
    //     return tap(new Tab('filter', trans('group::groups.tabs.filter')), function (Tab $tab) {
    //         $tab->weight(55);
    //         $tab->view('group::admin.groups.tabs.filter');
    //     });
    // }

}
