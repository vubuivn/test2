<?php

namespace Modules\Store\Sidebar;

use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.content'), function (Group $group) {
            $group->item(trans('store::sidebar.stores'), function (Item $item) {
                $item->icon('fa fa-home');
                $item->weight(45);
                // $item->route('admin.stores.index');
                $item->authorize(
                    // $this->auth->hasAccess('admin.stores.index')
                );
                $item->item(trans('store::sidebar.stores'), function (Item $item) {
                    $item->weight(5);
                    $item->route('admin.stores.index');
                    $item->authorize(
                        $this->auth->hasAccess('admin.stores.index')
                    );
                });
                $item->item(trans('store::sidebar.cities'), function (Item $item) {
                    $item->weight(10);
                    $item->route('admin.stores.cities');
                    $item->authorize(
                        $this->auth->hasAccess('admin.stores.index')
                    );
                });
            });
        });
    }
}
