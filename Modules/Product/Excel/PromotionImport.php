<?php

namespace Modules\Product\Excel;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

use Modules\Attribute\Entities\Attribute;
use DB;
// use Log;

class PromotionImport
{
    const MAX_INT = 99999999999;
    const CHUNK_SIZE = 1000;

    // protected $productAttr = [];
    private $dbGroupCodes = [];
    private $dbAttributeValueCodes = [];
    private $dbAttributeFields = [];
    private $dbProductSkus = [];
    private $productsCfg = [];
    private $uniqueSkus = [];

    private $productIdsToUpdate = [];
    private $newProductsToInsert = [];
    private $productSkusToInsert = [];
    private $rawProductsToImport = [];
    private $productsToUpdate = [];
    private $productAttributesToInsert = [];
    private $productAttributeValuesToInsert = [];

    private $productCreatedCount = 0;
    private $productUpdatedCount = 0;
    private $errorProducts = [];

    public function __construct()
    {
        // ability to apply Redis Cache to hold all of this data instead of database query
        $this->generateDbProductSkus();
        $this->productsCfg = [
            'sku' => 'sku',
            'special_price' => 'Giá giảm',
            'special_price_end' => 'Số ngày giảm'
        ];
    }

    public function __toString()
    {
        return 'Fuck';
    }

    public function import($filePath)
    {
        $result = $this->preprocessImport($filePath);

        if (!$result['status']) {
            return [
                'status' => false,
                'message' => 'Thiếu cột: ' . implode(', ', $result['missingColumns'])
            ];
        }

        $this->doImport();
        // Log::info($this->errorProducts);
        return [
            'status' => true,
            'productCreatedCount' => $this->productCreatedCount,
            'productUpdatedCount' => $this->productUpdatedCount,
            'errorProducts' => $this->errorProducts,
            'errorProductCount' => count($this->errorProducts)
        ];
    }

    private function doImport()
    {
        DB::raw('LOCK TABLES products WRITE');
        DB::beginTransaction();

        try {

            // update products
            foreach (array_chunk($this->productsToUpdate, self::CHUNK_SIZE) AS $products) {
                updateBatch($table = 'products', $products, $index = 'id');
            }

            DB::table('products')->whereNotNull('special_price')->update([
                    'selling_price'=>DB::raw("`special_price`")
                ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }
    }

    private function preprocessImport($filePath)
    {
        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open($filePath);
        $reader->setShouldPreserveEmptyRows(true);
        foreach ($reader->getSheetIterator() as $sheet) {
            $headingKeys = [];
            // only read data from the first sheet
            if ($sheet->getIndex() === 0) { // index is 0-based
                foreach ($sheet->getRowIterator() as $rowNumber => $row) {
                    $cells = $row->getCells();
                    if ($rowNumber === 1) {
                        $heading = array_map(function($cell) {
                            return strtolower(trim($cell->getValue()));
                        }, $cells);

                        // do validation for header
                        $result = $this->validateHeading($heading);

                        if (!empty($result['missingColumns'])) {
                            return [
                                'status' => false,
                                'missingColumns' => $result['missingColumns']
                            ];
                        }
                        $headingKeys = $result['headingKeys'];
                        continue;
                    }

                    $formattedRow = $this->generateFormattedRow($headingKeys, $cells);

                    $result = $this->validateRow($formattedRow);
                    if (!$result['status']) {
                        $formattedRow['errorMessages'] = $result['messages'];
                        $this->errorProducts[] = $formattedRow;
                        continue;
                    }

                    $productId = $this->_getSku($formattedRow['sku']);
                    if ($productId) {
                        // update product
                        $this->generateProductsToUpdate($formattedRow, $productId);
                    }
                }
                break; // no need to read more sheets
            }
        }

        return [
            'status' => true
        ];
    }

    private function generateFormattedRow($headingKeys, $cells)
    {
        $formattedRow = [];
        foreach ($headingKeys AS $index => $headingKey) {
            $cellValue = isset($cells[$index]) ? trim($cells[$index]->getValue()) : null;

            if ($headingKey === 'sku' && $cellValue) {
                $cellValue = preg_replace('/[^A-Za-z0-9_\-]/m', '', $cellValue);
            }

            if (in_array($headingKey, $this->dbAttributeFields) && $cellValue) {
                $cellValue = preg_replace('/[^A-Za-z0-9_,\-]/m', '', $cellValue);
            }

            $formattedRow[$headingKey] = $cellValue;
        }

        return $formattedRow;
    }

    private function validateHeading($heading)
    {
        $headingKeys = [];
        $missingColumns = [];

        $validHeadingFields = custom_array_merge($this->productsCfg, $this->dbAttributeFields);

        foreach ($validHeadingFields AS $key => $name) {
            $index = array_search(strtolower($name), $heading);

            if ($index === false) {
                $missingColumns[] = $name;
            } else {
                unset($heading[$index]);
                $headingKeys[$index] = $key;
            }
        }

        return [
            'missingColumns' => $missingColumns,
            'headingKeys' => $headingKeys
        ];
    }

    private function validateRow(&$row)
    {
        $requiredFields = ['sku'];

        $messages = [];
        foreach ($requiredFields AS $field) {
            if (!strlen($row[$field])) {
                $fieldName = $this->productsCfg[$field];
                $messages[] = 'Trường: ' . $fieldName . ' không thể rỗng';
            }
        }

        // check number field
        $decimalFields = ['special_price']; // leave 'length', 'width', 'height', 'volume' fields
        foreach ($decimalFields AS $field) {
            $rowVal = $row[$field] ?? null;

            if (!empty($rowVal) && !is_numeric($rowVal)) {
                $messages[] = $this->productsCfg[$field] . ' phải là số';
            } else {
                $rowVal = abs($rowVal);
                if ($field !== 'price') {
                    $rowVal = (int) $rowVal;
                }

                if ($rowVal > self::MAX_INT) {
                    $messages[] = $this->productsCfg[$field] . ' vượt quá giá trị cho phép';
                }

                $row[$field] = $rowVal;
            }
        }


        if (!empty($messages)) {
            return [
                'status' => false,
                'messages' => $messages
            ];
        }

        return [
            'status' => true
        ];
    }


    private function generateDbProductSkus()
    {
        $products = DB::select(DB::raw('SELECT sku, id FROM products WHERE deleted_at IS NULL'));
        foreach ($products AS $product) {
            $this->dbProductSkus[strtolower($product->sku)] = $product->id;
        }
    }
    

    private function _getGroup($code)
    {
        return $this->dbGroupCodes[strtolower($code)] ?? null;
    }

    private function _getSku($sku)
    {
        return $this->dbProductSkus[strtolower($sku)] ?? null;
    }

    private function generateProductsToInsert(array $row)
    {
        $this->productCreatedCount++;
        $this->rawProductsToImport[] = $row;
        $this->productSkusToInsert[] = $row['sku'];
        $this->newProductsToInsert[$row['sku']] = [
            'sku' => $row['sku'],
            'wide' => $row['width'],
            'long' => $row['length'],
            'high' => $row['height'],
            'volume' => $row['volume'] ?? null,
            // 'seats' => abs((int) $row['seats']),
            'price' => $row['price'],
            'selling_price' => $row['price'],
            'group_id' => $row['group_id'],
            'product_attributes' => $row['product_attributes'],
            'is_active' => $row['is_active'],
            'image_count' => $row['image_count']
        ];
    }
    
    private function generateProductsToUpdate(array $row, int $productId)
    {
        $this->productUpdatedCount++;
        $this->productsToUpdate[] = [
            'id' => $productId,
            'sku' => $row['sku'],
            'special_price' => $row['special_price'] != '' ? $row['special_price'] : NULL,
            // 'selling_price' => $row['special_price'] != '' ? $row['special_price'] : NULL,
            'special_price_start' => $row['special_price_end'] != '' ? \Carbon\Carbon::now() : NULL,
            'special_price_end' => $row['special_price_end'] != '' ? \Carbon\Carbon::now()->addDays($row['special_price_end']) : NULL,
            'updated_at' => date('Y-m-d H:i:s')
        ];
        
        // if($row['special_price'] != ''){
        //     $this->productsToUpdate['selling_price'] = $row['special_price'];
        // }
        // dd($this->productsToUpdate);
        $this->prepareProductDataToImport($row, $productId);
        $this->productIdsToUpdate[] = $productId;
    }

    private function prepareProductDataToImport(array $row,int $productId)
    {
        foreach ($this->dbAttributeFields AS $attrId => $name) {
            if (isset($row[$attrId]) && strlen($row[$attrId])) {
                $this->productAttributesToInsert[] = [
                    'product_id' => $productId,
                    'attribute_id' => $attrId
                ];
                $attributeCodes = explode(',', $row[$attrId]);
                $attributeCodes = array_unique($attributeCodes);
                foreach ($attributeCodes AS $attrCode) {
                    $attrValueKey = $attrId . '_' . $attrCode;
                    $attrValueId = $this->dbAttributeValueCodes[$attrValueKey];
                    $this->productAttributeValuesToInsert[] = [
                        'product_id' => $productId,
                        'attribute_id' => $attrId,
                        'attribute_value_id' => $attrValueId
                    ];
                }
            }
        }
    }

    private function getProductIdsBySkus(array $skus)
    {
        $productIds = [];
        foreach (array_chunk($skus, 10 * self::CHUNK_SIZE) AS $skuChunk) {
            $items = DB::table('products')->select('id', 'sku')->whereIn('sku', $skuChunk)->get();
        
            foreach ($items AS $item) {
                $productIds[strtolower($item->sku)] = $item->id;
            }
        }

        return $productIds;
    }

}