<?php

namespace Modules\Designer\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Designer\Entities\Designer;
use Modules\Core\Http\Requests\Request;

class SaveDesignerRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var array
     */
    protected $availableAttributes = 'designer::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => $this->getSlugRules(),
            'name' => 'required',
            'is_active' => 'required|boolean',
        ];
    }

    private function getSlugRules()
    {
        $rules = $this->route()->getName() === 'admin.designers.update'
            ? ['required']
            : ['sometimes'];

        $slug = Designer::withoutGlobalScope('active')->where('id', $this->id)->value('slug');

        $rules[] = Rule::unique('designers', 'slug')->ignore($slug, 'slug');

        return $rules;
    }
}
