<?php

namespace Themes\Storefront\Http\ViewComposers;

// use Illuminate\Support\Facades\DB;
use Modules\Group\Entities\Group;
use Modules\Category\Entities\Category;
use Illuminate\Support\Facades\Cache;

class ProductShowComposer
{
    protected $_slug = null;

    public function compose ($view)
    {
        if (isset($view->getData()['slug'])) {
            $this->_slug = $view->getData()['slug'];
        }

        $group = Group::findBySlug($this->_slug);
        if (count($group->products) === 0) {
            abort(404);
        }
        $visitedGroupIds = session('visitedGroupIds', []);

        if (!in_array($group->id, $visitedGroupIds)) {
            array_unshift($visitedGroupIds, $group->id);
            session(['visitedGroupIds' => $visitedGroupIds]);
        }

        // $products = $this->products($group);

        if(Cache::tags([$group->id.'products'.locale()])->has($group->id.'products'.locale().'list')){
            $products = Cache::tags([$group->id.'products'.locale()])->get($group->id.'products'.locale().'list');
        }else{
            $products = Cache::tags([$group->id.'products'.locale()])->rememberForever($group->id.'products'.locale().'list',function() use($group){
                return $this->products($group);
            });
        }
        // dd($products);

        if(Cache::tags([$group->id.'products'.locale()])->has($group->id.'products'.locale().'productSku')){
            $products = Cache::tags([$group->id.'products'.locale()])->get($group->id.'products'.locale().'productSku');
        }else{
            $products = Cache::tags([$group->id.'products'.locale()])->rememberForever($group->id.'products'.locale().'productSku',function() use($products){
                return $this->formatProducts($products);
            });

        }
        // dd($products);
        // $products = $this->formatProducts($products);

        //WISHLIST ra rieng de khoi bi cache
        $products = $this->productWishlist($products);
        
        $productSku = request()->all()['sku'] ?? null;
        if ($productSku) {
            $defaultProduct = array_filter($products, function ($product) use ($productSku) {
                return $product['sku'] === $productSku;
            });

            $defaultProduct = reset($defaultProduct);
        } else {
            $defaultProduct = reset($products);
        }

        if(Cache::tags([$group->id.'products'.locale()])->has($group->id.'products'.locale().'attributes')){
            $attributes = Cache::tags([$group->id.'products'.locale()])->get($group->id.'products'.locale().'attributes');
        }else{
            $attributes = Cache::tags([$group->id.'products'.locale()])->rememberForever($group->id.'products'.locale().'attributes',function() use($group){
                return Category::find($group->categories[0]->id)
                        ->attributes()
                        ->with('values')
                        ->orderBy('id')
                        ->get();
                        });
        }

        // $attributes = Category::find($group->categories[0]->id)
            // ->attributes()
            // ->with('values')
            // ->orderBy('id')
            // ->get();

        $assocAttributes = format_assoc_product_attributes($attributes);
        $attributes = format_product_attributes($attributes);
        $attributeOrders = array_keys($attributes);
        $productAttributeRelationships = $this->generateProductAttributesRelationships($products, $attributeOrders);
        $groupAttributes = $this->generateGroupAttributes($products);

        $view->with(
            [
                'defaultProduct' => $defaultProduct,
                'group'      => $group,
                'products'   => $products,
                'relatedGroups' => $this->getRelatedGroups($group),
                'visitedGroups' => $this->getVisitedGroups($visitedGroupIds),
                'attributes' => $attributes,
                'attributeOrders' => $attributeOrders,
                'productAttributeRelationships' => $productAttributeRelationships,

                'assocAttributes' => $assocAttributes,
                'groupAttributes' => $groupAttributes
            ]
        );

    }

    private function getRelatedGroups($group)
    {
        $collections = explode(',', $group->collections);

        if(Cache::tags([$group->id.'products'.locale()])->has('product_realted_'.$group->id.':' . locale())){
            return Cache::tags(['product_detail'])->get('product_realted_'.$group->id.':' . locale());
        }else{
            return Cache::tags([$group->id.'products'.locale()])
                ->rememberForever('product_realted_'.$group->id.':' . locale(),function () use($group,$collections){
                    return buildFromToPriceQueryBuilder()
                            ->whereIn('collections', $collections)->where('id','<>',$group->id)->limit(4)->get();
            });
        }
    }

    private function getVisitedGroups(array $visitedGroupIds)
    {
        return buildFromToPriceQueryBuilder()
                ->whereIn('id', $visitedGroupIds)
                ->limit(8)->get();
    }

    private function products ($group)
    {
        $products = $group->products()
            ->where('is_active', '>=', 0)
            ->orderBy(
                'is_active',
                'DESC'
            )
            ->get();
            // ->groupBy('is_active');
        return $products;
    }

    private function productWishlist($products){
        $user = \Auth::user();
        $wishlist = false;
        foreach($products as $key=>$product){
            if ($user) {
            $wishlist = \DB::table('wish_lists')
                            ->where('user_id', $user->id)
                            ->where('product_id', $key)
                            ->count() > 0;
            }
            $products[$key]['wishlist'] = $wishlist;
        }
        
        
        return $products;
    }

    private function formatProducts($products)
    {
        
        $formattedProducts = [];
        foreach ($products AS $product) {
            $attributes = $this->formatProductAttribute($product->product_attributes);

            

            $formattedProducts[(string) $product->id] = [
                'id' => $product->id,
                'sku' => $product->sku,
                'width' => $product->wide,
                'height' => $product->high,
                'length' => $product->long,
                'volume' => $product->volume,
                'price' => (string) $product->selling_price,
                'normal_price' => (string) $product->price,
                'special_price' => (string) $product->special_price,
                'image_count' => (int) $product->image_count,
                'out_of_stock' => $product->out_of_stock,
                'attributes' => $attributes,
                'attribute_details' => $product->getAttributeValueDetails(),
                // 'wishlist' => $wishlist
            ];
        }

        return $formattedProducts;
    }

    private function formatProductAttribute($productAttributes)
    {
        $attributes = [];

        $productAttrs = explode(';', $productAttributes);
        foreach ($productAttrs AS $attrStr) {
            $arr = explode(':', $attrStr);
            $attributes[$arr[0]] = explode(',', $arr[1]);
        }

        return $attributes;
    }

    private function generateProductAttributesRelationships($products, $attributeOrders)
    {
        $relationships = [];
        foreach ($products AS $product) {
            $relationArr = [];
            $productAttributes = $product['attributes'];
            if (!$productAttributes) {
                continue;
            }

            foreach ($attributeOrders as $index => $attrId) {
                // skip products has no this attribute
                if (empty($productAttributes[(string) $attrId])) {
                    unset($attributeOrders[$index]);
                    break;
                }
                
                // do for first attribute
                if (empty($relationArr)) {
                    $relationArr = array_map(function($attrValueCode) {
                        return [$attrValueCode];
                    }, $productAttributes[$attrId]);

                    continue;
                }
                
                $newArr = [];
                // loop through attributeValueIds
                foreach ($productAttributes[$attrId] AS $attrValueCode) {
                    foreach ($relationArr AS $item) {
                        $item[] = $attrValueCode;
                        $newArr[] = $item;
                    }
                }
                
                $relationArr = $newArr;
            }

            foreach ($relationArr AS $attrItem) {
                $key = implode('_', $attrItem);
                $relationships[$key] = $product['id'];
            }
        }

        return $relationships;
    }

    private function generateGroupAttributes($products)
    {
        $data[0] = [];
        foreach ($products as $product) {
            $attributeDetails = $product['attribute_details'];

            $lastAttr = array_pop($attributeDetails);
            $count = 0;
            foreach ($attributeDetails as $id => $attributeDetail) {
                $tempArray = &$data[$count];

                if (!isset($tempArray[$attributeDetail->code])) {
                    $tempArray[$attributeDetail->code] = [];
                }

                // Prepare for next
                $data[++$count] = &$tempArray[$attributeDetail->code];
            }

            $array = &$data[$count];
            $array[$lastAttr->code] = $product['id'];
        }
        return $data[0];
    }
}