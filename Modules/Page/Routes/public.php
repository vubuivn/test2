<?php

Route::get('/', 'HomeController@index')->name('home');

Route::get('/page/{slug}', 'PageController@show')->name('page.detail');

Route::post('ajax/subscription','PageController@subscription')->name('page.subscription.ajax');
