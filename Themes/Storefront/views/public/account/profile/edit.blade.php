@extends('public.account.layout')

@section('title', trans('storefront::account.links.my_profile'))

@section('account_breadcrumb')
    <li class="active">{{ trans('storefront::account.links.my_profile') }}</li>
@endsection

@section('content_right')
    <h1 class="h4">{{trans('storefront::account.profile.profile_account')}}</h1>
    <form method="POST" action="{{ route('account.profile.update') }}" class="formaccount">
        {{ csrf_field() }}
        {{ method_field('put') }}

        <div class="inner">
            <div class="rowlabel label-150">
                <label class="item {{ $errors->has('first_name') ? 'has-error': '' }}">
                    <span class="title">{{ trans('storefront::account.profile.first_name') }}</span>
                    <span class="text"><input type="text" name="first_name" id="first-name" class="input" value="{{ old('first_name', $my->first_name) }}"></span>
                    {!! $errors->first('first_name', '<span class="error-message">:message</span>') !!}
                </label>
                <label class="item {{ $errors->has('last_name') ? 'has-error': '' }}">
                    <span class="title">{{ trans('storefront::account.profile.last_name') }}</span>
                    <span class="text"><input type="text" name="last_name" id="last-name" class="input" value="{{ old('last_name', $my->last_name) }}"></span>
                    {!! $errors->first('last_name', '<span class="error-message">:message</span>') !!}
                </label>


                <div class="item gender">
                    <span class="title">{{ trans('user::auth.gender') }}</span>
                    <span class="text {{ $errors->has('gender') ? 'gender': '' }}">

                      <label class="radio ">
                        <input name="gender" type="radio" {{ $my->gender == 0 ? 'checked=""' : '' }} value="0">
                        <span></span>
                          {{trans('storefront::account.profile.male')}}
                      </label>
                      <label class="radio ">
                        <input name="gender" type="radio" {{ $my->gender == 1 ? 'checked=""' : '' }} value="1">
                        <span></span>
                          {{trans('storefront::account.profile.female')}}
                      </label>    

                    </span>
                    {!! $errors->first('gender','<span class="error-message">:message</span>') !!}
                </div>
                <?php 
                $day = '';
                $month = '';
                $year = '';
                    if(isset($my->birthday)){
                        $day= date('j', strtotime($my->birthday));  
                        $month= date('n', strtotime($my->birthday));  
                        $year= date('Y', strtotime($my->birthday)); 
                    } 
                ?>
                <div class="item">
                    <span class="title">{{ trans('user::auth.birthday') }}</span>
                    <span class="text">
                        <div class="row grid-space-10">
                          <div class="col-sm-4 col-xs-4">
                            <select name="day"  class="select">
                              <option value="000">{{ trans('storefront::account.profile.day') }}</option>
                              <?php
                              for($i=1;$i<=31;$i++){ ?>
                                <option value="{{ $i }}" {{($day == $i) ? "selected" : ""}}>{{ $i }}</option>
                             <?php } ?>
                            </select>
                          </div>
                          <div class="col-sm-4 col-xs-4">
                            <select name="month"  class="select">
                              <option value="000">{{ trans('storefront::account.profile.month') }}</option>
                              <?php
                              for($i=1;$i<=12;$i++){ ?>
                                <option value="{{ $i }}" {{($month == $i) ? "selected" : ""}}>{{ $i }}</option>
                             <?php } ?>
                            </select>
                          </div>
                          <div class="col-sm-4 col-xs-4">
                            <select name="year"  class="select">
                              <option value="000" >{{ trans('storefront::account.profile.year') }}</option>
                              <?php for($i=1970;$i<=2020;$i++){ ?>
                                <option value="{{ $i }}" {{($year == $i) ? "selected" : ""}}>{{ $i }}</option>;
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        {!! $errors->first('birthday','<span class="error-message">:message</span>') !!}
                    </span>
                </div>


                <label class="item {{ $errors->has('phone') ? 'has-error': '' }}">
                    <span class="title">{{ trans('storefront::account.profile.phone') }}</span>
                    <span class="text"><input type="text" name="phone" id="phone" class="input" value="{{ old('phone', $my->phone) }}"></span>
                    {!! $errors->first('phone', '<span class="error-message">:message</span>') !!}
                </label>

                <label class="item {{ $errors->has('address') ? 'has-error': '' }}">
                    <span class="title">{{ trans('storefront::account.profile.address') }}</span>
                    <span class="text"><input type="text" name="address" id="address" class="input" value="{{ old('address', $my->address) }}"></span>
                    {!! $errors->first('address', '<span class="error-message">:message</span>') !!}
                </label>

                <label class="item {{ $errors->has('city_id') ? 'has-error': '' }}">
                    <span class="title">{{ trans('storefront::account.profile.cities') }}</span>
                    <span class="text">
                        <select name="city_id" class="select" id="city-id">
                            @if(count($cities)>0)
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" @if($my->city_id != null){{ ($city->id == $my->city_id) ? 'selected' : '' }}@endif>{{ get_local_title($city->title) }}</option>
                                @endforeach
                                @endif
                        </select>
                        
                    </span>
                    {!! $errors->first('city_id', '<span class="error-message">:message</span>') !!}
                </label>

                <label class="item {{ $errors->has('district_id') ? 'has-error': '' }}">
                    <span class="title">{{ trans('storefront::account.profile.districts') }}</span>
                    <span class="text">
                        <select name="district_id" class="select" id="district_id">
                            @if($my->city_id != null)
                                @foreach($districts[$my->city_id] as $id => $district)
                                    <option value="{{$id}}" @if($my->district_id != null){{ $id==$my->district_id ? 'selected' : ''}}@endif>{{$district}}</option>
                                @endforeach
                            @else
                                @foreach($districts[1] as $id => $district)
                                <option value="{{$id}}" @if($my->district_id != null){{ $id==$my->district_id ? 'selected' : ''}}@endif>{{$district}}</option>
                            @endforeach
                            @endif
                        </select>
                        
                    </span>
                    {!! $errors->first('district_id', '<span class="error-message">:message</span>') !!}
                </label>


                <label class="item {{ $errors->has('email') ? 'has-error': '' }}">
                    <span class="title">{{ trans('storefront::account.profile.email') }}</span>
                    <span class="text"><input type="text" name="email" id="email" class="input" value="{{ old('email', $my->email) }}"></span>
                    {!! $errors->first('email', '<span class="error-message">:message</span>') !!}
                </label>


                <label class="item {{ $errors->has('password') ? 'has-error': '' }}">
                    <span class="title">{{ trans('storefront::account.profile.new_password') }}</span>
                    <span class="text"><input type="text" name="password" id="password" class="input" value=""></span>
                    {!! $errors->first('password', '<span class="error-message">:message</span>') !!}
                </label>

                <label class="item {{ $errors->has('password_confirmation') ? 'has-error': '' }}">
                    <span class="title">{{ trans('storefront::account.profile.confirm_password') }}</span>
                    <span class="text"><input type="text" name="password_confirmation" id="confirm-password" class="input" value=""></span>
                    {!! $errors->first('password_confirmation', '<span class="error-message">:message</span>') !!}
                </label>

                <div class="item btnsubmit">
                    <span class="title"></span>
                    <span class="text">

                      <button type="submit" class="btn noshadow" data-loading>{{ trans('storefront::account.profile.save_changes') }}</button>

                    </span>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script type="text/javascript">
        (function($){
            const DISTRICTS = {!! json_encode($districts) !!};

            var oldDistrict = "{{ old('billing.city') }}";
            var firstDistrict = $('#district_id').html();

            $('#city-id').on('change', function(e) {
                var stateId = $(this).val();
                var districtsObj = DISTRICTS[stateId];
                var districts = [];
                for (var id in districtsObj) {
                    var districtName = districtsObj[id];
                    var selectedText = oldDistrict === districtName ? 'selected' : '';

                    districts.push('<option value="' + districtName + '" ' + selectedText + '>' + districtName + '</option>');
                }
                $('#district_id').html(districts.join(''));
            });
        })(jQuery)
    </script>
@endpush