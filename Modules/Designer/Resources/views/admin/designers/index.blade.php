@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('designer::designers.designers'))

    <li class="active">{{ trans('designer::designers.designers') }}</li>
@endcomponent

@component('admin::components.page.index_table')
    @slot('resource', 'designers')
    @slot('buttons', ['create'])
    @slot('name', trans('designer::designers.designer'))

    @slot('thead')
        <tr>
            @include('admin::partials.table.select_all')

            <th>{{ trans('designer::designers.table.name') }}</th>
            <th>{{ trans('admin::admin.table.status') }}</th>
            <th data-sort>{{ trans('admin::admin.table.created') }}</th>
        </tr>
    @endslot
@endcomponent

@push('scripts')
    <script>
        new DataTable('#designers-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                { data: 'name', name: 'translations.name', orderable: false, defaultContent: '' },
                { data: 'status', name: 'is_active', searchable: false },
                { data: 'created', name: 'created_at' },
            ],
        });
    </script>
@endpush
