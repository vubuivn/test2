<script type="text/html" id="element-select-values-template">
    <tr class="element-row">
        <td class="text-center">
            <span class="drag-icon">
                <i class="fa">&#xf142;</i>
                <i class="fa">&#xf142;</i>
            </span>
        </td>
        <td>
            <input
                type="hidden"
                <% if (elementId === undefined) { %>
                    name="values[<%- valueId %>][id]"
                    id="values-<%- valueId %>-id"
                <% } else { %>
                    name="elements[<%- elementId %>][values][<%- valueId %>][id]"
                    id="element-<%- elementId %>-values-<%- valueId %>-id"
                <% } %>

                value="<%- value.id %>"
            >

            <input
                type="text"
                <% if (elementId === undefined) { %>
                    name="values[<%- valueId %>][label]"
                    id="values-<%- valueId %>-label"
                <% } else { %>
                    name="elements[<%- elementId %>][values][<%- valueId %>][label]"
                    id="element-<%- elementId %>-values-<%- valueId %>-label"
                <% } %>

                class="form-control"
                value="<%- value.label %>"
            >
        </td>
        <td>
            <input
                type="number"
                <% if (elementId === undefined) { %>
                    name="values[<%- valueId %>][price]"
                    id="values-<%- valueId %>-price"
                <% } else { %>
                    name="elements[<%- elementId %>][values][<%- valueId %>][price]"
                    id="element-<%- elementId %>-values-<%- valueId %>-price"
                <% } %>
                class="form-control"
                value="<%- _.isObject(value.price) ? value.price.amount : value.price %>"
                step="0.01"
                min="0"
            >
        </td>
        <td>
            <select
                <% if (elementId === undefined) { %>
                    name="values[<%- valueId %>][price_type]"
                    id="values-<%- valueId %>-price_type"
                <% } else { %>
                    name="elements[<%- elementId %>][values][<%- valueId %>][price_type]"
                    id="element-<%- elementId %>-values-<%- valueId %>-price_type"
                <% } %>
                class="form-control custom-select-black"
            >
                <element value="fixed"
                    <%= value.price_type === 'fixed' ? 'selected' : '' %>
                >
                    {{ trans('element::elements.form.price_types.fixed') }}
                </element>

                <element value="percent"
                    <%= value.price_type === 'percent' ? 'selected' : '' %>
                >
                    {{ trans('element::elements.form.price_types.percent') }}
                </element>
            </select>
        </td>
        <td class="text-center">
            <button type="button" class="btn btn-default delete-row" data-toggle="tooltip" title="{{ trans('element::elements.form.delete_row') }}">
                <i class="fa fa-trash"></i>
            </button>
        </td>
    </td>
</script>
