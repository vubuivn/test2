<?php
use Modules\Category\Entities\Category;

// function group_block ($data)
// {
//     try {
//         $request = [];
//         $filter  = last(
//             explode(
//                 locale() . '/products',
//                 $data->filter
//             )
//         );
//         $filter  = explode(
//             '?',
//             $filter
//         );

//         $slug = str_replace(
//                 '/',
//                 '',
//                 $filter[0]
//             ) ?? null;
//         if ($filter[1]) {
//             $filter = urldecode($filter[1]);
//             $filter = explode(
//                 '&',
//                 $filter
//             );
//             foreach ($filter as $value) {
//                 $value = explode(
//                     '=',
//                     $value
//                 );

//                 if (strpos(
//                     $value[0],
//                     '[]'
//                 )) {
//                     $k             = str_replace(
//                         '[]',
//                         '',
//                         $value[0]
//                     );
//                     $request[$k][] = $value[1];
//                 } else {
//                     $request[$value[0]] = $value[1];
//                 }
//             }
//         }
//         $query = group_query(
//             $request,
//             $slug
//         );
//         if ($data->limit) {
//             $query = $query->limit($data->limit);
//         }
//         $result = $query->get();
//     }catch (Exception $e){
//         $result = null;
//     }
//     return $result;
// }

// function group_query ($request, $slug = null)
// {
//     $request = [
//         'fromPrice' => array_get(
//             $request,
//             'fromPrice',
//             null
//         ),
//         'toPrice'   => array_get(
//             $request,
//             'toPrice',
//             null
//         ),
//         'attribute' => array_get(
//             $request,
//             'attribute',
//             []
//         ),
//     ];

//     $query   = \Modules\Group\Entities\Group::with(
//         [
//             'products' => function ($q) use ($request) {
//                 $q->where(
//                         'is_active',
//                         '<>',
//                         '-1'
//                     )
//                     ->when(
//                         isset($request['fromPrice']),
//                         function ($q) use ($request) {
//                             if ($request['fromPrice'] != null && $request['fromPrice'] != '') {
//                                 return $q->where(
//                                     'selling_price',
//                                     '>=',
//                                     $request['fromPrice']
//                                 );
//                             }
//                         }
//                     )
//                     ->when(
//                         isset($request['toPrice']),
//                         function ($q) use ($request) {
//                             if ($request['toPrice'] != null && $request['toPrice'] != '') {
//                                 return $q->where(
//                                     'selling_price',
//                                     '<=',
//                                     $request['toPrice']
//                                 );
//                             }
//                         }
//                     )
//                     ->whereHas(
//                         'attributes',
//                         function ($query) use ($request) {
//                             return $query->whereHas(
//                                 'values',
//                                 function ($query) use ($request) {
//                                     return $query->whereHas(
//                                         'attributeValue',
//                                         function ($query) use ($request) {
//                                             $i = 0;
//                                             foreach ($request['attribute'] as $key => $value) {
//                                                 if ($i == 0) {
//                                                     $query = $query->where(
//                                                         'attribute_id',
//                                                         $key
//                                                     )
//                                                         ->whereIn(
//                                                             'other',
//                                                             $value
//                                                         );
//                                                 } else {
//                                                     $query = $query->orWhere(
//                                                         function ($q) use ($key, $value) {
//                                                             $q->where(
//                                                                 'attribute_id',
//                                                                 $key
//                                                             )
//                                                                 ->whereIn(
//                                                                     'other',
//                                                                     $value
//                                                                 );
//                                                         }
//                                                     );
//                                                 }
//                                                 $i++;
//                                             }
//                                             return $query;
//                                         }
//                                     );
//                                 }
//                             );
//                         }
//                     )
//                     ->orderBy(
//                         'is_active',
//                         'DESC'
//                     );
//             },

//         ]
//     )
//         ->where(
//             'is_active',
//             1
//         )
//         ->whereHas(
//             'products',
//             function ($query) use ($request) {
//                 return $query->where(
//                     'is_active',
//                     '<>',
//                     '-1'
//                 )
//                     ->when(
//                         isset($request['fromPrice']),
//                         function ($q) use ($request) {
//                             if ($request['fromPrice'] != null && $request['fromPrice'] != '') {
//                                 return $q->where(
//                                     'selling_price',
//                                     '>=',
//                                     $request['fromPrice']
//                                 );
//                             }
//                         }
//                     )
//                     ->when(
//                         isset($request['toPrice']),
//                         function ($q) use ($request) {
//                             if ($request['toPrice'] != null && $request['toPrice'] != '') {
//                                 return $q->where(
//                                     'selling_price',
//                                     '<=',
//                                     $request['toPrice']
//                                 );
//                             }
//                         }
//                     )
//                     ->when(
//                         is_array($request['attribute']),
//                         function ($q) use ($request) {
//                             return $q->whereHas(
//                                 'attributes',
//                                 function ($query) use ($request) {
//                                     return $query->whereHas(
//                                         'values',
//                                         function ($query) use ($request) {
//                                             return $query->whereHas(
//                                                 'attributeValue',
//                                                 function ($query) use ($request) {
//                                                     $i = 0;
//                                                     foreach ($request['attribute'] as $key => $value) {
//                                                         if ($i == 0) {
//                                                             $query = $query->where(
//                                                                 'attribute_id',
//                                                                 $key
//                                                             )
//                                                                 ->whereIn(
//                                                                     'other',
//                                                                     $value
//                                                                 );
//                                                         } else {
//                                                             $query = $query->orWhere(
//                                                                 function ($q) use ($key, $value) {
//                                                                     $q->where(
//                                                                         'attribute_id',
//                                                                         $key
//                                                                     )
//                                                                         ->whereIn(
//                                                                             'other',
//                                                                             $value
//                                                                         );
//                                                                 }
//                                                             );
//                                                         }
//                                                         $i++;
//                                                     }
//                                                     return $query;
//                                                 }
//                                             );
//                                         }
//                                     );
//                                 }
//                             );
//                         }
//                     );
//             }
//         );
//     if ($slug != null && $slug != '') {
//         $query = $query->whereHas(
//             'categories',
//             function ($query) use ($slug) {
//                 return $query->where(
//                     'slug',
//                     $slug
//                 );
//             }
//         );
//     }
//     return $query;
// }

// function priceGroup ($id)
// {
//     $query = \Modules\Product\Entities\Product::where(
//         'group_id',
//         $id
//     );
//     $min   = (clone $query)->min('selling_price');
//     $max   = (clone $query)->max('selling_price');
//     return [
//         'min' => $min,
//         'max' => $max
//     ];
// }

/**
 * @return ['slug' => 'url', ...]
*/
function generateCategorySlugUrl()
{
    $categorySlugUrlKey = 'categorySlugUrls_' . locale();

    if (Cache::has($categorySlugUrlKey)) {
        return Cache::get($categorySlugUrlKey);
    }

    $categorySlugUrls = [];
    $items = Category::whereNull('parent_id')->get();
    
    foreach ($items AS $item) {
        $categorySlugUrls[$item->slug] = route('products.category', ['slug1' => $item->slug]);

        $items2 = Category::where('parent_id', $item->id)->get();
        if ($items2->isNotEmpty()) {
            foreach ($items2 AS $item2) {
                $categorySlugUrls[$item2->slug] = route('products.category', [
                    'slug1' => $item->slug,
                    'slug2' => $item2->slug
                ]);

                $items3 = Category::where('parent_id', $item2->id)->get();
                if ($items3->isNotEmpty()) {
                    foreach ($items3 AS $item3) {
                        $categorySlugUrls[$item3->slug] = route('products.category', [
                            'slug1' => $item->slug,
                            'slug2' => $item2->slug,
                            'slug3' => $item3->slug
                        ]);
                    }
                }
            }
        }
    }

    Cache::forever($categorySlugUrlKey, $categorySlugUrls);

    return $categorySlugUrls;
}

function getCategoryUrlFromSlug($slug) {
    $categorySlugUrls = generateCategorySlugUrl();

    return $categorySlugUrls[$slug] ?? null;
    // return '';
}