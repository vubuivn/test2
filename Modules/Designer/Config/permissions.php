<?php

return [
    'admin.designers' => [
        'index' => 'designer::permissions.index',
        'create' => 'designer::permissions.create',
        'edit' => 'designer::permissions.edit',
        'destroy' => 'designer::permissions.destroy',
    ],
];
