<?php

namespace Modules\Builder\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReplyMail extends Mailable
{
    use Queueable, SerializesModels;

    public $dataMail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->dataMail = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $reply = $this->dataMail->reply;
        $entity = $this->dataMail->data;
        $reply_body = $reply->reply_body;
        $builder = $this->dataMail->builder;
        foreach ($builder->elements as $element){
            $reply_body = str_replace(['['.$element->element_name.']'],$entity->{$element->element_name},$reply_body);
        }
        return $this->subject($reply->reply_subject)
            ->view('emails.reply',['body' => $reply_body]);
    }
}
