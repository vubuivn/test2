@extends('public.layout')

@section('title', 'Designer')

@section('content')
    <div class="career-wrapper clearfix">
        @include('public.partials.bannertop',['banner'=>'/assets/images/banner-3.jpg','title'=>'GẶP GỠ CÁC NHÀ THIẾT KẾ'])
        <main id="main">
    <div class="container-designer">
        <div class="t4-detail">
                <div class = "detail-text">
                    <p>Hiểu thêm về các nhà thiết kế và quan điểm sáng tạo của họ.</p>
                    <p>Những cộng sự đến từ Châu Âu đã tạo nên các sản phẩm nội thất tinh tế - tiện dụng - thân thiện mà AConcept đang giới thiệu</p>
                </div>  
        </div>
        <div class= info-designer>
   
            @php($i = 1)
            @foreach($designers as $designer)          
                @include('public.designer.item',['i' => $i])
            @php($i++)
            @endforeach
           
            </div>
        </div>
        <div class="text-center " id="Viewmore">

	    	<a href="#" class="btn">{{trans('storefront::storefront.news.view_more')}}</a>
	    </div>
    </div>
</main>
    </div>
@endsection

@push('styles')
    <link rel='stylesheet' href="/assets/css/custom.css" type='text/css'/>
    <style type="text/css">
        #Viewmore a img {width:30px;height: auto;padding-top: 5px}
        .not-active {
          pointer-events: none;
          cursor: default;
          text-decoration: none;
          color: black;
        }
    </style>
@endpush

@push('scripts')
<script type="text/javascript">
    (function($){
    $('#Viewmore').on('click',function(e){
        e.preventDefault();
        var total = $('.info-designer>.row').length;
        console.log(total);
        $.ajax({
            url: '{{ route("designer.ajax.more") }}',
            method: 'GET',
            type: 'json',
            data: {
                total:total
            },
            beforeSend: function(){
                $('#Viewmore a').html('<img src="/assets/images/loading.gif">').addClass('not-active');
            },
            success: function(res) {
                if(res == 'fail'){
                    $('#Viewmore').html("{{trans('storefront::storefront.news.no_more_post')}}");

                }else{
                    $('.info-designer').append(res);
                    $('#Viewmore a').html("{{trans('storefront::storefront.news.view_more')}}").removeClass('not-active');
                }
                
            },
            error: function(xhr, err) {
                $('.text-alert.alert-error').text(xhr.responseJSON.errors.email[0]);
            }
        });
    });

    })(jQuery)
</script>
@endpush