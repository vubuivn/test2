<?php

namespace Modules\Builder\Entities;

use Illuminate\Database\Eloquent\Model;

class Noti extends Model
{
    protected $table = 'builder_noti';
    protected $fillable = ["builder_id","reply_to","reply_subject","reply_body","is_active"];
}
