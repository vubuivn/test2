<?php

namespace Modules\Attribute\Entities;

use Modules\Support\Eloquent\Model;
use Modules\Support\Eloquent\Translatable;

class AttributeValue extends Model
{
    use Translatable;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position',
        'key',
        'other',
        'code',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = ['value', 'group'];

    public function setKeyAttribute($value)
    {
        $value = str_replace(
            ' ',
            '',
            $value
        );
        $value = trim(
            preg_replace(
                '/\s+/',
                ' ',
                $value
            )
        );
        $this->attributes['key'] = $value;
    }
    public function setCodeAttribute($value)
    {
        $value = str_replace(
            ' ',
            '',
            $value
        );
        $value = trim(
            preg_replace(
                '/\s+/',
                ' ',
                $value
            )
        );
        $this->attributes['code'] = $value;
    }
    public function product_attribute_values(){
        return $this->hasMany(ProductAttributeValue::class,'attribute_value_id','id');
    }
}
