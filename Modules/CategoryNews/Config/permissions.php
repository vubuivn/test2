<?php

return [
    'admin.category_news' => [
        'index' => 'categorynews::permissions.index',
        'create' => 'categorynews::permissions.create',
        'edit' => 'categorynews::permissions.edit',
        'destroy' => 'categorynews::permissions.destroy',
    ],

// append

];
