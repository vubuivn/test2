<?php

namespace Modules\Block\Providers;

use Illuminate\Support\Facades\View;
use Modules\Admin\Ui\Facades\TabManager;
use Modules\Block\Admin\BlockTabs;
use Modules\Block\Http\ViewComposers\BlockComposer;
use Modules\Support\Traits\AddsAsset;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\LoadsConfig;
use Themes\Storefront\Http\ViewComposers\ProductsFilterComposer;

class BlockServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot ()
    {
        $this->addAdminAssets(
            'admin.blocks.(create|edit)',
            [
                'admin.attribute.css',
                'admin.attribute.js',
            ]
        );
        TabManager::register(
            'blocks',
            BlockTabs::class
        );
        View::composer(
            'block::admin.blocks.tabs.templates.block_value',
            BlockComposer::class
        );
        View::composer(
            'block::admin.blocks.html.group',
            ProductsFilterComposer::class
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register ()
    {
        $this->loadConfigs(
            [
                'assets.php',
                'permissions.php'
            ]
        );
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
