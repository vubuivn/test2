<?php

namespace Modules\Designer\Entities;

use Modules\Admin\Ui\AdminTable;
use Modules\Support\Eloquent\Model;
use Modules\Meta\Eloquent\HasMetaData;
use Modules\Support\Eloquent\Sluggable;
use Modules\Support\Eloquent\Translatable;
use Modules\Support\Search\Searchable;
use Modules\Media\Entities\File;
use Modules\Media\Eloquent\HasMedia;

class Designer extends Model
{
    use Translatable, Sluggable, HasMetaData, HasMedia;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['slug', 'is_active'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $translatedAttributes = ['name','body','short_description'];

    /**
     * The attribute that will be slugged.
     *
     * @var string
     */
    protected $slugAttribute = 'name';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        // static::saving(
        //     function ($designer) {
        //         Request::merge(
        //             [
        //                 'files' => [
        //                     'base_image'        => request()->input(
        //                         'files.base_image',
        //                         []
        //                     ),
        //                     'feature_image'        => request()->input(
        //                         'files.feature_image',
        //                         []
        //                     ),
        //                     'additional_images' => request()->input(
        //                         'files.additional_images',
        //                         []
        //                     ),
        //                 ]
        //             ]
        //         );
        //     }
        // );
        static::addActiveGlobalScope();
    }
    public function saveRelations(array $designer)
    {
        $this->saveBlocks(array_get($designer, 'blocks', []));

    }


    public function scopeWithBaseImage($query)
    {
        $query->with(['files' => function ($q) {
            $q->wherePivot('zone', 'base_image');
        }]);
    }

    public function getBaseImageAttribute()
    {
        return $this->files->where('pivot.zone', 'base_image')->first() ?: new File;
        // return $this->with('files')->whereHas('files', function($query){
        //     $query->where('files.disk','base_image');
        // })->first() ?: new File;
    }

    public function scopeWithFeatureImage($query)
    {
        $query->with(['files' => function ($q) {
            $q->wherePivot('zone', 'feature_image');
        }]);
    }

    public function getFeatureImageAttribute()
    {
        return $this->files->where('pivot.zone', 'feature_image')->first() ?: new File;
        // return $this->with('files')->whereHas('files', function($query){
        //     $query->where('files.disk','feature_image');
        // })->first() ?: new File;
    }

    public function saveBlocks($blocks = [])
    {
        $ids = $this->getDeleteCandidates($blocks);

        if ($ids->isNotEmpty()) {
            $this->blocks()->whereIn('id', $ids)->delete();
        }

        foreach (array_reset_index($blocks) as $index => $value) {
            $this->blocks()->updateOrCreate(
                ['id' => $value['id'] ?? null],
                $value + ['position' => $index]
            );
        }
    }
    private function getDeleteCandidates($values = [])
    {
        return $this->blocks()
            ->pluck('id')
            ->diff(array_pluck($values, 'id'));
    }
    public static function urlForDesigner($id)
    {
        return static::select('slug')->firstOrNew(['id' => $id])->url();
    }

    public function url()
    {
        // if (is_null($this->slug)) {
        //     return '#';
        // }

        // return localized_url(locale(), $this->slug);
        return route('designer.detail',$this->slug);
    }

    /**
     * Get table data for the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function table()
    {
        return new AdminTable($this->newQuery()->withoutGlobalScope('active'));
    }

    public function blocks(){
        return $this->hasMany(DesignerBlock::class)->orderBy('position');
    }

    public function scopeWithAdditionalImages($query)
    {
        $query->with(['files' => function ($q) {
            $q->wherePivot('zone', 'additional_images');
        }]);
    }

    public function getAdditionalImagesAttribute ()
    {
        return $this->files->where(
            'pivot.zone',
            'additional_images'
        )
            ->sortBy('pivot.id');
    }
}
