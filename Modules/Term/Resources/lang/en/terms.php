<?php

return [
    'term' => 'Term',
    'terms' => 'Terms',
    'show' => 'Detail',
    'table' => [
        'name' => 'Name',
    ],
    'tabs' => [
        'group' => [
            'term_information' => 'Term Infortmation',
        ],
        'general' => 'General',
        'seo' => 'SEO',
    ],
    'form' => [
        'enable_the_term' => 'Enable the term',
    ],
];
