<?php

namespace Modules\Career\Entities;

use Modules\Support\Eloquent\TranslationModel;

class CareerBlockTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content'
    ];
}
