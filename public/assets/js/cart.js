(function($){
    function add_to_cat_ajax(id,qty,option = []){
        $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
        $.ajax({
            type: 'POST',
            url: "/ajax/product/add",
            data: {id:id,qty:qty},
            beforeSend: function(){
                $('#loading_gif').show();
            },
            success: function(data){
                $('#loading_gif').hide();
                $('#header .widget-mini-cart').addClass('active');
                //update mini-cart
                var currentCart = $('.qty.cart-count').html();
                var newtotal = parseInt(currentCart)+parseInt(qty);
                $('.qty.cart-count').html(newtotal);
                $('.mini-cart-wrapper .dropdown-menu.content').html(data);
                $(".mini-cart-wrapper").trigger('mouseenter');
                $('.mini-cart-wrapper .dropdown-menu').css({'opacity':1,'visibility':'visible','margin-top':'0'});
                setTimeout( function(){
                  $('.mini-cart-wrapper .dropdown-menu').attr('style','');
                }, 3000);
            },
            error: function (data) {
                $('#notificationPopupAjax #modal-body').html('<div class="alert alert-success fade in">'+data+'</div>');
                $('#notificationPopupAjax').modal('show');
            }
        });


        return true;
    }


    // $('a.add_to_cart_2').on('click',function(e){
    //     e.preventDefault();
    //     var id = $(this).data('id');
    //     var check = false;
    //     // if(check = false){
    //     //     check = true;
    //         var returnC = add_to_cat_ajax(id,1,[]);
    //         // updateCat(qty);
    //     //     if(returnC == true){
    //     //         check = false;
    //     //     }
    //     // }
    // });
    $('a.detail_add_to_cart_2').on('click',function(e){
        e.preventDefault();
        var id = $('input[name="product_id"]').val(),
            qty = $('input[name="qty"]').val();
        var check = false;
  
        var returnC = add_to_cat_ajax(id,qty,[]);
        // updateCat(qty);

    });
})(jQuery); 