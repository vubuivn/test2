

@foreach ($primaryMenu->menus() AS $index => $menu)
    <?php 
        $active = '';
        $type = $menu->type();
        switch ($type){
            case 'category':
                $route = 'products.category';
                break;
            case 'page':
                $route = 'page.detail';
                break;
            default:
                $route = 'url';
                break;
        }
        if($route != 'url'){
            if($menu->slug() != null){
                if(url()->current() == route($route,$menu->slug())){
                    $active = 'current';
                }
            }   
        }elseif(url()->current() == $menu->url()){

            $active = 'current';
        }
        
    ?>
    
    @if ($index === 0)
    <li id="li-{{ $menu->id() }}" data-categoryid="{{ isset($menu->category_id) ? $menu->category_id : '' }}" class="children itemmega megascreen {{$active}}">
        <a href="#"><span>{{ $menu->name() }}</span></a>
        <ul>
            <li>
                @include('public.include.mega_menu.product-menu',['level'=>'level1','sublink'=>$menu->url()])
            </li>
        </ul>
    </li>
    @else
    @php($menuClass = $menu->hasSubMenus() ? 'children' : '')
    <li class="{{ $menuClass }} {{$active}} {{$menu->slug()}}" id="li-{{ $menu->id() }}" data-categoryid="{{ isset($menu->category_id) ? $menu->category_id : '' }}">
        <a href="{{ $menu->url() }}"><span>{{ $menu->name() }}</span></a>
        @if ($menu->hasSubMenus())
        @include('public.include.mega_menu.dropdown', ['subMenus' => $menu->subMenus()])
        @endif
    </li>
    @endif
@endforeach