<?php

namespace Modules\CategoryNews\Sidebar;

use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{

    /**
     * @param \Maatwebsite\Sidebar\Menu $menu
     * @return \Maatwebsite\Sidebar\Menu
     */
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.content'), function (Group $group) {
            $group->item(trans('news::news.news'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(0);
                $item->authorize(
                /* append */
                );

                $item->item(trans('categorynews::category_news.category_news'), function (Item $item) {
                    $item->weight(10);
                    $item->route('admin.category_news.index');
                    $item->authorize(
                        $this->auth->hasAccess('admin.category_news.index')
                    );
                });
            });
        });

        return $menu;
    }
}
