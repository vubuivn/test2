<section class="lists-4 sec-tb ">
    @if (count($groupList) > 0 || count($groupList2) > 0)
    <div class="row" id="group-item-loop">
        @foreach ($groupList AS $index => $group)
            @include('public.products.listing.group-list-item')
        @endforeach

        @foreach ($groupList2 AS $index => $group)
            @include('public.products.listing.group-list-item')
        @endforeach
    </div>
    @else
    <p class="text-center">
        <b>{{ trans('group::group-categories.product_not_found') }}</b>
    </p>
    @endif

    @if (count($groupList) === 6)
    <div class="showmore text-center">
        <a href="#" class="btn btn1" id="load-more-groups">
            {{ trans('group::group-categories.view_more') }}
        </a>
    </div>
    @endif
</section>

<script>
    (function($){
        var query = "{!! request()->fullUrlWithQuery(['ajax' => 1]) !!}";
        var page = 1;
        $('#load-more-groups').on('click', function(e) {
            e.preventDefault();
            var ajaxQuery = query + '&page=' + (page++);
            $.ajax(ajaxQuery).done(function(res) {
                if (res) {
                    $('#group-item-loop').append(res);
                } else {
                    $('#load-more-groups').hide();
                }
            });
        });
    })(jQuery);
</script>