<?php

return [
    'groups' => [
        'code' => 'Mã nhóm',
        'categories' => 'Danh mục',
        'vi_name' => 'Tiêu đề',
        'vi_description' => 'Mô tả',
        'vi_content_1' => 'Đặc điểm nổi bật',
        // 'vi_content_2' => 'Nội dung ở block 2',
        'en_name' => 'Title',
        'en_description' => 'Description',
        'en_content_1' => 'Highlight',
        // 'en_content_2' => 'Content in block 2',
        'is_active' => 'Trạng thái',
        'collections' => 'Tên bộ sưu tập',
        'image_count' => 'Số lượng hình CB',
        'is_new' => 'New',
        'new_end' =>'Số ngày new'
    ],

    /**
     * column and field defination for import
     */
    'products' => [
        'sku' => 'Mã sản phẩm (SKU)',
        'group_code' => 'Mã nhóm',
        'length' => 'Dài (cm)',
        'width' => 'Rộng (cm)',
        'height' => 'Cao (cm)',
        // 'volume' => 'Thể tích',
        'is_active' => 'Trạng thái',
        'price' => 'Giá',
        'seats' => 'Số chỗ ngồi',
        'image_count' => 'Số lượng hình',
        'out_of_stock' => 'Hết hàng'
    ],

    'product_promotions' => [

    ]

];
