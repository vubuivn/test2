@extends('admin::layout')

@component('admin::components.term.header')
    @slot('title', trans('term::terms.show', ['resource' => trans('term::terms.term')]))

    <li><a href="{{ route('admin.terms.index') }}">{{ trans('term::terms.terms') }}</a></li>
    <li class="active">{{ trans('term::terms.show', ['resource' => trans('term::terms.term')]) }}</li>
@endcomponent

@section('content')
    <div class="accordion-content clearfix">
        @foreach($builder->elements as $element)
            <div class="rows clearfix" style="margin-bottom: 10px">
            <div class="form-group ">
                <div class="col-md-3 control-label text-right">
                    {{ $element->name }}:</div>
                <div class="col-md-9">
                    @if ($element->type == 'checkbox' && is_array(json_decode($term->{$element->element_name})))
                        @foreach(json_decode($term->{$element->element_name}) as $value)
                            <p>{{ $value }}</p>
                        @endforeach
                    @elseif ($element->type == 'file')
                        @if (!is_null($term->{$element->element_name}))
                            <a href="{!! \Illuminate\Support\Facades\Storage::disk(config('filesystems.default'))->url($term->{$element->element_name}); !!}" target="_blank">{{ basename(\Illuminate\Support\Facades\Storage::disk(config('filesystems.default'))->url($term->{$element->element_name})) }}</a>
                        @endif
                    @else
                        {{ trim($term->{$element->element_name}, '"') }}
                    @endif
                </div>
            </div>
            </div>
        @endforeach
            <div class="rows">
                <div class="form-group ">
                    <div class="col-md-3 control-label text-right">
                        {{ trans('admin::admin.table.created') }}:</div>
                    <div class="col-md-9">
                        {{ $term->created_at->diffForHumans() }}
                    </div>
                </div>
            </div>
        <div class="rows">
            <button type="button" class="btn btn-default btn-danger" style="margin-top: 60px; margin-left: 10px" onclick="if (confirm('Are you sure you want to delete?')){document.getElementById('form-{{ $term->id }}').submit();}else{event.stopPropagation(); event.preventDefault();};
                    ">
                Delete
            </button>
        </div>
    </div>
    <form id="form-{{ $term->id }}" action="{{ route('admin.terms.destroy', ['ids?' => $term->id]) }}" method="POST" style="display: none;">
        @csrf
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection
