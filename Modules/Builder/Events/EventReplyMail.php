<?php

namespace Modules\Builder\Events;

use Illuminate\Queue\SerializesModels;

class EventReplyMail
{
    use SerializesModels;

    public $data;
    public $reply;
    public $builder;
    public $noti;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data,$reply,$builder,$noti=0)
    {
        $this->data = $data;
        $this->reply = $reply;
        $this->builder = $builder;
        $this->noti = $noti;
    }
}
