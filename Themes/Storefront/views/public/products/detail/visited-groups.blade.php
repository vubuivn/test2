@if (count($visitedGroups))
<section class="lists-4 sec-t ">
    <h3>{{ trans('group::group-categories.viewed_products') }}</h3>
    <div class="row">
        @foreach ($visitedGroups AS $index => $group)
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="item">
                    <a href="{{ route('products.show', ['slug' => $group->slug]) . '?sku=' . getSkuFromGroup($group) }}">
                        <div class="img">
                            
                            <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                                data-lazy-src="{{ productImageUrl(getSkuFromGroup($group), 1) }}" alt="" />
                        </div>
                        <div class="divtext ">
                            <h6 class="title">{{ $group->name }}</h6>
                            <div class="price">
                                <span>{{ trans('group::group-categories.from') }} {{ formatMoney($group->lowest_price) }}</span>

                                <!-- wishlist block -->
                                {!! printWishListIcon(getIdFromGroup($group)) !!}
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</section>
@endif