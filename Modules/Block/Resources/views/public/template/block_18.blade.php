<div class="page-about">
<div class="container">
<section class="about-3 ">
			<div class="max670">
		        <h2>{{$title}}</h2>
		        {!!$description!!}
	        </div>

	        <div class="row level1  grid-space-0 mb-20">
	        	<div class="col-lg-3 img ">
	        		<img class="lazy-hidden" data-lazy-type="image"   data-lazy-src="{{$image->file}}"   alt="">
	        	</div>
	        	<div class="col-lg-9">
	        		<div class="divtext">
	        			<div class="row grid-space-0">
							@php($i=1)
							@foreach($images as $key=>$value)
								<div class="col-md-6 @if(($i%2)==0){{'col-end'}}@else{{'col-frist'}}@endif">
									<div class="item" >   
										<span class="img2" >
											<img class="lazy-hidden" data-lazy-type="image"   data-lazy-src="{{$value->file}}"   alt="">
										</span>
										<div class="text">
											{{$value->caption_1}}
										</div>
									</div>
								</div>
								@php($i++)
							@endforeach	        				
	        			</div>
	        			
	        		</div>
	        	</div>
	        </div>
	    </section>
	</div>
</div>