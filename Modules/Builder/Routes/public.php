<?php

Route::get('/builder/{builder}/embedded-form', 'BuilderController@form_validate')->name('public.builders.embedded-form');
Route::post('/builder/{builder}/embedded-form', 'BuilderController@embedded');
