<?php

return [
    'name' => 'Name',
    'type' => 'Type',
    'is_required' => 'Required',
    'label' => 'Label',
    'price' => 'Price',
    'price_type' => 'Price Type',

    // Validations
    'values.*.label' => 'Label',
    'values.*.price' => 'Price',
    'values.*.price_type' => 'Price Type',

    'elements.*.name' => 'Name',
    'elements.*.type' => 'Type',
    'elements.*.values.*.label' => 'Label',
    'elements.*.values.*.price' => 'Price',
    'elements.*.values.*.price_type' => 'Price Type',
];
