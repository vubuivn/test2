<?php

return [
    'product'  => 'Product',
    'products' => 'Products',
    'table'    => [
        'thumbnail' => 'Thumbnail',
        'name'      => 'Name',
        'price'     => 'Price',
    ],
    'tabs'     => [
        'group'            => [
            'basic_information'    => 'Basic Information',
            'advanced_information' => 'Advanced Information',
        ],
        'general'          => 'General',
        'price'            => 'Price',
        'inventory'        => 'Inventory',
        'images'           => 'Images',
        'seo'              => 'SEO',
        'related_products' => 'Related Products',
        'up_sells'         => 'Up-Sells',
        'cross_sells'      => 'Cross-Sells',
        'additionals'      => 'Additionals',
        'related_groups'   => 'Related Groups',
        'filter'           => 'Filter',
    ],
    'form'     => [
        'please_select'             => 'Please Select',
        'enable_the_product'        => 'Enable the product',
        'manage_stock_states'       => [
            '0' => 'Don\'t Track Inventory',
            '1' => 'Track Inventory',
        ],
        'stock_availability_states' => [
            '1' => 'In Stock',
            '0' => 'Out of Stock',
        ],
        'base_image'                => 'Base Image',
        'additional_images'         => 'Additional Images',
        'show_main_products'        => 'Show main products',
        'show_products_by_side'     => 'Show products by side',
        'hide_from_the_web'         => 'Hide from the web',
        'back'                      => 'Back',
    ],
];
