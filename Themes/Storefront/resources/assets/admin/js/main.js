window.admin.removeSubmitButtonOffsetOn([
    '#logo', '#slider_banners', '#banner_section_1', '#banner_section_2', '#banner_section_3', '#brands'
]);
$(document).on('click', '.banner-image', function (e) {
    let picker = new MediaPicker({type: 'image'});

    picker.on('select', (file) => {
        $(this).find('i').remove();
        $(this).find('img').attr('src', file.path).removeClass('hide');
        $(this).find('.banner-file-id').val(file.id);
    });
});
