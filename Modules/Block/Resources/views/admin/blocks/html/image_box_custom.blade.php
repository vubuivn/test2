<div class="banner-body">
    <div class="banner-image-custom">
        @if(!isset($img))
            <i class="fa fa-picture-o" aria-hidden="true"></i>
        @endif
        <img
            data-input-name="blocks[{{$number}}][{{$block->key}}][{{$item}}][file]" {{isset($img) ? "src=$img" : 'class="hide"'}}>
        <input type="hidden" name="blocks[{{$number}}][{{$block->key}}][{{$item}}][file]"
               value="{{$image->file ?? '' }}" class="banner-file-id">
    </div>
  
        <button type="button" class="btn btn-default delete-image pull-right"
                style="position: absolute;right: 0;z-index: 500;top: 0;">
            <i class="fa fa-trash"></i>
        </button>

    <div class="banner-content clearfix">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-4">
                <div class="form-group">
                    <label>{{ trans('slider::attributes.caption_1') }}</label>
                    <input type="text" name="blocks[{{$number}}][{{$block->key}}][{{$item}}][caption_1]"
                           value="{{$image->caption_1 ?? '' }}" class="form-control">
                </div>
            </div>

            <div class="col-lg-4 col-md-12 col-sm-4">
                <div class="form-group">
                    <label>{{ trans('slider::attributes.caption_2') }}</label>
                    <input type="text" name="blocks[{{$number}}][{{$block->key}}][{{$item}}][caption_2]"
                           value="{{$image->caption_2 ?? '' }}" class="form-control">
                </div>
            </div>

            <div class="col-lg-4 col-md-12 col-sm-4">
                <div class="form-group">
                    <label>{{ trans('block::attributes.caption_3') }}</label>
                    <input type="text" name="blocks[{{$number}}][{{$block->key}}][{{$item}}][caption_3]"
                           value="{{$image->caption_3 ?? '' }}" class="form-control">
                </div>
            </div>

            <div class="col-lg-4 col-md-12 col-sm-4 clearfix">
                <div class="form-group">
                    <label>{{ trans('slider::attributes.call_to_action_text') }}</label>
                    <input type="text"
                           name="blocks[{{$number}}][{{$block->key}}][{{$item}}][call_to_action_text]"
                           value="{{$image->call_to_action_text ?? '' }}"
                           class="form-control">
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-4 clearfix">
                <div class="form-group">
                    <label>{{ trans('slider::attributes.call_to_action_url') }}</label>
                    <input type="text"
                           name="blocks[{{$number}}][{{$block->key}}][{{$item}}][call_to_action_url]"
                           value="{{$image->call_to_action_url ?? '' }}"
                           class="form-control">
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-4 clearfix">
                <div class="form-group">
                    <label>{{ trans('block::attributes.video_url') }}</label>
                    <input type="text"
                           name="blocks[{{$number}}][{{$block->key}}][{{$item}}][video_url]"
                           value="{{$image->video_url ?? '' }}"
                           class="form-control">
                </div>
            </div>
        </div>
    </div>
</div>
