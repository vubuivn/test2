{{ Form::text('name', trans('page::attributes.name'), $errors, $page, ['labelCol' => 2, 'required' => true]) }}
<div class="form-group ">
    <label for="name" class="col-md-2 control-label text-left">{{trans('page::pages.template')}}</label>
    <div class="col-md-10">
        <select name="template" class="form-control">
            <option value @if($page->template == '' || $page->template == null) selected @endif>{{trans('page::pages.normal')}}</option>
            <option value="has_sidebar" @if($page->template == 'has_sidebar') selected @endif>{{trans('page::pages.has_sidebar')}}</option>
        </select>
    </div>
</div>
<div class="form-group ">
    <div class="col-md-8">
        {{ Form::checkbox('is_active', trans('page::attributes.is_active'), trans('page::pages.form.enable_the_page'), $errors, $page) }}
    </div>
</div>

@include('media::admin.image_picker.single', [
    'title' => trans('page::pages.form.banner_image'),
    'inputName' => 'files[base_image]',
    'file' => $page->baseImage,
])


<input type="hidden" id="block-number" value="{{ isset($page->blocks) ? count($page->blocks) : 0 }}">
<div class="form-group ">
    <div class="col-md-12">
        <button class="btn btn-default btn-sm pull-right" id="expandAllFormats"></button>
    </div>
</div>
<div class="content-accordion panel-group options-group-wrapper" id="page-block" role="tablist"
     aria-multiselectable="true">
    @if($page->blocks)

        @foreach ($page->blocks as $key => $item)
            <form></form>
            @php($block = \Modules\Block\Entities\Block::where('id',$item->block_id)->withoutGlobalScope('active')->first())
            @php($data = null)
            @if($item->content != null)
                @php($data = json_decode($item->content))
            @endif
            <form name="block-form" id="block-form-{{$key}}">
                <input type="hidden" name="blocks[{{$key}}][main_block_id]" value="{{$item->id}}">
                <input type="hidden" name="blocks[{{$key}}][block_id]" value="{{$block->id}}">
                <div class="panel panel-default" style="margin-bottom: 15px">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="collapsed" data-parent="#block-{{$key}}-edit"
                               href="#custom-collapse-{{$key}}-edit">
                                <span class="drag-icon pull-left">
                                    <i class="fa"></i>
                                    <i class="fa"></i>
                                </span>
                                <span id="block-name" class="pull-left">
                                    {{$block->name}} - {{$item->title}}
                                </span>
                            </a>
                        </h4>
                    </div>
                    <div id="custom-collapse-{{$key}}-edit" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="new-option clearfix">
                                <div class="col-lg-6 col-md-12 p-l-0">
                                    <div class="form-group">
                                        <label for="block-{{$key}}-edit-name">
                                            {{ trans('block::blocks.form.name') }}
                                        </label>
                                        <input type="text" name="blocks[{{$key}}][main_block_title]"
                                               class="form-control block-name-field"
                                               id="block-{{$key}}-edit-name"
                                               value="{{$item->title}}">
                                    </div>
                                </div>

                                <button type="button" class="btn btn-default delete-block pull-right"
                                        data-toggle="tooltip" title=""
                                        data-original-title="{{trans('block::blocks.form.delete_block')}}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>

                            <div class="clearfix"></div>

                            <div class="option-values clearfix" id="block-{{$key}}-edit-values">
                                @foreach ($block->values as $value)
                                    {{block_html($value, $key, $data)}}
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @endforeach
</div>

@push('scripts')
    <script>
        $('.content-accordion').find('.banner-body:eq(0)').find('.delete-image').remove();
    </script>
@endpush
@endif
@push('globals')
    <script>
        FleetCart.langs['page::pages.expand_all'] = '{{trans('page::pages.expand_all')}}';
        FleetCart.langs['page::pages.collapse_all'] = '{{trans('page::pages.collapse_all')}}';
    </script>
@endpush

@push('styles')
    <style type="text/css">
        .banner-image-custom{position: absolute;
    top: 28px;
    left: 15px;
    border: 1px solid #d2d6de;
    border-radius: 3px;
    height: 150px;
    width: 150px;
    cursor: pointer;
    z-index: 0;}
    .banner-image-custom i{    position: absolute;
    left: 50%;
    top: 50%;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);}
    .banner-body .banner-image-custom>i {
    font-size: 70px;
    color: #d9d9d9;
    z-index: -1;
}
.banner-body .banner-image-custom>img {
    max-height: 100%;
    max-width: 100%;
}
.banner-body .banner-image-custom>img {
    position: absolute;
    left: 50%;
    top: 50%;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}
    </style>
@endpush