<div class="container">
    <section class="sec-catalogue-1 sec-tb">
        <div class="item" >   
            <div class="row center grid-space-0 @if($position == 'right'){{'end'}}@endif">
                <div class="col-md-5" >
                    <div class="img tRes_130">
                        <img  class="lazy-hidden" data-lazy-type="image"  data-lazy-src="@if(isset($image)){{$image->file}}@endif"   alt="">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="divtext">
                        <div class="t1">@if(isset($title1)){{$title1}}@endif</div>
                        <h2 class="title">@if(isset($title2)){{$title2}}@endif</h2>
                        <div class="t2">{{trans('storefront::storefront.page.year')}}:  @if(isset($year)){{$year}}@endif</div>
                        <div class="t3">{{trans('storefront::storefront.page.description')}}</div>
                        <div class="t4">
                            @if(isset($description)){!!$description!!}@endif
                        </div>
                        <div>
                            <a href="@if(isset($download)){{$download}}@endif" class="btn btn2">{{trans('storefront::storefront.page.download')}}</a>    <a href="#" class="btn btnfb"> <i class="icon-facebook1"></i> {{trans('storefront::storefront.page.share')}}</a>
                        </div>
                    </div>
                </div>
            </div>                              
        </div>
    </section>
</div>