<section class="lists-2 sec-tb group-ef lazy-hidden">
    <div class="container">
        <div class="entry-head">
            <h2 class="ht">{{ $main_title }}</h2>
        </div>
        <div class="row">
            @foreach ($categories as $key => $image)
            <div class="col-6 {{ in_array($key % 6, [2, 3]) ? "col-md-6" : "col-lg-3"}}
					efch-{{ $key + 1 }} ef-img-t">
                <a href="{{ $image->call_to_action_url }}" class="item">
                    <div class="img">
                        <img class="lazy-hidden" data-lazy-type="image" src="/assets/images/image.svg"
                            data-lazy-src="{{ $image->file }}" alt="{{ $image->caption_1 }}" />
                    </div>
                    <div class="title"> {{ $image->call_to_action_text }} </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>