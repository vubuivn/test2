@extends('admin::layout')

@component('admin::components.element.header')
    @slot('title', trans('element::elements.elements'))

    <li class="active">{{ trans('element::elements.elements') }}</li>
@endcomponent

@section('content')
    <div class="row">
        <div class="btn-group pull-right">
            <a href="{{ route("admin.builders.edit", ['id' => $builder]) }}" class="btn btn-primary btn-actions">
                Builder
            </a>
            <a href="{{ route("admin.builder.elements.create", ['builder' => $builder]) }}" class="btn btn-primary btn-actions btn-create">
                {{ trans("admin::resource.create", ['resource' => 'element']) }}
            </a>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-body index-table">
            <div class="table-responsive">
                <table class="table table-striped table-hover dataTable no-footer" id="{{ $id ?? '' }}">
                    <thead>
                    <tr>
                        <th>{{ trans('element::elements.table.name') }}</th>
                        <th>{{ trans('element::elements.table.type') }}</th>
                        <th>Required</th>
                        <th>{{ trans('admin::admin.table.created') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($elements as $element)
                        <tr role="row" class="clickable-row">
                            <td><a href="{{ route('admin.elements.edit', ['id' => $element->id]) }}">{{ $element->name }}</a></td>
                            <td>{{ $element->type }}</td>
                            <td>{!! $element->is_required
                    ? '<span class="dot red"></span>'
                    : '' !!}</td>
                            <td>
                                {{ $element->created_at }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>

            </div>
        </div>
    </div>
@endsection
