<div class=" inner" >
    <div class="mini-cart" >
        @foreach ($cart->items() as $cartItem)
        <div class="mini-cart-item clearfix">
            <div class="mini-cart-image">
                <img src="{{ productImageUrl($cartItem->product->sku, 1) }}">
            </div>
            <div class="mini-cart-details ">
                <a class="product-name" href="{{ route('products.show', ['slug' => $cartItem->product->group->slug]) . '?sku=' . $cartItem->product->sku }}">{{ $cartItem->product->name }}</a>
                <div class="product-quantity-wrapper">
                    <span class="product-quantity">{{ $cartItem->qty }} *</span>
                    <span class="product-price ">{{ $cartItem->unitPrice()->convertToCurrentCurrency()->format() }}</span>
                </div>
                <form method="POST" action="{{ route('cart.items.destroy', encrypt($cartItem->id)) }}"
                        onsubmit="return confirm('{{ trans('cart::messages.confirm') }}');">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}
                    <button type="submit" class="btn-close" data-toggle="tooltip" data-placement="left" title=""
                        data-original-title="{{ trans('storefront::cart.remove') }}">
                    ×
                    </button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
</div>
    <div class="subtotal">
        <label for="subtotal">{{ trans('storefront::cart.total') }}:</label>
        <span>{{ $cart->total()->convertToCurrentCurrency()->format() }}</span>
    </div>
<div class="mini-cart-buttons text-center">
    <a href="{{ route('cart.index') }}" class="btn btn-primary btn-view-cart full">
        {{ trans('storefront::layout.mini_cart.view_cart') }}
    </a>
</div>
