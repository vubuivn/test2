<section class="lists-2 sec-tb group-ef lazy-hidden">
    <div class="container">
        <div class="entry-head">
            <h2 class="ht">{{ $main_title }}</h2>
        </div>
        <div class="row">
            @if ($image1)
            <div class="col-6 col-lg-3 efch-1 ef-img-t">
                <a href="{{ $image1->call_to_action_link }}" class="item">
                    <div class="img  ">
                        <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                            data-lazy-src="{{ $image1->file }}" alt="" />
                    </div>
                    <div class="title "> {{ $image1->caption }} </div>
                </a>
            </div>
            @endif
            @if($image2)
            <div class="col-6 col-lg-3 efch-2 ef-img-t">
                <a href="#" class="item">
                    <div class="img  ">
                        <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                            data-lazy-src="/assets/images/cate-2.jpg" alt="" />
                    </div>
                    <div class="title "> Vật dụng trang trí </div>
                </a>
            </div>
            @endif
            @if ($image3)
            <div class="col-6 col-md-6 efch-3 ef-img-t">
                <a href="#" class="item">
                    <div class="img  ">
                        <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                            data-lazy-src="/assets/images/cate-3.jpg" alt="" />
                    </div>
                    <div class="title "> Thảm phòng khách </div>
                </a>
            </div>
            <div class="col-6 col-md-6 efch-4 ef-img-t">
                <a href="#" class="item">
                    <div class="img  ">
                        <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                            data-lazy-src="/assets/images/cate-4.jpg" alt="" />
                    </div>
                    <div class="title "> Ghế sofa </div>
                </a>
            </div>
            <div class="col-6 col-lg-3 efch-5 ef-img-t">
                <a href="#" class="item">
                    <div class="img  ">
                        <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                            data-lazy-src="/assets/images/cate-5.jpg" alt="" />
                    </div>
                    <div class="title "> Bàn phòng khách </div>
                </a>
            </div>
            <div class="col-6 col-lg-3 efch-6 ef-img-t">
                <a href="#" class="item">
                    <div class="img  ">
                        <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                            data-lazy-src="/assets/images/cate-6.jpg" alt="" />
                    </div>
                    <div class="title "> Ghế đơn </div>
                </a>
            </div>
        </div>
    </div>
</section>