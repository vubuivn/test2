@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => trans('mailtemplate::mail_templates.mail_template')]))
    @slot('subtitle', '')

    <li><a href="{{ route('admin.mail_templates.index') }}">{{ trans('mailtemplate::mail_templates.mail_templates') }}</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => trans('mailtemplate::mail_templates.mail_template')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.mail_templates.update', $mailTemplate) }}" class="form-horizontal" id="mail-template-edit-form" novalidate>
        {{ csrf_field() }}
        {{ method_field('put') }}
        {!! $tabs->render(compact('mailTemplate')) !!}
    </form>
@endsection

@include('mailtemplate::admin.mail_templates.partials.shortcuts')
