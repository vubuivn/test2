<?php

return [
    'not_found' => '404',
    'oops' => 'Oops!',
    'the_page_not_found' => 'The page not found.',
    'click_link_below' => 'It looks like nothing was found at this location. Click the link below to return home',
    'back_to_shop' => 'Back to Shop'
];
