<?php

return [
    'auth' => [
        'remember_me' => 'Remember me',
    ],

    'users' => [
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email',
        'password' => 'Password',
        'password_confirmation' => 'Confirm Password',
        'roles' => 'Roles',
        'activated' => 'Status',
        'new_password' => 'New Password',
        'new_password_confirmation' => 'Confirm New Password',
        'phone' => 'Phone',
        'birthday' => 'Birthday',
        'name' => 'Name',
        'gender' => 'Gender'
    ],

    'roles' => [
        'name' => 'Name',
    ],
];
