<?php

namespace Modules\Account\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class AccountWishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(auth()->user()->wishlist()->get());
        $arr =[];
        $wishlist = auth()->user()->wishlist()->with('group')->get();
        foreach($wishlist as $w){
            $arr[] = $w->group->id;
        }
        $products = buildFromToPriceQueryBuilder()->whereIn('groups.id',$arr)->paginate(15);
        // dd($products);
        // $lowestPrice = DB::table('products AS p')
        //             ->select('p.group_id',
        //                         DB::raw('MIN(p.selling_price) as lowest_price'),
        //                         DB::raw('MAX(p.selling_price) as highest_price'),
        //                         DB::raw('MAX(p.updated_at) as latest'),
        //                         DB::raw('MIN(CONCAT(id, "___", sku)) as idSku2'),
        //                         DB::raw('MIN(sku) as idSkuS')
        //                         )
        //             ->whereIn('p.is_active', [0, 1])
        //             ->groupBy('p.group_id');

        // $products = auth()->user()
        //     ->wishlist()->select('*')
        //     ->with('group')
        //     ->joinSub($lowestPrice, 'product2', function ($join) {
        //         $join->on('products.group_id', '=', 'product2.group_id');
        //     })
        //     ->with('files')
        //     ->paginate(15);
        $my = auth()->user();
        return view('public.account.wishlist.index', compact('products','my'));
    }

    public function destroy($productId)
    {
        auth()->user()->wishlist()->detach($productId);

        return back()->withSuccess(trans('account::messages.product_removed_from_wishlist'));
    }
}
