<?php

namespace Modules\Designer\Http\Controllers;

use Modules\Designer\Entities\Designer;
use Modules\Media\Entities\File;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class DesignerController extends Controller
{
    /**
     * Display designer for the slug.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    protected $perPage = 12;

    public function designer()
    {
        $designers = Designer::where('is_active',1)->limit($this->perPage)->get();
        return view('public.designer.show', compact('designers'));
    }

    public function designerDetail($slug){
        $designer = Designer::where('slug',$slug)->where('is_active',1)->firstOrFail();
        $related = Designer::where('is_active',1)->where('id','<>',$designer->id)->limit(4)->get();
        return view('public.designer.detail', compact('designer','related'));
    }
    public function designerMore(Request $request){
        if($request->has('total')){
            $total = $request->total;
            $designers = Designer::where('is_active',1)->skip($total)->take($this->perPage)->get();
            if($designers && count($designers)>0){
                return view('public.designer.ajax',compact('designers','total'));
            }else{
                return 'fail';
            } 
        }   
        return '';
    }
}
