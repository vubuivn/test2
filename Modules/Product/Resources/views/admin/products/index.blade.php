@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('product::products.products'))

    <li class="active">{{ trans('product::products.products') }}</li>
@endcomponent
@component('admin::components.page.index_table')
    @slot('buttons', ['create'])
    @slot('resource', 'products')
    @slot('name', trans('product::products.product'))
    @slot('buttons_js', ['export_excel', 'import_excel','import_promotion'])

    @slot('thead')
        @include('product::admin.products.partials.thead', ['name' => 'products-index'])
    @endslot
@endcomponent
<div style="display: none">
    <input type="file" name="file" id="file"
           accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
</div>
@push('styles')
    <link href="{{ asset('loading.css') }}" rel="stylesheet">
@endpush
@push('scripts')
    <script src="{{ asset('loading.js') }}"></script>
    <script>
        $("#btn_export_excel_id").on('click', () => {
            $('body').loading('start');
            $.ajax({
                type: 'GET',
                url: route('admin.products.exportAll.excel'),
                success: function (response, textStatus, request) {
                    $('body').loading('stop');
                    if (response.fileUrl) {
                        window.location.href = response.fileUrl;
                    }
                    
                    // var a = document.createElement("a");
                    // a.href = response.file;
                    // a.download = response.name;
                    // document.body.appendChild(a);
                    // a.click();
                    // a.remove();
                },
                error(xhr) {
                    $('body').loading('stop');
                    alert('{{trans('admin::messages.system_error')}}');
                },
            });
        });
        var importType = 0;
        $("#btn_import_excel_id").on('click', () => {
            importType = 0;
            $('#file').click();
        });
        $("#btn_import_promotion_id").on('click', () => {
            importType = 1;
            $('#file').click();
        });
        $("#btn_import_attribute_id").on('click', () => {
            importType = 2;
            $('#file').click();
        });
        new DataTable('#products-table .table', {
            columns: [
                {data: 'checkbox', orderable: false, searchable: false, width: '3%'},
                {data: 'sku', name: 'sku'},
                {data: 'group.name', searchable: false},
                {data: 'price', searchable: false},
                {data: 'status', name: 'is_active', searchable: false},
                {data: 'created', name: 'created_at'},
                {data: 'link', name: 'link',searchable: false,},
            ],
        }, function () {
            let exportButton = `
            <button type="button" class="btn btn-default btn-delete">
                    {{trans('category::categories.tree.export_excel')}}
                </button>
            `;
            exportButton = $(exportButton).appendTo(
                this.element.closest('.dataTables_wrapper').find('.dataTables_length')
            );
            exportButton.on('click', () => {
                let checked = $('#products-table .table').find('.select-row:checked');

                if (checked.length === 0) {
                    return;
                }
                let confirmationModal = $('#confirmation-export-modal');
                let exported = [];

                confirmationModal.modal('show').find('form').on('submit', (e) => {
                    e.preventDefault();

                    confirmationModal.modal('hide');

                    let table = $('#products-table .table').DataTable();

                    table.processing(true);

                    let ids = this.constructor.getRowIds(checked);

                    // Don't make ajax request if an id was previously deleted.
                    if (exported.length !== 0 && _.difference(exported, ids).length === 0) {
                        return;
                    }
                    console.log(ids.join());
                    window.DataTable.setSelectedIds($('#products-table .table'), []);

                    window.DataTable.reload($('#products-table .table'));
                    $.ajax({
                        type: 'GET',
                        url: route('admin.products.export.excel'),
                        data: {
                            ids: ids.join()
                        },
                        success: function (response) {
                            var a = document.createElement("a");
                            a.href = response.file;
                            a.download = response.name;
                            document.body.appendChild(a);
                            a.click();
                            a.remove();

                            exported = _.flatten(exported.concat(ids));

                            window.DataTable.setSelectedIds($('#products-table .table'), []);

                            // window.DataTable.reload($('#products-table .table'));
                        },
                        error: (xhr) => {
                            error(`${xhr.statusText}: ${xhr.responseJSON.message}`);

                            exported = _.flatten(exported.concat(ids));

                            window.DataTable.setSelectedIds($('#products-table .table'), []);

                            window.DataTable.reload($('#products-table .table'));
                        },
                    });
                    // $.ajax({
                    //     type: 'DELETE',
                    //     url: this.route('destroy', {ids: ids.join()}),
                    //     success: () => {
                    //         deleted = _.flatten(deleted.concat(ids));
                    //
                    //         this.constructor.setSelectedIds(this.selector, []);
                    //
                    //         this.constructor.reload(this.element);
                    //     },
                    //     error: (xhr) => {
                    //         error(`${xhr.statusText}: ${xhr.responseJSON.message}`);
                    //
                    //         deleted = _.flatten(deleted.concat(ids));
                    //
                    //         this.constructor.setSelectedIds(this.selector, []);
                    //
                    //         this.constructor.reload(this.element);
                    //     },
                    // });
                });
            })
        });

        function uploadFile() {
            var file_data = $('#file').prop('files')[0];
            var type = file_data.type;

            if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                var formData = new FormData();

                formData.append('import_file', file_data);
                formData.append('import_type', importType);
                $.ajaxSetup({
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Csrf-Token', $('input[name="_token"]').attr('content'));
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: route('admin.products.import.excel'),
                    data: formData,
                    processData: false,
                    async: false,
                    cache: false,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    success: function (res) {
                        if (!res.status) {
                            alert(res.message);
                        } else {
                            if (res.errorProductCount) {
                                var a = document.createElement("a");
                                a.href = res.errorFilePath;
                                a.download = res.errorFileName;
                                document.body.appendChild(a);
                                a.click();
                                a.remove();
                                // window.open(res.errorFilePath);
                                // location.href = res.errorFilePath;
                            }
                            
                            var messages = [
                                res.productCreatedCount + ' san pham duoc tao moi',
                                res.productUpdatedCount + ' san pham duoc cap nhat',
                                res.errorProductCount + ' san pham bi loi',
                            ];
                            alert(messages.join("\n"));
                        }

                        location.reload();
                    },
                    error(xhr) {
                        $('body').loading('stop');
                        alert('{{trans('admin::messages.system_error')}}');
                    },
                });
            } else {
                location.reload();
                $('body').loading('stop');
            }
        }

        $('#file').change(function () {
            $('body').loading('start');
            _.delay(uploadFile, 1000);
        });
    </script>
@endpush
