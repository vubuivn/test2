{{--<div class="form-group ">--}}
    {{--<label for="name" class="col-md-2 control-label text-left">--}}
        {{--Name--}}
    {{--</label>--}}
    {{--<div class="col-md-10">--}}
        {{--<input name="name" class="form-control " type="text" @if($mailTemplate->id) name="name" disabled readonly @endif value="{{$mailTemplate->name}}">--}}
    {{--</div>--}}
{{--</div>--}}
{{ Form::text('name', trans('mailtemplate::attributes.name'), $errors, $mailTemplate, ['labelCol' => 2, 'required' => true]) }}
{{ Form::text('code', trans('mailtemplate::attributes.code'), $errors, $mailTemplate, ['labelCol' => 2, 'required' => true]) }}
{{ Form::text('subject', trans('mailtemplate::attributes.subject'), $errors, $mailTemplate, ['labelCol' => 2, 'required' => true]) }}
{{ Form::text('email_cc', trans('mailtemplate::attributes.email_cc'), $errors, $mailTemplate, ['labelCol' => 2]) }}
{{ Form::wysiwyg('content', trans('mailtemplate::attributes.content'), $errors, $mailTemplate, ['labelCol' => 2, 'required' => true]) }}
