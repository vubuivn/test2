@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => trans('career::careers.career')]))
    @slot('subtitle', $career->title)

    <li><a href="{{ route('admin.careers.index') }}">{{ trans('career::careers.careers') }}</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => trans('career::careers.career')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.careers.update', $career) }}" class="form-horizontal" id="career-form" novalidate>
        {{ csrf_field() }}
        {{ method_field('put') }}

        {!! $tabs->render(compact('career')) !!}
    </form>
@endsection

@include('career::admin.careers.partials.shortcuts')