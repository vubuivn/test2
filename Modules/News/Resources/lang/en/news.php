<?php

return [
    'news' => 'News',
    'tabs' => [
        'news'    => [
            'basic_information'    => 'Basic Information',
            'advanced_information' => 'Advanced Information',
        ],
        'general' => 'General',
        'seo'     => 'SEO',
        'filter'  => 'Filter',
    ],
    'form' => [
        'enable_the_news' => 'Enable the News',
    ],
];
