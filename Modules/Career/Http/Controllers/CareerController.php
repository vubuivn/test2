<?php

namespace Modules\Career\Http\Controllers;

use Modules\Career\Entities\Career;
use Modules\Media\Entities\File;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class CareerController extends Controller
{
    /**
     * Display career for the slug.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $positions = Career::leftJoin('career_translations','careers.id','=','career_translations.career_id')->select('career_translations.position')->groupBy('position')->get();
        $city_ids = Career::select('city_id')->groupBy('city_id')->get()->toArray();
        $arrIds= [];
        foreach($city_ids as $c){
            $arrIds[] = $c['city_id'];
        }
        $cities = \DB::table('cities')->whereIn('id',$arrIds)->get();
        $query = new Career();

        if($request->has('position') && $request->position != ""){
            $query = $query->leftJoin('career_translations','careers.id','=','career_translations.career_id')->where('career_translations.position',$request->position);
        }
        if($request->has('location') && $request->location != ""){
            $query = $query->where('city_id',$request->location);
        }
        $careers = $query->where('is_active',1)->get();
        return view('public.career.show', compact('positions', 'cities','careers'));
    }

    public function detail($slug){
        $career = Career::where('slug',$slug)->firstOrFail();
        $city = \DB::table('cities')->where('id',$career->city_id)->first();
        return view('public.career.detail',compact('career','city'));
    }

}
