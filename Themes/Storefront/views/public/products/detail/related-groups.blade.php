@php ($groupCount = count($relatedGroups))
@php($groups=[])
@if ($groupCount > 0)
    @php($groups = $relatedGroups)
@endif

<?php 
    $mores = [];
    $total = 4;
    if($groupCount<$total){
        if(Cache::tags([$group->id.'products'.locale()])->has('product_realted_by_cat_'.$group->id.':' . locale())){
            $mores = Cache::tags(['product_detail'])->get('product_realted_by_cat_'.$group->id.':' . locale());
        }else{
            $mores = Cache::tags([$group->id.'products'.locale()])
                ->rememberForever('product_realted_by_cat_'.$group->id.':' . locale(),function () use($group,$total,$groupCount){
                    $cat = $group->categories()->first();
                    return buildFromToPriceQueryBuilder()->leftJoin('group_categories','groups.id','=','group_categories.group_id')->where('group_categories.category_id',$cat->id)->where('groups.id','<>',$group->id)->where('collections','<>',$group->collections)->limit($total-$groupCount)->get();

            });
        }
        // $cat = $group->categories()->first();
        // $mores = buildFromToPriceQueryBuilder()->leftJoin('group_categories','groups.id','=','group_categories.group_id')->where('group_categories.category_id',$cat->id)->where('groups.id','<>',$group->id)->where('collections','<>',$group->collections)->limit($total-$groupCount)->get();
    }
?>

<section class="lists-4 sec-t ">
    <h3>{{ trans('group::group-categories.related_products') }}</h3>
    <div class="row row-5">
    @if(count($groups)>0)
    @foreach ($groups AS $index => $group)
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="item">
                <a href="{{ route('products.show', ['slug' => $group->slug]) . '?sku=' . getSkuFromGroup($group) }}">
                    <div class="img">
                        <?php 
                        $mytime = Carbon\Carbon::now();
                        $new_start = strtotime($group->new_start);
                        $new_start = date('Y-m-d',$new_start);
                        $new_end = strtotime($group->new_end);
                        $new_end = date('Y-m-d',$new_end);
                        ?>
                        @if($group->is_new == 1)
                            @if($mytime >= $new_start && $mytime <= $new_end)                  
                                <span class="new"><img src="/assets/images/new.svg"  alt="" /></span> 
                            @endif
                        @endif
                        @if($group->selling_price != $group->price)
                            <span class="sale"><img src="/assets/images/sale.svg"  alt="" /></span>
                        @endif
                        <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                            data-lazy-src="{{productImageUrl(getSkuFromGroup($group), 1)}}" alt="" />
                    </div>
                    <div class="divtext ">
                        <h6 class="title">{{ $group->name }}</h6>
                        <div class="price">
                            @if ($group->lowest_price == $group->highest_price)
                            <span>{{ formatMoney($group->lowest_price) }}</span>
                            @else
                            <span>{{ trans('group::group-categories.from') }}
                                {{ formatMoney($group->lowest_price) }} - {{ formatMoney($group->highest_price) }}</span>
                            @endif

                            <!-- wishlist block -->
                            {!! printWishListIcon(getIdFromGroup($group)) !!}
                        </div>
                    </div>
                </a>
            </div>
        </div>
    @endforeach
    @endif
    @if(count($mores)>0)
        @foreach ($mores AS $index => $group)
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="item">
                    <a href="{{ route('products.show', ['slug' => $group->slug]) . '?sku=' . getSkuFromGroup($group) }}">
                        <div class="img">
                            
                            <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                                data-lazy-src="{{productImageUrl(getSkuFromGroup($group), 1)}}" alt="" />
                        </div>
                        <div class="divtext ">
                            <h6 class="title">{{ $group->name }}</h6>
                            <div class="price">
                                @if ($group->lowest_price == $group->highest_price)
                                <span>{{ formatMoney($group->lowest_price) }}</span>
                                @else
                                <span>{{ trans('group::group-categories.from') }}
                                    {{ formatMoney($group->lowest_price) }} - {{ formatMoney($group->highest_price) }}</span>
                                @endif

                                <!-- wishlist block -->
                                {!! printWishListIcon(getIdFromGroup($group)) !!}
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    @endif
    </div>
</section>
