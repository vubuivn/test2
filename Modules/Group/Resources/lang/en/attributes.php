<?php

return [
    'name' => 'Name',
    'slug' => 'URL',
    'description' => 'Description',
    'short_description' => 'Short Description',
    'categories' => 'Categories',
    'is_active' => 'Status',
    'code' => 'Code',
    'content_1' => 'Content 1',
    'content_2' => 'Content 2',
    'collection' => 'Collection',
    'new_start' => 'New start',
    'new_end' => 'New end',
    'is_new' => 'New Icon'
];
