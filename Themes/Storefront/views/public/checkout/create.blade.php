@extends('public.layout')

@section('title', trans('storefront::checkout.checkout'))

@push('meta')
    <meta name="cart-has-shipping-method" content="{{ $cart->hasShippingMethod() }}">
@endpush

@push('globals')
    <script>
        FleetCart.langs['storefront::checkout.please_select'] = '{{ trans("storefront::checkout.please_select") }}';
    </script>
@endpush

@section('content')

@php($page_title = trans('storefront::checkout.checkout') )
@include('public.checkout.partials.banner_cart')

<div class="container">
	<div class="row">
		<div class="col-sm-10 margin-auto">
			@php($active = 'payment')
			@include('public.checkout.partials.cart_wizard')
		</div>
	</div>
    <div class="cart-list-wrapper row">
       	<div class="col-lg-6 col-md-6 col-sm-12 cart-list-inner">
       		<form action="{{ route('checkout.store') }}" class="form-checkout" name="formCheckout" method="POST">
              @if($errors->any())
              <h4>{{$errors->first()}}</h4>
              @endif
       			
              @auth

              @if($my->phone == "")
                <h4>{{trans('storefront::checkout.update_phone')}}</h4>
              @endif

              @if($my->address == "")
                <h4>{{trans('storefront::checkout.update_address')}}</h4>
              @endif
              <!-- basic information -->
              @include('public.checkout.partials.address_payment')

              <!-- Billing address -->
              {{-- @include('public.checkout.partials.billing_address')  --}}

              <div class="checkbox ship-to-a-different-address">
                 {{ trans('storefront::checkout.ship_difference') }}
                  <input type="hidden" name="ship_to_a_different_address" value="0">
                  <input type="checkbox" name="ship_to_a_different_address" class="styled-checkbox" id="ship-to-a-different-address" value="1" {{ old('ship_to_a_different_address') ? 'checked' : '' }}>

                  <label for="ship-to-a-different-address">
                      {{ trans('storefront::checkout.add_difference_ship_address') }}
                  </label>
              </div>

              

              @include('public.checkout.partials.shipping_address')
              <!-- end billing address -->

              @else
       			      <div class="row show">
                  @include('public.checkout.partials.billing_address')
                  </div><!-- end row -->
              @endauth
                
              <div class="col-lg-12">
                <label for="shipping">{{ trans('storefront::checkout.shipping_fee') }}</label>
                <ul class="menu-shipping-method hidden">
                    <li class="active"><a href="#"></a></li>
                    <!-- <li><a href="#"><span class="price">600.000 VNĐ</span> <span class="duration">5 ngày</span></a></li> -->
                </ul>
            </div>
                <input type="hidden" name="shipping_method" value="{{ old('shipping_method') }}" />
                <input type="hidden" name="payment_method" value="bank_transfer" />
                <input type="hidden" name="terms_and_conditions" value="1" />
                <input type="hidden" name="billing[country]" value="VN" />
                <input type="hidden" name="billing[address_2]" value="" />
                <input type="hidden" name="billing[zip]" value="" />
                   
                @csrf()
       		</form>
	       
       </div>
       <div class="col-sm-10 margin-auto">
       		<div class="cart-list-actions">
                	 <a href="{{-- route('products.index') --}}" class="btn btn-back">{{ trans('storefront::checkout.back_to_store') }} </a>
                     <a href="javascript:document.forms['formCheckout'].submit()" class="btn btn-primary ico-r btn-checkout">
                       <span> {{ trans('storefront::checkout.confirm_payment') }}</span> | {{ $cart->total()->convertToCurrentCurrency()->format() }}
                        <i class="icon icon-arrow-14"></i>
                    </a>
			</div>
       </div>
   </div>
</div>

<script type="text/javascript">
    (function($){

        $('.btn-checkout').on('click',function(e){
          var checkshipping = $('#ship-to-a-different-address').val();

          // Nếu check giao hàng địa chỉ khác
          if($('#ship-to-a-different-address').prop("checked") == true){
              $('.personal-billing .basic_info').hide();
              var inputState = $('select[name="shipping[state]"]').val(),
              inputCity = $('select[name="shipping[city]"]').val(),
              inputPhone = $('input[name="shipping[phone]"]').val(),
              inputFirstName = $('input[name="shipping[first_name]"]').val(),
              inputLastName = $('input[name="shipping[last_name]"]').val(),
              inputaddress_1 = $('input[name="shipping[address_1]"]').val();

              if(inputState == '' || inputCity == '' || inputaddress_1== '' || inputFirstName == '' || inputLastName == '' || inputPhone== ''){
                e.preventDefault();
                $('.shipping-address').show();
                if(inputState == ''){
                  $('select[name="shipping[state]"]').addClass('has_error');
                }
                if(inputCity == ''){
                  $('select[name="shipping[city]"]').addClass('has_error');
                }
                if(inputaddress_1 == ''){
                  $('input[name="shipping[address_1]"]').addClass('has_error');
                }
                if(inputLastName == ''){
                  $('input[name="shipping[last_name]"]').addClass('has_error');
                }
                if(inputFirstName == ''){
                  $('input[name="shipping[first_name]"]').addClass('has_error');
                }
                if(inputPhone == '' || inputPhone.length<10 || inputPhone.length>11){
                  if(!$('input[name="shipping[phone]"]').hasClass('has_error')){
                    $('input[name="shipping[phone]"]').addClass('has_error');
                    if(inputPhone.length<10 || inputPhone.length>11){
                      $('input[name="shipping[phone]"]').parent().append("<span style='color:red'>{{trans('storefront::checkout.phone_required_10_to_11')}}</span>");
                    }
                  }
                  
                }
                if(inputaddress_1 == ''){
                  $('input[name="billing[address_1]"]').addClass('has_error');
                }
                $('html, body').animate({ scrollTop: $('#address').offset().top }, 'slow');
              }
              // Nếu không check giao hàng địa chỉ khác
          }else{
              var inputState = $('select[name="billing[state]"]').val(),
              inputCity = $('select[name="billing[city]"]').val(),
              inputEmail = $('input[name="customer_email"]').val(),
              inputPhone = $('input[name="customer_phone"]').val(),
              inputFirstName = $('input[name="billing[first_name]"]').val(),
              inputLastName = $('input[name="billing[last_name]"]').val(),
              inputaddress_1 = $('input[name="billing[address_1]"]').val();
              if(inputState == '' || inputCity == '' || inputaddress_1== '' || inputFirstName == '' || inputLastName == '' || inputPhone== ''){
                e.preventDefault();
                $('#billing-address').show();
                $('.personal-information').hide();
                if(inputState == ''){
                  $('select[name="billing[state]"]').addClass('has_error');
                }
                if(inputCity == ''){
                  $('select[name="billing[city]"]').addClass('has_error');
                }
                if(inputPhone == '' || inputPhone.length<10 || inputPhone.length>11){
                  if(!$('input[name="customer_phone').hasClass('has_error')){
                    $('input[name="customer_phone').addClass('has_error');
                    if(inputPhone.length<10 || inputPhone.length>11){
                      $('input[name="customer_phone').parent().append("<span style='color:red'>{{trans('storefront::checkout.phone_required_10_to_11')}}</span>");
                    }
                  }
                  
                }
                if(inputaddress_1 == ''){
                  $('input[name="billing[address_1]"]').addClass('has_error');
                }
                $('html, body').animate({ scrollTop: $('#address').offset().top }, 'slow');
              }
          }
         

          
        })


        if ($('#ship-to-a-different-address').is(':checked')) {
          $('.shipping-address').addClass('show');
        }


        $('#ship-to-a-different-address').change(function(){
          $('.shipping-address').toggleClass('show');
          if($('.shipping-address').hasClass('show')){
            $('.personal-information ').show();
            $('.personal-billing ').hide();
          }
        });

        

        const DISTRICTS = {!! json_encode($districts) !!};

        var oldDistrict = "{{ old('billing.city') }}";
        var firstDistrict = $('#billing-city').html();

        $('#billing-state').on('change', function(e) {
            var stateId = $(this).find('option:selected').data('id');
            var districtsObj = DISTRICTS[stateId];
            var districts = [firstDistrict];
            var cdistrict = '{{ isset($my) ? $my->district_id : ""}}';
            for (var id in districtsObj) {
                var districtName = districtsObj[id];
                if(cdistrict != ""){
                  var selectedText = districtsObj[cdistrict] === districtName ? 'selected' : '';
                }else{
                  var selectedText = oldDistrict === districtName ? 'selected' : '';

                }
                

                districts.push('<option value="' + districtName + '" ' + selectedText + '>' + districtName + '</option>');
            }
            
            $('#billing-city').html(districts.join(''));

            if (stateId == 1 || stateId == 65) {
                $('input[name=shipping_method').val('free_shipping');
                $('.menu-shipping-method a').text('{{ trans('storefront::checkout.free_shipping') }}');
            } else {
                $('input[name=shipping_method').val('local_pickup');
                $('.menu-shipping-method a').text('{{ trans('storefront::checkout.no_free_shipping') }}');
            }
            
            $('.menu-shipping-method').removeClass('hidden');
        });

        var firstDistrictShipping = $('#shipping-city').html();

        $('#shipping-state').on('change', function(e) {
            var stateId = $(this).find('option:selected').data('id');
            var districtsObj = DISTRICTS[stateId];
            var districts = [firstDistrictShipping];
            var cdistrict = '{{ isset($my) ? $my->district_id : ""}}';
            for (var id in districtsObj) {
                var districtName = districtsObj[id];
                if(cdistrict != ""){
                  var selectedText = districtsObj[cdistrict] === districtName ? 'selected' : '';
                }else{
                  var selectedText = oldDistrict === districtName ? 'selected' : '';

                }
                

                districts.push('<option value="' + districtName + '" ' + selectedText + '>' + districtName + '</option>');
            }
            
            $('#shipping-city').html(districts.join(''));

            if (stateId == 1 || stateId == 65) {
                $('input[name=shipping_method').val('free_shipping');
                $('.menu-shipping-method a').text('{{ trans('storefront::checkout.free_shipping') }}');
            } else {
                $('input[name=shipping_method').val('local_pickup');
                $('.menu-shipping-method a').text('{{ trans('storefront::checkout.no_free_shipping') }}');
            }
            
            $('.menu-shipping-method').removeClass('hidden');
        });

        if ($('#billing-state').val()) {
            $('#billing-state').trigger('change');
        }

        if ($('#shipping-state').val()) {
            $('#shipping-state').trigger('change');
        }
        
    })(jQuery);
</script>
@endsection

@push('styles')
<style type="text/css">
  .hide{display: none}
  .show{display: block}
  form .input.has_error,.has_error{border-color:red;}
  .show #billing-address{display: block}
    #billing-address,.shipping-address.hide{display: none;width:100%;}
    .shipping-address.hide.show{display: block !important}
    #billing-address.show{display: block}
    .basic_info>div>div{margin-bottom: 10px}
    .basic_info{background: #f9f9f9;padding:20px 20px 10px 20px;margin-bottom: 25px}
    .basic_info .row .col-md-8,.ship-to-a-different-address label {font-weight: 500;cursor: pointer;}
    .ship-to-a-different-address input {display: none}
    .checkbox.ship-to-a-different-address {margin-bottom: 25px;color: #8a8a8a}
    .personal-information>div h6 {text-transform: uppercase;margin:0 -15px 20px -15px;color: #8a8a8a}
</style>
@endpush