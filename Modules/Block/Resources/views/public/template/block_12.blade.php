<div class="container">

  	<div class="max650">
  		<h3 class="text-center">{{$title}}</h3>
 		<div class="row">
  			<div class="col-md-6">
			{!! $description_1!!}
			</div>
  			<div class="col-md-6">
			{!! $description_2!!}
			</div>

  		</div>
  		
  		<p><img class="   lazy-hidden" data-lazy-type="image"   data-lazy-src="{{$image->file}}" alt=""></p>

  	</div> 	
    



  </div>