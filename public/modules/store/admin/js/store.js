class Store{
    constructor() {
        // if (!$('body').hasClass('sidebar-collapse')) {
        //     $('body').addClass('sidebar-collapse');
        // }

        // $(document).on('click', '.banner-image', function (e) {
        //     let picker = new MediaPicker({type: 'image'});

        //     picker.on('select', (file) => {
        //         $(this).find('i').remove();
        //         $(this).find('img').attr('src', file.path).removeClass('hide');
        //         $(this).find('.banner-file-id').val(file.id);
        //     });
        // });

 
        // var toggleFormat = false;
        // $('#expandAllFormats').text(FleetCart.langs['store::stores.expand_all']);
        // $('#expandAllFormats').on('click', function (e) {
        //     e.preventDefault();
        //     $("#store-block .panel-collapse").each(function (index, value) {
        //         if (toggleFormat) {
        //             if ($(this).hasClass('in')) {
        //                 $(this).collapse('toggle');
        //             }
        //         } else {
        //             if (!$(this).hasClass('in')) {
        //                 $(this).collapse('toggle');
        //             }
        //         }

        //     });
        //     toggleFormat = toggleFormat ? false : true;
        //     if (toggleFormat === true) {
        //         $('#expandAllFormats').text(FleetCart.langs['store::stores.collapse_all']);
        //     } else {
        //         $('#expandAllFormats').text(FleetCart.langs['store::stores.expand_all']);
        //     }
        // });
        $('#btn-save-form').click(function () {
            $('#store-form .has-error').removeClass('has-error');
            var form = document.getElementsByName('block-form');
            tinymce.triggerSave();

            var myForm = document.getElementById('store-form');
            var formData = new FormData(myForm);

            var error = false;
            if ($.trim(formData.get('name')) === '') {
                $('#store-form #name').parent().parent().addClass('has-error');
                $('.accordion-tab li:eq(0)').addClass('has-error');
                error = true;
            }
            if (formData.has('slug')) {
                if ($.trim(formData.get('slug')) === '') {
                    $('.accordion-tab li:eq(2)').addClass('has-error');
                    error = true;
                }
            }

            var formData = $('#store-form').serializeArray();
            // var blockItem = 0;
            // form.forEach(function (element, index) {
            //     var blockData = $('#' + element.id).serializeArray();
            //     blockData.forEach(function (item) {
            //         formData.push(item);
            //     });
            //     blockItem++;
            // });
            // if (blockItem === 0) {
            //     $('.accordion-tab li:eq(1)').addClass('has-error');
            //     error = true;
            // }
            // if (error === true) {
            //     return false;
            // }

            var url = $('#store-form').attr('action');
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                success: function success(res) {
                    window.location.assign(route('admin.stores.index'));
                },
                error: function error(xhr) {
                    console.log(xhr);
                }
            });
        });
    }



}

new Store();