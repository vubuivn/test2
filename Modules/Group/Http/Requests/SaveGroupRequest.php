<?php

namespace Modules\Group\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Core\Http\Requests\Request;
use Modules\Group\Entities\Group;

class SaveGroupRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'group::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => $this->getSlugRules(),
            'code' => $this->getCodeRules(),
            // 'collections' => $this->getCollectionRules(),
            // 'collections' => 'exists:groups,code,Kier',
            'name' => 'required',
            'description' => 'required',
            'is_active' => 'required|boolean',
        ];
    }

    private function getSlugRules()
    {
        $rules = $this->route()->getName() === 'admin.groups.update'
            ? ['required']
            : ['sometimes'];

        $slug = Group::withoutGlobalScope('active')->where('id', $this->id)->value('slug');

        $rules[] = Rule::unique('groups', 'slug')->ignore($slug, 'slug');

        return $rules;
    }

    private function getCodeRules()
    {
        $rules = ['required'];

        $code = Group::withoutGlobalScope('active')->where('id', $this->id)->value('code');

        $rules[] = Rule::unique('groups', 'code')->ignore($code, 'code');

        return $rules;
    }

    private function getCollectionRules()
    {
        $collections = explode(',', $this->collections);

        $rules = [];

        if (!empty($collections)) {
            foreach ($collections AS $collection) {
                $rules[] = 'exists:groups,code,' . $collection;
                // $rules[] = Rule::exists('groups')->where(function ($query) use ($collection) {
                //     $query->where('code', $collection);
                // });
            }
        }
        \Log::info('rules implode: ' . implode('|', $rules));

        return implode('|', $rules);
    }
}
