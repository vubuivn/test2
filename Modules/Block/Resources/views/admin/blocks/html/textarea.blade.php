<div class="form-group ">
    <label class="col-md-2 control-label text-left">
        {{$block->title}}
    </label>
    <div class="col-md-10">
        <textarea class="form-control {{ $block->type == 'editor' ? 'wysiwyg' : '' }}" name="blocks[{{$number}}][{{$block->key}}]">{{ $data->{$block->key} ?? null }}</textarea>
    </div>
</div>
