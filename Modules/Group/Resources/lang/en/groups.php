<?php

return [
    'group'  => 'Group',
    'groups' => 'Groups',
    'tabs'   => [
        'group'          => [
            'basic_information'    => 'Basic Information',
            'advanced_information' => 'Advanced Information',
        ],
        'general'        => 'General',
        'inventory'      => 'Inventory',
        'images'         => 'Images',
        'seo'            => 'SEO',
        'related_groups' => 'Related Groups',
        'up_sells'       => 'Up-Sells',
        'cross_sells'    => 'Cross-Sells',
        'additionals'    => 'Additionals',
        'products'       => 'Products',
        'filter'         => 'Filter',
    ],
    'form'   => [
        'please_select'    => 'Please Select',
        'enable_the_group' => 'Enable the group',
        'is_new' => 'Enable new icon',
    ],
    'add_product_child' => 'Add Child Product'
];
