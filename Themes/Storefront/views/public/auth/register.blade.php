@extends('public.layout')

@section('title', trans('user::auth.register'))

@section('content')
    <div class="register-wrapper clearfix">
        <div class="width-custom col-md-7 col-sm-10">
            <div class="row">
                @include('public.partials.notification')

                <form method="POST" action="{{ route('register.post') }}" id="register_form">
                    {{ csrf_field() }}

                    <div class="box-wrapper register-form">
                        <!-- <div class="box-header">
                            <h4>{{ trans('user::auth.register') }}</h4>
                        </div> -->

                        <div class="form-inner clearfix">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('first_name') ? 'has-error': '' }}">
                                    <label for="first-name">{{ trans('user::auth.first_name') }}<span>*</span></label>

                                    <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control" id="first-name" autofocus required>

                                    {!! $errors->first('first_name','<span class="error-message">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('last_name') ? 'has-error': '' }}">
                                    <label for="last-name">{{ trans('user::auth.last_name') }}<span>*</span></label>

                                    <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control" id="last-name" required>

                                    {!! $errors->first('last_name','<span class="error-message">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('gender') ? 'gender': '' }}">
                                    <label for="first-name">{{ trans('user::auth.gender') }}<span>*</span></label>
                                    <select class="form-control" name="gender">
                                        <option value="0">{{trans('user::auth.men')}}</option>
                                        <option value="1">{{trans('user::auth.women')}}</option>
                                    </select>
                                    {!! $errors->first('gender','<span class="error-message">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('birthday') ? 'birthday': '' }}">
                                    <label for="birthday">{{ trans('user::auth.birthday') }}<span>*</span></label>

                                    <input type="text" name="birthday" value="{{ old('birthday') }}" class="form-control datetime-picker" id="birthday" autocomplete="off">

                                    {!! $errors->first('birthday','<span class="error-message">:message</span>') !!}
                                </div>
                            </div>
                                
                            <!-- HOTLINE // LOCATION -->
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('phone') ? 'phone': '' }}">
                                    <label for="first-name">{{ trans('user::auth.phone') }}<span>*</span></label>
                                    <input type="number" name="phone" value="{{ old('phone') }}" class="form-control" id="phone">
                                    {!! $errors->first('phone','<span class="error-message">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('location') ? 'location': '' }}">
                                    <label for="last-name">{{ trans('user::auth.location') }}<span>*</span></label>

                                    <select class="form-control" name="city_id">
                                        @if(count($cities)>0)
                                            @foreach($cities as $city)
                                            <?php 
                                            $arrtitle =[];
                                           
                                            $arr = explode('[:en]',$city->title);
                                            $arrtitle['vi'] = substr($arr[0],5);
                                            $arrtitle['en'] = substr($arr[1],-3);
                                            $arrCities[$city->id] = $arrtitle;
                                            ?>
                                            <option value="{{$city->id}}">{{ (locale() == 'en') ? $arrtitle['en'] : $arrtitle['vi']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    {!! $errors->first('birthday','<span class="error-message">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                                    <label for="email">{{ trans('user::auth.email') }}<span>*</span></label>

                                    <input type="text" name="email" value="{{ old('email') }}" class="form-control" id="email">

                                    {!! $errors->first('email','<span class="error-message">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('password') ? 'has-error': '' }}">
                                    <label for="password">{{ trans('user::auth.password') }}<span>*</span></label>

                                    <input type="password" name="password" class="form-control" id="password">

                                    {!! $errors->first('password','<span class="error-message">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error': '' }}">
                                    <label for="confirm-password">{{ trans('user::auth.password_confirmation') }}<span>*</span></label>

                                    <input type="password" name="password_confirmation" class="form-control" id="confirm-password">

                                    {!! $errors->first('password_confirmation', '<span class="error-message">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12">
                                <div class="checkbox">

                                    <label class="container_check">
                                        {{ trans('user::auth.i_agree_to_the') }} <a href="{{ $privacyPageURL }}">{{ trans('user::auth.privacy_policy') }}</a>
                                        <input type="hidden" value="0">
                                        <input type="checkbox" value="1" id="privacy" name="privacy_policy">
                                        <span class="checkmark"></span>
                                    </label>
                                    {!! $errors->first('privacy_policy','<span class="error-message">:message</span>') !!}
                                </div>
                                <div class="checkbox">
    
                                    <label class="container_check">
                                        {{ trans('user::auth.receive_email') }}
                                        <input type="hidden" value="0">
                                        <input type="checkbox" id="send_email" name="send_email">
                                        <span class="checkmark"></span>
                                    </label>
                                    {!! $errors->first('privacy_policy','<span class="error-message">:message</span>') !!}
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-12">
                             <button type="submit" class="btn btn-primary btn-register pull-right width100" data-loading>
                                    {{ trans('user::auth.register') }}
                                </button>
                            </div>
                            

                            <!-- FACEBOOK AND GOOGLE -->
                            <div class="social-button">
                                <div class="col-md-6 col-xs-6 fb">
                                    <a href="{{ route('login.redirect', ['provider' => 'facebook']) }}" class="btn btn-primary btn-custom btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                                </div>
                                <div class="col-md-6 col-xs-6 gg">
                                    <a href="{{ route('login.redirect', ['provider' => 'google']) }}" class="btn btn-primary btn-custom btn-google"><i class="fa fa-google"></i> Google</a>
                                </div>
                            </div>
                            
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link href="{{ v(Theme::url('public/css/custom-form.css')) }}" rel="stylesheet">
    <style type="text/css">
    .error{color: red}
    .register-form .checkmark{border:1px solid #dad8d8;}
    .register-form .checkbox{margin-top: 0;margin-bottom: 15px}
    </style>
@endpush

@push('scripts')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    (function($) {
        $( ".datetime-picker" ).datepicker();

        $(document).ready(function(){
            $("#register_form").validate({
                rules: {
                    first_name: "required",
                    last_name: "required",
                    birthday: "required",
                    phone: "required",  
                    email: "required",  
                    password: "required",
                    password_confirmation: {
                        required: true,
                        equalTo: '#password'
                    }
                },
                messages: {
                    first_name: "{{trans('storefront::account.messages.name_required')}}",
                    last_name: "{{trans('storefront::account.messages.name_required')}}",
                    phone: "{{trans('storefront::account.messages.phone_required')}}",  
                    email: "{{trans('storefront::account.messages.email_required')}}",  
                    password: "{{trans('storefront::account.messages.password_required')}}",
                    password_confirmation: "{{trans('storefront::account.messages.password_required')}}",
                    birthday: "{{trans('storefront::account.messages.birthday_required')}}",
                }
            });
        })
    })(jQuery);
</script>
@endpush