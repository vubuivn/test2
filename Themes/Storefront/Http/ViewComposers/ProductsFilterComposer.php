<?php

namespace Themes\Storefront\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\Product;
use Modules\Category\Entities\Category;
use Modules\Group\Entities\Group;
use Modules\Attribute\Entities\Attribute;
use Illuminate\Support\Facades\Cache;

class ProductsFilterComposer
{
    protected $slug = null;

    public function compose ($view)
    {
        if (isset($view->getData()['slug'])) {
            $this->slug = $view->getData()['slug'];
            $category = $view->getData()['category'];
            $currentSlug = $this->slug;
            // if(Cache::tags(['categories'])->has('category_detail_'.$currentSlug)){
            //     $category = Cache::tags(['categories'])->get('category_detail_'.$currentSlug);
            // }else{
            //     $category = Cache::tags(['categories'])
            //       ->rememberForever('category_detail_'.$currentSlug,function () use($currentSlug){
            //           return Category::where('slug',$currentSlug)->first();
            //     });
            // }
            // $category = Category::where('slug',$this->slug)->first();

            if($category){
                $catarr = [];
                $catarr[] = $category->id;
                if(Cache::tags(['categories'])->has('sub_category_'.$category->id)){
                    $subcats = Cache::tags(['categories'])->get('sub_category_'.$category->id);
                }else{
                    $subcats = Cache::tags(['categories'])
                      ->rememberForever('sub_category_'.$category->id,function () use($category){
                          return Category::where('slug',$category->id)->first();
                    });
                }
                // $subcats = Category::where('parent_id',$category->id)->get();
                if($subcats){
                    foreach ($subcats as $cat) {
                        $catarr[] = $cat->id;
                    }
                }
                $maxP = $this->maxPrice($catarr);
                // dd($maxP);
            }else{
                $maxP = $this->maxPrice();
            }
        }else{
            $maxP = $this->maxPrice();
        }

        $view->with([
            // 'categories' => $this->categories(),
            // 'attributes' => $this->attributes($view),
            'maxPrice' => (int)$maxP,
        ]);
    }

    private function categories()
    {
        return Category::tree();
    }

    private function attributes($view)
    {
        $attributes = Attribute::with('values')
            ->where('is_filterable', true)
            ->whereHas('categories', function ($query) use ($view) {
                $query->where('slug', $this->slug);
            })
            ->get();
        return $attributes;
    }

    private function maxPrice($category_id = [])
    {
        if(count($category_id) > 0){
            $groups = Group::select(DB::raw('(select max(selling_price) from products where products.group_id = groups.id group by products.group_id) as selling'))->whereHas('categories', function($q) use($category_id){
                $q->whereIn('categories.id',$category_id);
            })->orderBy('selling','desc')->limit(1)->get();
            // dd($category_id);
            if($groups && count($groups)>0){
               return $groups[0]->selling; 
           }else{
                return Product::max('selling_price');
           }
            
        }
        return Product::max('selling_price');
    }
}
