<?php

namespace Modules\CategoryNews\Http\Requests;

use Modules\Core\Http\Requests\Request;

class SaveCategoryNewsRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'categorynews::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }
}
