<div class="accordion-box-content form-group ">
    <div class="tab-content clearfix">
        <div class="banner-image-wrapper">
            <div class="single-banner">
                <div class="banner-header">
                    <h5>{{$block->title}}</h5>
                </div>
{{--                @php(dd($data->{$block->key}->filter))--}}
                <div class="banner-template">
                    <div class="banner-body">
                        <div class="col-lg-6 col-md-12 col-sm-6">
                            <div class="form-group">
                                <label>{{ trans('block::attributes.filter') }}</label>
                                <input type="text" name="blocks[{{$number}}][{{$block->key}}][filter]"
                                       value="{{$data->{$block->key}->filter ?? '' }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-6">
                            <div class="form-group">
                                <label>{{ trans('block::attributes.limit') }}</label>
                                <input type="text" name="blocks[{{$number}}][{{$block->key}}][limit]"
                                       value="{{$data->{$block->key}->limit ?? '' }}" class="form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
