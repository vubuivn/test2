<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Yangqi\\Htmldom' => array($vendorDir . '/yangqi/htmldom/src'),
    'UpdateHelper\\' => array($vendorDir . '/kylekatarnls/update-helper/src'),
    'Rap2hpoutre\\LaravelLogViewer\\' => array($vendorDir . '/rap2hpoutre/laravel-log-viewer/src'),
    'Pimple' => array($vendorDir . '/pimple/pimple/src'),
    'Parsedown' => array($vendorDir . '/erusev/parsedown'),
    'Mcamara\\LaravelLocalization' => array($vendorDir . '/mcamara/laravel-localization/src'),
    'AlternativeLaravelCache' => array($vendorDir . '/swayok/alternative-laravel-cache/src'),
    'AlgoliaSearch' => array($vendorDir . '/algolia/algoliasearch-client-php/src'),
);
