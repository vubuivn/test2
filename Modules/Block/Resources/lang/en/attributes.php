<?php

return [
    'name'      => 'Name',
    'title'     => 'Title',
    'is_active' => 'Status',
    'html'      => 'Html',
    'caption_3' => 'Caption 3',
    'video_url' => 'Video URL',
    'filter'    => 'Filter',
    'sort'      => 'Sort',
    'limit'     => 'Limit',
    'tag'		=> 'Tag',
    'select_tag' => 'Select tag',
];
