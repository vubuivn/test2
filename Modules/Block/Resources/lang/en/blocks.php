<?php

return [
    'block'      => 'Block',
    'blocks'     => 'Blocks',
    'form'       => [
        'name'             => 'Name Block',
        'new_block'        => 'New Block',
        'delete_block'     => 'Delete Block',
        'add_new_block'    => 'Add New Block',
        'enable_the_block' => 'Enable the block',
        'key'              => 'Key',
        'title'            => 'Title',
        'type'             => 'Type',
        'delete_image'     => 'Delete Image',
    ],
    'tabs'       => [
        'block'   => [
            'basic_information'    => 'Basic Information',
            'advanced_information' => 'Advanced Information',
        ],
        'general' => 'General',
        'values'  => 'Values',
        'html'    => 'Html',
    ],
    'select'     => [
        'input'           => 'Input',
        'textarea'        => 'Textarea',
        'editor'          => 'Editor',
        'single-image'    => 'Single Image',
        'multiple-images' => 'Multiple Images',
        'button'          => 'Button',
        'group'           => 'Group',
        'repeat'          => 'Repeat',
        'contact'         => 'Contact form',
        'select'          => 'Select',
    ],
    'all'        => 'All',
    'price_from' => 'Price From',
    'price_to'   => 'Price To',
    'attributes'   => 'Attributes',
    'tags'  => 'Tags',
    'create_tags' => 'Create tag',
    'tag_id' => 'ID',
    'tag_name' => 'Name',
    'tag_add_success' => 'Add tag success',
    'tag_add_error' => 'Add tag error',
    'tag_already_exsist' => 'Tag already exsist'
];
