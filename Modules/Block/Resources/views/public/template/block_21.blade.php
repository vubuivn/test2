<section class="sec-register sec-tb">
  <div class="container">
    <div class="row grid-space-0">

      <div class="col-lg-6">
        <div class="img tRes_58">
          <img class="lazy-hidden" data-lazy-type="image" data-lazy-src="{{$image->file}}" alt="">
        </div>
      </div>
      <div class="col-lg-6">
	  	 	
		<div class="divtext form_include">
			<h3>{{$title}}</h3>
          	{!!$content!!}
			@include('public.form.contact',['id'=>$contact->formcontact])
		</div>
      </div>
    </div>
  </div>
</section>
@include('public.include.stores')