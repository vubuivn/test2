@if($number === '<%- i %>')
    @php($item = '<%- y %>')
    @push('scripts')
        <script type="text/html" id="{{$block->key}}-template">
            @include('block::admin.blocks.html.image_box_custom')
        </script>
    @endpush
@endif
@php($item = 0)
<div class="accordion-box-content form-group ">
    <div class="tab-content clearfix">
        <div class="banner-image-wrapper">
            <div class="single-banner">
                <div class="banner-header">
                    <h5>{{$block->title}}</h5>
                </div>
                <div class="banner-template">
                    @if(isset($data->{$block->key}))
                        @foreach ($data->{$block->key} as $image)
                            @php ($img  = $image->file)
                            @include('block::admin.blocks.html.image_box_custom')
                            @php($item++)
                        @endforeach
                    @else
                        @include('block::admin.blocks.html.image_box_custom')
                    @endif
                </div>
            </div>
        </div>

            <button type="button" class="btn btn-default add_more_image" data-key="{{$block->key}}" data-block="{{$number}}" data-id="{{$item}}">
                {{trans('page::pages.form.add_more_image')}}
            </button>

    </div>
</div>
