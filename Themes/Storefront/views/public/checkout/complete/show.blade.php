@extends('public.layout')

@section('content')
@php($page_title = trans('storefront::checkout.thankyou'))
@include('public.checkout.partials.banner_cart')

<div class="container-fluid ">
    <div class="row">
		<div class="col-sm-10 margin-auto">
			@php($active = 'confirm')
			@include('public.checkout.partials.cart_wizard')
		</div>
    </div>
    
    <div class="cart-list-wrapper row">
       	<div class="col-md-12 cart-list-inner cart-completed">
			<div class="block-center">
				<img src="/assets/images/cart/completed.svg" alt="completed.svg">
				<div class="message">
                    <h1>{{ trans('storefront::order_placed.your_order_has_been_placed') }}</h1>
                    <p>{{ trans('storefront::order_placed.order_id') }}<span> #{{ $order->id }}</span></p>
				</div>
   				
   			</div>
       </div>
       <div class="col-lg-12 cart-completed-bottom">
       		<h3 class="thankyou">{{ trans('storefront::order_placed.thanks') }}</h3>
            <a href="{{-- route('products.index') --}}" class=" btn btn-grey ico-l btn-back">{{ trans('storefront::checkout.back_to_store') }}  <i class="icon icon-arrow-14"></i></a>
       </div>
   </div>
</div>
@endsection
