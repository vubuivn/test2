<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('builder_reply', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('builder_id')->unsigned();
            $table->boolean('is_active');
            //$table->string('reply_from');
            $table->string('reply_subject');
            $table->string('reply_to');
            $table->text('reply_body')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('builder_reply');
    }
}
