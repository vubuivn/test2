@if(isset($type))
    @if($type=='page')
        @php($entity = $page)
    @elseif($type=='category')
        @php($entity = $category)
    @elseif($type=='post')
        @php($entity = $post)
    @endif
    <meta name="title" content="{{ (isset($entity->meta->meta_title) && $entity->meta->meta_title!='') ? $entity->meta->meta_title : $entity->name}}">
    <meta name="keywords" content="{{ isset($entity->meta->meta_keywords) ? implode(',', $entity->meta->meta_keywords) : '' }}">
    <meta name="description" content="{{ $entity->meta->meta_description ?? $entity->short_description}}">
    <meta property="og:title" content="{{ (isset($entity->meta->meta_title) && $entity->meta->meta_title!='') ? $entity->meta->meta_title : $entity->name}}">
    <meta property="og:description" content="{{ $entity->meta->meta_description ?? $entity->short_description}}">
    <meta property="og:image" content="{{ isset($entity->base_image) ? $entity->base_image->path : ''}} " />
    <meta name="twitter:url" content="{{url()->current()}}">
    <meta name="twitter:title" content="{{ (isset($entity->meta->meta_title) && $entity->meta->meta_title!='') ? $entity->meta->meta_title : $entity->name}}">
    <meta name="twitter:description" content="{{ $entity->meta->meta_description ?? $entity->short_description}}">
    <meta property="og:url" itemprop="url" content="{{url()->current()}}" />
@endif