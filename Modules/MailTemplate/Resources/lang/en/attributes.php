<?php

return [
    'code'    => 'Code',
    'name'    => 'Name',
    'subject' => 'Subject',
    'content' => 'Content',
    'email_cc' => 'CC To',
];
