export default class {
    addElementsErrors(errors) {
        for (let key in errors) {
            let inputField = this.getInputFieldForErrorKey(key);

            inputField.closest('.element').addClass('element-has-errors');

            let parent = inputField.parent();

            parent.addClass('has-error');
            parent.append(`<span class="help-block">${errors[key][0]}</span>`);
        }
    }

    getRowTemplate(data) {
        let template = _.template($('#element-select-values-template').html());

        return $(template(data));
    }

    changeElementType({ elementId, values = [] }) {
        let elementValuesElement = this.getElementValuesElement(elementId);
        let elementValuesData = { elementId, value: { id: '', label: '', price: '', price_type: 'fixed' } };

        let template = _.template($('#element-select-template').html());

        elementValuesElement.html(template(elementValuesData));

        this.addElementRowEventListener(elementId);

        this.addElementRows({ elementId, values });

        if (values.length === 0) {
            this.getAddNewRowButton(elementId).trigger('click');
        }
    }

    addElementRows({ elementId, values }) {
        for (let [index, value] of values.entries()) {
            this.addElementRow({
                elementId,
                valueId: index,
                value,
            });
        }
    }

    initElementRow(template, selectValues) {
        if (selectValues.length !== 0 && ! selectValues.is('.sortable')) {
            this.makeSortable(selectValues[0]);

            selectValues.addClass('sortable');
        }

        this.deleteElementRowEventListener(template);

        window.admin.tooltip();
    }

    deleteElementRowEventListener(row) {
        row.find('.delete-row').on('click', (e) => {
            $(e.currentTarget).closest('.element-row').remove();
        });
    }

    makeSortable(el) {
        Sortable.create(el, {
            handle: '.drag-icon',
            animation: 150,
        });
    }
}
