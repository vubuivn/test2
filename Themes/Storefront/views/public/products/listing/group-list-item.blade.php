<div class="col-md-4 col-sm-6">
    <div class="item">
        <?php 
            if(request()->has('attribute')){
                $sku = getSkuFromGroup2($group);
            }else{
                $sku = getSkuFromGroup($group);
            }
        ?>
        <a href="{{ route('products.show', ['slug' => $group->slug]) . '?sku=' . $sku }}">
            <div class="img">
                <?php 
                $mytime = Carbon\Carbon::now();
                $new_start = strtotime($group->new_start);
                $new_start = date('Y-m-d',$new_start);
                $new_end = strtotime($group->new_end);
                $new_end = date('Y-m-d',$new_end);
                ?>
                @if($group->is_new == 1)
                    @if($mytime >= $new_start && $mytime <= $new_end)                  
                        <span class="new"><img src="/assets/images/new.svg"  alt="" /></span> 
                    @endif
                @endif
                @if($group->selling_price != $group->price)
                <span class="sale"><img src="/assets/images/sale.svg"  alt="" /></span>
                @endif
                <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                    data-lazy-src="{{ productImageUrl($sku, 1) }}" alt="" />
            </div>
            <div class="divtext ">
                <div class="wtitle">
                    <h3 class="title"> {{ $group->name }}</h3>
                </div>
                <div class="price">
                    <span> @if($group->lowest_price != $group->highest_price){{ trans('group::group-categories.from') }}@endif 
                        {{ formatMoney($group->lowest_price) }} @if($group->lowest_price != $group->highest_price)-
                            {{ formatMoney($group->highest_price) }}@endif</span>

                        <!-- wishlist block -->
                        {!! printWishListIcon(getIdFromGroup($group)) !!}
                </div>
            </div>
        </a>
    </div>
</div>