<div class="row">
<form method="POST" action=https://aconcept-vn.com/builder/5/embedded-form enctype="multipart/form-data" onsubmit="return get_action()">
    <input type="hidden" name="_token" value="yUYrEftK5wcGGAXbWlMe62tA7kaCCEmAzhgSEVvT">
    <div class="formeo-render formeo formeo-rendered-0" id="f-dd879fb8-de64-4e89-a059-b561a98756f4">
        <div class="formeo-stage" id="f-92ec4785-75da-4014-9596-04c8cd18455c">
            <div class="formeo-row-wrap" id="0d917cda-501b-4b87-a098-9b103aeb38c2">
                <div class="formeo-row" id="f-be653b7d-1c9e-4983-9b64-d5971563a83b">
                    <div class="formeo-column" id="f-9f4ac992-563d-4add-a7e8-fd5891ff3084" style="width: 100%;">
                        <div class="f-field-group">
                            <label for="mg_10_field">Họ</label>
                            <input name="mg_10_field" type="text" id="mg_10_field">
                        </div>
                        <div class="f-field-group">
                            <label for="mg_11_field">Tên</label>
                            <input name="mg_11_field" type="text" id="mg_11_field">
                        </div>
                        <div class="f-field-group">
                            <label for="mg_12_field">Điện thoại</label>
                            <input name="mg_12_field" type="text" id="mg_12_field">
                        </div>
                        <div class="f-field-group">
                            <label for="mg_13_field">Email</label>
                            <input name="mg_13_field" type="text" id="mg_13_field">
                        </div>
                        <div class="f-field-group">
                            <label for="mg_15_field">khu vực</label>
                            <select name="mg_15_field" id="mg_15_field">
                                <option label="Hồ Chí Minh" value="ho_chi_minh">Hồ Chí Minh</option>
                                <option label="Hà Nội" value="ha_noi">Hà Nội</option>
                                <option label="Miền Bắc" value="mien_bac">Miền Bắc</option>
                                <option label="Miền Nam" value="mien_nam">Miền Nam</option>
                            </select>
                        </div>
                        <div class="f-field-group">
                            <label for="mg_16_field">Sản phẩm quan tâm</label>
                            <select name="mg_16_field" id="mg_16_field">
                                <option label="Sofa" value="sofa">Sofa</option>
                                <option label="Bàn" value="ban">Bàn</option>
                                <option label="Ghế" value="ghe">Ghế</option>
                                <option label="Tủ" value="tu">Tủ</option>
                                <option label="Đồ trang trí" value="do_trang_tri">Đồ trang trí</option>
                                <option label="Hàng thanh lý" value="hang_thanh_ly">Hàng thanh lý</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="formeo-row-wrap" id="ef47ff41-4c9a-4440-bbb9-24d7b1d600e5">
                <div class="formeo-row" id="f-45c1d119-fdc5-4762-ab2f-1a5b7b6fc4df">
                    <div class="formeo-column" id="f-acaa71f5-abc0-41e8-ab97-83772a566c30" style="width: 100%;">
                        <div class="f-field-group w100">
                            <label for="mg_14_field">Nội dung</label>
                            <textarea name="mg_14_field" id="mg_14_field"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class='formeo-row-wrap'>
                <div class='formeo-row'>
                    <div class='formeo-column" style="width: 100%;'>
                        <div class='f-field-group w100'>
                            <div class='g-recaptcha' data-sitekey='6LeIab0UAAAAADXEqoh4yO5cTy6OslPqWpxLhTpw'></div><span id='captcha' style='color:red' /></div>
                    </div>
                </div>
            </div>
            <div class="formeo-row-wrap" id="ef47ff41-4c9a-4440-bbb9-24d7b1d600e5"  style="margin-top: 20px">
                <div class="formeo-row" id="f-45c1d119-fdc5-4762-ab2f-1a5b7b6fc4df">
                    <div class="formeo-column" id="f-acaa71f5-abc0-41e8-ab97-83772a566c30" style="width: 100%;">
                        <div class="f-btn-group">
                            <button id="efd19d66-82b0-4c31-89a9-9300689fc7af">Gửi</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script src='//www.google.com/recaptcha/api.js'></script>
<script>
    function get_action(form) {
        var v = grecaptcha.getResponse();
        if (v.length == 0) {
            document.getElementById('captcha').innerHTML = "You can't leave Captcha Code empty";
            return false;
        }
    }
</script>
</div>
@push('scripts')
<style type="text/css">
    .form_include .formeo-row-wrap .f-field-group{
            padding-right: 15px;
    padding-left: 15px;
    position: relative;
    width: 50%;
    float: left;
    }
    .mgb20{margin-bottom:20px;}
    .form_include .formeo-row-wrap .f-field-group.w100{
        width:100%;
    }
    .form_include .formeo-row-wrap:last-child .f-field-group{width:100%;}
    .form_include .formeo-row-wrap:last-child .f-btn-group{padding:0 15px;}
    .form_include .formeo-row-wrap select{
        -webkit-appearance: inherit;
    -moz-appearance: inherit;
    appearance: inherit;
    padding-right: 40px;
    background: #fff url(assets/images/select.svg) no-repeat right center;
        width: 100%;
    padding-left: 0;
    font-size: 14px;
    margin-bottom: 20px;
    border-radius: 0px;
    border: none;
    border-bottom: 1px solid #C1C1C1;

    }
    .form_include .f-btn-group button{
            position: relative;
    vertical-align: middle;
    display: inline-block;
    text-align: center;
    cursor: pointer;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    -webkit-transition: all 0.2s ease-in-out;
    overflow: hidden;
    font-weight: 500;
    font-size: 16px;
    background-color: transparent;
    border-radius: 0;
    border: 1px solid #232323;
    line-height: 38px;
    height: 40px;
    padding: 0 25px;
        background-color: #232323;
    border-color: #232323;
    color: #fff;
    }
    .form_include .f-btn-group button:hover{
            background-color: #9D0E01;
            border-color: #9D0E0
    }
    .form_include label ,.formeo-render label {
            font-size: 14px;
    color: #6C6C6C;
    }
</style>
@endpush