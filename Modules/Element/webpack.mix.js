let mix = require('laravel-mix');
let execSync = require('child_process').execSync;

mix.js('Modules/Element/Resources/assets/admin/js/main.js', 'Modules/Element/Assets/admin/js/element.js')
    .sass('Modules/Element/Resources/assets/admin/sass/main.scss', 'Modules/Element/Assets/admin/css/element.css')
    .then(() => {
        execSync('npm run rtlcss Modules/Element/Assets/admin/css/element.css Modules/Element/Assets/admin/css/element.rtl.css');
    });
