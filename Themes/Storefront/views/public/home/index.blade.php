@extends('public.layout')

@section('title', $page->meta->meta_title != '' ? $page->meta->meta_title : $page->name . setting('store_tagline'))

@push('meta')
	@include('public.partials.meta',['type'=>'page'])
@endpush


@section('content')

    @foreach($page->blocks as $block)
        @php($content = show_block_page($block))
        @include('block::public.template.block_'.$block->block_id, $content)
    @endforeach

@endsection
@push('scripts')
   
@endpush