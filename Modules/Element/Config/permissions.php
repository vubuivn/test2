<?php

return [
    'admin.elements' => [
        'index' => 'element::permissions.elements.index',
        'create' => 'element::permissions.elements.create',
        'edit' => 'element::permissions.elements.edit',
        'destroy' => 'element::permissions.elements.destroy',
    ],
];
