<?php

namespace Modules\Group\Admin;

use Modules\Admin\Ui\AdminTable;

class GroupTable extends AdminTable
{
    /**
     * Raw columns that will not be escaped.
     *
     * @var array
     */
    protected $rawColumns = ['link'];

    /**
     * Make table response for the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function make()
    {
        return $this->newTable()
        ->editColumn('link', function ($entity) {
            return '<a href="'.route('admin.groups.edit',$entity->id).'">Edit</a> | <a href="'.route('admin.groups.edit', $entity->slug).'">View</a>';
        });
    }
}
