<?php

Route::get('/career/{slug}', 'CareerController@detail')->name('career.detail');
Route::get('/career', 'CareerController@index')->name('career');