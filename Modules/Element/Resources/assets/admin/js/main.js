import Element from './Element';
import BuilderElement from './BuilderElement';

if ($('#element-create-form, #element-edit-form').length !== 0) {
    new Element();
}

if ($('#builder-create-form, #builder-edit-form').length !== 0) {
    new BuilderElement();
}
