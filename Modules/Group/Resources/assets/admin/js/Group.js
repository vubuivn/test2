// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

export default class {
    constructor() {
        // ClassicEditor
        //     .create(document.querySelector('#editor'), {
        //         toolbar: ['ckfinder', 'imageUpload', '|', 'heading', '|', 'bold', 'italic', '|', 'undo', 'redo']
        //     })
        //     .then(editor => {
        //         window.editor = editor;
        //     })
        //     .catch(err => {
        //         console.error(err.stack);
        //     });

        $(document).on('click', '.banner-image', function (e) {
            let picker = new MediaPicker({type: 'image'});

            picker.on('select', (file) => {
                $(this).find('i').remove();
                $(this).find('img').attr('src', file.path).removeClass('hide');
                $(this).find('.banner-file-id').val(file.id);
            });
        });
        if (!$('body').hasClass('sidebar-collapse')) {
            $('body').addClass('sidebar-collapse');
        }
        $(document).on('change', '.block-name-field', function (e) {
            $(this).parent().parent().parent().parent().parent().parent().find('#block-name').text($(this).val());
        });
        $(document).on('click', '.delete-block', function (e) {
            $(this).parent().parent().parent().parent().parent().remove();
        });
        $(document).on('click', '.add_more_image', function (e) {
            var block = $(this).attr('data-block');
            var number = $(this).attr('data-id');
            var key = $(this).attr('data-key');
            $(this).attr('data-id', ++number);

            var template = document.getElementById(key + '-template');
            var templateHtml = template.innerHTML;
            templateHtml = templateHtml.replace(/<%- i %>/gi, block);
            templateHtml = templateHtml.replace(/&lt;%- i %&gt;/gi, block);
            templateHtml = templateHtml.replace(/&lt;%- y %&gt;/gi, number);
            $(this).parent().find('.banner-template').append(templateHtml);
        });
        $(document).on('click', '.delete-image', function (e) {
            $(this).parent().remove();
        });
        var block = $('#block-number').val();
        $(document).on('click', '.block-item', function (e) {
            var blockId = $(this).data('id');
            var template = document.getElementById(blockId + '-template');
            var templateHtml = template.innerHTML;
            templateHtml = templateHtml.replace(/<%- i %>/gi, block);
            templateHtml = templateHtml.replace(/&lt;%- i %&gt;/gi, block);
            $('#page-block').append(templateHtml);
            $('.content-accordion').find('.banner-body:eq(0)').find('.delete-image').remove();
            tinymce.init({
                selector: '.wysiwyg',
                theme: 'modern',
                mobile: { theme: 'mobile' },
                height: 300,
                menubar: false,
                branding: false,
                image_advtab: true,
                image_title: true,
                relative_urls: false,
                cache_suffix: '?v=' + FleetCart.version,
                plugins: 'lists, link, table, paste, autosave, autolink, wordcount, code',
                toolbar: 'styleselect bold italic underline | bullist numlist | alignleft aligncenter alignright | outdent indent | link table code'
            });
            if (toggleFormat === true) {
                $('#custom-collapse-' + block).addClass('in');
                $('#block-form-' + block + ' .collapsed').removeClass('collapsed');
            }
            block++;
        });
        var toggleFormat = false;
        $('#expandAllFormats').text(FleetCart.langs['page::pages.expand_all']);
        $('#expandAllFormats').on('click', function (e) {
            e.preventDefault();
            $("#page-block .panel-collapse").each(function (index, value) {
                if (toggleFormat) {
                    if ($(this).hasClass('in')) {
                        $(this).collapse('toggle');
                    }
                } else {
                    if (!$(this).hasClass('in')) {
                        $(this).collapse('toggle');
                    }
                }
            });
            toggleFormat = toggleFormat ? false : true;
            if (toggleFormat === true) {
                $('#expandAllFormats').text(FleetCart.langs['page::pages.collapse_all']);
            } else {
                $('#expandAllFormats').text(FleetCart.langs['page::pages.expand_all']);
            }
        });
        $('#btn-save-form').click(function () {
            $('#group-form .has-error').removeClass('has-error');
            var form = document.getElementsByName('block-form');
            tinymce.triggerSave();

            var myForm = document.getElementById('group-form');
            var formData = new FormData(myForm);

            var error = false;
            if ($.trim(formData.get('name')) === '') {
                $('#group-form #name').parent().parent().addClass('has-error');
                $('.accordion-tab li:eq(0)').addClass('has-error');
                error = true;
            }
            if ($.trim(formData.get('description')) === '') {
                $('#group-form #description').parent().parent().addClass('has-error');
                $('.accordion-tab li:eq(0)').addClass('has-error');
                error = true;
            }
            if ($.trim(formData.get('code')) === '') {
                $('#group-form #code').parent().parent().addClass('has-error');
                $('.accordion-tab li:eq(0)').addClass('has-error');
                error = true;
            }
            if ($.trim(formData.get('content_1')) === '') {
                $('#group-form #content_1').parent().parent().addClass('has-error');
                $('.accordion-tab li:eq(1)').addClass('has-error');
                error = true;
            }
            // if ($.trim(formData.get('content_2')) === '') {
            //     $('#group-form #content_2').parent().parent().addClass('has-error');
            //     $('.accordion-tab li:eq(1)').addClass('has-error');
            //     error = true;
            // }
            if (formData.has('slug')) {
                if ($.trim(formData.get('slug')) === '') {
                    $('#group-form #slug').parent().parent().addClass('has-error');
                    $('#advanced_information .accordion-tab li:eq(0)').addClass('has-error');
                    error = true;
                }
            }

            var formData = $('#group-form').serializeArray();
            var blockItem = 0;
            form.forEach(function (element, index) {
                var blockData = $('#' + element.id).serializeArray();
                blockData.forEach(function (item) {
                    formData.push(item);
                });
                blockItem++;
            });
            // if (blockItem === 0) {
            //     $('.accordion-tab li:eq(1)').addClass('has-error');
            //     error = true;
            // }
            if (error === true) {
                return false;
            }

            var url = $('#group-form').attr('action');
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                success: function success(res) {
                    window.location.assign(route('admin.groups.index'));
                },
                error: function error(xhr) {
                    alert(xhr);
                }
            });
        });

        this.sortable();
    }

    sortable() {
        Sortable.create(document.getElementById('page-block'), {
            handle: '.drag-icon',
            animation: 150,
        });
    }

}
