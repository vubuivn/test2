<?php

return [
    'name' => 'Name',
    'slug' => 'URL',
    'body' => 'Body',
    'is_active' => 'Status',
    'block' => 'Block',
    'address' => 'Address',
    'phone' => 'Phone',
    'district' => 'District',
    'loc_lat' => 'Lat',
    'loc_long' => 'Long',
    'open' => 'Open',
    'select_district' => 'Select district',
    'email' => 'Email'
];
