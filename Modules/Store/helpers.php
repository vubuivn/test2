<?php

use Modules\Store\Entities\Store;

function getDistrictIds(){
    $data = [];
    $ids = Store::select('district_id')->where('is_active',1)->groupBy('district_id')->get();
    if($ids){
        foreach($ids as $id){
            $data[] = $id->district_id;
        }
        return $data; 
    }
    return;
}

function getCities($ids){
    $data = [];
    $districts = \DB::table('districts')->whereIn('id',$ids)->get();
    $arrIds = [];
    if($districts){
        $data['districts'] = $districts;
        foreach($districts as $district){
            $arrIds[] = $district->city_id;
        }
    }
    if(count($arrIds)>0){
       $cities = \DB::table('cities')->whereIn('id',$arrIds)->get();
        if($cities){
            $data['cities'] = $cities;
        }
    }
    return $data;
}

function getStores($district_id){
    return Store::where('is_active',1)->get();
}

function get_local_title($title){
    if(Lang::locale() == 'vi_VN' || Lang::locale() == 'vi' || Lang::locale() == 'vn'){
        return substr(substr($title, 0, strpos($title, '[:en]')),5);
    }else{
        return substr(substr(substr($title, strpos($title, '[:en]'), strpos($title, '[:]')),5),0,-3);
    }
}