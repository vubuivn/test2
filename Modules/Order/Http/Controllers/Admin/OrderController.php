<?php

namespace Modules\Order\Http\Controllers\Admin;

use Modules\Order\Entities\Order;
use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Order\Http\Requests\StoreOrderRequest;
use Modules\Support\Money;

class OrderController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['products', 'coupon', 'taxes'];

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'order::orders.order';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'order::admin.orders';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveOrderRequest::class;


    public function updateShipping($order){

        $request = \Request();
        if($request->has('shipping_cost')){
           try{
                $o = Order::where('id',$order)->first();
                if($o){
                    $total = $o->sub_total->amount() + $request->shipping_cost;
                    $money = new Money($total,currency());
                    Order::where('id',$order)->update([
                        'shipping_cost'=>$request->shipping_cost,
                        'total' => $money,
                    ]);
                    return back()->withSuccess(trans('order::messages.shipping_cost_update_success'));
                }
                return back()->withError(trans('order::messages.order_not_found'));
            }catch(\Exception $e){
                return $e->getMessage();
            } 
        }
        return back()->withError(trans('order::messages.shipping_cost_required'));
    }
}
