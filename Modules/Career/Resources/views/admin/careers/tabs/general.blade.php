{{ Form::text('name', trans('career::attributes.name'), $errors, $career, ['labelCol' => 2, 'required' => true]) }}


<div class="form-group ">
    <label for="name" class="col-md-2 control-label text-left">{{trans('career::attributes.city')}}</label>
    <div class="col-md-10">
        <select name="city_id" class="form-control">
            @foreach($cities as $city)
                <option value="{{$city->id}}" @if($career->city_id == $city->id) selected @endif>{{$city->title}}</option>
            @endforeach
        </select>
    </div>
</div>

@include('media::admin.image_picker.single', [
    'title' => trans('career::attributes.banner_image'),
    'inputName' => 'files[base_image]',
    'file' => $career->baseImage,
])

<div class="form-group ">
    <div class="col-md-8">
        {{ Form::checkbox('is_active', trans('career::attributes.is_active'), trans('career::careers.form.enable_the_career'), $errors, $career) }}
    </div>
</div>
{{ Form::text('position', trans('career::attributes.position'), $errors, $career, ['labelCol' => 2]) }}
{{ Form::text('time_job', trans('career::attributes.time_job'), $errors, $career, ['labelCol' => 2]) }}
{{ Form::wysiwyg('body', trans('career::attributes.body'), $errors, $career, ['labelCol' => 2]) }}
{{ Form::wysiwyg('request', trans('career::attributes.request'), $errors, $career, ['labelCol' => 2]) }}
{{ Form::wysiwyg('welfare', trans('career::attributes.welfare'), $errors, $career, ['labelCol' => 2]) }}
{{ Form::wysiwyg('note_content', trans('career::attributes.note_content'), $errors, $career, ['labelCol' => 2]) }}

@push('globals')
    <script>
        FleetCart.langs['career::careers.expand_all'] = '{{trans('career::careers.expand_all')}}';
        FleetCart.langs['career::careers.collapse_all'] = '{{trans('career::careers.collapse_all')}}';
    </script>
@endpush
