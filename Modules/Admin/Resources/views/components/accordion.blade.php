<div class="accordion-content clearfix">
    <div class="col-lg-3 col-md-4">
        <div class="accordion-box">
            <div class="panel-group" id="{{ $name }}">
                @foreach ($groups as $group => $options)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a
                                    @if (count($groups) > 1)
                                    class="{{ ($options['active'] ?? false) ? '' : 'collapsed' }} {{ $tabs->group($group)->hasError() ? 'has-error' : '' }}"
                                    data-toggle="collapse"
                                    data-parent="#{{ $name }}"
                                    href="#{{ $group }}"
                                    @endif
                                >
                                    {{ $options['title'] }}
                                </a>
                            </h4>
                        </div>

                        <div id="{{ $group }}"
                             class="panel-collapse collapse {{ ($options['active'] ?? false) ? 'in' : '' }}">
                            <div class="panel-body">
                                <ul class="accordion-tab nav nav-tabs">
                                    {{ $tabs->group($group)->navs() }}
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <br>
            <?php 
                $bs = [];
                $bso = [];
                foreach($block as $value){
                    if($value->tags != 0){
                        $bs[$value->tags][] = $value;
                    }else{
                        $bso[] = $value;
                    }
                }
            ?>
            @if(isset($tags))
            <label>Group block</label>
            <div class="panel-group">
                @foreach($tags as $tag)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="collapsed " data-toggle="collapse" data-parent="#PageTabs" href="#tag{{$tag->id}}">
                                    {{$tag->name}}
                                </a>
                            </h4>
                        </div>

                        <div id="tag{{$tag->id}}" class="panel-collapse collapse ">
                            <div class="panel-body">
                                <ul class="accordion-tab nav nav-tabs">
                                    @if(isset($bs[$tag->id]))
                                    @foreach ($bs[$tag->id] as $value)
                                        @if($value->is_active === 1)
                                        <li class=" ">
                                            <a data-id="{{$value->id}}" class="block-item collapsed" style="cursor: pointer">
                                                {{ $value->name }}
                                            </a>
                                        </li>
                                        <script type="text/html" id="{{$value->id}}-template">
                                            <form name="block-form" id="block-form-<%- i %>">
                                                <input type="hidden" name="blocks[<%- i %>][block_id]" value="{{$value->id}}">
                                                <div class="panel panel-default" style="margin-bottom: 15px">
                                                    <div class="panel-heading" role="tab">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" class="collapsed" data-parent="#block-<%- i %>"
                                                               href="#custom-collapse-<%- i %>">
                                                                <span class="drag-icon pull-left">
                                                                    <i class="fa"></i>
                                                                    <i class="fa"></i>
                                                                </span>

                                                                <span id="block-name" class="pull-left">
                                                                    {{ trans('block::blocks.form.new_block') }}
                                                                </span>
                                                            </a>
                                                        </h4>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div id="custom-collapse-<%- i %>" class="panel-collapse collapse" role="tabpanel">
                                                        <div class="panel-body">
                                                            <div class="new-option clearfix">
                                                                <div class="col-lg-6 col-md-12 p-l-0">
                                                                    <div class="form-group">
                                                                        <label for="block-<%- i %>-name">
                                                                            {{ trans('block::blocks.form.name') }}
                                                                        </label>
                                                                        <input type="text" name="blocks[<%- i %>][main_block_title]"
                                                                               class="form-control block-name-field"
                                                                               id="block-<%- i %>-name" value="">
                                                                    </div>
                                                                </div>

                                                                <button type="button"
                                                                        class="btn btn-default delete-block pull-right"
                                                                        data-toggle="tooltip" title=""
                                                                        data-original-title="{{trans('block::blocks.form.delete_block')}}">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                            <div class="option-values clearfix" id="block-<%- i %>-values">
                                                                @foreach($value->values as $value)
                                                                    {{block_html($value)}}
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </script>
                                        @endif
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @endif
  
            
            <br>
            @if(count($bso)>0)

            <div class="panel-group">
                @foreach ($bso as $value)
                    @if($value->is_active === 1)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-id="{{$value->id}}" class="block-item collapsed" style="cursor: pointer">
                                        {{ $value->name }}
                                    </a>
                                </h4>
                            </div>
                        </div>
                    @endif

                    <script type="text/html" id="{{$value->id}}-template">
                        <form name="block-form" id="block-form-<%- i %>">
                            <input type="hidden" name="blocks[<%- i %>][block_id]" value="{{$value->id}}">
                            <div class="panel panel-default" style="margin-bottom: 15px">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="collapsed" data-parent="#block-<%- i %>"
                                           href="#custom-collapse-<%- i %>">
                                            <span class="drag-icon pull-left">
                                                <i class="fa"></i>
                                                <i class="fa"></i>
                                            </span>

                                            <span id="block-name" class="pull-left">
                                                {{ trans('block::blocks.form.new_block') }}
                                            </span>
                                        </a>
                                    </h4>
                                    <div class="clearfix"></div>
                                </div>
                                <div id="custom-collapse-<%- i %>" class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="new-option clearfix">
                                            <div class="col-lg-6 col-md-12 p-l-0">
                                                <div class="form-group">
                                                    <label for="block-<%- i %>-name">
                                                        {{ trans('block::blocks.form.name') }}
                                                    </label>
                                                    <input type="text" name="blocks[<%- i %>][main_block_title]"
                                                           class="form-control block-name-field"
                                                           id="block-<%- i %>-name" value="">
                                                </div>
                                            </div>

                                            <button type="button"
                                                    class="btn btn-default delete-block pull-right"
                                                    data-toggle="tooltip" title=""
                                                    data-original-title="{{trans('block::blocks.form.delete_block')}}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="option-values clearfix" id="block-<%- i %>-values">
                                            @foreach($value->values as $value)
                                                {{block_html($value)}}
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </script>
                @endforeach
            </div>
            @endif
        </div>
    </div>

    <div class="col-lg-9 col-md-8">
        <div class="accordion-box-content">
            <div class="tab-content clearfix">
                {{ $contents }}

                @include('admin::form.footer')
            </div>
        </div>
    </div>
</div>
