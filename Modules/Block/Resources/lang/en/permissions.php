<?php

return [
    'index' => 'Index Block',
    'create' => 'Create Block',
    'edit' => 'Edit Block',
    'destroy' => 'Delete Block',
];
