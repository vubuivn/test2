<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsQueuedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_queue', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku');
            $table->text('value')->nullable();
            $table->enum('status', ['completed', 'failed', 'processing', 'queued'])->default('queued');
            $table->string('type', 20); // add, edit, delete, group_code
            // $table->string('time', 20)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_queued');
    }
}
