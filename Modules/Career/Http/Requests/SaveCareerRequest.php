<?php

namespace Modules\Career\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Career\Entities\Career;
use Modules\Core\Http\Requests\Request;

class SaveCareerRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var array
     */
    protected $availableAttributes = 'career::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => $this->getSlugRules(),
            'name' => 'required',
            'is_active' => 'required|boolean',
        ];
    }

    private function getSlugRules()
    {
        $rules = $this->route()->getName() === 'admin.careers.update'
            ? ['required']
            : ['sometimes'];

        $slug = Career::withoutGlobalScope('active')->where('id', $this->id)->value('slug');

        $rules[] = Rule::unique('careers', 'slug')->ignore($slug, 'slug');

        return $rules;
    }
}
