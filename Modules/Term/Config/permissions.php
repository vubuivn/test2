<?php

return [
    'admin.terms' => [
        'index' => 'term::permissions.index',
        'create' => 'term::permissions.create',
        'edit' => 'term::permissions.edit',
        'destroy' => 'term::permissions.destroy',
    ],
];
