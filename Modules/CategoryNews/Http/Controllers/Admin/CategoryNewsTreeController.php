<?php

namespace Modules\CategoryNews\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Category\Http\Responses\CategoryTreeResponse;
use Modules\CategoryNews\Entities\CategoryNews;
use Modules\CategoryNews\Services\CategoryNewsTreeUpdater;

class CategoryNewsTreeController extends Controller
{
    /**
     * Display category tree in json.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = CategoryNews::withoutGlobalScope('active')
                              ->orderByRaw('-position DESC')
                              ->get();

        return new CategoryTreeResponse($categories);
    }

    /**
     * Update category tree in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        CategoryNewsTreeUpdater::update(request('category_tree'));

        return trans('category::messages.category_order_saved');
    }
}
