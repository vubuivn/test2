<div class="accordion-box-content form-group ">
    <div class="tab-content clearfix">
        <div class="contact-image-wrapper">
            <div class="single-contact">
                <div class="contact-header">
                    <h5>{{$block->title}}</h5>
                </div>
                
                <div class="contact-template">
                    <div class="contact-body">
                        <div class="col-lg-6 col-md-12 col-sm-6 clearfix">
                            <div class="form-group">
                                <select name="blocks[{{$number}}][{{$block->key}}][formcontact]" class="form-control">
                                    <option>{{trans('block::blocks.select.select')}}</option>
                                    @if(count($formcontacts)>0)
                                        @foreach($formcontacts as $contact)
                                            @php($selected = '')
                                            @if(isset($data->{$block->key}) && $data->{$block->key}->formcontact == $contact->id)
                                                @php( $selected = 'selected')
                                            @endif
                                            <option value="{{$contact->id}}" {{$selected}}>{{$contact->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
