@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('block::blocks.blocks'))

    <li class="active">{{ trans('block::blocks.blocks') }}</li>
@endcomponent

@component('admin::components.page.index_table')
    @slot('buttons', ['create'])
    @slot('resource', 'blocks')
    @slot('name', trans('block::blocks.block'))

    @component('admin::components.table')
        @slot('thead')
            <tr>
                @include('admin::partials.table.select_all')
                <th>{{ trans('block::attributes.name') }}</th>
                <th>{{ trans('admin::admin.table.status') }}</th>
                <th>{{ trans('admin::admin.table.tags') }}</th>
                <th data-sort>{{ trans('admin::admin.table.created') }}</th>
            </tr>
        @endslot
    @endcomponent
@endcomponent

@push('scripts')
    <script>
        new DataTable('#blocks-table .table', {
            columns: [
                {data: 'checkbox', orderable: false, searchable: false, width: '3%'},
                {data: 'name', name: 'translations.title', orderable: false, defaultContent: ''},
                {data: 'status', name: 'is_active', searchable: false},
                {data: 'tags', name: 'tags'},
                {data: 'created', name: 'created_at'},
            ],
        });
    </script>
@endpush
