import BaseElement from './BaseElement';

export default class extends BaseElement {
    constructor() {
        super();

        this.elementsCount = 0;

        this.addElements(FleetCart.data['builder.elements']);

        if (this.elementsCount === 0) {
            this.addElement();
        }

        if (this.elementsCount > 3) {
            this.collapseElements();
        }

        super.addElementsErrors(FleetCart.errors['builder.elements']);

        $('#add-new-element').on('click', () => this.addElement());
        $('#add-global-element').on('click', () => this.addGlobalElement());
    }

    addElements(elements) {
        for (let element of elements) {
            this.addElement(element);
        }
    }

    collapseElements() {
        let elements = $('.element:not(.element-has-errors)');

        for (let element of elements) {
            $(element).find('[data-toggle=collapse]').trigger('click');
        }
    }

    addGlobalElement() {
        let globalElementId = $('#global-element').val();

        if (globalElementId === '') {
            return window.admin.stopButtonLoading($('#add-global-element'));
        }

        $.ajax({
            type: 'GET',
            url: route('admin.elements.show', globalElementId),
            dataType: 'json',
            success: element => {
                this.addElement(element);

                window.admin.stopButtonLoading($('#add-global-element'));
            },
        });
    }

    addElement(element = { id: null, name: null, type: null, is_required: false, values: [] }) {
        // Cast "is_required" field to boolean.
        element.is_required = !! JSON.parse(element.is_required);

        let elementId = this.elementsCount;

        let template = _.template($('#element-template').html());
        let html = $(template({ element, elementId }));

        if (element.name !== null) {
            setTimeout(() => {
                $(`#element-${elementId}`).find('#element-name').text(element.name);
            });
        }

        let elementGroup = $('#elements-group').append(html);

        if (! elementGroup.is('.sortable')) {
            super.makeSortable(elementGroup[0]);

            elementGroup.addClass('sortable');
        }

        this.deleteElementEventListener(html);
        this.updateElementNameEventListener(elementId);
        this.updateTemplateEventListener(elementId, element['values']);

        window.admin.tooltip();

        this.elementsCount++;
    }

    deleteElementEventListener(element) {
        element.find('.delete-element').on('click', (e) => $(e.currentTarget).closest('.element').remove());
    }

    updateElementNameEventListener(elementId) {
        let element = $(`#element-${elementId}`);
        let old = element.find('#element-name').text();

        $(element).find('.element-name-field').on('input', (e) => {
            let name = e.currentTarget.value !== '' ? e.currentTarget.value : old;

            element.find('#element-name').text(name);
        });
    }

    updateTemplateEventListener(elementId, values = []) {
        let elementTypeElement = $(`#element-${elementId}-type`);

        elementTypeElement.on('change', (e) => {
            if (e.currentTarget.value === '') {
                return this.getElementValuesElement(elementId).html('');
            }

            super.changeElementType({ elementId, values });
        });

        // Trigger the "change" event on element type after attaching the listener
        // this will automatically take effect of the current element which is
        // maybe loaded from the old input values or from the builder data.
        elementTypeElement.trigger('change');
    }

    addElementRow({ elementId, valueId, value = { label: '', price: '', price_type: 'fixed' } }) {
        let template = this.getRowTemplate({ elementId, valueId, value });

        let selectValues = $(`#element-${elementId}-select-values`).append(template);

        super.initElementRow(template, selectValues);
    }

    addElementRowEventListener(elementId) {
        $(`#element-${elementId}-add-new-row`).on('click', () => {
            let valueId = $(`#element-${elementId}-values .element-row`).length;

            this.addElementRow({ elementId, valueId });
        });
    }

    getElementValuesElement(elementId) {
        return $(`#element-${elementId}-values`);
    }

    getAddNewRowButton(elementId) {
        return $(`#element-${elementId}-add-new-row`);
    }

    getInputFieldForErrorKey(key) {
        let keyParts = key.split('.');

        // Remove the first element which is "element".
        keyParts.shift();

        // Replace all "_" to "-".
        keyParts = keyParts.map(k => {
            return k.split('_').join('-');
        });

        return $(`#element-${keyParts.join('-')}`);
    }
}
