<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ v(Theme::url('public/css/app.css')) }}">
    <title>Error</title>
</head>
<style>
    .alert-danger {
         color: #a94442!important;
    }
</style>
<body>
@if (count($errors) > 0)
    <div class="clearfix"></div>
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (session('error'))
    <div class="clearfix"></div>
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
@if (session('success'))
    <div class="clearfix"></div>
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
</body>
</html>