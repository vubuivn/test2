<?php

return [
    'career' => 'Career',
    'careers' => 'Careers',
    'table' => [
        'name' => 'Name',
    ],
    'tabs' => [
        'group' => [
            'career_information' => 'Career Infortmation',
        ],
        'general' => 'General',
        'seo' => 'SEO',
        'block' => 'Block',
    ],
    'form' => [
        'enable_the_career' => 'Enable the career',
        'add_more_image' => 'Add more image',
        'banner_image' => 'Banner image',
    ],
    'expand_all' => 'Expand All',
    'collapse_all' => 'Collapse All',
    'template' => 'Template',
    'normal' => 'Normal',
    'has_sidebar' => 'Has sidebar',
    'questions' => 'Question',
];
