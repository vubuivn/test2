<?php

Route::get('elements', [
    'as' => 'admin.elements.index',
    'uses' => 'ElementController@index',
    'middleware' => 'can:admin.elements.index',
]);

Route::get('{builder}/elements', [
    'as' => 'admin.elements.builder',
    'uses' => 'ElementController@index',
    'middleware' => 'can:admin.elements.index',
]);

Route::get('elements/create', [
    'as' => 'admin.elements.create',
    'uses' => 'ElementController@create',
    'middleware' => 'can:admin.elements.create',
]);

Route::get('{builder}/elements/create', [
    'as' => 'admin.builder.elements.create',
    'uses' => 'ElementController@create',
    'middleware' => 'can:admin.elements.create',
]);

Route::post('elements', [
    'as' => 'admin.elements.store',
    'uses' => 'ElementController@store',
    'middleware' => 'can:admin.elements.create',
]);

Route::get('elements/{id}', [
    'as' => 'admin.elements.show',
    'uses' => 'ElementController@show',
    'middleware' => 'can:admin.elements.edit',
]);

Route::get('elements/{id}/edit', [
    'as' => 'admin.elements.edit',
    'uses' => 'ElementController@edit',
    'middleware' => 'can:admin.elements.edit',
]);

Route::put('elements/{id}', [
    'as' => 'admin.elements.update',
    'uses' => 'ElementController@update',
    'middleware' => 'can:admin.elements.edit',
]);

Route::delete('elements/{ids}', [
    'as' => 'admin.elements.destroy',
    'uses' => 'ElementController@destroy',
    'middleware' => 'can:admin.elements.destroy',
]);
