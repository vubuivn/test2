<!-- Modal -->
<div class="modal fade formpopup" id="myLogin" data-show="flogin" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <span class="close" data-dismiss="modal"><i class="icon-close"></i></span>
            <form class="inner formlogin" name="formlogin">
                <h3>{{ trans('storefront::layout.log_in') }}</h3>
                <p class="text-alert alert-error"></p>
                <p class="winput">
                    <input class="input" type="email" placeholder="Email" id="email_login" required>
                    <i class="icon-user"></i>
                </p>
                <p class="winput">
                    <input class="input" type="password" id="password_login" placeholder="Password" required>
                    <i class="icon-lock"></i>
                </p>
                <p class="wremember">
                    <label class="remember">
                        <input type="checkbox" id="remember_login">
                        {{ trans('user::auth.remember_me') }}
                    </label>
                    <!-- <a href="javascript:void(0)" class="spass">
                        <span class="s">Show</span> <span class="h">Hide</span> password
                    </a> -->
                </p>
                <p class="text-center login-popup">
                    <button class="btn  noshadow full">Login</button>
                </p>
                <?php 
                    $check = request()->segment(count(request()->segments()));
                ?>
                @if($check == 'cart' || $check == 'checkout')
                <p class="text-center">
                    <a href="{{route('checkout.create')}}" class="btn  noshadow full">{{trans('storefront::account.checkout_without_login')}}</a>
                </p>
                @endif
                <p class="text-center">
                    <a href="#" class="switchform" data-form="fforgot">{{ trans('user::auth.forgot_password') }}</a>
                </p>
                <div class="logsocial">
                    <span class="title">{{ trans('user::auth.or') }}</span>
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            @if (setting('facebook_login_enabled'))
                                <a href="{{ route('login.redirect', ['provider' => 'facebook']) }}" class="fb"> <i class="icon-facebook"></i> Facebook</a>
                            @endif
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            @if (setting('google_login_enabled'))
                                <a href="{{ route('login.redirect', ['provider' => 'google']) }}" class="gg"> <i class="icon-google-plus"></i> Google</a>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="resge">{{trans('storefront::storefront.form.have_account')}} <a class="b switchform" data-form="fregister" href="javascript:void(0)">Sign Up</a></div>
            </form>

            <form class="inner formforgot">
                <h3>{{trans('storefront::storefront.form.forgot_password')}}</h3>
                <p>{{trans('storefront::storefront.form.get_password')}}</p>
                <p class="winput">
                    <input class="input" type="text" placeholder="Enter your email">
                    <i class="icon-mail-1"></i>
                </p>
                <p><button class="btn full noshadow"> {{trans('storefront::storefront.form.send')}}</button></p>
                <p class="text-center"><a class="switchform" data-form="flogin" href="javascript:void(0)">{{trans('storefront::storefront.form.login')}}</a></p>


            </form>

            <form class="inner formregister">
                <div class="loading_image"></div>
                <div class="logsocial2">
                    
                    @if (setting('facebook_login_enabled'))
                        @if (count(app('enabled_social_login_providers')) !== 0)
                        <span class="title">{{ trans('user::auth.or') }}</span>
                      @endif

                        Sing up with <a href="{{ route('login.redirect', ['provider' => 'facebook']) }}" class="fb"> {{ trans('user::auth.log_in_with_facebook') }}</a>@endif or @if (setting('google_login_enabled'))<a href="{{ route('login.redirect', ['provider' => 'google']) }}" class="gg">{{ trans('user::auth.log_in_with_google') }}</a>
                    @endif
                </div>
                <h3>{{ trans('user::auth.register') }}</h3>
                <div class="error_result"></div>
                <p class="winput">
                    <input class="input" type="text" name="name" placeholder="{{ trans('user::auth.first_name') }}" value="{{ old('name') }}">
                    <i class="icon-user"></i>
                </p>
                <p class="winput">
                    <input class="input" type="text" name="phone" placeholder="Phone">
                    <i class="icon-phone-4"></i>
                </p>
                <p class="winput">
                    <input class="input" type="text" name="address" placeholder="{{ trans('storefront::account.profile.address') }}">
                    <i class="icon-home"></i>
                </p>
                <p class="winput">
                    <input class="input" type="text" name="email" placeholder="Email">
                    <i class="icon-mail-1"></i>
                </p>
                <p class="winput">
                    <input class="input" type="password" name="password" placeholder="Password">
                    <i class="icon-lock"></i>
    
                </p>
                <p class="winput">
                    <input class="input" type="password" name="password_confirmation" placeholder="Re-type Password">
                    <i class="icon-lock"></i>
       
                </p>

                <p class="gender">
                    <span class="title">{{trans('storefront::storefront.form.gender')}} : </span>

                    <label class="radio ">
                        <input name="gender" type="radio" checked="" value="0">
                        <span></span>
                        {{trans('storefront::storefront.form.male')}}
                    </label>
                    <label class="radio ">
                        <input name="gender" type="radio" value="1">
                        <span></span>
                        {{trans('storefront::storefront.form.female')}}
                    </label>
                </p>

                <div>
                    <label class="checkbox ">
                        <input type="checkbox" name="receive" checked>
                        <span></span>
                        {{trans('storefront::storefront.form.receive_info')}}
                    </label>
                </div>
                <p>
                    <label class="checkbox ">
                        <input type="checkbox" name="agree" checked>
                        <span></span>
                        {{trans('storefront::storefront.form.i_agree')}}
                    </label>
                </p>

                <div class="row  mb-20">
                    <div class="col-sm-4 col-xs-4">
                        <select name="day" class="select">
                            <option value="000">{{trans('storefront::storefront.form.day')}}</option>
                            <?php
                            for ($i = 1; $i <= 31; $i++) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            } ?>
                        </select>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <select name="month" class="select">
                            <option value="000">{{trans('storefront::storefront.form.month')}}</option>
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            } ?>
                        </select>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <select name="Year" class="select">
                            <option value="000">{{trans('storefront::storefront.form.year')}}</option>
                            <?php
                            for ($i = 1970; $i <= 2020; $i++) {
                                echo '<option value="' . $i . '">' . $i . '</option>';
                            } ?>
                        </select>
                    </div>
                </div>

                <p class="text-center">
                    <button class="btn full noshadow submit_register"> {{trans('storefront::storefront.register.register')}}</button>
                </p>

                <div class="resge">{{trans('storefront::storefront.form.have_a_account')}} <a class="b switchform" data-form="flogin" href="javascript:void(0)">{{trans('storefront::storefront.form.login')}}</a></div>
            </form>

                            
        </div>

    </div>
</div>

<script>
    (function($) {
        $(document).ready(function() {
            $(".switchform").click(function() {
                var f = $(this).data('form');
                $(".formpopup").attr('data-show', f);
            });
            // $('.formpopup .spass').click(function() {
            //     if ($(this).hasClass('ac')) {
            //         $(this).removeClass('ac');
            //         $('#password').attr('type', 'password');
            //     } else {
            //         $(this).addClass('ac');
            //         $('#password').attr('type', 'text');
            //     }
            // });
        });
    })(jQuery);
</script>
<script>
    (function($) {
        $('form[name=formlogin]').on('submit', function(e) {
            e.preventDefault();

            var email = $('#email_login').val();
            var password = $('#password_login').val();
            var remember_me = $('#remember_login').is(":checked");
            var _token = $('meta[name=csrf-token]').attr('content');

            $.ajax({
                url: '{{ route('login.ajax') }}',
                method: 'POST',
                type: 'json',
                data: {
                    email: email,
                    password: password,
                    remember_me: remember_me,
                    _token: _token
                },
                success: function(res) {
                    if (!res.status) {
                        $('.text-alert.alert-error').text(res.message);
                    } else {
                        window.location.reload();
                    }
                },
                error: function(xhr, err) {
                    $('.text-alert.alert-error').text(xhr.responseJSON.errors.email[0]);
                }
            });
        });
    })(jQuery);
</script>

@push('scripts')
<style>
    .error_result .error{margin-bottom: 0;color: red}
    .loading_image,.success_page{position: absolute;top: 0;left: 0;width: 100%;height: 100%;background: #fff;z-index: 10;display: none}
    .loading_image{opacity: 0.6}
    .error_result .success{margin-bottom: 0;color:green;}
    .popup-subscription {position: fixed;top: calc(50% - 50px);left: calc(50% - 150px);width: 300px;background: rgba(0, 0, 0, 0.45);height: 100px;z-index: 500000;line-height: 100px;text-align: center;border-radius: 30px;color: #fff;
        -webkit-transition: opacity 0.7s ease-in-out;
        -moz-transition: opacity 0.7s ease-in-out;
        -ms-transition: opacity 0.7s ease-in-out;
        -o-transition: opacity 0.7s ease-in-out;
        transition: opacity 0.7s ease-in-out;
        opacity: :0;
    }
    .submit_register img {height: calc(100% - 20px);margin-top: 10px;}
    .formpopup .winput .input.has-error{border-color: #bf0404}
</style>
<script>
    (function($){
        var check = false;
        $('.formregister').submit(function(e){
            e.stopPropagation(); 
            e.stopImmediatePropagation();
            e.preventDefault();
            if(check != true){
                var name = $(this).find('input[name="name"]').val();
                var email = $(this).find('input[name="email"]').val();
                var password = $(this).find('input[name="password"]').val();
                var password_confirmation = $(this).find('input[name="password_confirmation"]').val();
                var gender = $(this).find('input[name="gender"]:checked').val();
                var agree = $(this).find('input[name="agree"]').val();
                var day = $(this).find('select[name="day"]').val();
                var month = $(this).find('select[name="month"]').val();
                var year = $(this).find('select[name="year"]').val();
                var phone = $(this).find('input[name="phone"]').val();
                var address = $(this).find('input[name="address"]').val();
                jQuery.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="_token"]').attr('content')
                  }
                });

                jQuery.ajax({
                    type: 'POST',
                    url: "{{ route('register.post.ajax') }}",
                    data: {'name':name,'email':email,'password':password,'password_confirmation':password_confirmation,'gender':gender,'agree':agree,'day':day,'month':month,'year':year,'phone':phone,'address':address},
                    beforeSend: function() {
                        $('.loading_image').show();
                        $('.submit_register').html('<img src="/assets/images/loading.gif">');
                    },
                    success: function(data){
                        console.log(data);
                        if(data.phone == 'phone'){
                            $('input[name="'+data.phone+'"]').addClass('has-error');
                        }
                        if(data.email == 'email'){
                            $('input[name="'+data.email+'"]').addClass('has-error');
                        }
                        if(data.name == 'name'){
                            $('input[name="'+data.name+'"]').addClass('has-error');
                        }
                        if(data.password == 'password'){
                            $('input[name="'+data.password+'"]').addClass('has-error');
                        }
                        if(data.status =='error'){
                            $('.error_result').html(data.text);
                        }else{
                            console.log('a');
                            $('.error_result').html(data.text);
                            $('.formregister input').each(function(){
                                $(this).removeClass('has-error');
                                // if($(this).is(':checkbox')){
                                //     $(this).val(1);
                                // }else{
                                //     $(this).val('');
                                // }
                            });
                            // $('.formpopup').attr('data-show','flogin');
                        }
                        
                    },
                    complete: function(){
                        $('.loading_image').hide();
                        $('.submit_register').html("{{trans('storefront::storefront.register.register')}}");
                        check = false;
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    })(jQuery)
</script>
@endpush