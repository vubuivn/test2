<?php

namespace Modules\Block\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;
use Modules\Tax\Entities\TaxClass;
use Modules\Category\Entities\Category;

class BlockTabs extends Tabs
{
    public function make()
    {
        $this->group('basic_information', trans('block::blocks.tabs.block.basic_information'))
            ->active()
            ->add($this->general())
            ->add($this->values());
    }

    private function general()
    {
        $tags = \DB::table('block_tags')->get();
        return tap(new Tab('general', trans('block::blocks.tabs.general')), function (Tab $tab) use($tags){
            $tab->active();
            $tab->weight(5);
            $tab->fields(['name', 'description', 'is_active']);
            $tab->view('block::admin.blocks.tabs.general',['tags'=>$tags]);
        });
    }

    private function values()
    {
        return tap(new Tab('values', trans('block::blocks.tabs.values')), function (Tab $tab) {
            $tab->weight(10);
            $tab->view('block::admin.blocks.tabs.values');
        });
    }
}
