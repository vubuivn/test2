<?php

namespace Modules\Term\Entities;

use Modules\Admin\Ui\AdminTable;
use Modules\Builder\Entities\Builder;
use Modules\Support\Eloquent\Model;
use Modules\Meta\Eloquent\HasMetaData;
use Modules\Support\Eloquent\Sluggable;
use Modules\Support\Eloquent\Translatable;

class Term extends Model
{
    use Translatable, Sluggable, HasMetaData;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['slug', 'is_active', 'builder_id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $translatedAttributes = ['name'];

    /**
     * The attribute that will be slugged.
     *
     * @var string
     */
    protected $slugAttribute = 'name';

    public function __construct()
    {
        if (config('mango.translatedAttributes')){
            parent::__construct();
            $translatedAttributes = array_merge($this->translatedAttributes, config('mango.translatedAttributes'));
            $translatedAttributes =  array_unique($translatedAttributes);
            $this->translatedAttributes = $translatedAttributes;
        }
        config(['mango.translatedAttributes' => $this->translatedAttributes]);
    }

    public function builder()
    {
        return $this->belongsTo(Builder::class);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addActiveGlobalScope();
    }

    public static function urlForTerm($id)
    {
        return static::select('slug')->firstOrNew(['id' => $id])->url();
    }

    public function url()
    {
        if (is_null($this->slug)) {
            return '#';
        }

        return localized_url(locale(), $this->slug);
    }

    /**
     * Get table data for the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function table()
    {
        return new AdminTable($this->newQuery()->withoutGlobalScope('active'));
    }

//    public function setAttribute($key, $value) {
//        if (is_array($value)) {
//            $value = json_encode($value);
//        }
//        return parent::setAttribute($key, $value);
//    }
}
