<?php

return [
    'name' => 'Name',
    'slug' => 'URL',
    'body' => 'Body',
    'from' => 'From',
    'reply-to' => 'Reply to',
    'noti-to' => 'Notification to',
    'subject' => 'Subject',
    'message-body' => 'Message Body',
    'is_active' => 'Status',
];
