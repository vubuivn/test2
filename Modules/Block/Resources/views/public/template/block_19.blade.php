@php($i = 1)
@foreach($category as $c)
	<div class="tab ">
		<div class="tab-title" >
			<?php echo $i; ?>. {{$c->group->title}}
		</div>
		<div class="tab-content entry-content" style="display: <?php if($i==1) echo 'block';else echo 'none'; ?>;">
			{{$c->group->content}}
		</div>
	</div>
	@php($i++)
@endforeach