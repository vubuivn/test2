<div class="row">
    <div class="col-md-8">
        {{ Form::text('name', trans('element::attributes.name'), $errors, $element, ['required' => true]) }}

        <div class="form-group required {{ $errors->has('type') ? 'has-error': '' }}">
            <label for="type" class="col-md-3 control-label text-left">
                {{ trans('element::attributes.type') }}<span class="m-l-5 text-red">*</span>
            </label>

            <div class="col-md-9">
                <select name="type" class="form-control custom-select-black" id="type">
                    <option value="dropdown" {{ old('type', $element->type) === 'dropdown' ? 'selected' : '' }}>
                        {{ trans('element::elements.form.element_types.dropdown') }}
                    </option>

                    <option value="checkbox" {{ old('type', $element->type) === 'checkbox' ? 'selected' : '' }}>
                        {{ trans('element::elements.form.element_types.checkbox') }}
                    </option>

                    <option value="radio" {{ old('type', $element->type) === 'radio' ? 'selected' : '' }}>
                        {{ trans('element::elements.form.element_types.radio') }}
                    </option>

                    <option value="multiple_select" {{ old('type', $element->type) === 'multiple_select' ? 'selected' : '' }}>
                        {{ trans('element::elements.form.element_types.multiple_select') }}
                    </option>

                    <option value="text" {{ old('type', $element->type) === 'text' ? 'selected' : '' }}>
                        {{ trans('element::elements.form.element_types.text') }}
                    </option>
                </select>

                {!! $errors->first('type', '<span class="help-block">:message</span>') !!}
            </div>
        </div>

        {{ Form::checkbox('is_required', trans('element::attributes.is_required'), trans('element::elements.form.this_element_is_required'), $errors, $element) }}
    </div>
</div>
