<?php

namespace Modules\Category\Http\ViewComposers;

use Modules\Category\Entities\Category;

class CategoryComposer
{
    public function compose ($view)
    {
        $view->with(
            [
                'category'        => Category::treeList(),
                'category_entity' => Category::class
            ]
        );
    }
}
