<?php

namespace Modules\Checkout\Services;

use DB;

class CityService {

    public static function loadStateProvinceList() {
        $locale = locale();
        $formattedProvinces = [];
        $provinces = DB::table('cities')->get();
    
        foreach ($provinces AS $city) {
            $formattedProvinces[$city->id] = self::getRightLocale($city->title, $locale);
        }
    
        return $formattedProvinces;
    }
    
    public static function loadDistrictList() {
        $locale = locale();
        $formattedDistricts = [];
        $districts = DB::table('districts')->get();
    
        foreach ($districts AS $district) {
            $formattedDistricts[$district->city_id][$district->id] = self::getRightLocale($district->title, $locale);
        }
    
        return $formattedDistricts;
    }
    
    public static function getRightLocale($text, $locale) {
        if ($locale === 'vi_VN') {
            $locale = 'vi';
        }
        $startMark = '[:' . $locale . ']';
        $startPos = strpos($text, $startMark) + strlen($startMark);
        
        $text = substr($text, $startPos);
        $endPos = strpos($text, '[:');
        
        return substr($text, 0, $endPos);
    }

}