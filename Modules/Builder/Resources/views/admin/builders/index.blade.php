@extends('admin::layout')

@component('admin::components.builder.header')
    @slot('title', trans('builder::builders.builders'))

    <li class="active">{{ trans('builder::builders.builders') }}</li>
@endcomponent

@component('admin::components.builder.index_table')
    @slot('resource', 'builders')
    @slot('buttons', ['create'])
    @slot('name', trans('builder::builders.builder'))

    @slot('thead')
        <tr>
            @include('admin::partials.table.select_all')

            <th>{{ trans('builder::builders.table.name') }}</th>
            <th>{{ trans('admin::admin.table.status') }}</th>
            <th>Form</th>
            <th data-sort>{{ trans('admin::admin.table.created') }}</th>
        </tr>
    @endslot
@endcomponent

@push('scripts')
    <script>
        new DataTable('#builders-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                { data: 'name', name: 'translations.name', orderable: false, defaultContent: '' },
                { data: 'status', name: 'is_active', searchable: false },
                { data: 'term', name: 'term', searchable: false, orderable: false },
                { data: 'created', name: 'created_at' },
            ],
        });
    </script>
@endpush
