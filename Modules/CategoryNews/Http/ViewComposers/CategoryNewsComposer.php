<?php

namespace Modules\CategoryNews\Http\ViewComposers;

use Modules\CategoryNews\Entities\CategoryNews;

class CategoryNewsComposer
{
    public function compose ($view)
    {
        $view->with(
            [
                'category'        => CategoryNews::treeList(),
                'category_entity' => CategoryNews::class
            ]
        );
    }
}
