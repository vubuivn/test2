<?php

namespace Modules\MailTemplate\Http\Requests;

use Modules\Core\Http\Requests\Request;

class SaveMailTemplateRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'mailtemplate::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required',
            'code'    => 'required',
            'subject' => 'required',
            'content' => 'required',
        ];
    }
}
