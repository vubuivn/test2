<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
        <style>
            .container {
                margin-right: auto;
                margin-left: auto;
                padding-left: 15px;
                padding-right: 15px;
            }
	    .logo-aishi > img{
		width: 250px;
		}
	    .button-inline{
		display:inline-block!important;
		}
            @media (min-width: 768px) {
                .container {
                    width: 750px;
                }
            }

            @media (min-width: 992px) {
                .container {
                    width: 970px;
                }
            }

            @media (min-width: 1200px) {
                .container {
                    width: 1170px;
                }
            }

            .col-md-6,
            .col-sm-7 {
                position: relative;
                min-height: 1px;
                padding-left: 15px;
                padding-right: 15px;
            }

            @media (min-width: 768px) {
                .col-sm-7 {
                    float: left;
                }

                .col-sm-7 {
                    width: 58.33333333%;
                }
            }

            @media (min-width: 992px) {
                .col-md-6 {
                    float: left;
                }

                .col-md-6 {
                    width: 50%;
                }
            }

            .container:before,
            .container:after {
                content: " ";
                display: table;
            }

            .container:after {
                clear: both;
            }

            .text-center {
                text-align: center;
            }

            .center-block {
                display: block;
                margin-left: auto;
                margin-right: auto;
                float: none;
		text-align: center;
            }

            /* reset */
            html {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
                -webkit-print-color-adjust: exact;
            }

            body {
                font-family: 'Open Sans', sans-serif;
                font-size: 15px;
                min-width: 350px;
                margin: 0;
            }

            h1, h2, h3, h4, h5, h6,
            ul, li {
                margin: 0;
                padding: 0;
                color: #555555;
            }

            h1 {
                font-size: 36px;
                line-height: 44px;
            }

            h2 {
                font-size: 30px;
                line-height: 36px;
            }

            h3 {
                font-size: 24px;
                line-height: 29px;
            }

            h4 {
                font-size: 21px;
                line-height: 26px;
            }

            h5 {
                font-size: 18px;
                line-height: 22px;
            }

            h6 {
                font-size: 16px;
                line-height: 20px;
            }

            p {
                font-size: 16px;
                line-height: 26px;
                color: #666666;
                margin: 0;
            }

            span {
                font-size: 16px;
                line-height: 26px;
                color: #666666;
                display: block;
                margin-bottom: 30px;
            }

            .btn {
                display: inline-block;
                background: transparent;
                border: none;
                border-radius: 3px;
                outline: 0;
                padding: 11px 30px;
                text-decoration: none;
                -webkit-transition: 200ms ease-in-out;
                -o-transition: 200ms ease-in-out;
                transition: 200ms ease-in-out;
            }

            .rtl {
                direction: rtl;
            }

            .theme-blue .btn-primary {
                background: #e3007f;
                color: #fafafa;
            }

            .theme-violet .btn-primary {
                background: #783392;
                color: #fafafa;
            }

            .theme-red .btn-primary {
                background: #e30047;
                color: #fafafa;
            }

            .theme-sky-blue .btn-primary {
                background: #2ba1c0;
                color: #fafafa;
            }

            .theme-marrs-green .btn-primary {
                background: #0a6f75;
                color: #fafafa;
            }

            .theme-navy-blue .btn-primary {
                background: #31629f;
                color: #fafafa;
            }

            .theme-pink .btn-primary {
                background: #f15497;
                color: #fafafa;
            }

            .theme-black .btn-primary {
                background: #333645;
                color: #fafafa;
            }

            .theme-blue .btn-primary:hover {
                background: #1a86ff;
            }

            .theme-violet .btn-primary:hover {
                background: #9d44be;
            }

            .theme-red .btn-primary:hover {
                background: #ff2167;
            }

            .theme-sky-blue .btn-primary:hover {
                background: #3bb4d3;
            }

            .theme-marrs-green .btn-primary:hover {
                background: #0c858c;
            }

            .theme-navy-blue .btn-primary:hover {
                background: #3b75be;
            }

            .theme-pink .btn-primary:hover {
                background: #f36ca5;
            }

            .theme-black .btn-primary:hover {
                background: #494d62;
            }

            .header {
                padding: 30px 0;
            }

            .theme-blue .header {
                background: #F4B8DA;
            }

            .theme-violet .header {
                background: #783392;
            }

            .theme-red .header {
                background: #e30047;
            }

            .theme-sky-blue .header {
                background: #2ba1c0;
            }

            .theme-marrs-green .header {
                background: #0a6f75;
            }

            .theme-navy-blue .header {
                background: #31629f;
            }

            .theme-pink .header {
                background: #f15497;
            }

            .theme-black .header {
                background: #333645;
            }

            .header h2 a {
                text-decoration: none;
                color: #fafafa;
            }

            .content-wrapper {
                background: #fafafa;
            }

            .content-wrapper .content {
                max-width: 600px;
                margin: auto;
                padding: 40px 0;
            }

            .content h3 {
                margin-bottom: 15px;
            }

            .content .link {
                word-break: break-all;
                -webkit-transition: 200ms ease-in-out;
                -o-transition: 200ms ease-in-out;
                transition: 200ms ease-in-out;
            }

            .theme-blue .content .link {
                color: #0068e1;
            }

            .theme-violet .content .link {
                color: #783392;
            }

            .theme-red .content .link {
                color: #e30047;
            }

            .theme-sky-blue .content .link {
                color: #2ba1c0;
            }

            .theme-marrs-green .content .link {
                color: #0a6f75;
            }

            .theme-navy-blue .content .link {
                color: #31629f;
            }

            .theme-pink .content .link {
                color: #f15497;
            }

            .theme-black .content .link {
                color: #333645;
            }

            .theme-blue .content .link:hover {
                color: #1a86ff;
            }

            .theme-violet .content .link:hover {
                color: #9d44be;
            }

            .theme-red .content .link:hover {
                color: #ff2167;
            }

            .theme-sky-blue .content .link:hover {
                color: #3bb4d3;
            }

            .theme-marrs-green .content .link:hover {
                color: #0c858c;
            }

            .theme-navy-blue .content .link:hover {
                color: #3b75be;
            }

            .theme-pink .content .link:hover {
                color: #f36ca5;
            }

            .theme-black .content .link:hover {
                color: #494d62;
            }

            .content .btn {
                display: table;
                margin: 30px auto;
            }

            .content .content-bottom {
                border-top: 1px solid #e9e9e9;
                margin-top: 24px;
            }

            .content-bottom span {
                font-size: 15px;
                line-height: 24px;
                margin: 5px 0 10px;
            }

            .footer {
                background: #f1f3f7;
                padding: 15px 0;
            }

            .footer span {
                color: #555555;
            }

            .footer span a {
                text-decoration: none;
                -webkit-transition: 200ms ease-in-out;
                -o-transition: 200ms ease-in-out;
                transition: 200ms ease-in-out;
            }

            .theme-blue .footer span a {
                color: #0068e1;
            }

            .theme-violet .footer span a {
                color: #783392;
            }

            .theme-red .footer span a {
                color: #e30047;
            }

            .theme-sky-blue .footer span a {
                color: #2ba1c0;
            }

            .theme-marrs-green .footer span a {
                color: #0a6f75;
            }

            .theme-navy-blue .footer span a {
                color: #31629f;
            }

            .theme-pink .footer span a {
                color: #f15497;
            }

            .theme-black .footer span a {
                color: #333645;
            }

            .footer span a:hover {
                text-decoration: underline;
            }

            .theme-blue .footer span a:hover {
                color: #1a86ff;
            }

            .theme-violet .footer span a:hover {
                color: #9d44be;
            }

            .theme-red .footer span a:hover {
                color: #ff2167;
            }

            .theme-sky-blue .footer span a:hover {
                color: #3bb4d3;
            }

            .theme-marrs-green .footer span a:hover {
                color: #0c858c;
            }

            .theme-navy-blue .footer span a:hover {
                color: #3b75be;
            }

            .theme-pink .footer span a:hover {
                color: #f36ca5;
            }

            .theme-black .footer span a:hover {
                color: #494d62;
            }
        </style>
    </head>

    <body class="{{ setting('storefront_theme', 'theme-blue') }} {{ is_rtl() ? 'rtl' : 'ltr' }}">
        <header class="header">
            <div class="col-md-6 col-sm-7 center-block">
		<a class="logo-aishi" href="https://aishitoto.mangoads.com.vn/en" id="logo"> 
                   <img src="https://aishitoto.mangoads.com.vn/assets/img/logo.png">
            </a>
            </div>
        </header>

        <section class="content-wrapper">
            <div class="col-md-6 col-sm-7 center-block">
                <div class="content">
                    <h3>
                        Your Registration was Successful
			</h3>

                    <span>
			Dear {{$user->first_name}}, welcome to Aconcept.
                    </span>
		<span>
			By registering with us, you will hear about all the latest promotion,<br/> manage your orders better and periodically get discount voucher codes for our campaigns.
                    </span>




                    <span>
			Happy Shopping!<br/>Best Regards,<br/>Aconcept
                    </span>

    </body>
</html>