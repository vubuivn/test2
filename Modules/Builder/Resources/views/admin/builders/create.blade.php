@extends('admin::layout')

@component('admin::components.builder.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('builder::builders.builder')]))

    <li><a href="{{ route('admin.builders.index') }}">{{ trans('builder::builders.builders') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('builder::builders.builder')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.builders.store') }}" class="form-horizontal" id="builder-create-form" onsubmit="meo()" novalidate>
        {{ csrf_field() }}

        {!! $tabs->render(compact('builder')) !!}
    </form>
@endsection

@include('builder::admin.builders.partials.shortcuts')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('modules/common/css/formeo.min.css') }}" />
    <style>
        .formeo-controls .edit-settings,
        .formeo-controls .save-form {
            display: none
        }

        .formeo-controls .save-form.can-save {
            display: inline-block;
        }
        #formeo-editor .label:empty {
            display: block!important;
        }
        #formeo-editor .label {
            color: #1a2226;
            text-align: left;
        }
        ul.accordion-tab li:last-child {
            display: none;
        }
    </style>
@endpush
@push('scripts')
    <script src="{{ v(asset('modules/common/js/formeo.min.js')) }}"></script>

    <script>
        var formeo = new FormeoEditor({
            editorContainer: '#formeo-editor',
            svgSprite: '{{ asset('modules/common/svg/formeo-sprite.svg') }}',
            controls: {
                sortable: !1,
                groupOrder: ["common", "html"],
                disable: {},
                elements: [{
                    tag: "input",
                    config: {
                        label: "Email",
                        disabledAttrs: ["type"],
                        lockedAttrs: ["required", "className"]
                    },
                    meta: {
                        group: "common",
                        id: "email",
                        icon: "@"
                    },
                    attrs: {
                        className: "custom-email",
                        type: "email",
                        required: !0
                    }
                }],
                elementOrder: {
                    common: ["button", "checkbox", "date-input", "hidden", "upload", "number", "radio", "select", "text-input", "textarea"]
                }
            },
            events: {
                onSave: (evt) => {
                    $('#body').val(this.formeo.json);
                }
            },
        });
        var renderer = new FormeoRenderer({
            renderContainer: '.render-form',
        });
        function meo() {
            $('#body').val(formeo.json);
            renderer.render(formeo.formData);
            $('#embedded').val($('.render-form').html());
        }
    </script>

@endpush