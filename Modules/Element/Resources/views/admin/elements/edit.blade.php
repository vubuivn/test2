@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => trans('element::elements.element')]))
    @slot('subtitle', $element->name)

    <li><a href="{{ route('admin.elements.index') }}">{{ trans('element::elements.elements') }}</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => trans('element::elements.element')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.elements.update', $element) }}" class="form-horizontal" id="element-edit-form" novalidate>
        {{ csrf_field() }}
        {{ method_field('put') }}

        {!! $tabs->render(compact('element')) !!}
    </form>
@endsection

@include('element::admin.elements.partials.scripts')
