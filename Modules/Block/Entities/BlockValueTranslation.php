<?php

namespace Modules\Block\Entities;

use Modules\Support\Eloquent\TranslationModel;

class BlockValueTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];
}
