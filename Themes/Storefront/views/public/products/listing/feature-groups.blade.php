<section class="lists-3 sec-tb ">
    @foreach ($featureGroups AS $index => $group)
        <div class="item">
            <div class="row bottom {{ $index % 2 ? 'end' : '' }}">
                <div class="col-lg-6">
                    <a href="{{ route('products.show', ['slug' => $group->slug]) . '?sku=' . getSkuFromGroup($group) }}" class="img tRes_58">
                        <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                            data-lazy-src="{{ groupImageUrl($group->code, 1) }}" alt="" />
                    </a>
                </div>

                <div class="col-lg-6">
                    <div class="divtext ">
                            <?php 
                            $mytime = Carbon\Carbon::now();
                            $new_start = strtotime($group->new_start);
                            $new_start = date('Y-m-d',$new_start);
                            $new_end = strtotime($group->new_end);
                            $new_end = date('Y-m-d',$new_end);
                            ?>
                            
                            <a href="{{ route('products.show', ['slug' => $group->slug]) . '?sku=' . getSkuFromGroup($group) }}" class="img2">
                                @if($group->is_new == 1)
                                @if($mytime >= $new_start && $mytime <= $new_end)   
                                <span class="new"><img  src="/assets/images/new.svg"  alt="" /></span>
                                @endif
                                @endif
                                @if($group->selling_price != $group->price)
                                <span class="sale"><img  src="/assets/images/sale.svg"  alt="" /></span>
                                @endif
                            <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                                data-lazy-src="{{ productImageUrl(getSkuFromGroup($group), 1) }}" alt="" />
                            </a>
             
               
                            <a href="{{ route('products.show', ['slug' => $group->slug]) . '?sku=' . getSkuFromGroup($group) }}" class="h3 title">
                                {{ $group->name }}
                            </a>
          
                        <p class="desc">{!! $group->description !!}</p>

                        <div class="price">
                            @if ($group->lowest_price === $group->highest_price)
                            <span>{{ formatMoney($group->lowest_price) }}</span>
                            @else
                            <span> Từ {{ formatMoney($group->lowest_price) }} -
                                    {{ formatMoney($group->highest_price) }}</span>
                            @endif

                            <!-- wishlist block -->
                            {!! printWishListIcon(getIdFromGroup($group)) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

</section>