<?php

namespace Modules\Term\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Term\Entities\Term;
use Modules\Core\Http\Requests\Request;

class SaveTermRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var array
     */
    protected $availableAttributes = 'term::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => $this->getSlugRules(),
            'name' => 'required',
            'body' => 'required',
            'is_active' => 'required|boolean',
        ];
    }

    private function getSlugRules()
    {
        $rules = $this->route()->getName() === 'admin.terms.update'
            ? ['required']
            : ['sometimes'];

        $slug = Term::withoutGlobalScope('active')->where('id', $this->id)->value('slug');

        $rules[] = Rule::unique('terms', 'slug')->ignore($slug, 'slug');

        return $rules;
    }
}
