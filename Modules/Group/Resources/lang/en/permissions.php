<?php

return [
    'index' => 'Index Group',
    'create' => 'Create Group',
    'edit' => 'Edit Group',
    'destroy' => 'Delete Group',
];
