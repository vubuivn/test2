<?php

return [
    'sku'                 => 'sku',
    'group'               => 'group',
    'selling_price'       => 'selling price',
    'special_price'       => 'special price',
    'special_price_start' => 'special price start',
    'special_price_end'   => 'special price end',
    'long'                => 'long',
    'wide'                => 'wide',
    'high'                => 'high',
    'volume'              => 'volume',
    'status'              => 'status',
];
