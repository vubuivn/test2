<tr>
    @include('admin::partials.table.select_all')

    <th>{{ trans('product::attributes.sku') }}</th>
    <th>{{ trans('product::attributes.group') }}</th>
    <th>{{ trans('product::products.table.price') }}</th>
    <th>{{ trans('admin::admin.table.status') }}</th>
    <th data-sort>{{ trans('admin::admin.table.created') }}</th>
</tr>
