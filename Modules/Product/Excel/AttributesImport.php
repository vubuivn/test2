<?php

namespace Modules\Product\Excel;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

use Modules\Attribute\Entities\Attribute;
use Modules\Attribute\Entities\AttributeValue;
use Modules\Attribute\Entities\AttributeValueTranslation;
use DB;

class AttributesImport
{

    private $attributeFields = [
        'en_title' => 'Title',
        'vi_title' => 'Tiêu đề',
        'code' => 'Code',
        'en_group' => 'Group',
        'vi_group' => 'Nhóm'
    ];
    private $uniqueCodes = [];
    private $attributesToInsert = [];
    private $errorAttributes = [];

    public function __toString()
    {
        return 'attribute inport';
    }

    public function import($filePath)
    {
        $result = $this->preprocessImport($filePath);

        if (!$result['status']) {
            return [
                'status' => false,
                'message' => 'Thiếu cột: ' . implode(', ', $result['missingColumns'])
            ];
        }

        $this->doImport();
        
        return [
            'status' => true,
            'attributeCreatedCount' => count($this->attributesToInsert),
            'errorAttributes' => $this->errorAttributes
        ];
    }

    private function doImport()
    {
        try {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('product_attribute_values')->truncate();
            DB::table('product_attributes')->truncate();
            DB::table('attribute_value_translations')->truncate();
            DB::table('attribute_values')->truncate();
            DB::table('attribute_categories')->truncate();
            DB::table('attribute_translations')->truncate();
            DB::table('attributes')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            foreach ($this->attributesToInsert AS $sheetName => $attributes) {
                $attribute = Attribute::create([
                                'attribute_set_id' => 4,
                                'name' => $sheetName,
                            ]);

                foreach ($attributes as $item) {
                    $value               = new AttributeValue();
                    $value->attribute_id = $attribute->id;
                    $value->code         = $item['code'];
                    // $value->other        = $item['other'];
                    $value->save();

                    $value_vi                     = new AttributeValueTranslation();
                    $value_vi->attribute_value_id = $value->id;
                    $value_vi->locale             = 'vi_VN';
                    $value_vi->value              = $item['vi_title'];
                    $value_vi->group              = $item['vi_group'];
                    $value_vi->save();

                    $value_en                     = new AttributeValueTranslation();
                    $value_en->attribute_value_id = $value->id;
                    $value_en->locale             = 'en';
                    $value_en->value              = $item['en_title'];
                    $value_en->group              = $item['en_group'];
                    $value_en->save();
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }
    }

    private function preprocessImport($filePath)
    {
        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open($filePath);
        $reader->setShouldPreserveEmptyRows(true);
        
        foreach ($reader->getSheetIterator() as $sheet) {
            // reset uniqueCode for each sheet
            $this->uniqueCodes = [];
            $headingKeys = [];
            $sheetName = $sheet->getName();
            
            foreach ($sheet->getRowIterator() as $rowNumber => $row) {
                $cells = $row->getCells();
                if ($rowNumber === 1) {
                    $heading = array_map(function($cell) {
                        return strtolower($cell->getValue());
                    }, $cells);

                    // do validation for header
                    $result = $this->validateHeading($heading);

                    if (!empty($result['missingColumns'])) {
                        return [
                            'status' => false,
                            'missingColumns' => $result['missingColumns']
                        ];
                    }
                    $headingKeys = $result['headingKeys'];
                    continue;
                }

                $formattedRow = $this->generateFormattedRow($headingKeys, $cells);

                $result = $this->validateRow($formattedRow);
                if (!$result['status']) {
                    $formattedRow['errorMessages'] = $result['messages'];
                    $this->errorAttributes[$sheetName][] = $formattedRow;
                    continue;
                }

                $this->attributesToInsert[$sheetName][] = $formattedRow;
            }
        }

        return [
            'status' => true
        ];
    }

    // ok
    private function generateFormattedRow($headingKeys, $cells)
    {
        foreach ($headingKeys AS $index => $key) {
            $cellValue = isset($cells[$index]) ? trim($cells[$index]->getValue()) : null;

            if ($key === 'code' && $cellValue) {
                $cellValue = preg_replace('/[^A-Za-z0-9_\-]/m', '', $cellValue);
            }

            $formattedRow[$key] = $cellValue;
        }

        return $formattedRow;
    }

    // ok
    private function validateHeading($heading)
    {
        $headingKeys = [];
        $missingColumns = [];

        $validHeadingFields = $this->attributeFields;

        foreach ($validHeadingFields AS $key => $name) {
            $index = array_search(strtolower($name), $heading);

            if ($index === false) {
                $missingColumns[] = $name;
            } else {
                unset($heading[$index]);
                $headingKeys[$index] = $key;
            }
        }

        return [
            'missingColumns' => $missingColumns,
            'headingKeys' => $headingKeys
        ];
    }

    // ok
    private function validateRow(&$row)
    {
        $requiredFields = ['code', 'en_title', 'vi_title'];

        $messages = [];
        foreach ($requiredFields AS $field) {
            if (!strlen($row[$field])) {
                $fieldName = $this->attributeFields[$field];
                $messages[] = 'Trường: ' . $fieldName . ' không thể rỗng';
            }
        }

        if (empty($messages)) {
            // check duplicate sku
            if (isset($this->uniqueCodes[strtolower($row['code'])])) {
                $messages[] = 'Trùng lấp ' . $this->attributeFields['code'] . ': ' . $row['code'];
            } else {
                $this->uniqueCodes[strtolower($row['code'])] = 1;
            }
        }

        if (!empty($messages)) {
            return [
                'status' => false,
                'messages' => $messages
            ];
        }

        return [
            'status' => true
        ];
    }

}