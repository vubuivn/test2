<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Page\Entities\Page;
use Illuminate\Support\Facades\Cache;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Cache::tags(['pages'])->has('home_page')){
            $page = Cache::tags(['pages'])->get('home_page');
        }else{
            $page = Cache::tags(['pages'])
              ->rememberForever('home_page',function (){
                  return Page::with(['blocks'])->where('slug', 'home')->first();
            });
        }
        // $page = Page::with(['blocks'])->where('slug', $slug)->first();

        if (!$page) {
            echo 'Please create a "Home" page with the slug "home" first';
            die;
        }

        return view('public.home.index', compact('page'));
    }
}
