<?php

return [
    'index' => 'Index Category News',
    'create' => 'Create Category News',
    'edit' => 'Edit Category News',
    'destroy' => 'Delete Category News',
];
