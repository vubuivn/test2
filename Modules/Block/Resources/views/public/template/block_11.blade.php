<section class="lists-1 sec-b sec-3 group-ef lazy-hidden">
    <div class="container">
        <div class="row grid-space-20">
            @foreach ($collections AS $index => $image)
                <div class="col-sm-6 col-md-6 col-lg-3  efch-{{ $index + 1 }} ef-img-t">
                    <a href="{{ $image->call_to_action_url }}" class="item">
                        <div class="img  tRes_150">
                            <img class="lazy-hidden" data-lazy-type="image" src="/assets/images/image.svg"
                                data-lazy-src="{{ $image->file }}"
								alt="{{ $image->caption_1 }}" />

                            <div class="divtext ">
                                <h3 class="title">{{ $image->caption_1 }}</h3>
                                <div class="more">
                                    <span class="readmore">{{ $image->call_to_action_text }}
                                        <i class="icon-arrow-14"></i>
                                    </span>
                                </div>
                            </div>

                            <h3 class="title2"><span>{{ $image->caption_1 }}</span></h3>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>