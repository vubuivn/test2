<?php

namespace Modules\Element\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;

class ElementTabs extends Tabs
{
    public function make()
    {
        $this->group('element_information', trans('element::elements.tabs.group.element_information'))
            ->active()
            ->add($this->general());
            //->add($this->values());
    }

    private function general()
    {
        return tap(new Tab('general', trans('element::elements.tabs.general')), function (Tab $tab) {
            $tab->active();
            $tab->weight(10);
            $tab->fields(['name', 'type', 'is_required']);
            $tab->view('element::admin.elements.tabs.general');
        });
    }

//     private function values()
//     {
//         return tap(new Tab('values', trans('element::elements.tabs.values')), function (Tab $tab) {
//             $tab->weight(20);
//             $tab->fields(['values.*.label', 'values.*.price', 'values.*.price_type']);
//             $tab->view('element::admin.elements.tabs.values');
//         });
//     }
}
