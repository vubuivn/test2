<div id="billing-address">
    <div class="row">
<div class="col-lg-6">
    <div class="rowblock {{ $errors->has('billing.first_name') ? 'has-error': '' }}">
        <label for="firstname">
            {{ trans('storefront::checkout.tabs.attributes.billing.last_name') }}
            <span>*</span>
        </label>
        <input type="text" class="form-control input"  name="billing[last_name]"
            placeholder="{{ trans('storefront::checkout.tabs.attributes.billing.last_name') }}"
            value="{{ isset($my) ? $my->last_name : old('billing.last_name') }}">
        {!! $errors->first('billing.last_name', '<span class="error-message">:message</span>') !!}
    </div>
</div>

<div class="col-lg-6">
    <div class="rowblock">
        <label for="lastname">
            {{ trans('storefront::checkout.tabs.attributes.billing.first_name') }}
            <span>*</span>
        </label>
        <input type="text" class="form-control input" name="billing[first_name]"
            placeholder="{{ trans('storefront::checkout.tabs.attributes.billing.first_name') }}"
            value="{{ isset($my) ? $my->first_name : old('billing.first_name') }}">
        {!! $errors->first('billing.first_name','<span class="error-message">:message</span>') !!}
    </div>
</div>
   
<div class="col-lg-6">
    <div class="rowblock">
        <label for="billing-state">
            {{ trans('storefront::checkout.tabs.attributes.billing.state') }}
            <span>*</span>
        </label>
       <select name="billing[state]" class="custom-select-black " id="billing-state">
               <option value="">{{ trans("storefront::checkout.please_select") }}</option>
               @foreach ($states AS $id => $state)
                <option value="{{ $state }}" data-id="{{ $id }}" {{ (isset($my) && $my->city_id == $id) ? 'selected' : $state == old('billing.state') ? 'selected' : '' }}
                >{{ $state }}</option>
               @endforeach
       </select>
       {!! $errors->first('billing.state','<span class="error-message">:message</span>') !!}
    </div>
</div>
   
<div class="col-lg-6">
    <div class="rowblock">
        <label for="billing-city">
            {{ trans('storefront::checkout.tabs.attributes.billing.city') }}
            <span>*</span>
        </label>
       <select name="billing[city]" class="custom-select-black " id="billing-city">
               <option value="">{{ trans("storefront::checkout.please_select") }}</option>
       </select>
       {!! $errors->first('billing.city','<span class="error-message">:message</span>') !!}
    </div>
</div>
   
<div class="col-lg-12">
    <div class="rowblock">
        <label for="address">
               {{ trans('storefront::checkout.tabs.attributes.billing.address_1') }}
               <span>*</span>
        </label>
        <input type="text" class="form-control input" name="billing[address_1]"
            placeholder="{{ trans('storefront::checkout.tabs.attributes.billing.address_1') }}"
            value="{{ old('billing.address_1') ? old('billing.address_1') : isset($my) ? $my->address : '' }}">
        {!! $errors->first('billing.address_1','<span class="error-message">:message</span>') !!}
    </div>
</div>

<div class="col-lg-12">
    <div class="rowblock">
        <label for="phone">
           {{ trans('storefront::checkout.tabs.attributes.customer_email') }}
           <span>*</span>
        </label>
        <input type="email" class="form-control input" name="customer_email"
            placeholder="{{ trans('storefront::checkout.tabs.attributes.customer_email') }}"
            value="{{ old('customer_email') ? old('customer_email') : isset($my) ? $my->email : '' }}">
        {!! $errors->first('customer_email','<span class="error-message">:message</span>') !!}
    </div>
</div>
   
<div class="col-lg-12">
    <div class="rowblock">
        <label for="phone">
            {{ trans('storefront::checkout.tabs.attributes.customer_phone') }}
            <span>*</span>
        </label>
        <input type="number" class="form-control input"  name="customer_phone"
            placeholder="{{ trans('storefront::checkout.tabs.attributes.customer_phone') }}"
            value="{{ old('customer_phone') ? old('customer_phone') : isset($my) ? $my->phone : '' }}" min="10" max="11">
        {!! $errors->first('customer_phone','<span class="error-message">:message</span>') !!}
    </div>
</div>

</div>
</div>