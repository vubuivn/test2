@extends('public.layout')

@section('title', trans('storefront::account.links.my_wishlist'))

@section('content')
    @include('public.partials.bannertop',['banner'=>'/assets/images/faq.jpg','title' => trans('storefront::account.wishlist.my_wishlist')])
    <div class="container page-wishlist">
        @if ($products->isEmpty())
            <h3 class="text-center">{{ trans('storefront::account.wishlist.empty_wishlist') }}</h3>
        @else
        <section class="lists-4 sec-tb ">
        <div class="row">
            
            @foreach ($products as $group)
                <?php 
                $idsku = $group->idSku;
                $arr = explode('__',$idsku);
                ?>
                <div class="col-md-4 col-sm-6">
                <div class="item">
                    <a class="remove_wishlist" data-id="{{$arr[0]}}"><i class="icon-close"></i></a>
                    <?php 
                    if(request()->has('attribute')){
                        $sku = getSkuFromGroup2($group);
                    }else{
                        $sku = getSkuFromGroup($group);
                    }
                    ?>
                    <a href="{{ route('products.show', ['slug' => $group->slug]) . '?sku=' . $sku }}">
                        <div class="img">
                            <?php 
                            $mytime = Carbon\Carbon::now();
                            $new_start = strtotime($group->new_start);
                            $new_start = date('Y-m-d',$new_start);
                            $new_end = strtotime($group->new_end);
                            $new_end = date('Y-m-d',$new_end);
                            ?>
                            @if($group->is_new == 1)
                                @if($mytime >= $new_start && $mytime <= $new_end)                  
                                    <span class="new"><img src="/assets/images/new.svg"  alt="" /></span> 
                                @endif
                            @endif
                            @if($group->selling_price != $group->price)
                            <span class="sale"><img src="/assets/images/sale.svg"  alt="" /></span>
                            @endif
                            <img class="lazy-hidden " data-lazy-type="image" src="/assets/images/image.svg"
                                data-lazy-src="{{ productImageUrl($sku, 1) }}" alt="" />
                        </div>
                        <div class="divtext ">
                            <div class="wtitle">
                                <h3 class="title"> {{ $group->name }}</h3>
                            </div>
                            <div class="price">
                                <span> @if($group->lowest_price != $group->highest_price){{ trans('group::group-categories.from') }}@endif 
                                    {{ formatMoney($group->lowest_price) }} @if($group->lowest_price != $group->highest_price)-
                                        {{ formatMoney($group->highest_price) }}@endif</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach

        </div>
        </section>
        @endif
    </div>

    @if ($products->isNotEmpty())
        <div class="pull-right">
            {!! $products->links() !!}
        </div>
    @endif
@endsection

@push('styles')
    <style>
        .remove_wishlist{position: absolute;top: 0;right: 0;cursor: pointer;}
        .page-wishlist{padding: 100px 0;}
        @media screen and (max-width: 1024px){
            .page-wishlist{padding: 70px 0;}
        }
        @media screen and (max-width: 767px){
            .page-wishlist{padding: 50px 0;}
        }
    </style>
@endpush

@push('scripts')
<script type="text/javascript">
    (function($){
        $('.remove_wishlist').on('click',function(){
            var id = $(this).data('id');
            var check = false;
            if(check != true){
                check = true;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: "{{route('wishlist.remove-wishlist')}}",
                    data: { id: id },
                    beforeSend: function() {
                        $('#loading_gif').show();
                    },
                    success: function(data){
                        window.location.reload();
                    },
                    complete: function(){
                        check = false;
                        $('#loading_gif').hide();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    })(jQuery)
</script>
@endpush