<?php

// Route::post('wishlist', 'WishlistController@store')->middleware('auth')->name('wishlist.store');
Route::get('wishlist/toggle-wishlist', 'WishlistController@toggleWishlist')->name('wishlist.toggle-wishlist');

Route::post('wishlist/remove-wishlist', 'WishlistController@removeWishlist')->name('wishlist.remove-wishlist');
