<?php 
    function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
?>
<html>
<body>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Tên</th>
        <th>Name</th>
    </tr>
    </thead>
    <tbody>
    @foreach($category as $item)
        <tr>
            <td>{{ $item->id }}</td>
            @php
                $en = $item->translations()->where('locale', 'en')->first();
                $vi = $item->translations()->where('locale', 'vi_VN')->first();
            @endphp
            <td>{{ $vi ? $vi->name : '' }}</td>
            <td>{{ $en ? $en->name : '' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>