@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('element::elements.element')]))

    <li><a href="{{ route('admin.elements.index') }}">{{ trans('element::elements.elements') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('element::elements.element')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.elements.store') }}" class="form-horizontal" id="element-create-form" novalidate>
        {{ csrf_field() }}
        <input type="hidden" name="builder_id" value="{{ $builder }}">

        {!! $tabs->render(compact('element')) !!}
    </form>
@endsection

@include('element::admin.elements.partials.scripts')
