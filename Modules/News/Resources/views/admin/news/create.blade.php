@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('news::news.news')]))

    <li><a href="{{ route('admin.news.index') }}">{{ trans('news::news.news') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('news::news.news')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.news.store') }}" class="form-horizontal" id="news-form" novalidate>
        {{ csrf_field() }}
        {!! $tabs->render(compact('news')) !!}
    </form>
@endsection

@include('news::admin.news.partials.shortcuts')

@push('scripts')
    <script>
        (function($){

            $('.image_picker').on('click', function (e) {
                e.preventDefault();
                let picker = new MediaPicker({type: 'image'});

                picker.on('select', (file) => {
                    $(this).find('i').remove();
                    // $(this).find('img').attr('src', file.path).removeClass('hide');
                    // $(this).find('.banner-file-id').val(file.id);
                    tinymce.get("description").execCommand('mceInsertContent', false, '<img src="'+file.path+'">');
                });
            });

        })(jQuery)
    </script>
@endpush