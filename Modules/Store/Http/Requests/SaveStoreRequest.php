<?php

namespace Modules\Store\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Store\Entities\Store;
use Modules\Core\Http\Requests\Request;

class SaveStoreRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var array
     */
    protected $availableAttributes = 'store::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => $this->getSlugRules(),
            'name' => 'required',
            'is_active' => 'required|boolean',
            'address' => 'required',
            'phone' => 'required',
            'loc_lat' =>'required',
            'loc_long' => 'required',
            'district_id' => 'required',
        ];
    }

    private function getSlugRules()
    {
        $rules = $this->route()->getName() === 'admin.stores.update'
            ? ['required']
            : ['sometimes'];

        $slug = Store::withoutGlobalScope('active')->where('id', $this->id)->value('slug');

        $rules[] = Rule::unique('stores', 'slug')->ignore($slug, 'slug');

        return $rules;
    }
}
