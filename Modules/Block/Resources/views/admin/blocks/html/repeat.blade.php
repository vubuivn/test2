<div class="form-group ">
    <label class="col-md-2 control-label text-left">
        {{$block->title}}
    </label>
    <div class="col-md-10">
	   	<?php 
	   		$datas  = $data->{$block->key} ?? null ;
	   	?>
	   	<a class="btn btn-default add_repeat" data-key="{{$block->key}}" data-number="{{$number}}">add repeat</a>
	   	<div class="list-repeat">
	   		@if($datas)
	   		<?php $i = 1?>
	   		@foreach($datas as $data)
	   			<div class="row group-outner" data-group="{{$i}}" style="margin-bottom:20px">
            		<div class="group-content">
	            		<div class="col-md-11">
		                    <input type="text" style="margin-bottom: 15px" class="form-control" name="blocks[{{$number}}][{{$block->key}}][{{$i}}][group][title]" value="{{$data->group->title}}">
		                    <textarea class="wysiwyg" name="blocks[{{$number}}][{{$block->key}}][{{$i}}][group][content]">{{$data->group->content}}</textarea>
		                </div>
		                <div class="col-md-1">
		        			<a class="btn btn-default remove_repeat" data-key="[]" data-number="[]"><i class="fa fa-trash"></i></a>
		        		</div>
	                </div>
                </div>
                <?php $i++?>
	   		@endforeach
	   		@endif
	   	</div>
    </div>
</div>
