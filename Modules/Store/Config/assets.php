<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Define which assets will be available through the asset manager
    |--------------------------------------------------------------------------
    | These assets are registered on the asset manager
    */
    'all_assets'      => [
        'admin.store.js' => ['module' => 'store:admin/js/store.js'],
    ],

    /*
    |--------------------------------------------------------------------------
    | Define which default assets will always be included in your stores
    | through the asset pipeline
    |--------------------------------------------------------------------------
    */
    'required_assets' => [],
];
