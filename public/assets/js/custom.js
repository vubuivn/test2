(function($) {
    $(document).on('click','.a_wishlist_cls, #detail_wishlist', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var totalWishlist = $('#clicksCount').html();
        var productId = $(this).data('id');
        var _this = $(this);
        var type = 'list';
        if($(this).attr('id') == 'detail_wishlist'){
            type = 'detail';
        }
        $.ajax({
            url: SITE_URL + '/wishlist/toggle-wishlist',
            method: 'GET',
            type: 'json',
            data: {
                product_id: productId,type:type
            },
            beforeSend: function(){
                $('#loading_gif').show();
                setTimeout(function() { $('#loading_gif').hide(); }, 1000);
            },
            success: function(res) {
                console.log(res.html);
                if (_this.data('type') == 'detail') {
                    _this.toggleClass('btn3');
                    _this.html(res.html);
                } else {

                    if(!_this.children('span').hasClass('active')){
                        var total = parseInt(totalWishlist)+1;
                    }else{
                        var total = parseInt(totalWishlist)-1;
                    }
                    _this.html(res.html);

                }
                // $('#loading_gif').hide();
                
                $('#clicksCount').html(total);
            },
            error: function(xhr, status, error) {
                if (xhr.status === 401) {
                    // show login popup
                    $('#myLogin').modal('show');
                } else {
                    alert(status);
                }
            }
        });
    });


})(jQuery);