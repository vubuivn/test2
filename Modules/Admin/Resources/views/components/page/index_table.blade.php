@section('content')
    <div class="row">
        <div class="btn-group pull-right">
            @if (isset($buttons_js))
                @foreach($buttons_js as $view)
                    <button id="btn_{{$view}}_id"
                            class="btn btn-primary btn-actions">
                        {{ trans("admin::resource.{$view}") }}
                    </button>
                @endforeach
            @endif
            @if (isset($buttons, $name))
                @foreach($buttons as $view)
                    <a href="{{ route("admin.{$resource}.{$view}") }}"
                       class="btn btn-primary btn-actions btn-{{ $view }}">
                        {{ trans("admin::resource.{$view}", ['resource' => $name]) }}
                    </a>
                @endforeach
            @else
                {{ $buttons ?? '' }}
            @endif
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-body index-table" id="{{ isset($resource) ? "{$resource}-table" : '' }}">
            @if (isset($search))
                <div class="row">
                    @foreach($search as $item)
                        @if($item === 'category')
                            @php($category = \Modules\Category\Entities\Category::treeList())
                        @elseif($item === 'category_news')
                            @php($category = \Modules\CategoryNews\Entities\CategoryNews::treeList())
                        @endif

                        <div class="col-md-3">
                            <select id="search_{{$item}}" class="form-control">
                                <option value="">{{trans('admin::admin.all')}}</option>
                                @foreach($category as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    @endforeach

                    <div class="col-md-3">
                        <button id="btn_search"
                                class="btn btn-primary">
                            {{ trans('storefront::layout.search') }}
                        </button>
                    </div>
                </div>
            @endif
            @if (isset($thead))
                @include('admin::components.table')
            @else
                {{ $slot }}
            @endif
        </div>
    </div>
@endsection

@isset($name)
    @push('shortcuts')
        @if (isset($buttons) && in_array('create', $buttons))
            <dl class="dl-horizontal">
                <dt><code>c</code></dt>
                <dd>{{ trans('admin::resource.create', ['resource' => $name]) }}</dd>
            </dl>
        @endif

        <dl class="dl-horizontal">
            <dt><code>Del</code></dt>
            <dd>{{ trans('admin::resource.delete', ['resource' => $name]) }}</dd>
        </dl>
    @endpush

    @push('scripts')
        <script>
            @if (isset($buttons) && in_array('create', $buttons))
            keypressAction([
                {key: 'c', route: '{{ route("admin.{$resource}.create") }}'}
            ]);
            @endif

            Mousetrap.bind('del', function () {
                $('{{ $selector ?? '' }} .btn-delete').trigger('click');
            });

            @isset($resource)
            DataTable.setRoutes('#{{ $resource }}-table .table', {
                index: '{{ "admin.{$resource}.index" }}',
                edit: '{{ "admin.{$resource}.edit" }}',
                destroy: '{{ "admin.{$resource}.destroy" }}',
            });
            @if (isset($search))
            $("#btn_search").click(function (e) {
                @foreach($search as $item)
                    FleetCart.dataTable.search['{{$item}}'] = $('#search_{{$item}}').val();
                @endforeach
                DataTable.reload('#{{ $resource }}-table .table', null, true);
            });
            @endif
            @endisset
        </script>
    @endpush
@endisset
