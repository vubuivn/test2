<?php

namespace Modules\Builder\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Builder\Entities\Builder;
use Modules\Core\Http\Requests\Request;

class SaveBuilderRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var array
     */
    protected $availableAttributes = 'builder::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => $this->getSlugRules(),
            'name' => 'required',
            'body' => 'required',
            'is_active' => 'required|boolean',
        ];
    }

    private function getSlugRules()
    {
        $rules = $this->route()->getName() === 'admin.builders.update'
            ? ['required']
            : ['sometimes'];

        $slug = Builder::withoutGlobalScope('active')->where('id', $this->id)->value('slug');

        $rules[] = Rule::unique('builders', 'slug')->ignore($slug, 'slug');

        return $rules;
    }
}
