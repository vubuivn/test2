<?php

namespace Modules\News\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Media\Eloquent\HasMedia;
use Modules\Meta\Eloquent\HasMetaData;
use Modules\News\Admin\NewsTable;
use Modules\Support\Eloquent\Model;
use Modules\Support\Eloquent\Sluggable;
use Modules\Support\Eloquent\Translatable;
use Modules\Support\Search\Searchable;
use Modules\Media\Entities\File;

class News extends Model
{
    use Translatable, Searchable, Sluggable, HasMedia, HasMetaData, SoftDeletes;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'is_active',
        'category_news_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'description',
        'short_description',
    ];
    /**
     * The attribute that will be slugged.
     *
     * @var string
     */
    protected $slugAttribute = 'name';

    /**
     * Get table data for the resource
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function table ($request)
    {
        $query = $this->newQuery()
            ->withoutGlobalScope('active')
            ->addSelect(
                [
                    'id',
                    'is_active',
                    'created_at'
                ]
            )
            ->when(
                $request->has('except'),
                function ($query) use ($request) {
                    $query->whereNotIn(
                        'id',
                        explode(
                            ',',
                            $request->except
                        )
                    );
                }
            )
            ->when(
                $request->get('category_news') != null,
                function ($query) use ($request) {
                    $query->where(
                        'category_news_id',
                        $request->get('category_news')
                    );
                }
            );

        return new NewsTable($query);
    }

    public function scopeWithBaseImage ($query)
    {
        $query->with(
            [
                'files' => function ($q) {
                    $q->wherePivot(
                        'zone',
                        'base_image'
                    );
                }
            ]
        );
    }

    /**
     * Get the product's base image.
     *
     * @return \Modules\Media\Entities\File
     */
    public function getBaseImageAttribute ()
    {
        return $this->files->where(
            'pivot.zone',
            'base_image'
        )
            ->first() ?: new File;
    }

    public function saveBlocks($blocks = [])
    {
        $ids = $this->getDeleteCandidates($blocks);

        if ($ids->isNotEmpty()) {
            $this->blocks()->whereIn('id', $ids)->delete();
        }

        foreach (array_reset_index($blocks) as $index => $value) {
            $this->blocks()->updateOrCreate(
                ['id' => $value['id'] ?? null],
                $value + ['position' => $index]
            );
        }
    }
    private function getDeleteCandidates($values = [])
    {
        return $this->blocks()
            ->pluck('id')
            ->diff(array_pluck($values, 'id'));
    }
    public function blocks(){
        return $this->hasMany(NewsBlock::class)->orderBy('position');
    }
}
