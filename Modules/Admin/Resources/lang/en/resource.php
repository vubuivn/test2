<?php

return [
    'create' => 'Create :resource',
    'show' => 'Show :resource',
    'edit' => 'Edit :resource',
    'delete' => 'Delete :resource',
    'import_excel' => 'Import Excel',
    'export_excel' => 'Export Excel',
    'import_promotion' => 'Import Promotion',
    'list_product'	=> 'Child products'
];
