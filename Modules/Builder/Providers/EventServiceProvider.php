<?php

namespace Modules\Builder\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \Modules\Builder\Events\EventReplyMail::class => [
            \Modules\Builder\Listeners\SendMailReply::class
        ]
    ];
}
