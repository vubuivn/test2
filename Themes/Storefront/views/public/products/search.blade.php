@extends('public.layout')

@section('title')
    @if (request()->has('query'))
        {{ trans('storefront::products.search_results_for') }}: "{{ request('query') }}"
    @else
        {{ trans('storefront::products.shop') }}
    @endif
@endsection

@section('content')
<!--     <section id="bannertop" class="lazy-hidden tRes_48 subbanner" data-lazy-type="bg"
        data-lazy-src="{{-- $category->coverImage->path --}}" style="background-image: url(//via.placeholder.com/50x50);">
        <div class="divtext"><h1>{{-- $category->name --}}</h1></div>
    </section> -->



    <main id="main">
        <div class="container">
            @include('public.products.listing.group-list-search')
        </div>
    </main>
@endsection