<?php

namespace Modules\Filter\Eloquent;

use Modules\Filter\Entities\Filter;

trait HasFilter
{
    /**
     * The "booting" method of the trait.
     *
     * @return void
     */
    public static function bootHasFilter()
    {
        static::saved(function ($entity) {
            $entity->saveFilter(request('filter', []));
        });
    }

    /**
     * Get the meta for the entity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function filter()
    {
        return $this->morphOne(Filter::class, 'entity')->withDefault();
    }

    /**
     * Save meta data for the entity.
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function saveFilter($data = [])
    {
        $data['entity_filter_value'] = json_encode($data['entity_filter_value']);
        $this->filter->fill($data)->save();
    }
}
