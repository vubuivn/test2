<?php

namespace Modules\Element\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Element\Entities\Element;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Element\Http\Requests\SaveElementRequest;

use Illuminate\Http\Request;
use Modules\Admin\Ui\Facades\TabManager;
use Modules\Support\Search\Builder;

class ElementController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Element::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'element::elements.element';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'element::admin.elements';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveElementRequest::class;

    public function index(Request $request, $builder=null)
    {
        $elements = $terms = Element::where('builder_id', $builder)->get();

        return view("{$this->viewPath}.index", ['builder' => $builder, 'elements' => $elements]);
    }

    public function create($builder)
    {
        $data = array_merge([
            'tabs' => TabManager::get($this->getModel()->getTable()),
            $this->getResourceName() => $this->getModel(),
            'builder' => $builder,
        ], $this->getFormData('create'));

        return view("{$this->viewPath}.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $entity = $this->getModel()->create(
            $this->getRequest('store')->all()
        );

        return redirect()->route("admin.builders.edit", ['id' => $entity->builder_id])
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $entity = $this->getEntity($id);

        $this->disableSearchSyncing();

        $entity->update(
            $this->getRequest('update')->all()
        );

        $this->searchable($entity);

        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo($entity)
                ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
        }

        return redirect()->route("admin.builders.edit", ['id' => $entity->builder_id])
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
    }
}
