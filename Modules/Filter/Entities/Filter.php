<?php

namespace Modules\Filter\Entities;

use Modules\Support\Eloquent\Model;
use Modules\Support\Eloquent\Translatable;

class Filter extends Model
{
    protected $fillable = [
        'entity_id',
        'entity_type',
        'entity_filter',
        'entity_filter_value',
        'is_filter',
    ];

}
