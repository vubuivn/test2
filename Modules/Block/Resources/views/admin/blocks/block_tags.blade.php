@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('block::blocks.blocks'))

    <li class="active">{{ trans('block::blocks.blocks') }}</li>
@endcomponent

@component('admin::components.page.index_table')

	<div class="btn-group pull-right">
		<a href="{{route('admin.blocks.create_block_tags')}}" class="btn btn-primary btn-actions btn-create">{{trans('block::blocks.create_tags')}}</a>
	</div>
    <div class="clearfix"></div>
	<div class="col s12">
        <table class="table table-striped table-hover dataTable no-footer" cellspacing="0">
            <thead class="cyan white-text">
                <th class="no-sort">
                    <div class="checkbox">
                        <input type="checkbox" class="select-all" id="-select-all">
                        <label for="-select-all"></label>
                    </div>
                </th>
                <th>@lang('admin::blocks.tag_id')</th>
                <th>@lang('admin::blocks.tag_name')</th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@endcomponent

@push('scripts')
    <script>
        // new DataTable('#blocks-table .table', {
        //     columns: [
        //         {data: 'checkbox', orderable: false, searchable: false, width: '3%'},
        //         {data: 'name', name: 'translations.title', orderable: false, defaultContent: ''},
        //         {data: 'status', name: 'is_active', searchable: false},
        //         {data: 'created', name: 'created_at'},
        //     ],
        // });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: "{{ route('admin.blocks.block_tags') }}",
                    data: function(d) {
                    }
                },
                "columnDefs": [
                    { orderable: false, targets: 'no-sort' },
                    { className: 'select-checkbox', targets: 0 }
                ],
            });
            $('.category-filter').change(function(event) {
                table.ajax.reload();
            });

        } );
    </script>
@endpush
