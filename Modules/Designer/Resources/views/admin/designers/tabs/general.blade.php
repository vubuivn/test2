{{ Form::text('name', trans('designer::attributes.name'), $errors, $designer, ['labelCol' => 2, 'required' => true]) }}
<div class="form-group ">
    <div class="col-md-8">
        {{ Form::checkbox('is_active', trans('designer::attributes.is_active'), trans('designer::designers.form.enable_the_designer'), $errors, $designer) }}
    </div>
</div>

{{ Form::textarea('short_description', trans('designer::attributes.short_description'), $errors, $designer, ['labelCol' => 2]) }}

{{ Form::wysiwyg('body', trans('designer::attributes.description'), $errors, $designer, ['labelCol' => 2]) }}

@include('media::admin.image_picker.single', [
    'title' => trans('designer::designers.form.banner_image'),
    'inputName' => 'files[base_image]',
    'file' => $designer->baseImage,
])
@include('media::admin.image_picker.single', [
    'title' => trans('designer::designers.form.feature'),
    'inputName' => 'files[feature_image]',
    'file' => $designer->featureImage,
])

@include('media::admin.image_picker.multiple', [
    'title' => trans('designer::designers.form.additional_images'),
    'inputName' => 'files[additional_images][]',
    'files' => $designer->additionalImages,
])

@push('globals')
    <script>
        FleetCart.langs['designer::designers.expand_all'] = '{{trans('designer::designers.expand_all')}}';
        FleetCart.langs['designer::designers.collapse_all'] = '{{trans('designer::designers.collapse_all')}}';
    </script>
@endpush
