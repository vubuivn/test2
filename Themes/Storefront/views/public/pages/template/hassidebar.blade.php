<div class="col-md-3 col-sm-4 sidebar-faqs">
    <h3><img src="/assets/images/svg/question-2.svg"   alt=""> {{trans('storefront::storefront.page.questions')}}</h3>
    <ul class="menu">
        @php($current = request()->segment(count(request()->segments())))
        @if(count($pages))
            @foreach($pages as $p)
                <li  class="@if($p->slug == $current) active @endif"><a href="{{route('page.detail',$p->slug)}}">{{$p->name}}</a></li>
            @endforeach
        @endif
    </ul>
</div>
<div class="col-md-9 col-sm-8">
    <div class="acordion-faqs box-acordion">
        @foreach($page->blocks as $block)
            @php($content = show_block_page($block))
                @includeIf('block::public.template.block_'.$block->block_id, $content)
        @endforeach
    </div>
</div>


@push('scripts')
<script type="text/javascript">
    (function($){
        $('.box-acordion > .tab > .tab-title').each(function(){
            $(this).click(function(){ 
                var parent = $(this).parent(),
                    acordion = $(this).parent().parent(),
                    tab = acordion.children('.tab');
                if (parent.hasClass('show')) {
                    $(this).next().slideToggle(300);
                    parent.removeClass('show'); 
                } else {
                    //hide
                    acordion.children('.tab').children('.tab-content').slideUp(300);
                    tab.removeClass('show');
                    //show
                    $(this).next().slideDown(300);
                    parent.addClass('show');
                };
            });
        });  
    })(jQuery);

</script>
@endpush