<?php

namespace Modules\Builder\Http\Controllers;

use Modules\Builder\Entities\Builder;
use Modules\Media\Entities\File;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Term\Entities\Term;
use Illuminate\Support\Facades\Storage;
use Modules\Builder\Events\EventReplyMail;
use Mail;

class BuilderController extends Controller
{
    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'builder::public.builders';

    public function form_validate(Request $request){
        return view("{$this->viewPath}.form_validate");
    }

    public function embedded(Builder $builder, Request $request){
        $validator = \Validator::make($request->all(), $this->termValidate($builder));
        if ($validator->fails()) {
            return redirect()->route('public.builders.embedded-form',['builder' => $builder])
                ->withErrors($validator)
                ->withInput();
        }
        # Verify ReCaptcha
        if($builder->secret_key != '' && $builder->secret_key != null){
            $recaptcha = new \ReCaptcha\ReCaptcha($builder->secret_key);
            $resp = $recaptcha->verify($request->get('g-recaptcha-response'), $_SERVER['REMOTE_ADDR']);

            if (!$resp->isSuccess())
                return redirect()->route('public.builders.embedded-form',['builder' => $builder])
                    ->with("error",'Recaptcha validation fail');
        }
        

        trade_model($builder);
        $data=[];
        $data['builder_id'] = $builder->id;
        $entity = new Term();
        $entity->builder_id = $builder->id;
        $entity->is_active = true;
        $entity->name = uniqid();
        foreach ($builder->elements as $element){
            switch ($element->type){
                case 'checkbox':
                    $entity->{$element->element_name} = json_encode($request->{$element->element_name});
                    break;
                case 'file':
                    if ($request->hasFile($element->element_name))
                    $entity->{$element->element_name} = Storage::putFile('media', $request->file($element->element_name));
                    break;
                default :
                    $entity->{$element->element_name} = $request->{$element->element_name};
            }
        }
        $entity->save();

        //Send test
        $customer_email = $request->email;
        $subject = 'test';
        try{
            Mail::send('emails.test',compact('order'),function ($message) use ($customer_email,$subject) {
                $message->from(setting('mail_from_address'), setting('mail_from_name'));
                $message->to($customer_email);
                $message->subject($subject);
            });
        }catch(\Exception $e){
            echo "Mail can't send";
        }


        $reply = $builder->reply->first();
        if (optional($reply)->is_active){
            event(new EventReplyMail($entity, $reply, $builder));
        }

        $noti = $builder->noti->first();
        if (optional($noti)->is_active){
            event(new EventReplyMail($entity, $noti, $builder,1));
        }

        if ($builder->is_redirect != '')
            return redirect()->away($builder->is_redirect);
        return redirect()->back()->withSuccess(trans('storefront::storefront.page.service_success'));
    }

    private function termValidate($builder){
        $validate = [];
        foreach ($builder->elements as $element){
            if ($element->is_required){
                $validate[$element->element_name] = 'required';
            }
        }
        return $validate;
    }
}
