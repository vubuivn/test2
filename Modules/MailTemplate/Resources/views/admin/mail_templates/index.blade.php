@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('mailtemplate::mail_templates.mail_templates'))

    <li class="active">{{ trans('mailtemplate::mail_templates.mail_templates') }}</li>
@endcomponent

@component('admin::components.page.index_table')
    @slot('buttons', ['create'])
    @slot('resource', 'mail_templates')
    @slot('name', trans('mailtemplate::mail_templates.mail_template'))

    @component('admin::components.table')
        @slot('thead')
            <tr>
                @include('admin::partials.table.select_all')
                <th>{{ trans('news::attributes.name') }}</th>
                <th>{{ trans('admin::admin.table.status') }}</th>
                <th data-sort>{{ trans('admin::admin.table.created') }}</th>
            </tr>
        @endslot
    @endcomponent
@endcomponent

@push('scripts')
    <script>
        new DataTable('#mail_templates-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                {data: 'name', name: 'translations.name', orderable: false, defaultContent: ''},
                {data: 'status', name: 'is_active', searchable: false},
                {data: 'created', name: 'created_at'},
            ],
        });
    </script>
@endpush
