<?php

namespace Modules\News\Providers;

use Modules\Admin\Ui\Facades\TabManager;
use Modules\News\Admin\NewsTabs;
use Modules\Support\Traits\AddsAsset;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\LoadsConfig;

class NewsServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        TabManager::register('news', NewsTabs::class);
        $this->addAdminAssets(
            'admin.news.(create|edit)',
            [
                'admin.media.css',
                'admin.media.js',
                // 'admin.option.css',
                // 'admin.option.js',
                // 'admin.storefront.css',
                // 'admin.storefront.js',
            ]
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadConfigs(['assets.php', 'permissions.php']);
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
