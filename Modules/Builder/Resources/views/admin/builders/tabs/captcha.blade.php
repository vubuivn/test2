<div class="row">
    <div class="col-md-8">
        {{ Form::text('site_key', 'Site key', $errors, $builder, ['labelCol' => 2, 'required' => false]) }}

    </div>
</div>
<div class="row">
    <div class="col-md-8">
        {{ Form::text('secret_key', 'Secret key', $errors, $builder, ['labelCol' => 2, 'required' => false]) }}

    </div>
</div>
<div class="row">
    <div class="col-md-8">
        {{ Form::checkbox('is_captcha', trans('builder::attributes.is_active'), 'Enable the form captcha', $errors, $builder) }}
    </div>
</div>