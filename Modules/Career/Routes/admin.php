<?php

/** @var Illuminate\Routing\Router $router */
Route::get('careers', [
    'as' => 'admin.careers.index',
    'uses' => 'CareerController@index',
    'middleware' => 'can:admin.careers.index',
]);

Route::get('careers/create', [
    'as' => 'admin.careers.create',
    'uses' => 'CareerController@create',
    'middleware' => 'can:admin.careers.create',
]);

Route::post('careers', [
    'as' => 'admin.careers.store',
    'uses' => 'CareerController@store',
    'middleware' => 'can:admin.careers.create',
]);

Route::get('careers/{id}/edit', [
    'as' => 'admin.careers.edit',
    'uses' => 'CareerController@edit',
    'middleware' => 'can:admin.careers.edit',
]);

Route::put('careers/{id}/edit', [
    'as' => 'admin.careers.update',
    'uses' => 'CareerController@update',
    'middleware' => 'can:admin.careers.edit',
]);

Route::delete('careers/{ids?}', [
    'as' => 'admin.careers.destroy',
    'uses' => 'CareerController@destroy',
    'middleware' => 'can:admin.careers.destroy',
]);
