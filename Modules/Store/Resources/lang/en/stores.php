<?php

return [
    'store' => 'Store',
    'stores' => 'Stores',
    'table' => [
        'name' => 'Name',
    ],
    'tabs' => [
        'group' => [
            'store_information' => 'Store Infortmation',
        ],
        'general' => 'General',
        'seo' => 'SEO',
        'block' => 'Block',
    ],
    'form' => [
        'enable_the_store' => 'Enable the store',
        'add_more_image' => 'Add more image',
        'banner_image' => 'Banner image',
    ],
    'expand_all' => 'Expand All',
    'collapse_all' => 'Collapse All',
    'template' => 'Template',
    'normal' => 'Normal',
    'has_sidebar' => 'Has sidebar',
    'questions' => 'Question',
];
