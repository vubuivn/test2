<section class="sec-filter">
    <form class="container" name="groupFilter">
        <div class="filter">
            <div class="bottomsearch">
                <span class="t1">{{ trans('group::group-categories.advanced') }}</span>
                <span class="t2">{{ trans('group::group-categories.collapse') }}</span>
            </div>

            <div class="row">
                <div class="col-lg-9">
                    <div class="row grid-space-10">

                        @foreach ($categories AS $item)
                        <div class="col-6 col-md-3">
                            <div class="item">
                                @if(isset($category_type) && $category_type == 'sale' || \Route::current()->getName() == 'products.categorysale')
                                    <div class="key">
                                        <span>{{ $item['name'] }}</span>
                                        <i class="icon-arrow-3"></i>
                                    </div>
                                    <!--  -->
        
                                    <select class="selectpicker"
                                        onchange="location=this.value"
                                        {{-- title="------" --}}
                                        data-hide-disabled="false" data-actions-box="true">
                                        <option value="{{route('products.category','sale')}}">------</option>
                                        @foreach ($item['values'] AS $catOption)
                                            @if($catOption['value'] != 'sale')
                                                <option value="{{route('products.categorysale',$catOption['value'])}}"  {{ $catOption['value'] == $item['selected'] ? 'selected' : '' }} >{{ $catOption['text'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                @else
                                    <div class="key">
                                        <span>{{ $item['name'] }}</span>
                                        <i class="icon-arrow-3"></i>
                                    </div>
                                    <!--  -->

                                    <select class="selectpicker"
                                        onchange="location=this.value"
                                        title="{{ trans('group::group-categories.choices') }}"
                                        data-hide-disabled="false" data-actions-box="true">

                                        @foreach ($item['values'] AS $catOption)
                                        <option value="{{route('products.category',$catOption['value'])}}" {{ $catOption['value'] == $item['selected'] ? 'selected' : '' }}>{{ $catOption['text'] }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                        @endforeach

                        @php ($firstAttrPart = array_slice($attributes, 0, 4 - count($categories), true))
                        @foreach ($firstAttrPart AS $attrId => $attr)
                        <div class="col-6 col-md-3">
                            <div class="item">
                                <div class="key"> <span>{{ $attr['name']}}</span> <i class="icon-arrow-3"></i></div>
                                <select name="attribute[{{ $attrId }}][]" class="selectpicker" title="{{ trans('group::group-categories.choices') }}"
                                    multiple data-live-search="true" data-hide-disabled="false" data-actions-box="true" >
                                    @foreach ($attr['values'] AS $option)
                                    <option value="{{ $option['value'] }}"
                                        {{ in_array($option['value'], ($request['attribute'][$attrId]) ?? []) ? 'selected' : '' }}
                                    >{{ $option['text'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="showmore" style="display: none;">
                        <div class="row grid-space-10">
                            @php ($lastAttributePart = array_slice($attributes, 4 - count($categories), count($attributes), true))
                            @foreach ($lastAttributePart AS $attrId => $attr)
                            <div class="col-6 col-md-3">
                                <div class="item">
                                    <div class="key"> <span>{{$attr['name'] }}<span> <i class="icon-arrow-3"></i></div>
                                    <select name="attribute[{{ $attrId }}][]" class="selectpicker" title="{{ trans('group::group-categories.choices') }}"
                                        multiple data-live-search="true" data-hide-disabled="false" data-actions-box="true">
                                        @foreach ($attr['values'] AS $option)
                                            <option value="{{ $option['value'] }}"
                                                {{ in_array($option['value'], ($request['attribute'][$attrId]) ?? []) ? 'selected' : '' }}
                                            >{{ $option['text'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endforeach

                            <div class="col-12  col-md-6">
                                <div class="item wrangeprice toggleClass">
                                    <div class="key toggle">
                                        <span>{{ trans('group::group-categories.price') }}</span>
                                        <i class="icon-arrow-3"></i>
                                    </div>
                                    <div class="value toggle">{{ trans('group::group-categories.from') }}
                                        <span id="amount"></span> {{ currency() }}</div>
                                    <div class="rangeprice">

                                        <div id="slider-range"></div>
                                        <input type="hidden" id="amount1" name="fromPrice">
                                        <input type="hidden" id="amount2" name="toPrice">
                                        <script src="/assets/js/jquery-ui.js"></script>
                                         <script>
                                            (function ($) {
                                                $(document).ready(function() {
                                                    $("#slider-range").slider({
                                                        range: true,
                                                        min: 0,
                                                        // Trường hợp số lẻ nên công thêm để khỏi bị lọt
                                                        max: {{$maxPrice}} + 1000000,
                                                        step: 500000,
                                                        values: [{{ $request['fromPrice'] ?? 0 }}, {{ $request['toPrice'] ?? $maxPrice+1000000 }}],
                                                        slide: function(event, ui) {
                                                            $("#amount").html(ui.values[0] + " - " + ui.values[1]);
                                                            $("#amount1").val(ui.values[0]);
                                                            $("#amount2").val(ui.values[1]);
                                                        }
                                                    });
                                                    // TODO, need to format money
                                                    var fromPrice = $("#slider-range").slider("values", 0);
                                                    var toPrice = $("#slider-range").slider("values", 1);
                                                   
                                                    $("#amount").html( fromPrice + " - " + toPrice);
                                                    $("#amount1").val($("#slider-range").slider("values", 0));
                                                    $("#amount2").val($("#slider-range").slider("values", 1));
                                                });
                                            })(jQuery);
                                        </script> 
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-3 group-btn">
                    <button type="reset" class="btnreset">
                        <img src="/assets/images/svg/reset.svg" alt="" />
                        {{ trans('group::group-categories.reset') }}
                    </button>
                    <button class="btn btnfilter" onclick="document.forms['groupFilter'].submit()">
                        <i class="icon-search-2"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>

</section>