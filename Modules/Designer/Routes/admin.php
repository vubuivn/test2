<?php

/** @var Illuminate\Routing\Router $router */
Route::get('designers', [
    'as' => 'admin.designers.index',
    'uses' => 'DesignerController@index',
    'middleware' => 'can:admin.designers.index',
]);

Route::get('designers/create', [
    'as' => 'admin.designers.create',
    'uses' => 'DesignerController@create',
    'middleware' => 'can:admin.designers.create',
]);

Route::post('designers', [
    'as' => 'admin.designers.store',
    'uses' => 'DesignerController@store',
    'middleware' => 'can:admin.designers.create',
]);

Route::get('designers/{id}/edit', [
    'as' => 'admin.designers.edit',
    'uses' => 'DesignerController@edit',
    'middleware' => 'can:admin.designers.edit',
]);

Route::put('designers/{id}/edit', [
    'as' => 'admin.designers.update',
    'uses' => 'DesignerController@update',
    'middleware' => 'can:admin.designers.edit',
]);

Route::delete('designers/{ids?}', [
    'as' => 'admin.designers.destroy',
    'uses' => 'DesignerController@destroy',
    'middleware' => 'can:admin.designers.destroy',
]);
