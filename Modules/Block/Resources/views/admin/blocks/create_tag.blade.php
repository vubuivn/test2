@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('block::blocks.block')]))

    <li><a href="{{ route('admin.blocks.index') }}">{{ trans('block::blocks.blocks') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('block::blocks.block')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.blocks.tag_store') }}" class="form-horizontal" id="block-create-form" novalidate>
        {{ csrf_field() }}
	<div class="box-body index-table" id="">
		<div class="col s12">
			<div class="accordion-content clearfix">
			<div class="col-lg-9 col-md-8">
				<div class="form-group ">
					<label for="name" class="col-md-2 control-label text-left">Name<span class="m-l-5 text-red">*</span></label>
					<div class="col-md-10"><input name="name" class="form-control " id="name" labelcol="2" type="text" value=""></div>
				</div>
				<div class="form-group">
				<div class="col-md-offset-2 col-md-10">
				<button id="btn-save-form" type="submit" class="btn btn-primary">
				Save
				</button>
				</div>
				</div>
	        </div>
	        </div>
	    </div>
	</div>
        
    </form>
@endsection

@include('block::admin.blocks.partials.shortcuts')
