<div class="col-md-3 col-sm-12">
    <div class="product-list-sidebar clearfix">
        @include('public.products.partials.category_filter')

        {{--<form method="GET" action="{{ route('products.index') }}" id="product-filter-form">--}}
        <form method="GET" action="{{url()->current()}}" id="product-filter-form">
            @foreach (request()->except(['attribute', 'fromPrice', 'toPrice','materialCode','colorCode']) as $query => $value)
                @if (! is_array($value))
                    <input type="hidden" name="{{ $query }}" value="{{ $value }}">
                @endif
            @endforeach
            @foreach ($attributes as $attribute)
                <div class="filter-section clearfix">
                    <h4>{{ $attribute->name }}</h4>
                    @php($values = $attribute->values->groupBy('other'))
                    <div class="{{ $values->count() > 5 ? 'filter-scroll' : '' }}">
                        @foreach ($values as $value)
                            @php($value = $value[0])
                            <div class="filter-options">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox"
                                               name="attribute[{{$attribute->id}}][]"
                                               value="{{ mb_strtolower($value->other) }}"
                                               id="attribute-{{ $value->id }}"
                                            {{ is_filtering($value->other) ? 'checked' : '' }}
                                        >

                                        <label for="attribute-{{ $value->id }}">{{ $value->other }}</label>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
            <div class="price-range-picker">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6 col-sm-3 col-xs-6">
                            <label for="price-from">{{ trans('storefront::products.from') }}</label>
                            <input type="text" name="fromPrice" class="from-control range-from" id="price-from">
                        </div>

                        <div class="col-md-6 col-sm-3 col-xs-6">
                            <label for="price-to">{{ trans('storefront::products.to') }}</label>
                            <input type="text" name="toPrice" class="from-control range-to" id="price-to">
                        </div>
                    </div>
                </div>

                <div class="slider noUi-target noUi-ltr noUi-horizontal" id="price-range-slider"
                     data-to-price="{{ request('toPrice', $maxPrice) }}" data-max="{{ $maxPrice }}">
                    <div class="noUi-base">
                        <div class="noUi-connects"></div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-filter pull-right"
                    data-loading>{{ trans('storefront::products.filter') }}</button>
        </form>
    </div>
</div>
@push('styles')
    <link href="{{ asset('select2.css') }}" rel="stylesheet">
@endpush
@push('scripts')
    <script src="{{ asset('select2.js') }}"></script>
    <script>
        $("#color_code").select2({
            theme: "bootstrap",
            placeholder: "Choose color...",
        });
        $("#material_code").select2({
            theme: "bootstrap",
            placeholder: "Choose material...",
        });
    </script>
@endpush
