<?php

namespace Modules\News\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;
use Modules\Block\Entities\Block;
use Modules\CategoryNews\Entities\CategoryNews;
use Modules\Tax\Entities\TaxClass;
use Modules\Category\Entities\Category;

class NewsTabs extends Tabs
{
    public function make ()
    {
        $this->group(
            'basic_information',
            trans('news::news.tabs.news.basic_information')
        )
            ->active()
            ->add($this->general())
            ->add($this->seo());

//        $this->group(
//            'advanced_information',
//            trans('group::groups.tabs.group.advanced_information')
//        )
//
//            ->add($this->filter());

        $this->submit = false;
        $this->block(Block::get());
    }

    private function general ()
    {
        return tap(
            new Tab(
                'general',
                trans('news::news.tabs.general')
            ),
            function (Tab $tab) {
                $tab->active();
                $tab->weight(5);
                $tab->fields(
                    [
                        'name',
                        'description',
                        'is_active'
                    ]
                );
                $tab->view(
                    'news::admin.news.tabs.general',
                    [
                        'categories' => CategoryNews::treeList()
                    ]
                );
            }
        );
    }

    private function seo ()
    {
        return tap(
            new Tab(
                'seo',
                trans('news::news.tabs.seo')
            ),
            function (Tab $tab) {
                $tab->weight(25);
                $tab->fields('slug');
                $tab->view('news::admin.news.tabs.seo');
            }
        );
    }

    private function filter ()
    {
        return tap(
            new Tab(
                'filter',
                trans('news::news.tabs.filter')
            ),
            function (Tab $tab) {
                $tab->weight(55);
                $tab->view('news::admin.news.tabs.filter');
            }
        );
    }
}
