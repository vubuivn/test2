<?php

namespace Modules\Element\Providers;

use Modules\Element\Entities\Element;
use Modules\Element\Admin\ElementTabs;
use Modules\Support\Traits\AddsAsset;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\LoadsConfig;
use Modules\Admin\Ui\Facades\TabManager;
use Modules\Element\Admin\BuilderTabsExtender;

class ElementServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        TabManager::register('elements', ElementTabs::class);

        $this->addAdminAssets('admin.elements.(create|edit)', ['admin.element.css', 'admin.element.js']);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadConfigs(['assets.php', 'permissions.php']);
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
