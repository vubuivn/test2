<div class="form-group ">
    <label class="col-md-2 control-label text-left">
        {{$block->title}}
    </label>
    <div class="col-md-10">
        <input name="blocks[{{$number}}][{{$block->key}}]" class="form-control" type="text" value="{{ $data->{$block->key} ?? null }}">
    </div>
</div>
