<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Define which assets will be available through the asset manager
    |--------------------------------------------------------------------------
    | These assets are registered on the asset manager
    */
    'all-assets' => [
        'admin.mailtemplate.css' => ['module' => 'mailtemplate:admin/css/mailtemplate.css'],
        'admin.mailtemplate.js' => ['module' => 'mailtemplate:admin/js/mailtemplate.js'],
    ],

    /*
    |--------------------------------------------------------------------------
    | Define which default assets will always be included in your pages
    | through the asset pipeline
    |--------------------------------------------------------------------------
    */
    'required-assets' => [],
    'mailTemplate_dir' => app_path('Mail/'),
];
