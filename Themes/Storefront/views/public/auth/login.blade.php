@extends('public.layout')

@section('title', trans('user::auth.login'))

@section('content')
    @include('public.partials.notification')
    <div class="form-wrapper-login row">
        
        <div class="left-image col-md-5 col-xs-12 img tRes_100">
            
                {{ Theme::image('public/images/left-image-login.jpg') }}
           
            <!-- <img src=""> -->
        </div>
        <div class="form form-page col-7">
            <form method="POST" action="{{ route('login.post') }}" class="login-form clearfix">
                {{ csrf_field() }}
                <div class="login form-inner clearfix no-border">
                    <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                        <label for="email">{{ trans('user::auth.email') }}<span>*</span></label>

                        <input type="text" name="email" value="{{ old('email') }}" class="form-control" id="email" placeholder="{{ trans('user::attributes.users.email') }}" autofocus>

                        <div class="input-icon">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        </div>

                        {!! $errors->first('email','<span class="error-message">:message</span>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error': '' }}">
                        <label for="password">{{ trans('user::auth.password') }}<span>*</span></label>

                        <input type="password" name="password" class="form-control" id="password" placeholder="{{ trans('user::attributes.users.password') }}">

                        <div class="input-icon">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </div>

                        {!! $errors->first('password','<span class="error-message">:message</span>') !!}
                    </div>

                    <div class="clearfix"></div>

                    <div class="checkbox pull-left">
                        <label class="container_check">{{ trans('user::auth.remember_me') }}
                            <input type="hidden" value="0">
                            <input type="checkbox" value="1" id="remember">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <button type="submit" class="btn btn-primary btn-center btn-login" data-loading>
                        {{ trans('user::auth.login') }}
                    </button>
                    <div class="text-center forgot-content">
                        <a href="{{ route('reset') }}" class="forgot-password">
                            {{ trans('user::auth.forgot_password') }}
                        </a>
                    </div>

                    <div class="social-login-buttons text-center">
                        @if (count(app('enabled_social_login_providers')) !== 0)
                            <span>{{ trans('user::auth.or') }}</span>
                        @endif            
                    </div>

                    @if (setting('facebook_login_enabled'))
                        <a class="btn btn-login-custom login-facebook" href="{{ route('login.redirect', ['provider' => 'facebook']) }}"><i class="fa fa-facebook"></i> {{ trans('storefront::storefront.form.login_with_facebook')}}</a>
                    @endif
                    @if (setting('google_login_enabled'))
                        <a class="btn btn-login-custom login-google" href="{{ route('login.redirect', ['provider' => 'google']) }}"><i class="fa fa-google"></i> {{ trans('storefront::storefront.form.login_with_google')}}</a>
                    @endif
                    
                     
                </div>
                <div class="login-bottom col-12">
                    <div class="text-center">
                        {{ trans('storefront::storefront.form.no_account')}} <a href="{{route('register')}}">{{ trans('storefront::storefront.form.click_to_register') }}</a>
                    </div>
                </div>   

            </form>
        </div>

        
    </div>
@endsection


@push('styles')
    <link href="{{ v(Theme::url('public/css/custom-form.css')) }}" rel="stylesheet">
    <style type="text/css">
        .btn.login-facebook:hover, .btn.login-facebook:active{background-color: #607fbe;border-color:#607fbe;}
    </style>
@endpush
