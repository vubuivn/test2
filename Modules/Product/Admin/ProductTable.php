<?php

namespace Modules\Product\Admin;

use Modules\Admin\Ui\AdminTable;

class ProductTable extends AdminTable
{
    /**
     * Raw columns that will not be escaped.
     *
     * @var array
     */
    protected $rawColumns = ['price','link'];

    /**
     * Make table response for the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function make()
    {
        return $this->newTable()
            ->editColumn('thumbnail', function ($product) {
                $path = optional($product->base_image)->path;

                return view('product::admin.products.partials.table.thumbnail', compact('path'));
            })
            ->editColumn('status', function ($entity) {
                return product_status($entity->is_active);
            })
            ->editColumn('link', function ($entity) {
                return '<a href="'.route('admin.products.edit',$entity->id).'">Edit</a> | <a href="'.route('products.show',$entity->group()->first()->slug).'">View</a>';
            })
            ->editColumn('price', function ($product) {
                if (! $product->hasSpecialPrice()) {
                    return $product->price->format();
                }

                return "<span class='m-r-5'>{$product->special_price->format()}</span>
                    <del class='text-red'>{$product->price->format()}</del>";
            });
    }
}
