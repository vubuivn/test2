@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('mailtemplate::mail_templates.mail_template')]))

    <li><a href="{{ route('admin.mail_templates.index') }}">{{ trans('mailtemplate::mail_templates.mail_templates') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('mailtemplate::mail_templates.mail_template')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.mail_templates.store') }}" class="form-horizontal" id="mail-template-create-form" novalidate>
        {{ csrf_field() }}
        {!! $tabs->render(compact('mailTemplate')) !!}
    </form>
@endsection

@include('mailtemplate::admin.mail_templates.partials.shortcuts')
