<?php

use Illuminate\Support\HtmlString;

if (! function_exists('element_name')) {
    function element_name($element)
    {
        return new HtmlString($element->name . ($element->is_required ? '<span>*</span>' : ''));
    }
}

if (! function_exists('element_value')) {
    function element_value($value)
    {
        $price = is_null($value->price->amount())
            ? '' :
            "+ {$value->price->convertToCurrentCurrency()->format()}";

        return new HtmlString("{$value->label} <span class='value-price'>{$price}</span>");
    }
}
