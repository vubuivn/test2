<div id="footer" class="sec-tb">
	<div class="container">
		@if ($footerMenu1->isNotEmpty())
		<ul class="menu menu1">
			@foreach ($footerMenu1 as $menuItem)
				<li><a href="{{ $menuItem->url() }}">{{ $menuItem->name }}</a></li>
			@endforeach
		</ul>
		@endif

		@if ($footerMenu2->isNotEmpty())
		<ul class="menu">
			@foreach ($footerMenu2 as $menuItem)
				<li><a href="{{ $menuItem->url() }}">{{ $menuItem->name }}</a></li>
			@endforeach
		</ul>
		@endif

		<ul class="blog-item-social ">
			<li><a href="https://www.facebook.com/aconceptvietnam/" class="item" title="" target="_blank" href="#"><i class="icon-facebook1"></i></a></li>
			<li><a href="https://www.instagram.com/aconcept_vietnam/" class="item" title="" target="_blank" href="#"><i class="icon-instagram1"></i></a></li>
			<li><a href="https://www.youtube.com/channel/UCfDg6t9S8km49od4y8JOXPQ" class="item" title="" target="_blank" href="#"><i class="icon-youtube1"></i></a></li>
		</ul>
		<div class="copyright">© AConcept 2019</div>


	  <!-- Your customer chat code -->
	  <div class="fb-customerchat"
			attribution=setup_tool
			page_id="1732204863758377"
			theme_color="#6699cc"
			greeting_dialog_display='hide'>
    	</div>
  
	</div>
	<!--End #footer-->
</div>
</div>
<!--End #wrapper-->
<div id="loading_gif"><img src="/assets/img/loading.gif"></div>
<script type="text/javascript">
	(function($){
		$('.subscription').on('click',function(e){
            var email = $(this).prev().val();
            if(email == ''){
                e.preventDefault();
                $('.subscription_warning').html('email required');
                return false;
            }
            var button = $(this);
            var check = false;
            if(check != true){
                check = true;
                // $.ajaxSetup({
                //     headers: {
                //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //     }
                // });
       //          $.ajax({
       //              type: 'POST',
       //              url: "{{--route('page.subscription.ajax')--}}",
       //              data: { email: email },
       //              beforeSend: function() {
       //                  button.html('<img src="/assets/images/loading.gif">');
       //              },
       //              success: function(data){
       //              	console.log(data);
 						// if($('.subscription_warning').length>0){
 						// 	$('.subscription_warning').html(data);
 						// }
                        
       //              },
       //              complete: function(){
       //                  button.html("{{trans('storefront::storefront.news.view_more')}}");
       //                  check = false;
       //              },
       //              error: function (data) {
       //                  console.log('Error:', data);
       //              }
       //          });

            }


            
        });


        // 

		$('.search_button').on('click',function(){
			if($('.search_form').hasClass('show')){
				$('.search_form').removeClass('show');
			}else{
				$('.search_form').addClass('show');
			}
		})

		$(document).ready(function(){

			$(document).on('click','.wrap-menu-mb ul.menu li.children>a', function (e) {
	        	e.preventDefault();
		    	var p = $(this),
		    	parent = p.parent(),
		        idli = p.attr('id'),
		        ul = p.parent().children('ul');
	            if (parent.hasClass('parent-showsub')) {
	                ul.slideUp(300, function () {
	                    parent.removeClass('parent-showsub');
	                });
	            } else {
	                ul.slideDown(300, function () {
	                    parent.addClass('parent-showsub');
	                });
	            }

	        });


			// auto menu first sofa

			var menu_first = $('#product-menu .col-md-7>ul.menu>li:first-child>span');
            var img = menu_first.data('img');
        	var title = menu_first.children('a').html();
        	var link = menu_first.data('link');
        	var synproduct = $('#product-menu .synproduct');
        	synproduct.children('div.img').find('img').attr('src',img);
        	synproduct.find('h2').html(title); 	
        	synproduct.find('a').attr('href',link); 


        	// CUSTOM SHARE FACEBOOK
   //      	window.fbAsyncInit = function(){
			// 	FB.init({
			//     	appId: '1347885215370200', status: true, cookie: true, xfbml: true }); 
			// };
			// (function(d, debug){var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
			//     if(d.getElementById(id)) {return;}
			//     js = d.createElement('script'); js.id = id; 
			//     js.async = true;js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
			//     ref.parentNode.insertBefore(js, ref);}(document, /*debug*/ false));

			function postToFeed(title, desc, url, image){
				var obj = {method: 'feed',link: url, picture: 'http://www.url.com/images/'+image,name: title,description: desc};
				function callback(response){}
					FB.ui(obj, callback);
			}



			$('.btnfb').click(function(e){
			e.preventDefault();
			var item = $(this).closest('.item');
			var title = item.find('.title').html();
			var content = item.find('.t4').html();
			var image = item.find('.img').children('img').attr('data-lazy-src');
			var clink = '{{url()->current()}}';
			postToFeed(title, content, clink, image);

				// FB.ui(
				// {
				// method: 'feed',
				// name: title,
				// link: clink,
				// picture: image,
				// caption: title,
				// description: content,
				// message: ""
				// });
			});
		});
		ajaxCategoryImage();
		function ajaxCategoryImage(ids){
			var ids =[];
			$('#product-menu li').each(function(){
				var idcat = $(this).find('span').data('categoryid');
				ids.push(idcat);
			});

			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
			$.ajax({
                url: '{{ route("products.category.ajaximage") }}',
                method: 'POST',
                type: 'json',
                data: {
                    ids: ids
                },
                success: function(res) {
                	// console.log(res);
                	// $('#product-menu li span').each(function(){
                	// 	$(this.)
                	// })
                	$.each(res,function(index,value){
                		$('#product-menu li span[data-categoryid="'+index+'"]').attr('data-img','{{url("/")}}/storage/'+value)
                		// spanLi.attr('data-img',val);
                	});

                },
                error: function(xhr, err) {
                    $('.text-alert.alert-error').text(xhr.responseJSON.errors.email[0]);
                }
            });
		}

	})(jQuery);
</script>
<!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
          	appId: '1347885215370200',
            xfbml            : true,
            version          : 'v4.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
@include('public._module.login-register')

<script type='text/javascript' src='/assets/js/bootstrap.min.js'></script>
<!-- <script type="text/javascript" src="/assets/js/select2.full.js"></script> -->

<!-- <link rel='stylesheet' href='/assets/js/bootstrap-select/bootstrap-select.min___.css' type='text/css' media='all' /> -->
<script type="text/javascript" src="/assets/js/bootstrap-select/bootstrap-select.min.js"></script>

<script type='text/javascript' src='/assets/js/owl.carousel.min.js'></script>
<script type='text/javascript' src='/assets/js/imagesloaded.pkgd.min.js'></script>
<script type='text/javascript' src='/assets/js/script.js'></script>
<script type='text/javascript' src='/assets/js/custom.js'></script>
<script type='text/javascript' src='/assets/js/script_owl.js'></script>
@stack('scripts')
<meta name="_token" content="{{ csrf_token() }}">

		
</body>

</html>
