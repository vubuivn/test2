@extends('admin::layout')

@component('admin::components.term.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('term::terms.term')]))

    <li><a href="{{ route('admin.terms.index') }}">{{ trans('term::terms.terms') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('term::terms.term')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.terms.store') }}" class="form-horizontal" id="term-create-form" novalidate>
        {{ csrf_field() }}

        {!! $tabs->render(compact('term')) !!}
    </form>
@endsection

@include('term::admin.terms.partials.shortcuts')
