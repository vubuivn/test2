<?php

return [
    'index' => 'Index Terms',
    'create' => 'Create Terms',
    'edit' => 'Edit Terms',
    'destroy' => 'Delete Terms',
];
