<script type="text/html" id="attribute-value-template">
    <tr>
        <td class="text-center">
            <span class="drag-icon">
                <i class="fa">&#xf142;</i>
                <i class="fa">&#xf142;</i>
            </span>
        </td>

        <td>
            <div class="form-group">
                <input type="text" name="values[<%- valueId %>][key]" value="<%- value.key %>" class="form-control">
            </div>
        </td>
        <td>
            <input type="hidden" name="values[<%- valueId %>][id]" value="<%- value.id %>">
            <div class="form-group">
                <input type="text" name="values[<%- valueId %>][title]" value="<%- value.title %>" class="form-control">
            </div>
        </td>
        <td>
            <div class="form-group">
                <select name="values[<%- valueId %>][type]" class="form-control">
                    @foreach($blockItem as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </td>

        <td class="text-center">
            <button type="button" class="btn btn-default delete-row" data-toggle="tooltip" data-title="{{ trans('block::blocks.form.delete_block') }}">
                <i class="fa fa-trash"></i>
            </button>
        </td>
    </tr>
</script>
