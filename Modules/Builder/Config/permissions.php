<?php

return [
    'admin.builders' => [
        'index' => 'builder::permissions.index',
        'create' => 'builder::permissions.create',
        'edit' => 'builder::permissions.edit',
        'destroy' => 'builder::permissions.destroy',
    ],
];
