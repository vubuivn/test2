<?php

namespace Modules\Term\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;

class TermTabs extends Tabs
{
    public function make()
    {
        $this->group('term_information', trans('term::terms.tabs.group.term_information'))
            ->active()
            ->add($this->general())
            ->add($this->seo());
    }

    private function general()
    {
        return tap(new Tab('general', trans('term::terms.tabs.general')), function (Tab $tab) {
            $tab->active();
            $tab->weight(5);
            $tab->fields(['title', 'body', 'is_active', 'slug']);
            $tab->view('term::admin.terms.tabs.general');
        });
    }

    private function seo()
    {
        return tap(new Tab('seo', trans('term::terms.tabs.seo')), function (Tab $tab) {
            $tab->weight(10);
            $tab->view('term::admin.terms.tabs.seo');
        });
    }
}
