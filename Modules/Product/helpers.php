<?php

use Illuminate\Support\HtmlString;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Modules\Support\Money;
use Modules\Group\Entities\Group;
use Modules\Category\Entities\Category;

if (!function_exists('product_price')) {
    /**
     * Get the selling price of the given product.
     *
     * @param \Modules\Product\Entities\Product $product
     * @param string                            $class
     *
     * @return \Illuminate\Support\HtmlString|string
     */
    function product_price ($product, $class = 'previous-price')
    {
        if (!$product->hasSpecialPrice()) {
            return $product->price->convertToCurrentCurrency()
                ->format();
        }

        $price        = $product->price->convertToCurrentCurrency()
            ->format();
        $specialPrice = $product->special_price->convertToCurrentCurrency()
            ->format();

        return new HtmlString("{$specialPrice} <span class='{$class}'>{$price}</span>");
    }
}

function product_status ($status)
{
    switch ($status) {
        case 0:
            return trans('product::products.form.show_products_by_side');
            break;
        case 1:
            return trans('product::products.form.show_main_products');
            break;
        default:
            return trans('product::products.form.hide_from_the_web');
            break;
    }
}

function custom_array_merge($array1, $array2)
{
    foreach ($array2 AS $key => $val) {
        $array1[$key] = $val;
    }

    return $array1;
}

/**
* Update Multi fields
* $table String
* $value Array
* $index String
*
* Example
*
* $table = 'users';
* $value = [
*      [
*          'id' => 1,
*          'status' => 'active',
*          'nickname' => 'Mohammad'
*      ] ,
*      [
*          'id' => 5,
*          'status' => 'deactive',
*          'nickname' => 'Ghanbari'
*      ] ,
* ];
*
* $index = 'id';
*
*/
function updateBatch($table, $values, $index)
{
    $final  = array();
    $ids    = array();
    if (!count($values))
        return false;
    if (!isset($index) AND empty($index))
        return false;
    foreach ($values as $key => $val)
    {
        $ids[] = "'" . $val[$index] . "'";
        foreach (array_keys($val) as $field)
        {
            if ($field !== $index)
            {
                $value              = (is_null($val[$field]) ? 'NULL' : '"' . $val[$field] . '"' );
                $final[$field][]    = 'WHEN `'. $index .'` = "' . $val[$index] . '" THEN ' . $value . ' ';
            }
        }
    }
    $cases = '';
    foreach ($final as $k => $v)
    {
        $cases .=  '`'. $k.'` = (CASE '. implode("\n", $v) . "\n"
                        . 'ELSE `'.$k.'` END), ';
    }
    $query = 'UPDATE ' . $table . ' SET '. substr($cases, 0, -2) . ' WHERE ' . $index . ' IN('.implode(',', $ids).')';
    
    return DB::statement($query);
}

function formatMoney($amount) {
    $money = new Money($amount, currency());
    
    return $money->convertToCurrentCurrency()->format();
}

function buildFromToPriceQueryBuilder($attributes = [],$type = 'normal') {
    $lowestPrice = DB::table('products AS p')
                    ->select('p.group_id',
                                DB::raw('MIN(p.selling_price) as lowest_price'),
                                DB::raw('MAX(p.selling_price) as highest_price'),
                                DB::raw('MAX(p.updated_at) as latest'),
                                DB::raw('MIN(CONCAT(id, "___", sku)) as idSku2'),
                                DB::raw('MIN(sku) as idSkuS')
                                )
                    ->whereIn('p.is_active', [0, 1])
                    ->groupBy('p.group_id');

    // sales, new, wishlist ()
    // $new = DB::table('products')->select('group_id')->where('new', 1)->groupBy('group_id');
    // $sales = DB::table('products')->select('group_id')->where('new', 1)->groupBy('group_id');
    // select group_id from products WHERE special_price > 0 AND special_price < selling_price AND special_price_end > NOW() GROUP BY group_id

   

    $featureProduct = DB::table('products')
                    ->select('group_id',
                                DB::raw('MIN(CONCAT(id, "___", sku)) as idSku'),
                                'selling_price',
                                'price'
                                )
                    ->where('is_active', 1)
                    ->groupBy('group_id');


    foreach ($attributes AS $attrId => $attrValueIds) {
        $tableName = 'pav_' . $attrId;
        $lowestPrice->join("product_attribute_values AS {$tableName}", "{$tableName}.product_id", '=', 'p.id')
            ->where("{$tableName}.attribute_id", $attrId)
            ->whereIn("{$tableName}.attribute_value_id", $attrValueIds);
    }

    return Group::joinSub($lowestPrice, 'product', function ($join) {
                $join->on('groups.id', '=', 'product.group_id');
            })->joinSub($featureProduct, 'featureProduct', function ($join) {
                $join->on('groups.id', '=', 'featureProduct.group_id');
            });
}


function buildFromToPriceQueryBuilderSale($attributes = []){
    $lowestPrice = DB::table('products AS p')
                    ->select('p.group_id',
                                DB::raw('MIN(p.selling_price) as lowest_price'),
                                DB::raw('MAX(p.selling_price) as highest_price'),
                                DB::raw('MAX(p.updated_at) as latest'),
                                DB::raw('MIN(CONCAT(id, "___", sku)) as idSku2'),
                                DB::raw('MIN(sku) as idSkuS')
                                )
                    ->whereIn('p.is_active', [0, 1])
                    ->groupBy('p.group_id');

    // sales, new, wishlist ()
    // $new = DB::table('products')->select('group_id')->where('new', 1)->groupBy('group_id');
    // $sales = DB::table('products')->select('group_id')->where('new', 1)->groupBy('group_id');
    // select group_id from products WHERE special_price > 0 AND special_price < selling_price AND special_price_end > NOW() GROUP BY group_id

   


    // IF FOR GET SALE
    $featureProduct = DB::table('products')
                ->select('group_id',
                            DB::raw('MIN(CONCAT(id, "___", sku)) as idSku'),
                            'selling_price',
                            'price'
                            )
                ->where('is_active', 1)
                ->where('selling_price','<>', DB::raw('products.price'))
                ->groupBy('group_id');


    foreach ($attributes AS $attrId => $attrValueIds) {
        $tableName = 'pav_' . $attrId;
        $lowestPrice->join("product_attribute_values AS {$tableName}", "{$tableName}.product_id", '=', 'p.id')
            ->where("{$tableName}.attribute_id", $attrId)
            ->whereIn("{$tableName}.attribute_value_id", $attrValueIds);
    }

    return Group::joinSub($lowestPrice, 'product', function ($join) {
                $join->on('groups.id', '=', 'product.group_id');
            })->joinSub($featureProduct, 'featureProduct', function ($join) {
                $join->on('groups.id', '=', 'featureProduct.group_id');
            })->with(array(
                'categories' => function ($query){
                    $query->orderBy('position','ASC');
                }
            ));
}

function productImageUrl($sku, $index) {
    return url('storage/files/products/' . $sku . '-NB00' . $index . '.png');
}

function groupImageUrl($code, $index) {
    return url('storage/files/groups/' . $code . '-00' . $index . '.jpg');
}

function reformatCategoryUrl($url) {
    $category = explode('=', $url)[1] ?? null;

    if ($category) {
        return getCategoryUrlFromSlug($category);
    }
    return $url;
}

function getCategoryImageFromCategoryUrl($url) {
    $slug = explode('=', $url)[1] ?? null;

    $category = Category::without('translations')->with('files')->where('slug', $slug)->first();
    $featuredImage = $category ? $category->featureImage : null;
    if (is_object($featuredImage) && $featuredImage->isImage()) {
        
        return $featuredImage->path;
    }
    
    return '';
}

function _getIdSkuFromGroup($group, $type = 'sku') {
    $idSku = empty($group->idSku) ? $group->idSku2 : $group->idSku;
    $index = $type === 'id' ? 0 : 1;
    
    return explode('___', $idSku)[$index];
}

function getSkuFromGroup($group) {
    return _getIdSkuFromGroup($group);
}

function _getIdSkuFromGroup2($group, $type = 'sku') {
    $idSku = empty($group->idSku2) ? $group->idSku : $group->idSku2;
    $index = $type === 'id' ? 0 : 1;
    
    return explode('___', $idSku)[$index];
}

function getSkuFromGroup2($group) {
    return _getIdSkuFromGroup2($group);
}

function getIdFromGroup($group) {
    return _getIdSkuFromGroup($group, 'id');
}

function _getWishlistProductIdsFromCache($userId) {
    return Cache::remember('wishlistProductIds_' . $userId, $seconds = 3600, function () use ($userId) {
        $wishlistProductIds = [];
        $items = DB::table('wish_lists')->where('user_id', $userId)->select('product_id')->get();
        foreach ($items AS $item) {
            $wishlistProductIds[$item->product_id] = 1;
        }

        return $wishlistProductIds;
    });
}

function printWishListIcon($productId) {
    $user = auth()->user();
    $userId = $user ? $user->id : null;
    $isAlreadyWishList = false;
    if ($userId) {
        $wishlistProductIds = _getWishlistProductIdsFromCache($userId);
        $isAlreadyWishList = $wishlistProductIds[$productId] ?? false;
    }
    
    $wishlistStr = '<span class="like"><i class="icon-heart cl1"></i></span>';
    if ($isAlreadyWishList) {
        $wishlistStr = '<span class="like active">
                            <i class="icon-heart-add">
                                <span class="path1"></span>
                                <span class="path2"></span>
                                <span class="path3"></span>
                            </i>
                        </span>';
    }
    
    return '<a href="#" class="a_wishlist_cls" data-id="' . $productId . '">' . $wishlistStr . '</a>';
}

function checkImageUrl($url){
    $handle = curl_init($url);
    curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

    /* Get the HTML or whatever is linked in $url. */
    $response = curl_exec($handle);

    /* Check for 404 (file not found). */
    $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
    if($httpCode == 404) {
        return false;
    }
    return true;
}