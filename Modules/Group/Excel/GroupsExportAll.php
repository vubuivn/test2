<?php

namespace Modules\Group\Excel;

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
// use Box\Spout\Common\Entity\Row;

use Modules\Group\Entities\Group;
use DB;
// use Log;

class GroupsExportAll
{
    private $groupIds;
    private $groupFields;

    public function __construct(string $filePath, array $groupIds = [], array $errorGroups = [])
    {
        // make groupId is integer to prevent sql injection
        $this->groupIds = array_map(function($id) {
            return (int) $id;
        }, $groupIds);

        $this->groupFields = config('imports.groups');
        if (!empty($errorGroups)) {
            // Log::info($errorGroups);

            $this->groupFields['errorMessages'] = 'Error Messages';
        }

        $writer = WriterEntityFactory::createXLSXWriter();

        $writer->openToFile($filePath); // write data to a file or to a PHP stream

        // create heading row
        $headings = $this->headings();
        $headingsRow = WriterEntityFactory::createRowFromArray($headings);
        $writer->addRow($headingsRow);

        // create body rows
        if (empty($errorGroups)) {
            $this->body($writer);
        } else {
            $this->bodyError($writer, $errorGroups);
        }

        // finish and close the file
        $writer->close();
    }

    private function body($writer)
    {
        $query = 'SELECT g.code, g.is_active, g.collections, g.image_count,
                        GROUP_CONCAT(gc.category_id SEPARATOR ",") AS categories,
                        en.name en_name, en.description en_description, en.content_1 en_content_1, en.content_2 en_content_2,
                        vi.name vi_name, vi.description vi_description, vi.content_1 vi_content_1, vi.content_2 vi_content_2, is_new, new_end
                    FROM groups AS g
                    LEFT JOIN group_categories AS gc
                        ON gc.group_id=g.id
                    LEFT JOIN group_translations AS en
                        ON en.group_id = g.id AND en.locale = "en"
                    LEFT JOIN group_translations AS vi
                        ON vi.group_id = g.id AND vi.locale = "vi_VN"
                    %s
                    GROUP BY g.id
                    ';

        $whereStr = '';
        if (!empty($this->groupIds)) {
            $groupIdString = implode(',', $this->groupIds);
            $whereStr = 'WHERE g.id IN (' . $groupIdString . ')';
        }
        $query = sprintf($query, $whereStr);
            
        $rows = DB::select(DB::raw($query));

        foreach ($rows as $row) {
            $formattedRow = [];

            foreach ($this->groupFields AS $key => $val) {
                $formattedRow[] = $row->{$key};
            }

            $bodyRow = WriterEntityFactory::createRowFromArray($formattedRow);
            $writer->addRow($bodyRow);
        }
    }

    private function bodyError($writer, $errorGroups)
    {
        foreach ($errorGroups as $row) {
            $formattedRow = [];

            foreach ($this->groupFields AS $key => $val) {
                $groupVal = $row[$key];
                if (is_array($groupVal)) {
                    $groupVal = implode(';', $groupVal);
                }
                $formattedRow[] = $groupVal;
            }

            $bodyRow = WriterEntityFactory::createRowFromArray($formattedRow);
            $writer->addRow($bodyRow);
        }
    }

    private function headings(): array
    {
        // [
        //     'code' => 'Mã nhóm',
        //     'categories' => 'Danh mục',
        //     'vi_name' => 'Tiêu đề',
        //     'vi_description' => 'Mô tả',
        //     'vi_content_1' => 'Nội dung ở block 1',
        //     'vi_content_2' => 'Nội dung ở block 2',
        //     'en_name' => 'Title',
        //     'en_description' => 'Description',
        //     'en_content_1' => 'Content in block 1',
        //     'en_content_2' => 'Content in block 2',
        //     'is_active' => 'Trạng thái',
        // ]

        return array_values($this->groupFields);
    }
}