<?php

return [
    'name' => 'Name',
    'featured_groups' => 'Featured Groups',
    'slug' => 'URL',
    'is_searchable' => 'Searchable',
    'is_active' => 'Status',
    'description' => 'Description',
    'feature_image' => 'Feature Image',
    'cover_image' => 'Cover Image',
    'meta_title' => 'Meta Title',
    'position' => 'Position'
];
