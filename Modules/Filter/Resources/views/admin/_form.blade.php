<div class="form-group">
    <label for="meta-title" class="col-md-3 control-label text-left">
        {{ trans('filter::attributes.filter_categories') }}
    </label>

    <div class="col-md-9">
        <select name="filter[entity_filter_value][]" class="selectize prevent-creation" multiple>
            @foreach($category as $id => $name)
                <option value="{{$id}}" {{in_array($id, $entity_filter_value) ? 'selected' : ''}}>{{$name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="meta-title" class="col-md-3 control-label text-left">
        {{ trans('filter::attributes.is_filter') }}
    </label>

    <div class="col-md-9">
        <div class="checkbox">
            <input type="hidden" value="0" name="filter[is_filter]">
            <input type="hidden" value="{{$category_entity}}" name="filter[entity_filter]">
            <input id="filter_is_filter" {{($entity->filter->is_filter === 1) ? 'checked' : ''}} type="checkbox"
                   name="filter[is_filter]" value="1">
            <label for="filter_is_filter">
                {{trans('filter::attributes.enable_filter_categories')}}
            </label>
        </div>
    </div>
</div>
