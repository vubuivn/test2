<ul class="menu">
@foreach ($subMenus as $subMenu)
    <li id="li-{{ $subMenu->id() }}" class="{{ $subMenu->hasItems() ? 'children' : '' }}">
        <a href="{{ $subMenu->url() }}">
            <span>{{ $subMenu->name() }}</span>
        </a>
        @if ($subMenu->hasItems())
            @include('public.include.mega_menu.dropdown', ['subMenus' => $subMenu->items()])
        @endif
    </li>
@endforeach
</ul>