<?php

namespace Modules\Builder\Http\Controllers\Admin;

use Modules\Builder\Entities\Builder;
use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Builder\Http\Requests\SaveBuilderRequest;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Modules\Element\Entities\Element;
use Modules\Builder\Entities\Reply;
use Modules\Builder\Entities\Noti;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class BuilderController extends Controller
{
    use HasCrudActions;
    private $field_type;
    private $field_name;
    private $field_nullable = true;
    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Builder::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'builder::builders.builder';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'builder::admin.builders';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveBuilderRequest::class;

    public function api_index(){
        $builder = Builder::jsonPaginate(20);
        return $builder;
    }

    public function reply($id){
        $entity = $this->getEntity($id);
        $reply = ($entity->reply->first()) ? $entity->reply->first() : null;
        return view("{$this->viewPath}.reply")->with(['builder' => $entity, 'reply' => $reply]);
    }

    public function noti($id){
        $entity = $this->getEntity($id);
        $reply = ($entity->noti->first()) ? $entity->noti->first() : null;
        return view("{$this->viewPath}.noti")->with(['builder' => $entity, 'reply' => $reply]);
    }

    public function reply_store(Request $request, $id){
        $request->validate([
            'reply_subject' => 'required',
            'reply_to' => 'required',
            'reply_body' => 'required',
        ]);
        $is_active = 0;
        if ($request->has('is_active')) {
            $is_active = $request->is_active;
        }
        $reply = Reply::updateOrCreate(['builder_id' => $id], $request->merge(['builder_id' => $id, 'is_active' => $is_active])->except('_token', '_method'));
        return redirect()->back()
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
    }

    public function noti_store(Request $request, $id){
        $request->validate([
            'reply_subject' => 'required',
            'reply_to' => 'required',
            'reply_body' => 'required',
        ]);
        $is_active = 0;
        if ($request->has('is_active')) {
            $is_active = $request->is_active;
        }
        $reply = Noti::updateOrCreate(['builder_id' => $id], $request->merge(['builder_id' => $id, 'is_active' => $is_active])->except('_token', '_method'));
        return redirect()->back()
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
    }

    public function embedded($id){
        $entity = $this->getEntity($id);
        $html = $entity->embedded;
        foreach ($entity->elements as $element){
            $html = str_replace($element->element_stages, $element->element_name, $html);
        }
        $html = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".asset('modules/common/css/formeo.min.css')."\" /><form method=\"POST\" action=".route('public.builders.embedded-form', ['builder' => $entity])." enctype=\"multipart/form-data\" onsubmit=\"return get_action()\">".csrf_field()."{$html}</form>";
        if ($entity->is_captcha){
            $capt = "<div class='formeo-row-wrap'>
            <div class='formeo-row'>
            <div class='formeo-column\" style=\"width: 100%;'>
            <div class='f-field-group'><div class='g-recaptcha' data-sitekey='{$entity->site_key}'></div><span id='captcha' style='color:red' /></div>
            </div>
            </div>
            </div>";
            $_html = new \Htmldom($html);
            $ret = $_html->find('div[class=formeo-row-wrap]', -1);
            $html = str_replace($ret->outertext, $capt.$ret->outertext, $html);
            $html .= "<script src='//www.google.com/recaptcha/api.js'></script>";
            $html .= "<script>function get_action(form){ var v = grecaptcha.getResponse(); if(v.length == 0) { document.getElementById('captcha').innerHTML=\"You can't leave Captcha Code empty\"; return false; }}</script>";
        }

        return view("{$this->viewPath}.embedded")->with(['embedded' => $html, 'builder' => $entity]);
    }

    public function store()
    {
        $this->disableSearchSyncing();

        $entity = $this->getModel()->create(
            $this->getRequest('store')->all()
        );

        $html = new \Htmldom($this->getRequest('store')->embedded);
        /*
         * add or update
        */
        //input
        if ($html->find('input'))
            $this->_element($entity, 'input', $html);
        //textarea
        if ($html->find('textarea'))
            $this->_element($entity, 'textarea', $html);
        //select
        if ($html->find('select'))
            $this->_element($entity, 'select', $html);

        $entity = $this->getEntity($entity->id);

        $this->createTerm($entity->id);
        foreach ($entity->elements as $element){
            $this->field_type = $element->type;
            $this->field_name = $element->element_name;
            $this->field_nullable = 1;//($element->is_required ? 0 : 1);
            $this->addField($entity->id);
        }

        //$this->searchable($entity);

        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo($entity);
        }

        return redirect()->route("{$this->getRoutePrefix()}.index")
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
    }

    public function update($id)
    {
        $entity = $this->getEntity($id);

        $entity->update(
            $this->getRequest('update')->all()
        );

        $html = new \Htmldom($this->getRequest('update')->embedded);
        //remove fields
        $field_all = array_merge_recursive($html->find('input'), $html->find('textarea'), $html->find('select'));
        $collection = collect($field_all)->pluck('name')->unique()->map(function ($name) {
            return Str::substr($name, 0, 38);
        })->toArray();
        $entities = $entity->elements->pluck('element_stages')->toArray();
        $no_longer_fields = array_diff($entities, $collection);
        $entity->elements()->where('builder_id', $entity->id)->whereIn('element_stages', $no_longer_fields)->delete();

        /*
         * add or update
        */
        //input
        if ($html->find('input'))
        $this->_element($entity, 'input', $html);
        //textarea
        if ($html->find('textarea'))
        $this->_element($entity, 'textarea', $html);
        //select
        if ($html->find('select'))
        $this->_element($entity, 'select', $html);

        $entity = $this->getEntity($id);

        foreach ($entity->elements as $element){
            $this->field_type = $element->type;
            $this->field_name = $element->element_name;
            $this->field_nullable = 1;//($element->is_required ? 0 : 1);
            $this->addField($entity->id);
        }

        $field_protected = $entity->elements->pluck('element_name')->toArray();
        $field_protected = array_merge($field_protected, ['id','locale','name','term_id']);
        $column_list = Schema::getColumnListing("term{$id}_translations");
        $no_longer = array_diff($column_list, $field_protected);

        if ($no_longer){
            foreach ($no_longer as $del){
                $this->field_name = $del;
                if (Schema::hasColumn("term{$id}_translations", $this->field_name)){
                    Schema::table("term{$id}_translations", function ($table) {
                        $table->dropColumn($this->field_name);
                    });
                }
            }
        }        
        Artisan::call('view:clear');
        return redirect()->route("{$this->getRoutePrefix()}.index")
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
    }

    private function _element($entity, $type, $html){
        foreach ($html->find($type) as $element){
            $id = Str::substr($element->name, 0, 38);
            $field = $entity->elements()->where('element_stages', $id)->first();
            $name = (isset($html->find('label[for='.$id.']')[0]->plaintext) ? $html->find('label[for='.$id.']')[0]->plaintext : 0);
            if (!$name){
                $name = $html->find('input[name^='.$id.']')[0]->parent()->parent()->parent()->first_child()->plaintext;
            }
            if ($field){
                $field->update([
                    'name' => (isset($name) ? $name : $id),
                    'element_stages' => $id,
                    'type' => ($type == 'input' ? $element->type : $type),
                    'is_required' => ($element->required ? 1 : 0),
                    'is_global' => true
                ]);
            }else{
                $entity->elements()->create([
                    'name' => (isset($name) ? $name : $id),
                    'element_stages' => $id,
                    'type' => ($type == 'input' ? $element->type : $type),
                    'is_required' => ($element->required ? 1 : 0),
                    'is_global' => true
                ]);
            }
        }
    }

    private function createTerm($id)
    {
        Schema::create("term{$id}_translations", function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale');
            $table->string('name');
            $table->integer('term_id')->unsigned();
            $table->unique(['term_id','locale']);
            $table->foreign('term_id')->references('id')->on('terms')->onDelete('cascade');
        });
    }

    private function addField($id)
    {
        Schema::table("term{$id}_translations", function ($table) use ($id) {
        if (!Schema::hasColumn("term{$id}_translations", $this->field_name)){
            switch ($this->field_type) {
                case 'text':
                    if ($this->field_nullable)
                        $table->string($this->field_name)->nullable();
                    else
                        $table->string($this->field_name);
                    break;
                case 'email':
                    if ($this->field_nullable)
                        $table->string($this->field_name)->nullable();
                    else
                        $table->string($this->field_name);
                    break;
                case 'select':
                    if ($this->field_nullable)
                        $table->string($this->field_name)->nullable();
                    else
                        $table->string($this->field_name);
                    break;
                case 'radio':
                    if ($this->field_nullable)
                        $table->string($this->field_name)->nullable();
                    else
                        $table->string($this->field_name);
                    break;
                case 'checkbox':
                    if ($this->field_nullable)
                        $table->text($this->field_name)->nullable();
                    else
                        $table->string($this->field_name);
                    break;
                case 'number':
                    if ($this->field_nullable)
                        $table->integer($this->field_name)->nullable();
                    else
                        $table->integer($this->field_name);
                    break;
                case 'textarea':
                    if ($this->field_nullable)
                        $table->text($this->field_name)->nullable();
                    else
                        $table->text($this->field_name);
                    break;
                case 'date':
                    if ($this->field_nullable)
                        $table->dateTime($this->field_name)->nullable();
                    else
                        $table->dateTime($this->field_name);
                    break;
                case 'file':
                    if ($this->field_nullable)
                        $table->string($this->field_name, 300)->nullable();
                    else
                        $table->string($this->field_name, 300);
                    break;
//                default:$table->string('type', 20);
//                    $table->string($this->field_name)->nullable()->after('id');
//                    break;
            }
        }
        });
    }

    public function destroy($ids)
    {
        $this->getModel()
            ->withoutGlobalScope('active')
            ->whereIn('id', explode(',', $ids))
            ->delete();
        foreach (explode(',', $ids) as $id){
            Element::where('builder_id', $id)->delete();
            Schema::dropIfExists("term{$id}_translations");
        }
    }
}
