<?php

namespace Modules\Page\Http\Controllers;

use Modules\Page\Entities\Page;
use Modules\Media\Entities\File;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display page for the slug.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $logo = File::findOrNew(setting('storefront_header_logo'))->path;
        $page = Page::where('slug', $slug)->firstOrFail();
        $pages = [];
        if($page->template == 'has_sidebar'){
            $pages = Page::where('template','has_sidebar')->get();
        }
        return view('public.pages.show', compact('page', 'logo','pages'));
    }

    public function subscription(Request $request){
        if($request->has('email')){
            try{
                $email = $request->email;
                $check = \DB::table('subscriptions')->where('email',$email)->first();
                if($check){
                    return trans('storefront::storefront.subscription.email_unique');
                }
                $date = \Carbon\Carbon::now();
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                  $emailErr = "Invalid email format";
                  return $emailErr;
                }
                \DB::table('subscriptions')->insert(['email'=>$email,'created_at'=> $date]);
                return trans('storefront::storefront.subscription.subcription_complete');
            }catch(\Exception $e){
                return $e->getMessage();
            }
        }else{
            return trans('storefront::storefront.subscription.email_required');
        }
    }
}
