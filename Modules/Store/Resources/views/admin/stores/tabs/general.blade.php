{{ Form::text('name', trans('store::attributes.name'), $errors, $store, ['labelCol' => 2, 'required' => true]) }}

{{ Form::text('address', trans('store::attributes.address'), $errors, $store, ['labelCol' => 2, 'required' => true]) }}
<div class="form-group ">
    <label for="district_id" class="col-md-2 control-label text-left">{{trans('store::attributes.district')}}<span class="m-l-5 text-red">*</span></label>
    <div class="col-md-10">
        <select name="district_id" id="district_id" class="form-control" tabindex="-1" aria-hidden="true">
            <option>{{trans('store::attributes.select_district')}}</option>
            @foreach($districts as $district)
                <option value="{{$district->id}}" @if($store->district_id == $district->id) selected @endif>{{$district->title}}</option>
            @endforeach
            
        </select>
    </div>
</div>
{{ Form::text('email', trans('store::attributes.email'), $errors, $store, ['labelCol' => 2, 'required' => true]) }}
<div class="row">
    <div class="col-md-6">
    {{ Form::text('phone', trans('store::attributes.phone'), $errors, $store, ['labelCol' => 4, 'required' => true]) }}
    </div>
    <div class="col-md-6">
    {{ Form::text('open', trans('store::attributes.open'), $errors, $store, ['labelCol' => 4]) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
    {{ Form::text('loc_lat', trans('store::attributes.loc_lat'), $errors, $store, ['labelCol' => 4, 'required' => true]) }}
    </div>
    <div class="col-md-6">
    {{ Form::text('loc_long', trans('store::attributes.loc_long'), $errors, $store, ['labelCol' => 4, 'required' => true]) }}
    </div>
</div>


<div class="form-group ">
    <div class="col-md-8">
        {{ Form::checkbox('is_active', trans('store::attributes.is_active'), trans('store::stores.form.enable_the_store'), $errors, $store) }}
    </div>
</div>

@push('globals')
    <script>
        FleetCart.langs['store::stores.expand_all'] = '{{trans('store::stores.expand_all')}}';
        FleetCart.langs['store::stores.collapse_all'] = '{{trans('store::stores.collapse_all')}}';
    </script>
@endpush

@push('styles')
    <link href="select2.css" rel="stylesheet">
@endpush

@push('scripts')
    <script src="select2.js"></script>
    <script>
        (function($){
            $('#district_id').select2({
                theme: "bootstrap",
                // placeholder: "Choose location...",
                // minimumInputLength: 5,
                // ajax: {
                //     url: 'http://aconcept.test/admin/groups/list',
                //     dataType: 'json',
                //     data: function (params) {
                //         return {
                //             query: params.term,
                //             limit: 1000,
                //             name: params.name
                //         };
                //     },
                //     processResults: function (result) {
                //         return {
                //             results: $.map(result.data, function (item) {
                //                 return {
                //                     text: item.name,
                //                     id: item.id
                //                 }
                //             }),
                //         };
                //     },
                //     cache: true
                // },
            });
        })(jQuery)
        
    </script>
@endpush