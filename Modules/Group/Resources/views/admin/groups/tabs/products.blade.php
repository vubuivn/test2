@component('admin::components.table')
    @slot('thead')
        @include('product::admin.products.partials.thead')
    @endslot
    <a class="btn btn-default" href="{{route('admin.products.create',['group'=>$group->id,'name'=>$group->name])}}">{{trans('group::groups.add_product_child')}}</a>
@endcomponent
@php($name = 'group_products')
@push('scripts')
    <script>

        DataTable.setRoutes('#{{ $name }} .table', {
            index: {
                name: 'admin.products.index',
                params: {ids: '{!! old_json('group_products', $group->products->pluck('id')) !!}'}
            },
            edit: 'admin.products.edit',
            destroy: 'admin.products.destroy',
        });

        new DataTable('#{{ $name }} .table', {
            pageLength: 10,
            columns: [
                {data: 'checkbox', orderable: false, searchable: false, width: '3%'},
                {data: 'sku', name: 'sku'},
                {data: 'group.name', searchable: false},
                {data: 'price', searchable: false},
                {data: 'status', name: 'is_active', searchable: false},
                {data: 'created', name: 'created_at'},
            ],
        });
    </script>
@endpush
