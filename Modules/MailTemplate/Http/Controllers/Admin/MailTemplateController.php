<?php

namespace Modules\MailTemplate\Http\Controllers\Admin;

use FleetCart\Jobs\SendEmail;
use FleetCart\Jobs\SentMailJob;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Admin\Traits\HasCrudActions;
use Modules\MailTemplate\Entities\MailTemplate;
use Modules\MailTemplate\Http\Requests\SaveMailTemplateRequest;

class MailTemplateController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = MailTemplate::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'mailtemplate::mail_templates.mail_template';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'mailtemplate::admin.mail_templates';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveMailTemplateRequest::class;
    public function test(){
        $details['email'] = 'info@mangoads.vn';
        dispatch(new SendEmail($details));
        dd('Done');
    }
}
