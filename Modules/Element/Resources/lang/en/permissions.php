<?php

return [
    'elements' => [
        'index' => 'Index Elements',
        'create' => 'Create Elements',
        'edit' => 'Edit Elements',
        'destroy' => 'Delete Elements',
    ],
];
