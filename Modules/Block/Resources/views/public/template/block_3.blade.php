<a id="bannertop" href="{{$banner_url}}" class="lazy-hidden tRes_48" data-lazy-type="bg"
    data-lazy-src="{{ $banner ? $banner->file : '' }}" style="background-image: url({{ $banner ? $banner->file : '' }});">
    <div class="divtext"><h1>{{ $title }}</h1></div>
</a>
