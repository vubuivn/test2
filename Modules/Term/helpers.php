<?php
if (! function_exists('trade_model')) {
    function trade_model($term, $push=null){
        config(['term' => $term]);
        config(['mango.translation_table' => "term{$term->id}_translations"]);
        if ($term->elements) {
            $term_element = $term->elements
                //->where('type', '<>', 'file')
                //->where('type', '<>', 'image')
                ->pluck('element_name')->toArray();
            if ($push)
                array_push($term_element, $push);

            config(['mango.translatedAttributes' => $term_element]);
        }
    }
}
