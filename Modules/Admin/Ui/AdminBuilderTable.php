<?php

namespace Modules\Admin\Ui;

use Illuminate\Contracts\Support\Responsable;

class AdminBuilderTable implements Responsable
{
    /**
     * Raw columns that will not be escaped.
     *
     * @var array
     */
    protected $rawColumns = [];

    /**
     * Raw columns that will not be escaped.
     *
     * @var array
     */
    protected $defaultRawColumns = [
        'checkbox', 'thumbnail', 'status', 'created', 'term',
    ];

    /**
     * Source of the table.
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $source;

    /**
     * Create a new table instance.
     *
     * @param \Illuminate\Database\Eloquent\Builder $source
     * @return void
     */
    public function __construct($source = null)
    {
        $this->source = $source;
    }

    /**
     * Make table response for the resource.
     *
     * @param mixed $source
     * @return \Illuminate\Http\JsonResponse
     */
    public function make()
    {
        return $this->newTable();
    }

    /**
     * Create a new datatable instance;
     *
     * @param mixed $source
     * @return \Yajra\DataTables\DataTables
     */
    public function newTable()
    {
        return datatables($this->source)
            ->addColumn('checkbox', function ($entity) {
                return view('admin::partials.table.checkbox', compact('entity'));
            })
            ->editColumn('status', function ($entity) {
                return $entity->is_active
                    ? '<span class="dot green"></span>'
                    : '<span class="dot red"></span>';
            })
            ->addColumn('term', function ($entity) {
                return '<a href="'.route('admin.terms.builder',['builder' => $entity->id]).'">'.trans('builder::builders.form-data').'</a> | <a href="'.route('admin.builders.embedded',['id' => $entity->id]).'">'.trans('builder::builders.embedded').'</a> | <a href="'.route('admin.builders.reply',['id' => $entity->id]).'">'.trans('builder::builders.reply').'</a> | <a href="'.route('admin.builders.noti',['id' => $entity->id]).'">'.trans('builder::builders.noti').'</a> | Shortcut @mgform('.$entity->id.')';
            })
            ->editColumn('created', function ($entity) {
                return view('admin::partials.table.date')->with('date', $entity->created_at);
            })
            ->rawColumns(array_merge($this->defaultRawColumns, $this->rawColumns))
            ->removeColumn('translations');
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        return $this->make()->toJson();
    }
}
