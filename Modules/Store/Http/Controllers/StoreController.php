<?php

namespace Modules\Store\Http\Controllers;

use Modules\Store\Entities\Store;
use Modules\Media\Entities\File;
use Illuminate\Routing\Controller;

class StoreController extends Controller
{
    /**
     * Display store for the slug.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $logo = File::findOrNew(setting('storefront_header_logo'))->path;
        $store = Store::where('slug', $slug)->firstOrFail();
        $stores = [];
        if($store->template == 'has_sidebar'){
            $stores = Store::where('template','has_sidebar')->get();
        }
        return view('public.stores.show', compact('store', 'logo','stores'));
    }
}
