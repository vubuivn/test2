import BaseElement from './BaseElement';

export default class extends BaseElement {
    constructor() {
        super();

        $('#type').on('change', () => {
            super.changeElementType({ values: FleetCart.data['element.values'] });
            super.addElementsErrors(FleetCart.errors['element.values']);
        });

        $('#type').trigger('change');

        window.admin.removeSubmitButtonOffsetOn('#values');
    }

    addElementRow({ valueId, value = { label: '', price: '', price_type: 'fixed' } }) {
        let template = this.getRowTemplate({ elementId: undefined, valueId, value });

        let selectValues = $('#select-values').append(template);

        super.initElementRow(template, selectValues);
    }

    addElementRowEventListener() {
        $('#add-new-row').on('click', () => {
            let valueId = $('#element-values .element-row').length;

            this.addElementRow({ valueId });
        });
    }

    getElementValuesElement() {
        return $('#element-values');
    }

    getAddNewRowButton() {
        return $('#add-new-row');
    }

    getInputFieldForErrorKey(key) {
        let keyParts = key.split('.');

        // Replace all "_" to "-".
        keyParts = keyParts.map(k => {
            return k.split('_').join('-');
        });

        return $(`#${keyParts.join('-')}`);
    }
}
