<?php

namespace FleetCart\Providers;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use FloatingPoint\Stylist\StylistServiceProvider;
use Nwidart\Modules\LaravelModulesServiceProvider;
use Jackiedo\DotenvEditor\DotenvEditorServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Modules\Builder\Entities\Builder;
use Illuminate\Support\Facades\Validator;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (Request::secure()) {
            URL::forceScheme('https');
        }
        Validator::extend('recaptcha', 'App\\Validators\\ReCaptcha@validate');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(StylistServiceProvider::class);
        $this->app->register(LaravelModulesServiceProvider::class);

        if (! config('app.installed')) {
            $this->app->register(DotenvEditorServiceProvider::class);
        }
        $this->registerBladeExtensions();
    }

    protected function registerBladeExtensions()
    {
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
            $bladeCompiler->directive('mgform', function ($arguments) {
                $builder = Builder::find($arguments);
                if (!$builder)
                    return 'Error';
                $html = $builder->embedded;
                foreach ($builder->elements as $element){
                    $html = str_replace($element->element_stages, $element->element_name, $html);
                }
                $html = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".asset('modules/common/css/formeo.min.css')."\" /><form method=\"POST\" action=".route('public.builders.embedded-form', ['builder' => $builder])." enctype=\"multipart/form-data\" onsubmit=\"return get_action()\">".csrf_field()."{$html}</form>";
                if ($builder->is_captcha){
                    $capt = "<div class='formeo-row-wrap'>
                    <div class='formeo-row'>
                    <div class='formeo-column\" style=\"width: 100%;'>
                    <div class='f-field-group'><div class='g-recaptcha' data-sitekey='{$builder->site_key}'></div><span id='captcha' style='color:red' /></div>
                    </div>
                    </div>
                    </div>";
                    $_html = new \Htmldom($html);
                    $ret = $_html->find('div[class=formeo-row-wrap]', -1);
                    $html = str_replace($ret->outertext, $capt.$ret->outertext, $html);
                    $html .= "<script src='//www.google.com/recaptcha/api.js'></script>";
                    $html .= "<script>function get_action(form){ var v = grecaptcha.getResponse(); if(v.length == 0) { document.getElementById('captcha').innerHTML=\"You can't leave Captcha Code empty\"; return false; }}</script>";
                }
                return $html;
            });
            $bladeCompiler->directive('endmgform', function () {
                return '<?php endif; ?>';
            });

        });
    }
}
