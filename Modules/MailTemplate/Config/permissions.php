<?php

return [
    'admin.mail_templates' => [
        'index' => 'mailtemplate::permissions.index',
        'create' => 'mailtemplate::permissions.create',
        'edit' => 'mailtemplate::permissions.edit',
        'destroy' => 'mailtemplate::permissions.destroy',
    ],

// append

];
