<section id="bannertop" class="lazy-hidden tRes_48 subbanner cart" data-lazy-type="bg"
    data-lazy-src="/assets/images/cart/bg.jpg"
    style="background-image: url(https://aconcept-vn.com/assets/images/background.png);">
	<div class="divtext">
        <h1 style="text-transform: uppercase;">{{ $page_title }}</h1>
    </div>
</section>