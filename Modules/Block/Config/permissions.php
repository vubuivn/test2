<?php

return [
    'admin.blocks' => [
        'index' => 'block::permissions.index',
        'create' => 'block::permissions.create',
        'edit' => 'block::permissions.edit',
        'destroy' => 'block::permissions.destroy',
    ],

// append

];
