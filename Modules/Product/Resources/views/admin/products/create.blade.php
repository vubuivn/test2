@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('product::products.product')]))

    <li><a href="{{ route('admin.products.index') }}">{{ trans('product::products.products') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('product::products.product')]) }}</li>
@endcomponent

@section('content')
@if(isset($_REQUEST['group']))<a href="@if(isset($_REQUEST['group'])){{route('admin.groups.edit',$_REQUEST['group'])}}@endif" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> {{trans('product::products.back')}}</a>@endif
<div style="margin-bottom: 20px"></div>
    <form method="POST" action="{{ route('admin.products.store') }}" class="form-horizontal" id="product-create-form" novalidate>
        {{ csrf_field() }}

        {!! $tabs->render(compact('product')) !!}
    </form>
@endsection

@include('product::admin.products.partials.shortcuts')
