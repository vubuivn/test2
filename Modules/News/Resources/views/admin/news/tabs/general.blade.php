{{ Form::text('name', trans('news::attributes.name'), $errors, $news, ['labelCol' => 2, 'required' => true]) }}
{{ Form::textarea('short_description', trans('news::attributes.short_description'), $errors, $news, ['labelCol' => 2, 'required' => true]) }}
<div class="form-group">
<div class="col-md-2 control-label text-left"></div>
<div clas="col-md-10"><a class="btn btn-default image_picker">Image</a></div>
</div>
{{ Form::wysiwyg('description', trans('news::attributes.description'), $errors, $news, ['labelCol' => 2, 'required' => true]) }}

@include('media::admin.image_picker.single', [
    'title' => trans('product::products.form.base_image'),
    'inputName' => 'files[base_image]',
    'file' => $news->baseImage,
])
<div class="row">
    <div class="col-md-8">
        {{ Form::select('category_news_id', trans('news::attributes.categories'), $errors, $categories, $news, ['class' => 'prevent-creation']) }}
        {{ Form::checkbox('is_active', trans('news::attributes.is_active'), trans('news::news.form.enable_the_news'), $errors, $news) }}
    </div>
</div>
<input type="hidden" id="block-number" value="{{ isset($news->blocks) ? count($news->blocks) : 0 }}">
<div class="form-group ">
    <div class="col-md-12">
        <button class="btn btn-default btn-sm pull-right" id="expandAllFormats"></button>
    </div>
</div>
<div class="content-accordion panel-group options-group-wrapper" id="page-block" role="tablist"
     aria-multiselectable="true">
    @if($news->blocks)
        @foreach ($news->blocks as $key => $item)
            <form></form>
            @php($block = \Modules\Block\Entities\Block::where('id',$item->block_id)->withoutGlobalScope('active')->first())
            @php($data = null)
            @if($item->content != null)
                @php($data = json_decode($item->content))
            @endif
            <form name="block-form" id="block-form-{{$key}}">
                <input type="hidden" name="blocks[{{$key}}][main_block_id]" value="{{$item->id}}">
                <input type="hidden" name="blocks[{{$key}}][block_id]" value="{{$block->id}}">
                <div class="panel panel-default" style="margin-bottom: 15px">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="collapsed" data-parent="#block-{{$key}}-edit"
                               href="#custom-collapse-{{$key}}-edit">
                                <span class="drag-icon pull-left">
                                    <i class="fa"></i>
                                    <i class="fa"></i>
                                </span>

                                <span id="block-name" class="pull-left">
                                    {{$item->title}}
                                </span>
                            </a>
                        </h4>
                    </div>
                    <div id="custom-collapse-{{$key}}-edit" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="new-option clearfix">
                                <div class="col-lg-6 col-md-12 p-l-0">
                                    <div class="form-group">
                                        <label for="block-{{$key}}-edit-name">
                                            {{ trans('block::blocks.form.name') }}
                                        </label>
                                        <input type="text" name="blocks[{{$key}}][main_block_title]"
                                               class="form-control block-name-field"
                                               id="block-{{$key}}-edit-name"
                                               value="{{$item->title}}">
                                    </div>
                                </div>

                                <button type="button" class="btn btn-default delete-block pull-right"
                                        data-toggle="tooltip" title=""
                                        data-original-title="{{trans('block::blocks.form.delete_block')}}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>

                            <div class="clearfix"></div>

                            <div class="option-values clearfix" id="block-{{$key}}-edit-values">
                                @foreach ($block->values as $value)
                                    {{block_html($value, $key, $data)}}
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @endforeach
    @endif
</div>
@push('scripts')
    <script src="{{ asset('modules/news/admin/js/news.js') }}"></script>
    <script>
        $('.content-accordion').find('.banner-body:eq(0)').find('.delete-image').remove();
        

        // (function($){

            // $('.image_picker').on('click', function (e) {
            //     e.preventDefault();
            //     console.log('a');
            //     let picker = new MediaPicker({type: 'image'});

            //     picker.on('select', (file) => {
            //         $(this).find('i').remove();
            //         // $(this).find('img').attr('src', file.path).removeClass('hide');
            //         // $(this).find('.banner-file-id').val(file.id);
            //         tinymce.get("description").execCommand('mceInsertContent', false, file.path);
            //     });
            // });

        // })(jQuery)
    </script>
@endpush
@push('globals')
    <script>
        FleetCart.langs['page::pages.expand_all'] = '{{trans('page::pages.expand_all')}}';
        FleetCart.langs['page::pages.collapse_all'] = '{{trans('page::pages.collapse_all')}}';
    </script>
@endpush
