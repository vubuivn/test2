@if (session()->has('success') || session()->has('error'))
<div class="modal fade notification" id="notificationPopup" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<a href="#" class="close" data-dismiss="modal"><i class="icon icon-close"></i></a>
			<div class="modal-body" id='modal-body'>

				@if (session()->has('success'))
				<div class="alert alert-success fade in">
					{{ session('success') }}
				</div>
				@endif
				@if (session()->has('error'))
				<div class="alert alert-danger fade in">
					{{ session('error') }}
				</div>
				@endif

			</div>
		</div>
	</div>
</div>
@endif

<div class="modal fade notification" id="notificationPopupAjax" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<a href="#" class="close" data-dismiss="modal"><i class="icon icon-close"></i></a>
			<div class="modal-body" id='modal-body'>
				<div class="alert alert-success fade in">
				</div>
			</div>
		</div>
	</div>
</div>


@push('scripts')
<script type="text/javascript">
	(function($){
    	$(document).ready(function(){
    		if($('#notificationPopup').length>0){
	  			$('#notificationPopup').modal('show');
    		}
    	});
	})(jQuery);
</script>

      
@endpush

