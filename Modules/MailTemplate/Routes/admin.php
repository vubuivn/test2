<?php

Route::get('mail-templates/test', [
    'as' => 'admin.mail_templates.test',
    'uses' => 'MailTemplateController@test',
]);
Route::get('mail-templates', [
    'as' => 'admin.mail_templates.index',
    'uses' => 'MailTemplateController@index',
    'middleware' => 'can:admin.mail_templates.index',
]);

Route::get('mail-templates/create', [
    'as' => 'admin.mail_templates.create',
    'uses' => 'MailTemplateController@create',
    'middleware' => 'can:admin.mail_templates.create',
]);

Route::post('mail-templates', [
    'as' => 'admin.mail_templates.store',
    'uses' => 'MailTemplateController@store',
    'middleware' => 'can:admin.mail_templates.create',
]);

Route::get('mail-templates/{id}/edit', [
    'as' => 'admin.mail_templates.edit',
    'uses' => 'MailTemplateController@edit',
    'middleware' => 'can:admin.mail_templates.edit',
]);

Route::put('mail-templates/{id}', [
    'as' => 'admin.mail_templates.update',
    'uses' => 'MailTemplateController@update',
    'middleware' => 'can:admin.mail_templates.edit',
]);

Route::delete('mail-templates/{ids?}', [
    'as' => 'admin.mail_templates.destroy',
    'uses' => 'MailTemplateController@destroy',
    'middleware' => 'can:admin.mail_templates.destroy',
]);

// append

