<?php

return [
    'mail_template' => 'Mail Template',
    'mail_templates' => 'Mail Templates',
    'tabs' => [
        'mail_template' => [
            'basic_information' => 'Basic Information',
            'advanced_information' => 'Advanced Information',
        ],
        'general' => 'General',
    ],
];
