<?php

/** @var Illuminate\Routing\Router $router */
Route::get('api-builders', [
    'as' => 'admin.builders.api-index',
    'uses' => 'BuilderController@api_index',
]);
Route::get('builders', [
    'as' => 'admin.builders.index',
    'uses' => 'BuilderController@index',
    'middleware' => 'can:admin.builders.index',
]);

Route::get('builders/create', [
    'as' => 'admin.builders.create',
    'uses' => 'BuilderController@create',
    'middleware' => 'can:admin.builders.create',
]);

Route::post('builders', [
    'as' => 'admin.builders.store',
    'uses' => 'BuilderController@store',
    'middleware' => 'can:admin.builders.create',
]);

Route::get('builders/{id}/embedded-form', [
    'as' => 'admin.builders.embedded',
    'uses' => 'BuilderController@embedded',
    'middleware' => 'can:admin.builders.edit',
]);

Route::get('builders/{id}/reply', [
    'as' => 'admin.builders.reply',
    'uses' => 'BuilderController@reply',
    'middleware' => 'can:admin.builders.edit',
]);

Route::get('builders/{id}/noti', [
    'as' => 'admin.builders.noti',
    'uses' => 'BuilderController@noti',
    'middleware' => 'can:admin.builders.edit',
]);

Route::post('builders/{id}/reply', [
    'as' => 'admin.builders.reply.store',
    'uses' => 'BuilderController@reply_store',
    'middleware' => 'can:admin.builders.edit',
]);

Route::post('builders/{id}/noti', [
    'as' => 'admin.builders.noti.store',
    'uses' => 'BuilderController@noti_store',
    'middleware' => 'can:admin.builders.edit',
]);

Route::get('builders/{id}/edit', [
    'as' => 'admin.builders.edit',
    'uses' => 'BuilderController@edit',
    'middleware' => 'can:admin.builders.edit',
]);

Route::put('builders/{id}/edit', [
    'as' => 'admin.builders.update',
    'uses' => 'BuilderController@update',
    'middleware' => 'can:admin.builders.edit',
]);

Route::delete('builders/{ids?}', [
    'as' => 'admin.builders.destroy',
    'uses' => 'BuilderController@destroy',
    'middleware' => 'can:admin.builders.destroy',
]);
