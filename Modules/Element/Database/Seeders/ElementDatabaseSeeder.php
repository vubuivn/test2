<?php

namespace Modules\Element\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Element\Entities\Element;
use Modules\Element\Entities\ElementValue;

class ElementDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Element::class, 10)
            ->create()
            ->each(function ($element) {
                $times = $element->type === 'dropdown' ? 5 : 1;

                factory(ElementValue::class, $times)->create(['element_id' => $element->id]);
            });
    }
}
