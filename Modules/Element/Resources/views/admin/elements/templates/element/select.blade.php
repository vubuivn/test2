<script type="text/html" id="element-select-template">
    <div class="element-select <% if (elementId === undefined) { %> m-b-15 <% } %>">
        <div class="table-responsive">
            <table class="elements table table-bordered table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>{{ trans('element::attributes.label') }}</th>
                        <th>{{ trans('element::attributes.price') }}</th>
                        <th>{{ trans('element::attributes.price_type') }}</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody
                    <% if (elementId === undefined) { %>
                        id="select-values"
                    <% } else { %>
                        id="element-<%- elementId %>-select-values"
                    <% } %>
                >
                    {{--  Custom element dropdown rows will be added here dynamically using JS  --}}
                </tbody>
            </table>
        </div>

        <button
            type="button"
            class="btn btn-default"
            <% if (elementId === undefined) { %>
                id="add-new-row"
            <% } else { %>
                id="element-<%- elementId %>-add-new-row"
            <% } %>
        >
            {{ trans('element::elements.form.add_new_row') }}
        </button>
    </div>
</script>
