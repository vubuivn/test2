<?php

namespace Modules\Element\Admin;

use Modules\Admin\Ui\AdminTable;

class ElementTable extends AdminTable
{
    /**
     * Make table response for the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function make()
    {
        return $this->newTable()
            ->editColumn('type', function ($element) {
                return trans("element::elements.form.element_types.{$element->type}");
            })
            ->removeColumn('values');
    }
}
