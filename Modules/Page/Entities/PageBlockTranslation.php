<?php

namespace Modules\Page\Entities;

use Modules\Support\Eloquent\TranslationModel;

class PageBlockTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content'
    ];
}
