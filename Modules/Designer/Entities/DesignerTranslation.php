<?php

namespace Modules\Designer\Entities;

use Modules\Support\Eloquent\TranslationModel;

class DesignerTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','body','short_description'];
}
