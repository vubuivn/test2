<?php

return [
    'element' => 'Element',
    'elements' => 'Elements',
    'select_global_element' => 'Select Global Element',
    'table' => [
        'name' => 'Name',
        'type' => 'Type',
    ],
    'tabs' => [
        'group' => [
            'element_information' => 'Element Information',
        ],
        'general' => 'General',
        'values' => 'Values',
        'builder' => [
            'elements' => 'Elements',
        ],
    ],
    'form' => [
        'this_element_is_required' => 'This element is required',
        'add_new_element' => 'Add New Element',
        'add_global_element' => 'Add Global Element',
        'new_element' => 'New Element',
        'element_types' => [
            'please_select' => 'Please Select',
            'dropdown' => 'Dropdown',
            'checkbox' => 'Checkbox',
            'radio' => 'Radio Button',
            'multiple_select' => 'Multiple Select',
            'text' => 'Text Box',
        ],
        'delete_element' => 'Delete Element',
        'price' => 'Price',
        'price_types' => [
            'fixed' => 'Fixed',
            'percent' => 'Percent',
        ],
        'add_new_row' => 'Add New Row',
        'delete_row' => 'Delete Row',
    ],
];
