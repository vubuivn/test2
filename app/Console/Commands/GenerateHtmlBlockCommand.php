<?php

namespace FleetCart\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Modules\Block\Entities\Block;
use Symfony\Component\Console\Input\InputArgument;
use FleetCart\Scaffold\Module\Generators\EntityGenerator;

class GenerateHtmlBlockCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'block:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate html block template';

    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle ()
    {
        $id = $this->argument('id');
        $this->info($this->argument('id'));
        $block = Block::withoutGlobalScope('active')->where('id', $id)->first();
        $html  = html_entity_decode($block->html);
        File::makeDirectory(base_path('Modules/Block/Resources/views/public'),0777, true, true);
        File::makeDirectory(base_path('Modules/Block/Resources/views/public/template'),0777, true, true);
        $file  = base_path('Modules/Block/Resources/views/public/template/block_' . $id . '.blade.php');
        File::put(
            $file,
            $html
        );
    }

    protected function getArguments ()
    {
        return [
            [
                'id',
                InputArgument::REQUIRED,
                'id of block.'
            ],
        ];
    }

}
