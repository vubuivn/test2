<?php

namespace Modules\Category\Entities;

use Modules\Media\Eloquent\HasMedia;
use TypiCMS\NestableTrait;
use Modules\Support\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Modules\Support\Eloquent\Sluggable;
use Modules\Support\Eloquent\Translatable;
use Modules\Media\Entities\File;
use Modules\Attribute\Entities\Attribute;
use Modules\Group\Entities\Group;

class Category extends Model
{
    use Translatable, Sluggable, NestableTrait, HasMedia;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'slug',
        'position',
        'is_searchable',
        'is_active',
        'featured_groups',
        'pin_groups'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_searchable' => 'boolean',
        'is_active'     => 'boolean',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'name',
        'description',
        'meta_title'
    ];

    /**
     * The attribute that will be slugged.
     *
     * @var string
     */
    protected $slugAttribute = 'name';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addActiveGlobalScope();

        static::saved(
            function ($category) {
                $category->saveRelations(request()->all());
            }
        );
    }

    /**
     * Returns the public url for the category.
     *
     * @return string
     */

    public function url()
    {
        return route(
            'products.category',
            ['slug1' => $this->slug]
        );
    }

    public static function tree()
    {
        return Cache::tags(['categories'])
                    ->rememberForever(
                        'categories.tree:' . locale(),
                        function () {
                            return static::orderByRaw('-position DESC')
                                         ->get()
                                         ->nest();
                        }
                    );
    }

    public static function treeList()
    {
        return Cache::tags(['categories'])
                    ->rememberForever(
                        'categories.tree_list:' . locale(),
                        function () {
                            return static::orderByRaw('-position DESC')
                                         ->get()
                                         ->nest()
                                         ->setIndent('¦–– ')
                                         ->listsFlattened('name');
                        }
                    );
    }

    /**
     * Get searchable categoires.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function searchable()
    {
        return Cache::tags(['categories'])
                    ->rememberForever(
                        'categories.searchable:' . locale(),
                        function () {
                            return static::where(
                                'is_searchable',
                                true
                            )
                                         ->get();
                        }
                    );
    }

    public function categoryTranslations()
    {
        return $this->hasMany(CategoryTranslation::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_categories');
    }

    public function scopeWithCoverImage($query)
    {
        $query->with(
            [
                'files' => function ($q) {
                    $q->wherePivot(
                        'zone',
                        'cover_image'
                    );
                }
            ]
        );
    }
    public function scopeWithFeatureImage($query)
    {
        $query->with(
            [
                'files' => function ($q) {
                    $q->wherePivot(
                        'zone',
                        'feature_image'
                    );
                }
            ]
        );
    }
    /**
     * Get the product's base image.
     *
     * @return \Modules\Media\Entities\File
     */
    public function getCoverImageAttribute()
    {
        return $this->files->where('pivot.zone', 'cover_image')->first() ?: new File;
    }
    /**
     * Get the product's base image.
     *
     * @return \Modules\Media\Entities\File
     */
    public function getFeatureImageAttribute()
    {
        return $this->files->where('pivot.zone', 'feature_image')->first() ?: new File;
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'attribute_categories');
    }

    /**
     * Save associated relations.
     *
     * @param array $attributes
     *
     * @return void
     */
    public function saveRelations ($attributes = [])
    {
        $groups = array_get($attributes, 'featured_groups', []);
        $syncData = [];
        foreach ($groups as $key => $id) {
            $syncData[$id] = ['featured' => FEATURED];
        }

        $this->groups()
            ->newPivotStatement()
            ->where('category_id', '=', $this->id)
            ->update(array('featured' => 0))
            ;
        $this->groups()->syncWithoutDetaching($syncData);
    }
}
