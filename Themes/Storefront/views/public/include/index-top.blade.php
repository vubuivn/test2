<!DOCTYPE html>
<html lang="{{ locale() }} xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>
        @hasSection('title')
            @yield('title') - {{ setting('store_name') }}
        @else
            {{ setting('store_name') }}
        @endif
    </title>
    <link rel="shortcut icon" href="{{ $favicon }}" type="image/x-icon">
    <!-- <link rel="shortcut icon" href="/assets/images/favicon.svg" type="image/x-icon"> -->

    <link rel='stylesheet' href="{{ url('/assets/css/main.css') }}" type='text/css' media='all' />
    <link rel='stylesheet'  href="{{ url('assets/css/cart.css') }}" type='text/css' media='all' />

    <script type="text/javascript" src="{{ url('/assets/js/jquery.js') }}"></script>
    <script>
        window.SITE_URL = '{{ url('/') }}';
    </script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @stack('meta')
    @stack('styles')

    <style>
    .filter-option{overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 1;
        line-height: 28px;
        height: 28px;
    }
    .icon-heart-add .path1:before {
      content: "\e958";
      color: rgb(157, 14, 1);
    }
    .icon-heart-add .path2:before {
      content: "\e959";
      margin-left: -1em;
      color: rgb(255, 255, 255);
    }
    .icon-heart-add .path3:before {
      content: "\e95a";
      margin-left: -1em;
      color: rgb(255, 255, 255);
    }
    .wrap-list-career .col-4{padding-left:0;}
    .btn.btn-custom{background-color: #fff;color: #222!important;border: .0625rem solid #fff;color: #222;text-transform: uppercase;}
    .btn.btn-custom:hover{background-color: transparent;color: #fff!important;;}
    #bannertop:before{background: none}
    #loading_gif{position: fixed;bottom: 0; width: 70px;left: 50%; transform: translateX(-50%);height: 50%;text-align: center;display: none;z-index: 99999}
    .mini-cart-wrapper{cursor: pointer;}
    .detailbanner .info .variation .item .key i{margin-left:5px;}
    .modal#notificationPopup{top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.modal#notificationPopup .modal-dialog{width:300px}.modal#notificationPopup .alert{margin-bottom:0}.modal#quick-view-modal{text-align:center}.modal#quick-view-modal .modal-content{padding:80px 64px}.modal#quick-view-modal .modal-content h3.product-title{font-size:24px}.modal#notificationPopupAjax .modal-dialog{width:300px}.modal#notificationPopupAjax .modal-dialog .alert{padding:30px}
       ul.menu-top-header>li .wrapul li a,ul.menu-top-header>li>ul li a{padding:0}.error-message{color:red}form .input,form.form-checkout select{padding-left:0;padding-right:0}ul{padding-left:25px}.has_sidebar .container{max-width:940px}.search_group{position:relative}.search_form{position:absolute;top:100%;right:0;background:#fff;padding:10px;border:1px solid #ddd;min-width:220px}.search_form input{width:calc(100% - 30px);height:25px;line-height:25px;float:left;padding:0 5px}.search_form button{width:25px;height:25px;padding:0;float:right;line-height:25px;border:0}.search_form{display:none}.search_form.show{display:block}@media screen and (max-width:1024px){#header .menu-btn{display:block}}
    ul.list-career-info li{padding: 0;}
    ul.list-career-info li a{width:100%;}
    .wrap-list-career .col-4{float:left;}
    ul.list-career-info{padding-left:0;}
    .btn.out_of_stock_button,.btn.out_of_stock_button:hover{margin: 20px 0 20px 5px;background: #9D0E01;border:1px solid #9D0E01;color:#fff;}
    .detailbanner .price-info{border-top:0;}
    .info .price-info{float:left;margin-right:10px;}
    .line-border{border-top:1px solid #979797;}
    .detailbanner .price-cost{padding: 25px 10px;}
    a.out_of_stock{pointer-events: none;cursor: default;text-decoration: none;color: black;}
    </style>
    <link rel="stylesheet" type="text/css" href="/assets/js/collagePlus/transitions.css" media="all" />
    <script src="/assets/js/collagePlus/jquery.collagePlus.js"></script>
    <script src="/assets/js/collagePlus/jquery.removeWhitespace.js"></script>
    <script src="/assets/js/collagePlus/jquery.collageCaption.js"></script>
    <script type="text/javascript">
        !function(a){a(document).ready(function(){function e(){a(".Collage").removeWhitespace().collagePlus({fadeSpeed:2e3,targetHeight:750,effect:"effect-2",direction:"vertical",allowPartialLastRow:!0})}var o=null;a(window).bind("resize",function(){a(".Collage .Image_Wrapper").css("opacity",0),o&&clearTimeout(o),o=setTimeout(e,200)}),a(window).bind("load",function(){e(),a(".Collage").collageCaption()})})}(jQuery);
    </script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '686060221875796');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=686060221875796&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125262116-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125262116-1');
</script>  
</head>

<body>
    @include('public.partials.notification_popup')
    <div id="wrapper">
        <span class="menu-btn overlay"> </span>
        <header id="header" class="fixe" role="banner">
            <div class="container">
                <a href="{{URL::to('/')}}" id="logo">
                    <img class="white" src="{{ $headerLogo }}" alt="">
                    <img class="dark" src="/assets/images/aconcept-dark.svg" alt="">
                </a>
                
                <div class="wrap-menu-header ">
                    <!--Detect only show PC-->
                    
                    @if ($primaryMenu->menus()->isNotEmpty())
                    <ul class="menu-top-header">
                        @include('public.include.mainmenu')
                    </ul>
                    @endif 
                    
                </div>
                <div class="group-header">
                    <div class="item search_group">
                        <a href="#" class="search_button"><i class="icon-search"></i></a>
                        <form class="search_form" method="GET" action="{{route('products.search.show')}}">
                            <input type="text" name="name" value="" placeholder="{{trans('storefront::storefront.form.search')}}">
                            <button type="submit" class="btn btn-default"><i class="icon-search"></i></button>
                        </form>
                    </div>
                    <div class="item">
                        <a href="{{route('login')}}"><i class="icon-user-3"></i></a>
                    </div>


                    <div class="item dropdown">
                        <a href="{{route('account.wishlist.index')}}">
                            <i class="icon-heart"></i>
                            <span id="clicksCount">{{$totalwishlist}}</span>
                        </a>
                    </div>
                    <div class="item dropdown mini-cart-wrapper subright">
                        <a href="#">
                            <i class="icon-shop"></i>
                            <span class="qty cart-count">{{ $cart->quantity() }}</span>
                        </a>
                        
                        @include ('public.include.minicart')
                    </div>

                    @if (count(supported_locales()) > 1)
                    <div class="item ilang">
                        <div class="dropdown subright language">
                            <div class="title"> {{ strtoupper(locale()) }} </div>
                            <div class="content">
                                <div class="inner">
                                    <ul class="menu">
                                    @foreach (supported_locales() as $locale => $language)
                                        <li class="lang-{{ $locale }}">
                                            <a href="{{ localized_url($locale) }}" hreflang="{{ $locale }}" title="{{ $language['name'] }}">
                                                <span>{{ $language['name'] }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <span class="menu-btn x"><span></span></span>
            </div>
        </header>

        <!-- End Mainmenu -->

        <div class="wrap-menu-mb" data-style="1">
            <div class="wrapul main">
                <div class="menu-head">
                    <h3>Main menu</h3>
                    <span class="menu-btn x"><span></span></span>
                </div>
                <div class="inner">
                    <ul class="menu">
                        @include('public.include.mainmenu')
                    </ul>

                    <div class="language">
                        <ul class="row">
                        @foreach (supported_locales() as $locale => $language)
                            <li class="lang-{{ $locale }} col-sm-6 col-xs-6 {{ locale() === $locale ? 'active' : '' }}">
                                <a href="{{ localized_url($locale) }}" hreflang="{{ $locale }}" title="{{ $language['name'] }}">
                                    <img src="https://aconcept-vn.com/assets/images/background.png" alt="" />
                                    <span>{{ $language['name'] }}</span>
                                </a>
                            </li>
                        @endforeach
                        </ul>
                    </div>

                </div>
            </div>

        </div>

