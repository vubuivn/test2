<?php

namespace Themes\Storefront\Http\ViewComposers;

use Illuminate\Http\Request;
use Modules\Category\Entities\Category;
// use Modules\Attribute\Entities\Attribute;
use Illuminate\Support\Facades\Cache;
use DB;

class ProductsComposer
{
    protected $slug = 'sofa';
    protected $catId = null;
    protected $request = [];

    const MA_GOC = 3;

    public function compose ($view)
    {
        if (isset($view->getData()['slug'])) {
            $this->slug = $view->getData()['slug'];
        }
        $pinGroups = [];
        if(isset($view->getData()['category'])){
            $category = $view->getData()['category'];
            $pinGroups = explode(',',$category->pin_groups);
        }
        
        $this->request = $this->reformatRequest(Request::capture()->all());
        
        if ($this->request['ajax']) {
            $page = (int) $this->request['page'];

            $category = DB::table('categories')->where('slug', $this->slug)->first();
   
                $this->catId = $category->id;
            

            if($category->slug == 'sale' || \Route::current()->getName() == 'products.categorysale'){
                return $view->with([
                        'groupList' => $this->getSaleGroups($page * 24,$category->slug)
                    ]
                );
            }
            return $view->with([
                    'groupList' => $this->getGroupList($page * 6,$pinGroups)
                ]
            );
        }
        
        $categories = $this->generateCategoryListFromSlug($this->slug);
        
        // $flattenCategories = Category::treeList();
        // $categorySlugNames = $this->generateCategorySlugNames($flattenCategories);
        // $currentSlug = $this->slug;
        // if(Cache::tags(['category_'.$category->slug])->has('category_attributes_'.$category->slug)){
        //     $attributes = Cache::tags(['category_'.$category->slug])->get('category_attributes_'.$category->slug);
        // }else{
        //     $attributes = Cache::tags(['category_'.$category->slug])
        //       ->rememberForever('category_attributes_'.$category->slug,function () use($category){
            
        //           return $this->getAttributesByCategorySlug($category->slug);
        //     });
        // }
        $attributes = $this->getAttributesByCategorySlug($this->slug);

        //cache featuregroups
        $slugcat = $this->slug;
        if(Cache::tags(['groups'])->has('group_feature_by_cat'.$slugcat)){
            $features = Cache::tags(['groups'])->get('group_feature_by_cat'.$slugcat);
        }else{
            $features = Cache::tags(['groups'])
              ->rememberForever('group_feature_by_cat'.$slugcat,function (){
                  return $this->getFeatureGroups();
            });
        }
        // $features = $this->getFeatureGroups();
        // dd(explode(',',$category->pin_groups));
        $view->with([
                'featureGroups'   => $features,
                'groupList' => (($category->slug == 'sale') || \Route::current()->getName() == 'products.categorysale') ? $this->getSaleGroups(0,$category->slug) : $this->getGroupList(0,$pinGroups),
                'categories' => $categories,
                'attributes' => $attributes,
                'slug' => $this->slug,
                'request' => $this->request,
                'category' => $this->getCategoryBySlug($this->slug)
            ]
        );
    }

    private function getCategoryBySlug($slug)
    {
        return Category::where('slug', $slug)->first();
    }

    // SALE PRPODUCT
    private function getSaleGroups($offset=0,$slug){
        return $this->buildSearchQuery($slug)->limit(24)->offset($offset)->get();
        // return buildFromToPriceQueryBuilderSale()->limit(24)->offset($offset)->get();
        
    }


    private function generateCategoryListFromSlug($slug)
    {
        $categories = [];
        // if(Cache::tags(['categories'])->has('category_'.$slug)){
        //     $categories = Cache::tags(['categories'])->get('category_'.$slug);
        // }else{
            // $categories = Cache::tags(['categories'])
            //   ->rememberForever('category_'.$slug,function () use($slug,$categories){
                    $currentCategory = Category::where('slug', $slug)->first();
                    $this->getParentCategoryListFromCategory($categories, $currentCategory);
                    $this->getChildrenCategoryFromCurrentCategory($categories, $currentCategory);
        //           return $categories;
        //     });
        // }

        
        // $this->getParentCategoryListFromCategory($categories, $currentCategory);
        // $this->getChildrenCategoryFromCurrentCategory($categories, $currentCategory);

        return $categories;
    }

    private function getParentCategoryListFromCategory(&$categories, $category)
    {
        $selected = $category->slug;
        $parentCategory = null;
        $parentCategoryId = $category->parent_id;

        $name = trans('group::group-categories.category');
        if ($parentCategoryId) {
            $parentCategory = Category::find($parentCategoryId);
            $name = $parentCategory->name;
        }
        $items = Category::where('parent_id', $parentCategoryId)->orderBy('position', 'ASC')->get();
        
        $returnData = ['name' => $name, 'selected' => $selected, 'values' => []];
        foreach ($items AS $item) {
            $returnData['values'][] = [
                'text' => $item->name,
                'value' => $item->slug
            ];
        }
        array_unshift($categories, $returnData);
        if ($parentCategoryId) {
            $this->getParentCategoryListFromCategory($categories, $parentCategory);
        }

        return $categories;
    }

    private function getChildrenCategoryFromCurrentCategory(&$categories, $category)
    {
        $result = Category::where('parent_id', $category->id)->get();
        if ($result->isEmpty()) {
            return null;
        }

        $returnData = [
            'name' => $category->name,
            'selected' => $this->slug,
            'values' => [['text' => '', 'value' => '']]
        ];
        foreach ($result AS $item) {
            $returnData['values'][] = [
                'value' => $item->slug,
                'text' => $item->name
            ];
        }

        $categories[] = $returnData;

        return $returnData;
    }

    private function getAttributesByCategorySlug($slug)
    {
        if (empty($slug)) {
            return [];
        }

        $category = Category::without('translations')->where('slug', $slug)->first();
        $this->catId = $category->id;

        $attributes = [];
        foreach ($category->attributes AS $attribute) {
            // special case for Ma Goc attribute, then no group
            if ((int) $attribute->id === self::MA_GOC) {
                $attributes[$attribute->id] = $this->getAttributeValuesByAttribute($attribute);
            } else {
                $attributes[$attribute->id] = $this->getAttributeGroupsByAttribute($attribute);
            }
        }

        return $attributes;
    }

    private function getAttributeGroupsByAttribute($attribute)
    {
        $locale = locale();
        $query = "SELECT avt.group FROM attribute_values AS av
                    INNER JOIN attribute_value_translations AS avt
                        ON avt.attribute_value_id=av.id
                    WHERE avt.locale = '{$locale}'
                        AND av.attribute_id = {$attribute->id}
                    GROUP By avt.group";

        $rows = DB::select(DB::raw($query));
        $result = [
            'name' => $attribute->name,
            'values' => []
        ];
        foreach ($rows AS $row) {
            $result['values'][] = [
                'value' => strtolower($row->group),
                'text' => $row->group
            ];
        }

        return $result;
    }

    private function getAttributeValuesByAttribute($attribute)
    {
        $locale = locale();
        $query = "SELECT av.id, avt.value FROM attribute_values AS av
                    INNER JOIN attribute_value_translations AS avt
                        ON avt.attribute_value_id=av.id
                    WHERE avt.locale = '{$locale}'
                        AND av.attribute_id = {$attribute->id}";

        $rows = DB::select(DB::raw($query));
        $result = [
            'name' => $attribute->name,
            'values' => []
        ];
        foreach ($rows AS $row) {
            $result['values'][] = [
                'value' => $row->id,
                'text' => $row->value
            ];
        }

        return $result;
    }

    // private function generateCategorySlugNames($flattenCategories)
    // {
    //     $categoryIds = array_keys($flattenCategories);
    //     $categories = DB::table('categories')->select('id', 'slug')->whereIn('id', $categoryIds)->get();
    //     $categoryIdSlugs = [];
    //     foreach ($categories AS $item) {
    //         $categoryIdSlugs[$item->id] = $item->slug;
    //     }

    //     $categorySlugNames = [];
    //     foreach ($flattenCategories AS $catId => $name) {
    //         $categorySlugNames[$categoryIdSlugs[$catId]] = $name;
    //     }

    //     return $categorySlugNames;
    // }

    private function reformatRequest($request)
    {
        return [
            'fromPrice' => array_get($request, 'fromPrice', null),
            'toPrice'   => array_get($request, 'toPrice', null),
            'sort' => array_get($request, 'sort', null),
            'attribute' => array_get($request, 'attribute', []),
            'ajax' => array_get($request, 'ajax', null),
            'page' => array_get($request, 'page', null)
        ];
    }

    private function buildSearchQuery($type='')
    {
        $request = $this->request;

        $attributes = $this->formatAttributes($request['attribute']);
        if($type != ''){
            $builder = buildFromToPriceQueryBuilderSale($attributes);
        }else{
            $builder = buildFromToPriceQueryBuilder($attributes);
        }
        
        if($type != 'sale'){
            if ($this->catId) {
                $builder->join('group_categories AS gc', 'gc.group_id', '=', 'groups.id')
                        ->where('gc.category_id', $this->catId);
            }
        }
        if($type != 'sale' && \Route::current()->getName() != 'products.categorysale'){

            switch ($request['sort']) {
                case 'priceLowToHigh':
                    $builder->orderBy('lowest_price', 'ASC');
                    break;

                case 'priceHighToLow':
                    $builder->orderBy('highest_price', 'DESC');
                    break;

                default:
                    $builder->orderBy('id', 'DESC');
            }
        }

        if ($request['fromPrice']) {
            $builder->where('lowest_price', '>=', $request['fromPrice']);
        }

        if ($request['toPrice']) {
            $builder->where('highest_price', '<=', $request['toPrice']);
        }

        return $builder;
    }

    private function formatAttributes($attributes)
    {
        $formattedAttributes = [];
        foreach ($attributes AS $attrId => $attrValues) {
            if ($attrId == self::MA_GOC) {
                $formattedAttributes[$attrId] = $attrValues;
            } else {
                $formattedAttributes[$attrId] = $this->getAttributeValuesFromAttributeGroup($attrId, $attrValues);
            }
        }

        return $formattedAttributes;
    }

    private function getAttributeValuesFromAttributeGroup($attrId, $attrGroups)
    {
        $attrGroups = array_map(function($item) {
            return "'$item'";
        }, $attrGroups);
        $attrGroupStr = implode(',', $attrGroups);
        
        $locale = locale();
        $query = "SELECT av.id FROM attribute_values AS av
                INNER JOIN attribute_value_translations AS avt
                    ON avt.attribute_value_id = av.id AND avt.locale = '{$locale}'
                WHERE av.attribute_id = $attrId
                    AND avt.group IN ($attrGroupStr)";
        
        $items = DB::select(DB::raw($query));
        
        return array_map(function($item) {
            return $item->id;
        }, $items);
    }

    private function getFeatureGroups()
    {
        $builder = buildFromToPriceQueryBuilder();

        if ($this->catId) {
            $builder->join('group_categories AS gc', 'gc.group_id', '=', 'groups.id')
                    ->where('gc.featured', 1)
                    ->where('gc.category_id', $this->catId)
                    ;
        }

        return $builder->where('is_active', 1)
                ->limit(3)->get();
    }

    private function getGroupList($offset = 0,$pinGroups = [])
    {
        if(count($pinGroups)>0){
            return $this->buildSearchQuery()->whereNotIn('id',$pinGroups)
                ->limit(6)->offset($offset)->get();
        }
        return $this->buildSearchQuery()
                ->limit(6)->offset($offset)->get();
    }

    // private function query ()
    // {
    //     $request = Request::capture();
    //     return group_query(
    //         $request->all(),
    //         $this->slug
    //     );
    // }

    // private function product ()
    // {
    //     $products = $this->query()
    //         ->paginate(
    //             request(
    //                 'perPage',
    //                 24
    //             )
    //         );
    //     return $products;
    // }

}
