@extends('public.layout')

@section('title', trans('storefront::404.not_found'))

@section('content')
<main id="main">
    <div class="container">
        <div class= "max670">
            <div class="page404-icon">
                <i class="icon-close icon-page404"></i>
            </div>
            <div class="page404-text">
                <h1>{{ trans('storefront::404.the_page_not_found') }}</h1>
                <p class="content-career">{{ trans('storefront::404.click_link_below') }}</p>
            </div>
            <div class="page404-btn">
                <a href="{{url()->to('/')}}" class=" btn btn-back"><i class="icon-arrow-14"></i>{{trans('storefront::404.back_to_shop')}} </a>
            </div>
        </div>
    </div>
</main>
@endsection

@push('styles')
<link rel='stylesheet' href="/assets/css/custom.css" type='text/css' media='all' />
@endpush
