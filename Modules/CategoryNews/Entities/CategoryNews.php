<?php

namespace Modules\CategoryNews\Entities;

use Modules\Media\Eloquent\HasMedia;
use TypiCMS\NestableTrait;
use Modules\Support\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Modules\Support\Eloquent\Sluggable;
use Modules\Support\Eloquent\Translatable;
use Modules\Media\Entities\File;
use Modules\Meta\Eloquent\HasMetaData;

class CategoryNews extends Model
{
    use Translatable,
        Sluggable,
        NestableTrait,
        HasMetaData,
        HasMedia;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'slug',
        'position',
        'is_searchable',
        'is_active'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_searchable' => 'boolean',
        'is_active'     => 'boolean',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'name',
        'description',
    ];
    /**
     * The attribute that will be slugged.
     *
     * @var string
     */
    protected $slugAttribute = 'name';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addActiveGlobalScope();
    }

    /**
     * Returns the public url for the category.
     *
     * @return string
     */
    public function url()
    {
        return route(
            'products.index',
            ['category' => $this->slug]
        );
    }

    public static function tree()
    {
        return static::orderByRaw('-position DESC')
                     ->get()
                     ->nest();
//        return Cache::tags(['category_news'])
//                    ->rememberForever(
//                        'category_news.tree:' . locale(),
//                        function () {
//
//                        }
//                    );
    }

    public static function treeList()
    {
        return static::orderByRaw('-position DESC')
                     ->get()
                     ->nest()
                     ->setIndent('¦–– ')
                     ->listsFlattened('name');
//        return Cache::tags(['category_news'])
//                    ->rememberForever(
//                        'category_news.tree_list:' . locale(),
//                        function () {
//
//                        }
//                    );
    }

    /**
     * Get searchable categoires.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function searchable()
    {
        return static::where(
            'is_searchable',
            true
        )
                     ->get();
//        return Cache::tags(['category_news'])
//                    ->rememberForever(
//                        'category_news.searchable:' . locale(),
//                        function () {
//
//                        }
//                    );
    }

    public function categoryTranslations()
    {
        return $this->hasMany(CategoryNewsTranslation::class);
    }

    public function scopeWithCoverImage($query)
    {
        $query->with(
            [
                'files' => function ($q) {
                    $q->wherePivot(
                        'zone',
                        'cover_image'
                    );
                }
            ]
        );
    }

    public function scopeWithFeatureImage($query)
    {
        $query->with(
            [
                'files' => function ($q) {
                    $q->wherePivot(
                        'zone',
                        'feature_image'
                    );
                }
            ]
        );
    }

    /**
     * Get the product's base image.
     *
     * @return \Modules\Media\Entities\File
     */
    public function getCoverImageAttribute()
    {
        return $this->files->where(
            'pivot.zone',
            'cover_image'
        )
                           ->first() ?: new File;
    }

    /**
     * Get the product's base image.
     *
     * @return \Modules\Media\Entities\File
     */
    public function getFeatureImageAttribute()
    {
        return $this->files->where(
            'pivot.zone',
            'feature_image'
        )
                           ->first() ?: new File;
    }
}
