<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailTemplateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_template_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mail_template_id');

            $table->string('locale');
            $table->string('name');
            $table->text('content');

            $table->unique(['mail_template_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_template_translations');
    }
}
