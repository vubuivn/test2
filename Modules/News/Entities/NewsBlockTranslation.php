<?php

namespace Modules\News\Entities;

use Modules\Support\Eloquent\TranslationModel;

class NewsBlockTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content'
    ];
}
